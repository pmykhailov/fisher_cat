package level_editor.model {
import model.level.ICoreLevelModel;

import model.level.settings.GameSettings;
import model.level.settings.LevelSettings;

    public class LevelEditorLevelModel implements ICoreLevelModel{
        private var _gameSettings: GameSettings;
        private var _levelSettings: LevelSettings;

        public function LevelEditorLevelModel() {
            _gameSettings = new GameSettings();
            _levelSettings = new LevelSettings();
        }


        public function get gameSettings(): GameSettings {
            return _gameSettings;
        }


        public function set gameSettings(value: GameSettings): void {
            _gameSettings = value;
        }

        public function get levelSettings():LevelSettings {
            return _levelSettings;
        }

        public function set levelSettings(value:LevelSettings):void {
            _levelSettings = value;
        }
}
}
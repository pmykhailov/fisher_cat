package level_editor.model.events {
    import flash.events.Event;

    /**
     * LevelEditorModelEvent class.
     * User: Paul Makarenko
     * Date: 05.10.2014
     */
    public class LevelEditorModelEvent extends Event {

        public static const LEVELS_DATA_UPDATED:String = "levelsDataUpdated";
        public static const CURRENT_LEVEL_SELECTED_UPDATED:String = "currentLevelSelected";

        public function LevelEditorModelEvent(type: String, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
        }


        override public function clone(): Event {
            return new LevelEditorModelEvent(type, bubbles, cancelable);
        }
    }
}
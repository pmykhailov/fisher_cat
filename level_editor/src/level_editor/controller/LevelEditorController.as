package level_editor.controller {

    import level_editor.controller.commands.LoadLevelsCommand;
    import level_editor.controller.commands.SaveLevelsCommand;
    import level_editor.model.LevelEditorLevelModel;
    import level_editor.model.LevelEditorModel;
    import level_editor.model.events.LevelEditorModelEvent;
    import level_editor.view.LevelEditorView;
    import level_editor.view.events.GridEvent;
    import level_editor.view.events.LevelEditorViewEvent;
import level_editor.view.events.LevelSettingsEvent;
import level_editor.view.events.LevelsControllEvent;
    import level_editor.view.grid.GridCell;

import model.grids.enum.GridTypeEnum;

import model.grids.items.BaseGridItemEnum;
import model.grids.items.GridItemTypeEnum;
import model.level.settings.GameSettings;

/**
     * LevelEditorController class.
     * User: Paul Makarenko
     * Date: 26.09.2014
     */
    public class LevelEditorController {

        private var _model: LevelEditorModel;
        private var _view: LevelEditorView;


        public function LevelEditorController(model: LevelEditorModel, view: LevelEditorView) {
            _model = model;
            _view = view;
            init();
        }


        private function init(): void {
            _model.addEventListener(LevelEditorModelEvent.LEVELS_DATA_UPDATED, onLevelsDataUpdatedHandler);
            _model.addEventListener(LevelEditorModelEvent.CURRENT_LEVEL_SELECTED_UPDATED, onCurrentLevelSelectedHandler);

            _view.addEventListener(LevelEditorViewEvent.SAVE_BUTTON_CLICKED, onSaveButtonClickedHandler);
            _view.addEventListener(LevelEditorViewEvent.LOAD_BUTTON_CLICKED, onLoadButtonClickedHandler);
            _view.addEventListener(LevelsControllEvent.ADD_LEVEL_BUTTON_CLICKED, onAddLevelButtonClickedHandler);
            _view.addEventListener(LevelsControllEvent.REMOVE_LEVEL_BUTTON_CLICKED, onRemoveLevelButtonClickedHandler);
            _view.addEventListener(LevelsControllEvent.MOVE_LEVEL_UP_BUTTON_CLICKED, onMoveLevelUpButtonClickedHandler);
            _view.addEventListener(LevelsControllEvent.MOVE_LEVEL_DOWN_BUTTON_CLICKED, onMoveLevelDownButtonClickedHandler);
            _view.addEventListener(LevelsControllEvent.LEVEL_SELECTED, onLevelSelectedHandler);
            _view.addEventListener(LevelSettingsEvent.APPLY_GRID_DIMENSIONS , onApplyGridDimensionsHandler);
            _view.addEventListener(LevelSettingsEvent.ITEM_PICKED, onItemPickedHandler);
            _view.addEventListener(LevelSettingsEvent.CHANGE_LEVEL_NAME, onChangeLevelNameHandler);
            _view.addEventListener(GridEvent.GRID_CELL_CLICKED, onGridCellClickedHandler);
        }

        private function onLevelsDataUpdatedHandler(event: LevelEditorModelEvent): void {
            _view.data = _model;
        }

        private function onCurrentLevelSelectedHandler(event:LevelEditorModelEvent):void {
            _view.data = _model;
        }

        private function onGridCellClickedHandler(event: GridEvent): void {
            var cell: GridCell = event.data.cell as GridCell;
            var gridType: String = event.data.grid_type as String;
            var itemType: int = cell.selected ? BaseGridItemEnum.EXISTING : BaseGridItemEnum.NOT_EXISTING;

            _model.currentLevel.gameSettings.getGridSettingsVOByType(gridType).elements[cell.row][cell.col] = itemType;
        }

        private function onAddLevelButtonClickedHandler(event:LevelsControllEvent):void {
            var levels:Array = _model.levels;

            levels.push(new LevelEditorLevelModel());
            _model.levels = levels;
        }

        private function onMoveLevelUpButtonClickedHandler(event:LevelsControllEvent):void {
            var index:int = event.data as int;
            var levels:Array = _model.levels;
            var tmp:Object;

            tmp = levels[index - 1];
            levels[index - 1] = levels[index];
            levels[index] = tmp;

            _model.levels = levels;
        }

        private function onMoveLevelDownButtonClickedHandler(event:LevelsControllEvent):void {
            var index:int = event.data as int;
            var levels:Array = _model.levels;
            var tmp:Object;

            tmp = levels[index + 1];
            levels[index + 1] = levels[index];
            levels[index] = tmp;

            _model.levels = levels;
        }

        private function onRemoveLevelButtonClickedHandler(event:LevelsControllEvent):void {
            var index:int = event.data as int;
            var levels:Array = _model.levels;

            levels.splice(index, 1);

            _model.levels = levels;
        }

        private function onLevelSelectedHandler(event:LevelsControllEvent):void {
            var index:int = int(event.data);

            _model.currentLevelNumber = index;
        }

        private function onApplyGridDimensionsHandler(event:LevelSettingsEvent):void {
            var rows:int = event.data.rows;
            var cols:int = event.data.cols;

            _model.currentLevel.gameSettings.gameFieldRows = rows;
            _model.currentLevel.gameSettings.gameFieldCols = cols;

            _view.data = _model;
        }

        private function onChangeLevelNameHandler(event:LevelSettingsEvent):void {
            var name:String = event.data.name as String;

            _model.currentLevel.levelSettings.name = name;
        }

        private function onItemPickedHandler(event:LevelSettingsEvent):void {
            var id:int = event.data.id;
            var isPicked: Boolean = event.data.is_picked;
            var gameSettings:GameSettings = _model.currentLevel.gameSettings;
            var gridItemTypes:Array = GridItemTypeEnum.USUAL_ITEMS_IDS.indexOf(id) >= 0 ? gameSettings.usualGridItemTypes : gameSettings.bonusGridItemTypes;

            if (isPicked) {
                gridItemTypes.push(id);
            } else {
                gridItemTypes.splice(gridItemTypes.indexOf(id), 1);
            }

            gridItemTypes.sort(Array.NUMERIC);
        }

        private function onLoadButtonClickedHandler(event: LevelEditorViewEvent): void {
            // TODO Can be eaten by GC. Check this somehow
            (new LoadLevelsCommand(_model, _view)).execute();
        }

        private function onSaveButtonClickedHandler(event: LevelEditorViewEvent): void {
            // TODO Can be eaten by GC. Check this somehow
            (new SaveLevelsCommand(_model, _view)).execute();
        }

    }
}
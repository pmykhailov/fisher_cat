package level_editor.controller.commands {
    import flash.net.FileReference;

import level_editor.controller.commands.base.BaseLevelEditorCommand;

import level_editor.model.LevelEditorModel;
    import level_editor.model.LevelEditorLevelModel;
    import level_editor.view.LevelEditorView;

    import com.game.model.grids.grid_items.GridModel;

    import model.level.settings.vo.GridSettingsVO;

    /**
     * Takes data from model and saves all levels data
     * to XML file
     *
     * User: Paul Makarenko
     * Date: 22.09.2014
     */
    public class SaveLevelsCommand extends BaseLevelEditorCommand{

        public function SaveLevelsCommand(model: LevelEditorModel, view: LevelEditorView) {
            super(model, view);
        }


        override public function execute(): void {
            var levelsData: XML = <levels></levels>;
            var levels: Array = _model.levels;
            var levelCount: int = levels.length;

            for (var i: int = 0; i < levelCount; i++) {
                levelsData.appendChild(_getlevelData(levels[i]));
            }

            _showSaveDialog(levelsData);
        }


        private function _showSaveDialog(data: XML): void {
            var file: FileReference = new FileReference();
            file.save(data);
        }


        private function _getlevelData(levelModel: LevelEditorLevelModel): XML {

            var levelData: XML = <level></level>;

            var levelSettings: XML = <level_settings></level_settings>
            var gameSettingsData: XML = <game_settings></game_settings>

            var dimensionsData: XML = <dimensions></dimensions>;
            var gridItemsTypesData: XML = <grid_items_types></grid_items_types>;
            var grids: Array = levelModel.gameSettings.gridSettingsVOs;
            var n: int = grids.length;

            // Add level settings data
            levelSettings.level_name = levelModel.levelSettings.name;

            // Add game settings data
            dimensionsData.@rows = levelModel.gameSettings.gameFieldRows;
            dimensionsData.@cols = levelModel.gameSettings.gameFieldCols;
            gameSettingsData.appendChild(dimensionsData);

            gridItemsTypesData.@usual = levelModel.gameSettings.usualGridItemTypes;
            gridItemsTypesData.@bonus = levelModel.gameSettings.bonusGridItemTypes;
            gameSettingsData.appendChild(gridItemsTypesData);

            for (var i: int = 0; i < n; i++) {
                var gridSettingsVO: GridSettingsVO = grids[i] as GridSettingsVO;
                var data: XML = _getGridData(gridSettingsVO);

                gameSettingsData.appendChild(data);
            }

            // Fill level data
            levelData.appendChild(levelSettings);
            levelData.appendChild(gameSettingsData);

            return levelData;
        }


        private function _getGridData(gridSettingsVO: GridSettingsVO): XML {
            var res: XML = <grid></grid>;
            var rowChild: XML;
            var rowString: String;

            res.@type = gridSettingsVO.type;

            for (var i: int = 0; i < gridSettingsVO.elements.length; i++) {
                rowString = gridSettingsVO.elements[i][0] + "";

                for (var j: int = 1; j < gridSettingsVO.elements[i].length; j++) {
                    rowString += "," + gridSettingsVO.elements[i][j];
                }
                rowChild = <row></row>;
                rowChild.@data = rowString;
                res.appendChild(rowChild);
            }

            return res;

        }

    }
}
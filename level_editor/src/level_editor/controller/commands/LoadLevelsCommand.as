package level_editor.controller.commands {
    import flash.events.Event;
    import flash.net.FileReference;

import level_editor.controller.commands.base.BaseLevelEditorCommand;

import level_editor.model.LevelEditorModel;
    import level_editor.model.LevelEditorLevelModel;
    import level_editor.view.LevelEditorView;

    import model.level.parsers.LevelsDataParser;
    import model.level.settings.vo.GridSettingsVO;

    /**
     * LoadLevelsCommand class.
     * User: Paul Makarenko
     * Date: 27.09.2014
     */
    public class LoadLevelsCommand extends BaseLevelEditorCommand{

        private var _fileReference: FileReference;

        public function LoadLevelsCommand(model: LevelEditorModel, view: LevelEditorView) {
            super(model, view)
        }


        override public function execute(): void {
            _showLoadDialog();
        }


        private function _showLoadDialog(): void {
            _fileReference = new FileReference();

            _fileReference.addEventListener(Event.SELECT, onFileSelected);

            _fileReference.browse(); //[new FileFilter("Images", "*.jpg;*.gif;*.png")]
        }


        private function parseLevels(levelsData: XML): Array {
            var dataParser: LevelsDataParser = new LevelsDataParser();
            var levelsCount: int = levelsData.level.length();
            var levels: Array = [];

            for (var i: int = 0; i < levelsCount; i++) {
                levels[i] = new LevelEditorLevelModel();
            }

            dataParser.parse(levelsData, levels);

            return levels;
        }


        private function onFileSelected(event: Event): void {
            _fileReference.addEventListener(Event.COMPLETE, onFileLoaded);
            _fileReference.load();
        }


        private function onFileLoaded(e: Event): void {
            var str: String = e.target.data;
            var levelsData: XML = new XML(str);

            // In case of loaded image we should load bytes array in loader
            //var loader:Loader = new Loader();
            //loader.loadBytes(e.target.data);
            //addChild(loader);

            _model.levels = parseLevels(levelsData);

        }

    }
}

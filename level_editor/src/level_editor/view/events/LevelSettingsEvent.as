/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 08.10.14
 * Time: 10:38
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.events {
import flash.events.Event;

public class LevelSettingsEvent extends Event {

    public static const APPLY_GRID_DIMENSIONS:String = "applyGridDimensions";
    public static const CHANGE_LEVEL_NAME:String = "changeLevelName";
    public static const ITEM_PICKED:String = "itemPicked";

    private var _data:Object;

    public function LevelSettingsEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, data:Object = null) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():Object {
        return _data;
    }

    override public function clone(): Event {
        return new LevelSettingsEvent(type, bubbles, cancelable, _data);
    }
}
}

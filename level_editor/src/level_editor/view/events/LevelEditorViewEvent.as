package level_editor.view.events {
    import flash.events.Event;

    /**
     * ViewEvent class.
     * User: Paul Makarenko
     * Date: 22.09.2014
     */
    public class LevelEditorViewEvent extends Event {

        public static const SAVE_BUTTON_CLICKED:String = "saveButtonClicked";
        public static const LOAD_BUTTON_CLICKED:String = "loadButtonClicked";

        public function LevelEditorViewEvent(type: String, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
        }


        override public function clone(): Event {
            return new LevelEditorViewEvent(type, bubbles, cancelable);
        }
    }
}
package level_editor.view.statics {

    /**
     * LevelEditorDimensions class.
     * User: Paul Makarenko
     * Date: 20.09.2014
     */
    public class LevelEditorDimensions {
        public static const WIDTH:int = 800;
        public static const HEIGHT:int = 600;
    }
}

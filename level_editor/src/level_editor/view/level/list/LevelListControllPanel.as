/**
 * Created by MP on 12.09.2014.
 */
package level_editor.view.level.list {
import model.level.ICoreLevelModel;

import level_editor.view.level.*;
    import fl.controls.List;
    import fl.data.DataProvider;
    import fl.events.ListEvent;

    import flash.display.SimpleButton;

    import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;

    import level_editor.model.LevelEditorModel;

    import level_editor.view.events.LevelEditorViewEvent;
    import level_editor.view.events.LevelsControllEvent;

public class LevelListControllPanel extends Sprite {

        private var _levelsListControll:LevelsListControllButtons;
        private var _levelsList:List;

        public function LevelListControllPanel() {
            init();
        }

        public function set data(model: LevelEditorModel): void {
            var levelsListDataProvider:DataProvider = new DataProvider();
            var levels:Array = model.levels;
            var n:int = levels.length;

            for (var i:int = 0; i < n; i++) {
                var level:ICoreLevelModel = levels[i] as ICoreLevelModel;
                levelsListDataProvider.addItem({label:level.levelSettings.name, someOtherVariable:0});
            }

            _levelsList.dataProvider = levelsListDataProvider;
            _levelsList.selectedIndex = model.currentLevelNumber;
        }

        private function init(): void {
            initLevelsListControll();
            initLevelsList();
        }

        private function initLevelsListControll():void {
            _levelsListControll = new LevelsListControllButtons();
            addChild(_levelsListControll);

            var n:int = _levelsListControll.buttons.length;

            for (var i:int = 0; i < n; i++) {
                var button:SimpleButton = _levelsListControll.buttons[i] as SimpleButton;
                button.addEventListener(MouseEvent.CLICK, onLevelsListControllButtonMouseClickHandler);
            }
        }

        private function onLevelsListControllButtonMouseClickHandler(event:MouseEvent):void {
            switch (event.target)
            {
                case _levelsListControll.addLevel :
                        onAddLevel();
                    break;

                case _levelsListControll.removeLevel :
                        onRemoveLevel();
                    break;

                case _levelsListControll.moveUp :
                        onMoveLevelUp();
                    break;

                case _levelsListControll.moveDown :
                        onMoveLevelDown();
                    break;
            }
        }

        private function onMoveLevelDown():void {
            if (_levelsList.selectedIndex >= 0 && _levelsList.selectedIndex < _levelsList.dataProvider.length - 1)
            {
                dispatchEvent(new LevelsControllEvent(LevelsControllEvent.MOVE_LEVEL_DOWN_BUTTON_CLICKED, true, false, _levelsList.selectedIndex));
            }
        }

        private function onMoveLevelUp():void {
            if (_levelsList.selectedIndex > 0)
            {
                dispatchEvent(new LevelsControllEvent(LevelsControllEvent.MOVE_LEVEL_UP_BUTTON_CLICKED, true, false, _levelsList.selectedIndex));
            }
        }

        private function onRemoveLevel():void {
            if (_levelsList.selectedIndex >= 0)
            {
                dispatchEvent(new LevelsControllEvent(LevelsControllEvent.REMOVE_LEVEL_BUTTON_CLICKED, true, false, _levelsList.selectedIndex));
            }
        }

        private function onAddLevel():void {
            dispatchEvent(new LevelsControllEvent(LevelsControllEvent.ADD_LEVEL_BUTTON_CLICKED, true));
        }

        private function initLevelsList():void
        {
            _levelsList = new List();
            _levelsList.setSize(150,_levelsListControll.height);
            _levelsList.labelFunction = levelsListLabelFunction;
            _levelsList.addEventListener(Event.CHANGE, onLevelsListItemChangeHandler);
            _levelsList.addEventListener(ListEvent.ITEM_CLICK, onLevelsListItemClickHandler);

            _levelsList.x = _levelsListControll.x + _levelsListControll.width + 5
            _levelsList.y = _levelsListControll.y;

            addChild(_levelsList);
        }

        function levelsListLabelFunction(item:Object):String {
            var someOtherVariable:int = item.someOtherVariable;
            var label:String = item.label;

            return item.label;
        }

        function onLevelsListItemChangeHandler(e:Event):void {
            dispatchEvent(new LevelsControllEvent(LevelsControllEvent.LEVEL_SELECTED, true, false, _levelsList.selectedIndex));
        }

        private function onLevelsListItemClickHandler(event:ListEvent):void {

        }


}

}

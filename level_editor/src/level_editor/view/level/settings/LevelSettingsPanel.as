/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 08.10.14
 * Time: 10:32
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings {
import flash.display.Sprite;

import level_editor.model.LevelEditorLevelModel;

import level_editor.model.LevelEditorModel;
import level_editor.view.level.settings.available_items.AvailableItemsControll;
import level_editor.view.level.settings.grid_dimensions.GridDimensionsControll;
import level_editor.view.level.settings.level_name.LevelNameControll;

public class LevelSettingsPanel extends Sprite {

    private var _levelNameControll: LevelNameControll;
    private var _gridDimensionsControll: GridDimensionsControll;
    private var _availableItemsControll: AvailableItemsControll;

    public function LevelSettingsPanel() {
        super();
        init();
    }

    private function init():void {
        visible = false;

        _levelNameControll = new LevelNameControll();
        addChild(_levelNameControll);

        _gridDimensionsControll = new GridDimensionsControll();
        _gridDimensionsControll.y = _levelNameControll.y + _levelNameControll.height + 10;
        addChild(_gridDimensionsControll);

        _availableItemsControll = new AvailableItemsControll();
        _availableItemsControll.y = _gridDimensionsControll.y + _gridDimensionsControll.height + 10;
        addChild(_availableItemsControll);
    }

    public function set data(model:LevelEditorModel):void {
        if (model.currentLevel) {
            _levelNameControll.data = model.currentLevel;
            _gridDimensionsControll.data = model.currentLevel.gameSettings;
            _availableItemsControll.data = model.currentLevel.gameSettings;
        }

        model.currentLevel ? visible = true : false;
    }

}
}

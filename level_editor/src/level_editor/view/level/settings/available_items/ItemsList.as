/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.10.14
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings.available_items {
import flash.display.Sprite;
import flash.events.MouseEvent;

import level_editor.view.events.LevelSettingsEvent;

import level_editor.view.level.settings.available_items.vo.ItemsListIRVO;

import level_editor.view.level.settings.available_items.vo.ItemsListVO;

public class ItemsList extends Sprite {

    [ArrayElementType("level_editor.view.level.settings.available_items.ItemsListIR")]
    private var _items:Array;

    public function ItemsList() {
        addEventListener(MouseEvent.CLICK, onItemClickHandler);
    }

    private function onItemClickHandler(event:MouseEvent):void {
        var target: ItemsListIR = event.target as ItemsListIR;

        dispatchEvent(new LevelSettingsEvent(LevelSettingsEvent.ITEM_PICKED, true, false, {id: target.id, is_picked: target.isPicked}));
    }

    public function set data(itemsListVO:ItemsListVO):void {
        var xx:int;

        clearItemsList();

        _items = [];
        for (var i:int = 0; i < itemsListVO.items.length; i++) {
            var itemsListIRVO:ItemsListIRVO = itemsListVO.items[i];
            var itemsListIR:ItemsListIR = new ItemsListIR();

            itemsListIR.data = itemsListIRVO;
            itemsListIR.x = xx;

            xx += itemsListIR.width + 5;

            _items[i] = itemsListIR;
            addChild(itemsListIR);
        }
    }

    private function clearItemsList():void {
        if (_items) {
            for (var i:int = 0; i < _items.length; i++) {
                var itemsListIR:ItemsListIR = _items[i];
                removeChild(itemsListIR);
            }

            _items = null;
        }
    }
}
}

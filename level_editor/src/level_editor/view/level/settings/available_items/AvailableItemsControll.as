/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 16.10.14
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings.available_items {
import fl.controls.ScrollBarDirection;
import fl.controls.TileList;
import fl.data.DataProvider;

import flash.display.SimpleButton;

import flash.display.Sprite;
import flash.events.MouseEvent;

import level_editor.view.level.settings.available_items.vo.ItemsListVO;

import model.grids.items.GridItemTypeEnum;

import model.level.settings.GameSettings;

public class AvailableItemsControll extends Sprite{

    private var _usualItemsList:ItemsList;
    private var _bonusItemsList:ItemsList;

    public function AvailableItemsControll() {
        super();
        init();
    }

    public function set data(model:GameSettings):void {
        _usualItemsList.data = new ItemsListVO(GridItemTypeEnum.USUAL_ITEMS_IDS, model.usualGridItemTypes);
        _bonusItemsList.data = new ItemsListVO(GridItemTypeEnum.BONUS_ITEMS_IDS, model.bonusGridItemTypes);

        _bonusItemsList.y = _usualItemsList.y + _usualItemsList.height + 5;
    }

    private function init():void {
        _usualItemsList = new ItemsList();
        addChild(_usualItemsList);

        _bonusItemsList = new ItemsList();
        addChild(_bonusItemsList);
    }
}
}

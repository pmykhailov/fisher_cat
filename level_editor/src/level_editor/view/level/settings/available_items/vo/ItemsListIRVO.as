package level_editor.view.level.settings.available_items.vo {
public class ItemsListIRVO {
    private var _id:int;
    private var _isPicked:Boolean;

    public function ItemsListIRVO(id:int, isPicked:Boolean) {
        _id = id;
        _isPicked = isPicked;
    }

    public function get id():int {
        return _id;
    }

    public function get isPicked():Boolean {
        return _isPicked;
    }
}
}

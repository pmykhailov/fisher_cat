package level_editor.view.level.settings.available_items.vo {
public class ItemsListVO {

    [ArrayElementType("level_editor.view.level.settings.available_items.vo.ItemsListIRVO")]
    private var _items:Array;

    public function ItemsListVO(availableItems:Array, pickedItems:Array) {

        _items = [];

        for (var i:int = 0; i < availableItems.length; i++) {
            var isPicked:Boolean = pickedItems.indexOf(availableItems[i]) >= 0;
            var itemsListIRVO:ItemsListIRVO = new ItemsListIRVO(availableItems[i], isPicked);

            _items.push(itemsListIRVO);
        }
    }

    public function get items():Array {
        return _items;
    }
}
}

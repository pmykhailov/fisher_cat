/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.10.14
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings.available_items {
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

import level_editor.view.level.settings.available_items.vo.ItemsListIRVO;


public class ItemsListIR extends Sprite{

    private var _background:Sprite;
    private var _idTextField:TextField;
    private var _isPicked:Boolean;
    private var _id:int;

    public function ItemsListIR() {
        super();
        init();
    }

    private function init():void {

        mouseChildren = false;

        buttonMode = true;

        _background = new Sprite();
        addChild(_background);

        _idTextField = new TextField();
        _idTextField.textColor = 0xFFFFFF;
        _idTextField.autoSize = TextFieldAutoSize.CENTER;
        addChild(_idTextField);
        
        addEventListener(MouseEvent.CLICK, onMouseClickHandler);
    }

    private function onMouseClickHandler(event:MouseEvent):void {
        isPicked = !isPicked;
    }

    public function set data(itemsListIRVO:ItemsListIRVO):void {
        isPicked = itemsListIRVO.isPicked;
        _id = itemsListIRVO.id;

        var tf:TextFormat = new TextFormat();
        tf.bold = true;
        tf.size = 16;

        _idTextField.defaultTextFormat = tf;

        _idTextField.text = _id + "";
        _idTextField.x = (_background.width - _idTextField.width)/2;
        _idTextField.y = (_background.height - _idTextField.height)/2;

    }

    public function set isPicked(value:Boolean):void{
        _isPicked = value;

        drawBackground(value ? 1 : 0.5);
    }

    public function get isPicked():Boolean {
        return _isPicked;
    }

    public function get id():int {
        return _id;
    }

    private function drawBackground(alpha:Number = 1):void {
        _background.graphics.clear();
        _background.graphics.beginFill(0x0000FF, alpha);
        _background.graphics.drawRect(0,0,40,40);
        _background.graphics.endFill();
    }
}
}

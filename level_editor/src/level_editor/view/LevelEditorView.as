package level_editor.view {
import flash.display.SimpleButton;
import flash.display.Sprite;
import flash.events.MouseEvent;

import level_editor.model.LevelEditorModel;
import level_editor.view.events.LevelEditorViewEvent;
import level_editor.view.grid.GridHoldersContainer;
import level_editor.view.level.list.LevelListControllPanel;
import level_editor.view.level.settings.LevelSettingsPanel;
import level_editor.view.statics.LevelEditorDimensions;

/**
 * View class.
 * User: Paul Makarenko
 * Date: 22.09.2014
 */
public class LevelEditorView extends Sprite {


    private var _saveButton:SimpleButton;
    private var _loadButton:SimpleButton;
    private var _levelListControllPanel:LevelListControllPanel;
    private var _levelSettingsPanel:LevelSettingsPanel;
    private var _gridHoldersContainer:GridHoldersContainer;

    public function LevelEditorView() {
        super();
        _init();
    }

    public function set data(model:LevelEditorModel):void {
        _levelListControllPanel.data = model;
        _gridHoldersContainer.data = model;
        _levelSettingsPanel.data = model;

        reLayoutComponents();
    }

    private function reLayoutComponents(): void {
        _levelSettingsPanel.x = _levelListControllPanel.x + _levelListControllPanel.width + 10;
    }

    private function _init():void {
        _initLevelsPanel();
        _initLevelSettingsPanel();
        _intGridHoldersContainer();
        _initButtons();
    }

    private function _initLevelSettingsPanel():void {
        _levelSettingsPanel = new LevelSettingsPanel();
        _levelSettingsPanel.y = _levelListControllPanel.y;
        addChild(_levelSettingsPanel);
    }

    private function _intGridHoldersContainer():void {
        _gridHoldersContainer = new GridHoldersContainer();
        addChild(_gridHoldersContainer);

        _gridHoldersContainer.x = 10;
        _gridHoldersContainer.y = _levelListControllPanel.y + _levelListControllPanel.height + 10;
    }

    private function _initLevelsPanel():void {
        _levelListControllPanel = new LevelListControllPanel();
        _levelListControllPanel.x = 10;
        _levelListControllPanel.y = 10;
        addChild(_levelListControllPanel);
    }

    private function _initButtons():void {
        _saveButton = new View_SaveButton();
        _saveButton.x = LevelEditorDimensions.WIDTH - _saveButton.width - 10;
        _saveButton.y = LevelEditorDimensions.HEIGHT - _saveButton.height - 10;
        _saveButton.addEventListener(MouseEvent.CLICK, onSaveButtonMouseClickHandler);

        _loadButton = new View_LoadButton();
        _loadButton.x = _saveButton.x - _loadButton.width - 10;
        _loadButton.y = _saveButton.y;
        _loadButton.addEventListener(MouseEvent.CLICK, onLoadButtonMouseClickHandler);

        addChild(_saveButton);
        addChild(_loadButton);
    }

    private function onSaveButtonMouseClickHandler(event:MouseEvent):void {
        dispatchEvent(new LevelEditorViewEvent(LevelEditorViewEvent.SAVE_BUTTON_CLICKED));
    }

    private function onLoadButtonMouseClickHandler(event:MouseEvent):void {
        dispatchEvent(new LevelEditorViewEvent(LevelEditorViewEvent.LOAD_BUTTON_CLICKED));
    }
}

}
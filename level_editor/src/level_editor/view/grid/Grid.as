package level_editor.view.grid {
    import flash.display.Sprite;
    import flash.events.MouseEvent;

    import level_editor.view.events.GridEvent;

    /**
     * Grid class.
     * User: Paul Makarenko
     * Date: 18.04.14
     */
    public class Grid extends Sprite {

        private var _cols: int;
        private var _rows: int;
        private var _cells: Array;
        private var _type: String;


        public function Grid(type: String) {
            super();
            _type = type;
            init();
        }


        public function get type(): String {
            return _type;
        }


        public function set data(value: Object): void {
            _cols = value.cols;
            _rows = value.rows;
            _cells = new Array();

            for (var i: int = 0; i < _rows; i++) {
                _cells[i] = new Array();
                for (var j: int = 0; j < _cols; j++) {
                    var gridCell: GridCell = new GridCell();

                    gridCell.x = j * gridCell.width;
                    gridCell.y = i * gridCell.height;
                    gridCell.row = i;
                    gridCell.col = j;
                    gridCell.data = value.elements[i][j];

                    addChild(gridCell);

                    _cells[i][j] = gridCell;
                }
            }
        }


        public function getGridCell(row: int, col: int): GridCell {
            return _cells[row][col];
        }


        private function init(): void {
            addEventListener(MouseEvent.MOUSE_DOWN, onCellMouseDownHandler);
            addEventListener(MouseEvent.MOUSE_UP, onCellMouseUpHandler);
        }


        private function onCellMouseOverHandler(event: MouseEvent): void {
            var cell:GridCell = event.target as GridCell;
            cell.selected = !cell.selected;
            dispatchEvent(new GridEvent(GridEvent.GRID_CELL_CLICKED, true, false, {cell: cell, grid_type: _type}));
        }


        private function onCellMouseDownHandler(event: MouseEvent): void {
            var cell:GridCell = event.target as GridCell;
            dispatchEvent(new GridEvent(GridEvent.GRID_CELL_CLICKED, true, false, {cell: cell, grid_type: _type}));

            addEventListener(MouseEvent.MOUSE_OVER, onCellMouseOverHandler);
        }

        private function onCellMouseUpHandler(event: MouseEvent): void {
            removeEventListener(MouseEvent.MOUSE_OVER, onCellMouseOverHandler);
        }

    }
}
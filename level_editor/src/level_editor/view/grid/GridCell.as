package level_editor.view.grid {
    import flash.display.MovieClip;
    import flash.events.MouseEvent;

    import model.grids.items.BaseGridItemEnum;

    /**
     * GridCell class.
     * User: Paul Makarenko
     * Date: 18.04.14
     */
    public class GridCell extends MovieClip {

        private var _view: MovieClip;
        private var _frame: MovieClip;
        private var _isSelected: Boolean;
        private var _row: int;
        private var _col: int;


        public function GridCell() {
            super();
            init();
        }


        public function get row(): int {
            return _row;
        }


        public function set row(value: int): void {
            _row = value;
        }


        public function get col(): int {
            return _col;
        }


        public function set col(value: int): void {
            _col = value;
        }


        public function set highlight(value: Boolean): void {
            value ? _frame.gotoAndStop(2) : _frame.gotoAndStop(1);
        }


        public function get selected(): Boolean {
            return _isSelected;
        }


        public function set selected(value: Boolean): void {
            _isSelected = value;

            if (_isSelected) {
                _view.gotoAndStop(2);
            } else {
                _view.gotoAndStop(1);
            }
        }


        public function set data(value: int): void {
            selected = value != BaseGridItemEnum.NOT_EXISTING;
        }


        private function init(): void {
            mouseChildren = false;

            _view = new View_GridCell() as MovieClip;
            _view.gotoAndStop(1);

            _frame = _view["frame_mc"] as MovieClip;

            highlight = false;

            selected = false;

            addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler)

            addChild(_view);
        }


        private function onMouseDownHandler(event: MouseEvent): void {
            selected = !selected;
        }
    }
}
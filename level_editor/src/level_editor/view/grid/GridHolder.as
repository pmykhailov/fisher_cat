package level_editor.view.grid {
    import flash.display.Sprite;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;

    /**
     * GridHolder class.
     * User: Paul Makarenko
     * Date: 22.04.14
     */
    public class GridHolder extends Sprite {

        public var grid: Grid;
        public var txt: TextField;

        public function GridHolder(headerText:String, gridType: String) {
            super();

            grid = new Grid(gridType);
            addChild(grid);

            txt = new TextField();
            txt.autoSize = TextFieldAutoSize.LEFT;
            txt.selectable = false;
            txt.text = headerText;
            addChild(txt);

            grid.y = txt.height + 5;

            mouseEnabled = false;
        }

    }
}
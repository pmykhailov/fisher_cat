package level_editor.view.grid {
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.MouseEvent;

import level_editor.model.LevelEditorLevelModel;
import level_editor.model.LevelEditorModel;
import level_editor.view.events.LevelEditorViewEvent;

import model.level.settings.vo.GridSettingsVO;

public class GridHoldersContainer extends Sprite {

    public function GridHoldersContainer() {
        super();
    }

    [ArrayElementType("level_editor.view.grid.GridHolder")]
    public var _grids:Array;

    public function set data(model:LevelEditorModel):void {

        _clearView();

        if (model.currentLevelNumber == -1) {
            return;
        }

        _grids = [];

        var i:int;
        var currentLevelModel:LevelEditorLevelModel = model.currentLevel;
        var grids:Array = currentLevelModel.gameSettings.gridSettingsVOs;

        // 2.1 Fill grids with model
        for (i = 0; i < grids.length; i++) {
            var gridSettingsVO:GridSettingsVO = grids[i];
            var gridHolder:GridHolder = _addGrid(gridSettingsVO.type);

            var rows:int = currentLevelModel.gameSettings.gameFieldRows;
            var cols:int = currentLevelModel.gameSettings.gameFieldCols;

            gridHolder.grid.data = {elements: gridSettingsVO.elements, rows: rows, cols: cols};
        }

        // 2.2 Grids layout
        for (i = 1; i < _grids.length; i++) {
            _grids[i].x = _grids[i - 1].width + 20;
        }

    }

    private function _addGrid(type:String):GridHolder {
        var gridHolder:GridHolder = new GridHolder(type.toUpperCase(), type);

        gridHolder.addEventListener(MouseEvent.MOUSE_OVER, onGridCellMouseOverHandler);
        gridHolder.addEventListener(MouseEvent.MOUSE_OUT, onGridCellMouseOutHandler);

        _grids.push(gridHolder);

        addChild(gridHolder);

        return gridHolder;
    }

    private function _clearView():void {
        if (_grids) {
            for (var i:int = 0; i < _grids.length; i++) {
                var grid:DisplayObject = _grids[i];

                removeChild(grid);
            }

            _grids = null;
        }
    }

    private function onGridCellMouseOutHandler(event:MouseEvent):void {
        var target:GridCell = event.target as GridCell;
        if (target) {
            for (var i:int = 0; i < _grids.length; i++) {
                var gridHolder:GridHolder = _grids[i];
                gridHolder.grid.getGridCell(target.row, target.col).highlight = false;
            }
        }
    }

    private function onGridCellMouseOverHandler(event:MouseEvent):void {
        var target:GridCell = event.target as GridCell;
        if (target) {
            for (var i:int = 0; i < _grids.length; i++) {
                var gridHolder:GridHolder = _grids[i];
                gridHolder.grid.getGridCell(target.row, target.col).highlight = true;
            }
        }
    }


}
}

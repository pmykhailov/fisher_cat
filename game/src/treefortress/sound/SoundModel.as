package treefortress.sound {

    public class SoundModel {

        public static const GROUP_MUSIC: String = "group_music";
        public static const GROUP_SFX: String = "group_sfx";

        public static const AMBIENCE:String = "AmbienceOceanShore";

        public static const BUTTON_CLICK:String = "FLA_button_click";
        public static const TIME_IS_UP:String = "FLA_game_over";
        public static const FINAL_LEVEL_COMPLETE:String = "FLA_challenge_completed";
        public static const LEVEL_COMPLETE:String = "FLA_level_win";

        public static const READY_GO:String = "ReadyGo";

        public static const ITEMS_MATCHED:String = "FLA_grid_items_matched";
        public static const ITEM_FALL:String = "FLA_item_fall_sound";
        public static const ITEM_SELECT:String = "FLA_grid_item_select";
        public static const ITEM_UNLUCKY_SWAP:String = "FLA_grid_items_unlucky_swap";

        public static const ALL_FX:Array = [BUTTON_CLICK, TIME_IS_UP, FINAL_LEVEL_COMPLETE, LEVEL_COMPLETE, ITEMS_MATCHED, ITEM_FALL, ITEM_SELECT, ITEM_UNLUCKY_SWAP];
    }
}
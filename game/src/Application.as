package {
    import by.lord_xaoca.extensions.VirtualViewMapExtension;
    import by.lord_xaoca.helpers.StageReference;

    import com.application.ApplicationConfig;
    import com.game.GameConfig;
    import com.game_wrapper.GameWrapperConfig;
    import com.screens.ScreensConfig;
    import com.screens.model.Levels;
    import com.tutorial.TutorialConfig;

    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;

    import robotlegs.bender.bundles.mvcs.MVCSBundle;
    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.impl.Context;
    /*  720 x 1280 */
    /*  360 x 640 */
    [SWF(backgroundColor="0xCCCCCC", width="720", height="1280", frameRate="30")]
    public class Application extends Sprite {

        private var _context: IContext;

        [Embed(source="data/levels.xml")]
        private var xmlClass: Class;


        public function Application() {
            super();

            addEventListener(Event.ADDED_TO_STAGE, onAddedToStageHandler);
        }


        private function onAddedToStageHandler(event: Event): void {
            removeEventListener(Event.ADDED_TO_STAGE, onAddedToStageHandler);

            //this.scaleX = this.scaleY = 0.3;
            StageReference.init(stage);

            //stage.align = StageAlign.TOP_LEFT;
            //stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.scaleMode = StageScaleMode.SHOW_ALL;

            initLevelsData();

            _context = new Context()
                    .install(MVCSBundle, VirtualViewMapExtension)
                    .configure(ApplicationConfig, GameWrapperConfig, GameConfig, ScreensConfig, TutorialConfig)
                    //.configure(ApplicationConfig, GameConfig)
                    .configure(new ContextView(this));

            // TODO: move logic from view (popups) to commands
            // TODO: Добавить свойство которое будет отвечать за генезацию бонус симсолов при сгорании обычных символов

            addChild(new FPSCounter(0,0,0xFFFFFF,true));
        }


        public function initLevelsData(): void {
            var levelsData:XML = XML(xmlClass.data);

            Levels.init(levelsData);
        }
    }
}

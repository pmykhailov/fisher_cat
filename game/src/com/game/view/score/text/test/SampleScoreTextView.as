package com.game.view.score.text.test {
    import com.game.view.score.text.base.ScoreTextView;
    import com.greensock.TweenLite;

    public class SampleScoreTextView extends ScoreTextView {
        public function SampleScoreTextView() {
            super();
        }


        override public function runAnimation(): void {
            alpha = 0;
            TweenLite.to(this, 0.7, {alpha: 1, scaleX: 1.5, scaleY: 1.5, onComplete: notifyAnimationComplete});
        }

    }
}
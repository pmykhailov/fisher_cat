package com.game.view.score.manager {
    import com.game.view.score.layer.ScoreLayerView;

    public class ScoreViewManager {

        protected var _scoreLayerView: ScoreLayerView;


        public function ScoreViewManager() {
            init();
        }


        public function get scoreLayerView(): ScoreLayerView {
            return _scoreLayerView;
        }


        protected function init(): void {
            createScoreLayer();
        }


        protected function createScoreLayer(): void {
            _scoreLayerView = new ScoreLayerView();
            _scoreLayerView.mouseChildren = false;
            _scoreLayerView.mouseEnabled = false;
        }

    }
}
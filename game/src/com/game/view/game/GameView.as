package com.game.view.game {
    import by.lord_xaoca.robotlegs2.BaseView;

    import com.game.model.GameModel;
import com.game.view.game_filed.grids.base.BaseGridView;
import com.game.view.game_filed.grids.grid_items.GridView;
    import com.game.view.game_filed.grids.grid_background.GridBackgroundView;
    import com.game.view.score.manager.ScoreViewManager;
    import com.greensock.TimelineLite;

    import flash.display.Sprite;

import model.grids.enum.GridTypeEnum;

import model.level.settings.GameSettings;

public class GameView extends BaseView {

        private var _gameModel: GameModel;
        [ArrayElementType("com.game.view.game_filed.grids.base.BaseGridView")]
        private var _gridViews: Array;
        private var _itemsDeleteLayer: Sprite;
        private var _scoreViewManager: ScoreViewManager;
        private var _spawnTimeline: TimelineLite;
        private var _isSpawningItems: Boolean;
        private var _isDeletingItems: Boolean;


        public function GameView(gameModel: GameModel) {
            _gameModel = gameModel;
            super(new Sprite());
        }


        public function get scoreViewManager(): ScoreViewManager {
            return _scoreViewManager;
        }


        public function set scoreViewManager(value: ScoreViewManager): void {
            _scoreViewManager = value;
        }


        public function get itemsDeleteLayer(): Sprite {
            return _itemsDeleteLayer;
        }


        public function get isSpawningItems(): Boolean {
            return _isSpawningItems;
        }


        public function set isSpawningItems(value: Boolean): void {
            _isSpawningItems = value;
        }


        public function get isDeletingItems(): Boolean {
            return _isDeletingItems;
        }


        public function set isDeletingItems(value: Boolean): void {
            _isDeletingItems = value;
        }


        public function get spawnTimeline(): TimelineLite {
            return _spawnTimeline;
        }


        public function set spawnTimeline(value: TimelineLite): void {
            _spawnTimeline = value;
        }


        public function getGridViewByType(type: String): BaseGridView {
            var n:uint = _gridViews.length;

            for (var i:int = 0; i < n; i++) {
                if (_gridViews[i].type == type)
                {
                    return _gridViews[i]
                }

            }

            return null;
        }

        override protected function initialize(): void {
            super.initialize();

            createScoreViewManager();
            createItemsDeleteLayer();
            createGridViews();

            addGridViewsToDisplaylist();
            addChild(_itemsDeleteLayer);
            addChild(_scoreViewManager.scoreLayerView);

            // Model is needed only for view init
            // Let's make sure we will not use it after init
            _gameModel = null;
        }

        private function addGridViewsToDisplaylist():void {
            var n:uint = _gridViews.length;

            for (var i:int = 0; i < n; i++) {
                addChild(_gridViews[i]);
            }
        }


        protected function createGridViews(): void {
            var gameSettings:GameSettings = _gameModel.gameSettings;
            var gridSettingsVOs:Array = gameSettings.gridSettingsVOs;
            var n: int = gridSettingsVOs.length;

            _gridViews = [];

            for (var i:int = 0; i < n; i++) {
                var type: String = gridSettingsVOs[i].type;
                var GridViewClass: Class = getGridViewClassByType(type);

                _gridViews[i] = new GridViewClass(type, gameSettings.itemWidth, gameSettings.itemHeight, gameSettings.itemSpacing);
            }

            getGridViewByType(GridTypeEnum.TYPE_BACKGROUND).enable = false;
        }


        protected function createScoreViewManager(): void {
            _scoreViewManager = new ScoreViewManager();
        }


        private function createItemsDeleteLayer(): void {
            _itemsDeleteLayer = new Sprite();
            _itemsDeleteLayer.mouseChildren = false;
            _itemsDeleteLayer.mouseEnabled = false;
        }

        private function getGridViewClassByType(type: String): Class {
            switch (type) {
                case GridTypeEnum.TYPE_MAIN :
                    return GridView;
                    break;

                case GridTypeEnum.TYPE_BACKGROUND :
                    return GridBackgroundView;
                    break;
            }

            return null;
        }
    }
}

package com.game.view.game_filed.grids.base.item {
    import by.lord_xaoca.utils.ClassUtils;
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    /**
     * BaseGridItemView class.
     * User: Paul Makarenko
     * Date: 22.04.14
     */
    public class BaseGridItemView extends BaseDisplayObjectContainer {

        protected var _row: int;
        protected var _col: int;
        protected var _type: int;


        public function BaseGridItemView(className: String) {
            super(ClassUtils.getInstanceByClassName(className));
        }


        public function get col(): int {
            return _col;
        }


        public function set col(value: int): void {
            _col = value;
        }


        public function get row(): int {
            return _row;
        }


        public function set row(value: int): void {
            _row = value;
        }


        public function get type(): int {
            return _type;
        }


        public function set type(value: int): void {
            _type = value;
        }

    }
}
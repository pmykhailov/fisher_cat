package com.game.view.game_filed.grids.base {
    import by.lord_xaoca.utils.ArrayUtils;

    import com.game.model.grids.base.BaseGridModel;
    import com.game.model.grids.base.item.BaseGridItemModel;
    import com.game.view.game_filed.grids.base.item.BaseGridItemView;

    import flash.display.Sprite;
    import flash.geom.Point;

    /**
     * BaseGridView class.
     * User: Paul Makarenko
     * Date: 22.04.14
     */
    public class BaseGridView extends Sprite {

        protected var _rows: int;
        protected var _cols: int;
        [ArrayElementType("com.game.view.game_filed.grids.base.item.BaseGridItemView")]
        protected var _items: Array;
        protected var _itemWidth: int;
        protected var _itemHeight: int;
        protected var _itemSpacing: Point;
        protected var _type:String;


        public function BaseGridView(type: String, itemWidth: int, itemHeight: int, itemSpacing: Point) {
            super();

            _type = type;
            _itemWidth = itemWidth;
            _itemHeight = itemHeight;
            _itemSpacing = itemSpacing;
        }

        public function get type():String {
            return _type;
        }

        public function get itemHeight(): int {
            return _itemHeight;
        }


        public function get itemWidth(): int {
            return _itemWidth;
        }

        public function get itemSpacing():Point {
            return _itemSpacing;
        }

        public function set enable(value: Boolean): void {
            mouseChildren = mouseEnabled = value;
        }


        public function addGridItem(item: BaseGridItemView): void {
            _items.push(item);
            addChild(item.view);
        }


        public function removeGridItem(item: BaseGridItemView): void {
            if (ArrayUtils.removeEntry(_items, item)) {
                removeChild(item.view);
            }
        }


        public function removeGridItems(): void {
            if (!_items) return;

            while (_items.length > 0) {
                removeGridItem(_items[0]);
            }
        }


        public function getItemDesiredCoordinates(row: int, col: int): Point {
            var xx:Number = _itemWidth / 2 + col * (_itemWidth + _itemSpacing.x);
            var yy:Number = _itemHeight / 2 + row * (_itemHeight + +_itemSpacing.y);

            return new Point(xx, yy);
        }


        public function createGridItems(gridModel: BaseGridModel): void {
            _rows = gridModel.rows;
            _cols = gridModel.cols;

            _items = [];

            for (var i: int = 0; i < _rows; i++) {
                for (var j: int = 0; j < _cols; j++) {
                    addGridItem(createNewGridItem(gridModel, i, j));
                }
            }
        }


        public function createNewGridItem(gridModel: BaseGridModel, row: int, col: int): BaseGridItemView {
            var itemCoordinates: Point = getItemDesiredCoordinates(row, col);
            var itemView: BaseGridItemView = createItemViewInstance();
            var itemModel: BaseGridItemModel = gridModel.getGridItem(row, col);

            itemView.x = itemCoordinates.x;
            itemView.y = itemCoordinates.y;
            itemView.row = row;
            itemView.col = col;
            itemView.type = itemModel.type;

            return itemView;
        }


        public function getGridBackgroundItemView(row: int, col: int): BaseGridItemView {
            for (var i: int = 0; i < _items.length; i++) {
                var item: BaseGridItemView = _items[i] as BaseGridItemView;
                if (item.row == row && item.col == col) {
                    return item;
                }
            }

            return null;
        }


        /** ABSTRACT **/
        protected function createItemViewInstance(): BaseGridItemView {
            return null;
        }

    }
}
package com.game.view.game_filed.grids.grid_items.changed_grid_items_visualiser {
    import com.game.model.GameModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.logic.changed_grid_items_groups.groups.base.ChangedGridItemsGroup;
    import com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup;
    import com.game.view.events.GameViewEvent;
    import com.game.view.game.GameView;
    import com.game.view.game_filed.grids.grid_items.GridView;
    import com.game.view.game_filed.grids.grid_items.changed_grid_items_visualiser.base.BaseChangedGridItemsViewApplier;
    import com.game.view.game_filed.grids.grid_items.item.base.GridItemView;
    import com.game.view.score.text.base.ScoreTextView;
    import com.game.view.score.text.test.SampleScoreTextView;

    import model.grids.enum.GridTypeEnum;

    /**
     * RowColAnimationController class.
     * User: Paul Makarenko
     * Date: 10.01.2015
     */
    public class ScoreGivingGridItemsGroupViewApplier extends BaseChangedGridItemsViewApplier {

        private var _group: ScoreGivingGridItemsGroup;

        private var _items: Array;
        private var _scores: Array;

        private var _gridView: GridView;

        private var _finished: int;


        public function ScoreGivingGridItemsGroupViewApplier(gameModel: GameModel, gameView: GameView) {
            super(gameModel, gameView);
        }


        private function get gridView(): GridView {
            if (!_gridView) {
                _gridView = _gameView.getGridViewByType(GridTypeEnum.TYPE_MAIN) as GridView;
            }
            return _gridView;
        }


        override public function startAnimation(group: ChangedGridItemsGroup): void {


            _items = [];
            _scores = [];

            _group = group as ScoreGivingGridItemsGroup;

            // Mem items that will be animated
            for (var j: int = 0; j < group.gridItems.length; j++) {
                var item: GridItemModel = group.gridItems[j] as GridItemModel;
                var itemView: GridItemView = gridView.getItemViewByID(item.id);
                var isInArray: Boolean = false;

                if (_group.ignoreGridItems[item.id]) continue;

                // TODO There shoudn't be same items already
                for (var k: int = 0; k < _items.length; k++) {
                    if (_items[k] == itemView) {
                        isInArray = true;
                        break;
                    }
                }

                if (!isInArray) {
                    _items.push(itemView);
                }
            }

            // Create score texts
            var scoreText: ScoreTextView;

            if (_group.showScorePerEachItem) {
                for (var i: int = 0; i < _group.gridItems.length; i++) {
                    scoreText = createScoreTextViewForGridItem(_group.gridItems[i], _group.scorePerItem);
                    _scores.push(scoreText);
                    _gameView.scoreViewManager.scoreLayerView.addChild(scoreText.view);
                }
            }else
            {
                scoreText = createScoreTextViewForGridItemsGroup(_group);
                _scores.push(scoreText);
                _gameView.scoreViewManager.scoreLayerView.addChild(scoreText.view);
            }


            // Add items to another container
            for (var l: int = 0; l < _items.length; l++) {
                var gridItemView: GridItemView = _items[l];
                gridView.removeGridItem(gridItemView);
                _gameView.itemsDeleteLayer.addChild(gridItemView.view);
            }

            _finished = 0;

            // Run items animations
            for (var i2: int = 0; i2 < _items.length; i2++) {
                (_items[i2] as GridItemView).addEventListener(GameViewEvent.DELETE_ITEM_ANIMATION_COMPLETE, onDeleteItemAnimationComplete);
                (_items[i2] as GridItemView).runDeleteAnimation();
            }

            // Run scrore text animations
            for (var i3: int = 0; i3 < _scores.length; i3++) {
                (_scores[i3] as ScoreTextView).addEventListener(GameViewEvent.SCORE_TEXT_ANIMATION_COMPLETE, onScoreTextViewAnimationComplete);
                (_scores[i3] as ScoreTextView).runAnimation();
            }

        }


        private function createScoreTextViewForGridItem(gridItemModel: GridItemModel, score:int): ScoreTextView {
            var gridItemView: GridItemView = gridView.getItemViewByID(gridItemModel.id);
            var scoreTextView: ScoreTextView;

            scoreTextView = new SampleScoreTextView();
            scoreTextView.score = score;

            scoreTextView.x = gridItemView.x;
            scoreTextView.y = gridItemView.y;

            return scoreTextView;
        }
        private function createScoreTextViewForGridItemsGroup(group: ScoreGivingGridItemsGroup): ScoreTextView {
            var scoreTextView: ScoreTextView;
            var firstGridItem: GridItemView;
            var lastGridItem: GridItemView;
            var itemModel: GridItemModel;

            scoreTextView = new SampleScoreTextView();
            scoreTextView.score = group.score;

            itemModel = group.gridItems[0] as GridItemModel;
            firstGridItem = gridView.getItemViewByID(itemModel.id);

            itemModel = group.gridItems[group.gridItems.length - 1] as GridItemModel;
            lastGridItem = gridView.getItemViewByID(itemModel.id);

            scoreTextView.x = (firstGridItem.x + lastGridItem.x) / 2;
            scoreTextView.y = (firstGridItem.y + lastGridItem.y) / 2;

            return scoreTextView;
        }


        private function onScoreTextViewAnimationComplete(event: GameViewEvent): void {
            var scoreTextView: ScoreTextView = event.target as ScoreTextView;

            scoreTextView.removeEventListener(GameViewEvent.SCORE_TEXT_ANIMATION_COMPLETE, onScoreTextViewAnimationComplete);

            _gameView.scoreViewManager.scoreLayerView.removeChild(scoreTextView.view);
        }


        private function onDeleteItemAnimationComplete(event: GameViewEvent): void {
            var gridItemView: GridItemView = event.target as GridItemView;
            gridItemView.removeEventListener(GameViewEvent.DELETE_ITEM_ANIMATION_COMPLETE, onDeleteItemAnimationComplete);

            _gameView.itemsDeleteLayer.removeChild(gridItemView.view);

            _finished++;

            if (_finished == _items.length) {
                // delete items animation complete
                notifyAnimationComplete();
            }
        }

    }
}
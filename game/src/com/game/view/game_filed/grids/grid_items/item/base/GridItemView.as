package com.game.view.game_filed.grids.grid_items.item.base {
    import com.game.view.events.GameViewEvent;
    import com.game.view.game_filed.grids.base.item.BaseGridItemView;

import model.grids.items.GridItemTypeEnum;

import flash.events.Event;
    import flash.events.MouseEvent;

    public class GridItemView extends BaseGridItemView {

        protected var _id: int;
        protected var _isSelected: Boolean;


        public function GridItemView() {
            super("View_GridItem");
            init();
        }


        public function get id(): int {
            return _id;
        }


        public function set id(value: int): void {
            _id = value;
        }


        override public function set type(value: int): void {
            super.type = value;

            if (_type == GridItemTypeEnum.NON_EXISTING) {
                mouseChildren = mouseEnabled = false;
                visible = false;
            }
        }


        public function get isSelected(): Boolean {
            return _isSelected;
        }


        public function set isSelected(value: Boolean): void {
            _isSelected = value;
        }


        public function runDeleteAnimation(): void {
            // Run notifyDeleteAnimationComplete in delete animation complete event handler
            notifyDeleteAnimationComplete();
        }


        protected function init(): void {
            initElements();
            initHitArea();
            addListeners();
        }


        protected function initElements(): void {
            _isSelected = false;
        }


        protected function initHitArea(): void {
            mouseChildren = false;
            _view.hitArea = getSprite("_hitArea");
            getSprite("_hitArea").visible = false;
        }


        protected function addListeners(): void {
            addViewListener(Event.REMOVED_FROM_STAGE, onRemoveFromStage);

            addViewListener(MouseEvent.MOUSE_OVER, onMouseOverHandler);
            addViewListener(MouseEvent.MOUSE_OUT, onMouseOutHandler);
            addViewListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
        }


        protected function removeListeners(): void {
            removeViewListener(Event.REMOVED_FROM_STAGE, onRemoveFromStage);

            removeViewListener(MouseEvent.MOUSE_OVER, onMouseOverHandler);
            removeViewListener(MouseEvent.MOUSE_OUT, onMouseOutHandler);
            removeViewListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
        }


        protected function notifyDeleteAnimationComplete(): void {
            dispatchEvent(new GameViewEvent(GameViewEvent.DELETE_ITEM_ANIMATION_COMPLETE));
        }


        protected function onMouseOverHandler(event: MouseEvent): void {

        }


        protected function onMouseOutHandler(event: MouseEvent): void {

        }


        protected function onMouseDownHandler(event: MouseEvent): void {
            isSelected = !isSelected;
        }


        protected function onRemoveFromStage(event: Event): void {
            removeListeners();
        }

    }
}
package com.game.view.game_filed.grids.grid_items {
    import com.game.events.GameContextEvent;
    import com.game.view.events.GameViewEvent;

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.events.MouseEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    public class GridViewMediator extends Mediator {
        [Inject]
        public var gridView: GridView;


        public function GridViewMediator() {
        }


        override public function initialize(): void {
            super.initialize();

            addViewListener(MouseEvent.MOUSE_DOWN, onItemMouseDownHandler);
            addViewListener(MouseEvent.MOUSE_UP, onItemMouseUpHandler);

            addContextListener(GameContextEvent.START_ITEM_MOUSE_OVER_LISTENING, onStartItemMouseOverListeningHandler);
            addContextListener(GameContextEvent.STOP_ITEM_MOUSE_OVER_LISTENING, onStopItemMouseOverListeningHandler);
        }


        protected function onItemMouseDownHandler(event: MouseEvent): void {
            dispatch(new GameViewEvent(GameViewEvent.ITEM_MOUSE_DOWN, gridView.getItemViewByView(event.target as DisplayObject)));
        }


        protected function onItemMouseUpHandler(event: MouseEvent): void {
            dispatch(new GameViewEvent(GameViewEvent.ITEM_MOUSE_UP, gridView.getItemViewByView(event.target as MovieClip)));
        }


        protected function onItemMouseOverHandler(event: MouseEvent): void {
            dispatch(new GameViewEvent(GameViewEvent.ITEM_MOUSE_OVER, gridView.getItemViewByView(event.target as MovieClip)));

        }


        private function onStartItemMouseOverListeningHandler(event: GameContextEvent): void {
            addViewListener(MouseEvent.MOUSE_OVER, onItemMouseOverHandler)
        }


        private function onStopItemMouseOverListeningHandler(event: GameContextEvent): void {
            removeViewListener(MouseEvent.MOUSE_OVER, onItemMouseOverHandler)
        }

    }
}
package com.game.view.game_filed.grids.grid_items.changed_grid_items_visualiser.base {
    import com.game.model.GameModel;
    import com.game.model.logic.changed_grid_items_groups.groups.base.ChangedGridItemsGroup;
    import com.game.view.game.GameView;

    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IEventDispatcher;

    /**
     * BaseChangedGridItemsVisualiser class.
     * User: Paul Makarenko
     * Date: 10.01.2015
     */
    public class BaseChangedGridItemsViewApplier extends EventDispatcher {

        protected var _gameModel: GameModel;
        protected var _gameView: GameView;

        public function BaseChangedGridItemsViewApplier(gameModel: GameModel, gameView: GameView) {
            super();

            _gameModel = gameModel;
            _gameView = gameView;
        }


        public function startAnimation(group: ChangedGridItemsGroup): void {

        }


        protected function notifyAnimationComplete(): void {
            dispatchEvent(new Event(Event.COMPLETE));
        }
    }
}
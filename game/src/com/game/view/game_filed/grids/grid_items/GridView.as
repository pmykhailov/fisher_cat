package com.game.view.game_filed.grids.grid_items {
    import com.game.model.grids.base.BaseGridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.view.game_filed.grids.base.BaseGridView;
    import com.game.view.game_filed.grids.base.item.BaseGridItemView;
    import com.game.view.game_filed.grids.grid_items.item.base.GridItemView;
    import com.game.view.game_filed.grids.grid_items.item.test.SampleGridItemView;

    import flash.display.DisplayObject;
import flash.geom.Point;

public class GridView extends BaseGridView {

        public function GridView(type: String, itemWidth: int, itemHeight: int, itemSpacing: Point) {
            super(type, itemWidth, itemHeight, itemSpacing);
        }


        public function get selectedItems(): Array {
            var res: Array = [];

            for (var i: int = 0; i < _items.length; i++) {
                var item: GridItemView = _items[i] as GridItemView;
                if (item.isSelected) {
                    res.push(item);
                }
            }

            return res;
        }


        override public function createNewGridItem(gridModel: BaseGridModel, row: int, col: int): BaseGridItemView {
            var item: GridItemView = super.createNewGridItem(gridModel, row, col) as GridItemView;
            var itemModel: GridItemModel = gridModel.getGridItem(row, col) as GridItemModel;

            item.id = itemModel.id;

            return item;
        }


        public function getItemViewByID(id: uint): GridItemView {
            for (var i: int = 0; i < _items.length; i++) {
                var item: GridItemView = _items[i] as GridItemView;
                if (item.id == id) {
                    return item;
                }
            }

            return null;
        }


        public function getItemViewByView(view: DisplayObject): GridItemView {
            for (var i: int = 0; i < _items.length; i++) {
                var item: GridItemView = _items[i] as GridItemView;
                if (item.view == view) {
                    return item;
                }
            }

            return null;
        }


        override protected function createItemViewInstance(): BaseGridItemView {
            return new SampleGridItemView();
        }

    }
}
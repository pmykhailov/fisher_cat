package com.game.view.game_filed.grids.grid_items.item.test  {
import com.game.view.game_filed.grids.grid_items.item.base.GridItemView;
import com.greensock.TweenLite;

import flash.display.DisplayObject;

import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;

import org.flintparticles.common.actions.Age;

import org.flintparticles.common.actions.Fade;
import org.flintparticles.common.counters.Blast;

import org.flintparticles.common.counters.Steady;
import org.flintparticles.common.displayObjects.Dot;
import org.flintparticles.common.events.EmitterEvent;
import org.flintparticles.common.initializers.ColorsInit;
import org.flintparticles.common.initializers.ImageClass;
import org.flintparticles.common.initializers.Lifetime;
import org.flintparticles.twoD.actions.Move;
import org.flintparticles.twoD.emitters.Emitter2D;
import org.flintparticles.twoD.initializers.Position;
import org.flintparticles.twoD.initializers.Velocity;
import org.flintparticles.twoD.renderers.DisplayObjectRenderer;
import org.flintparticles.twoD.zones.DiscZone;
import org.flintparticles.twoD.zones.PointZone;

public class SampleGridItemView extends GridItemView {

    private const MATCH_ANIMATION_COMPLETE:String = "matchAnimationComplete";

    private var _selection:Sprite;
    private var _over:MovieClip;
    private var _match:MovieClip;
    private var _selected:MovieClip;

    public function SampleGridItemView() {
        super();
    }

    override public function set type(value:int):void {
        super.type = value;

        _currentView.gotoAndStop(value);

        _over = _view["_over"];
        _selected = _view["_selected"];
        _match = _view["_match"];

        _over && _over.stop();
        _selected && _selected.stop();
        _match.stop();
        _match.visible = false;
    }

    override public function set isSelected(value:Boolean):void {
        super.isSelected = value;

        _isSelected ? (_selected && _selected.gotoAndPlay(1)) : (_selected && _selected.gotoAndStop(1));

        _selection.visible = _isSelected;
    }

    private function get _currentView():MovieClip {
        return _view as MovieClip;
    }

    override protected function initView():void {
        super.initView();

        _selection = _view["_selection"];
        _selected = _view["_selected"];
        _over = _view["_over"];
        _match = _view["_match"];
    }

    override protected function initElements():void {
        super.initElements();
        _selection.visible = false;
    }

    override public function runDeleteAnimation():void {
        _selected && (_selected.visible = false);

        _match.visible = true;
        _match.addEventListener(MATCH_ANIMATION_COMPLETE, onMatchAnimationComplete, false, 0, true);
        _match.gotoAndPlay(1);
    }

    private function onMatchAnimationComplete(event:Event):void {
        _match.removeEventListener(MATCH_ANIMATION_COMPLETE, onMatchAnimationComplete);
        notifyDeleteAnimationComplete();
    }

    /*
    Test delete animation
    override public function runDeleteAnimation():void {
        var emitter:Emitter2D = new Emitter2D();

        emitter.counter = new Blast(30);

        emitter.addInitializer(new ImageClass(Dot, [4]));
        emitter.addInitializer(new ColorsInit([0xFFFF0000, 0xFF00FF00, 0xFF0000FF, 0xFFFFFF00, 0xFF00FFFF]));
        emitter.addInitializer(new Lifetime(0.2, 1.5));
        emitter.addInitializer(new Position(new PointZone(new Point())));
        emitter.addInitializer(new Velocity(new DiscZone(new Point(), 50, 30)));

        emitter.addAction(new Move());
        emitter.addAction(new Age());
        emitter.addAction(new Fade());

        var renderer:DisplayObjectRenderer = new DisplayObjectRenderer();
        renderer.addEmitter(emitter);
        addChild(renderer);

        emitter.addEventListener(EmitterEvent.EMITTER_EMPTY, onEmitterEmptyHandler);
        emitter.start();

        for (var i:int = 0; i < _currentView.numChildren; i++) {
            var child:DisplayObject = _currentView.getChildAt(i);

            if (child != renderer)
            {
                TweenLite.to(child, 0.3, {alpha:0});
            }
        }
    }

    private function onEmitterEmptyHandler(event:EmitterEvent):void {
        event.currentTarget.removeEventListener(EmitterEvent.EMITTER_EMPTY, onEmitterEmptyHandler);
        notifyDeleteAnimationComplete();
    }
    */

    override protected function onMouseOverHandler(event:MouseEvent):void {
        _over && _over.gotoAndPlay(1);
    }

    override protected function onMouseOutHandler(event:MouseEvent):void {
        _over && _over.gotoAndStop(1);
    }
}
}
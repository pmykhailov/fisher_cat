package com.game.view.game_filed.grids.grid_background {
    import com.game.view.game_filed.grids.base.BaseGridView;
    import com.game.view.game_filed.grids.base.item.BaseGridItemView;
    import com.game.view.game_filed.grids.grid_background.item.GridBackgroundItemView;

import flash.geom.Point;

public class GridBackgroundView extends BaseGridView {

        public function GridBackgroundView(type: String, itemWidth: int, itemHeight: int, itemSpacing: Point) {
            super(type, itemWidth, itemHeight, itemSpacing);
        }


        override protected function createItemViewInstance(): BaseGridItemView {
            return new GridBackgroundItemView();
        }
    }
}
package com.game.view.game_filed.grids.grid_background.item {
    import com.game.view.game_filed.grids.base.item.BaseGridItemView;

    import flash.display.MovieClip;

import model.grids.items.GridBackgroundItemEnum;

public class GridBackgroundItemView extends BaseGridItemView {

        public function GridBackgroundItemView() {
            super("View_GridBackgroundItem");
        }

        override public function set type(value: int): void {
            super.type = value;

            if (value == GridBackgroundItemEnum.WITH_BACKGROUND) {
                (_view as MovieClip).gotoAndStop(1);
            }else {
                (_view as MovieClip).gotoAndStop(2);
            }

        }


    }
}
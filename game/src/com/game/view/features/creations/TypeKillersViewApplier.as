package com.game.view.features.creations {
    import com.game.model.GameModel;
    import com.game.model.features.creations.row_col_killer.group.BaseRowColKillerGridItemModel;
    import com.game.model.features.creations.type_killer.group.TypeKillerGridItemModel;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.logic.changed_grid_items_groups.groups.base.ChangedGridItemsGroup;
    import com.game.view.game.GameView;
    import com.game.view.game_filed.grids.grid_items.GridView;
    import com.game.view.game_filed.grids.grid_items.changed_grid_items_visualiser.base.BaseChangedGridItemsViewApplier;
    import com.game.view.game_filed.grids.grid_items.item.base.GridItemView;

    import flash.filters.BlurFilter;

    import flash.filters.GlowFilter;

    import model.grids.enum.GridTypeEnum;

    /**
     * RowColKillerKilledGridItemsAnimationController class.
     * User: Paul Makarenko
     * Date: 10.01.2015
     */
    public class TypeKillersViewApplier extends BaseChangedGridItemsViewApplier {

        private var _gridView: GridView;


        public function TypeKillersViewApplier(gameModel: GameModel, gameView: GameView) {
            super(gameModel, gameView);
        }


        private function get gridView(): GridView {
            if (!_gridView) {
                _gridView = _gameView.getGridViewByType(GridTypeEnum.TYPE_MAIN) as GridView;
            }
            return _gridView;
        }


        override public function startAnimation(group: ChangedGridItemsGroup): void {

            var gridModel: GridModel = _gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;

            for (var j: int = 0; j < group.gridItems.length; j++) {
                var itemModel: GridItemModel = group.gridItems[j] as GridItemModel;
                var itemView: GridItemView = gridView.getItemViewByID(itemModel.id) as GridItemView;

                if (itemModel is TypeKillerGridItemModel) {
                    itemView.view.filters = [new BlurFilter(10,10)];
                }
            }

            notifyAnimationComplete();
        }

    }
}
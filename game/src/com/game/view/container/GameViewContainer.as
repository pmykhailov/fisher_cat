package com.game.view.container {
    import flash.display.Sprite;

    /**
     * GameViewContainer class.
     *
     * Here game view instance will be added
     *
     * User: Paul Makarenko
     * Date: 20.04.14
     */
    public class GameViewContainer extends Sprite {

        public function GameViewContainer() {
            super();
        }

    }
}
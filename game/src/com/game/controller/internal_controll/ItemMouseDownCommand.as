package com.game.controller.internal_controll {
    import com.game.events.GameContextEvent;
    import com.game.model.GameModel;

    import model.grids.enum.GridTypeEnum;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.view.events.GameViewEvent;
    import com.game.view.game.GameView;
    import com.game.view.game_filed.grids.grid_items.GridView;
    import com.game.view.game_filed.grids.grid_items.item.base.GridItemView;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class ItemMouseDownCommand extends Command {

        [Inject]
        public var event: GameViewEvent;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function ItemMouseDownCommand() {
            super();
        }


        override public function execute(): void {
            var selectedItem: GridItemView = event.data as GridItemView;
            var gridView: GridView = gameView.getGridViewByType(GridTypeEnum.TYPE_MAIN) as GridView;
            var sItems: Array = gridView.selectedItems;

            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEM_SELECT);

            if (sItems.length == 2) {
                var item1: GridItemView = sItems[0] as GridItemView;
                var item2: GridItemView = sItems[1] as GridItemView;

                if (!isNeighbors(item1, item2)) {
                    item1.isSelected = false;
                    item2.isSelected = false;

                    selectedItem.isSelected = true;
                    return;
                }
                else {
                    dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.NEIGHBOR_ITEMS_SELECTED, {item1: item1, item2: item2}));
                }
            }
            else if (sItems.length > 0) {
                dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.START_ITEM_MOUSE_OVER_LISTENING));
            }

        }


        private function isNeighbors(item1: GridItemView, item2: GridItemView): Boolean {
            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var itemModel1: GridItemModel = gridModel.getGridItemModelByID(item1.id);
            var itemModel2: GridItemModel = gridModel.getGridItemModelByID(item2.id);

            return gameModel.logicFacade.logicInfo.isNeighbors(gameModel, itemModel1, itemModel2);
        }
    }
}

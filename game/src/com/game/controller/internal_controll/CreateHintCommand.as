package com.game.controller.internal_controll {
    import by.lord_xaoca.helpers.StageReference;

    import com.game.model.GameModel;
    import com.game.model.vo.SwapItemsVO;
    import com.game.view.game.GameView;
import com.game.view.game_filed.grids.grid_items.GridView;

import flash.display.DisplayObject;
    import flash.events.KeyboardEvent;
    import flash.events.TimerEvent;
    import flash.filters.GlowFilter;
    import flash.ui.Keyboard;
    import flash.utils.Timer;

import model.grids.enum.GridTypeEnum;

import robotlegs.bender.bundles.mvcs.Command;

    public class CreateHintCommand extends Command {

        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var gameView: GameView;


        public function CreateHintCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            StageReference.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
        }


        private function onKeyDownHandler(event: KeyboardEvent): void {
            if (event.keyCode == Keyboard.SPACE) {
                var swapModel: SwapItemsVO = gameModel.logicFacade.logicInfo.getAnyItemsSwapThatMakesWinCombination(gameModel);
                var gridView: GridView = gameView.getGridViewByType(GridTypeEnum.TYPE_MAIN) as GridView;

                if (swapModel) {

                    var itemView1: DisplayObject = gridView.getItemViewByID(swapModel.item1.id).view;
                    var itemView2: DisplayObject = gridView.getItemViewByID(swapModel.item2.id).view;

                    itemView1.filters = [new GlowFilter()];
                    itemView2.filters = [new GlowFilter()];

                    var timer: Timer = new Timer(500);
                    timer.addEventListener(TimerEvent.TIMER, function (event: TimerEvent): void {
                        event.currentTarget.removeEventListener(TimerEvent.TIMER, arguments.callee);
                        if (itemView1) itemView1.filters = [];
                        if (itemView2) itemView2.filters = [];
                    });
                    timer.start();
                }
            }
        }
    }
}

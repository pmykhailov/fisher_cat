package com.game.controller.internal_controll {
    import com.game.view.game.GameView;

import model.grids.enum.GridTypeEnum;

import robotlegs.bender.bundles.mvcs.Command;

    /**
     * LockGameWhileAnimationsCommand class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class LockGameWhilePlayingAnimationsCommand extends Command {

        [Inject]
        public var gameView: GameView;


        public function LockGameWhilePlayingAnimationsCommand() {
            super();
        }


        override public function execute(): void {
            gameView.getGridViewByType(GridTypeEnum.TYPE_MAIN).enable = false;
        }
    }
}
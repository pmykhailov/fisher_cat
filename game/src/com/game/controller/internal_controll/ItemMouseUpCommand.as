package com.game.controller.internal_controll {
    import com.game.events.GameContextEvent;
    import com.game.view.events.GameViewEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    public class ItemMouseUpCommand extends Command {

        [Inject]
        public var event: GameViewEvent;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function ItemMouseUpCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.STOP_ITEM_MOUSE_OVER_LISTENING));
        }
    }
}

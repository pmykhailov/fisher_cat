package com.game.controller.internal_controll {
    import com.game.events.GameContextEvent;
    import com.game.model.GameModel;

    import model.grids.enum.GridTypeEnum;

    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.view.events.GameViewEvent;
    import com.game.view.game.GameView;
    import com.game.view.game_filed.grids.grid_items.GridView;
    import com.game.view.game_filed.grids.grid_items.item.base.GridItemView;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    public class ItemMouseOverCommand extends Command {

        [Inject]
        public var event: GameViewEvent;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function ItemMouseOverCommand() {
            super();
        }


        override public function execute(): void {

            var gridView: GridView = gameView.getGridViewByType(GridTypeEnum.TYPE_MAIN) as GridView;
            var overedItem: GridItemView = event.data as GridItemView;
            var selectedItem: GridItemView = gridView.selectedItems[0] as GridItemView;

            if (selectedItem && overedItem && selectedItem != overedItem && isNeighbors(selectedItem, overedItem)) {
                dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.STOP_ITEM_MOUSE_OVER_LISTENING));

                overedItem.isSelected = true;

                dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.NEIGHBOR_ITEMS_SELECTED, {item1: selectedItem, item2: overedItem}));
            }
        }


        private function isNeighbors(item1: GridItemView, item2: GridItemView): Boolean {
            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var itemModel1: GridItemModel = gridModel.getGridItemModelByID(item1.id);
            var itemModel2: GridItemModel = gridModel.getGridItemModelByID(item2.id);

            return gameModel.logicFacade.logicInfo.isNeighbors(gameModel, itemModel1, itemModel2);
        }
    }
}

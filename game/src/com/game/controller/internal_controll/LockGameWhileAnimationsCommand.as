package com.game.controller.internal_controll {
    import com.game.view.game.GameView;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * LockGameWhileAnimationsCommand class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class LockGameWhileAnimationsCommand extends Command {

        [Inject]
        public var gameView:GameView;

        public function LockGameWhileAnimationsCommand() {
            super();
        }


        override public function execute(): void {
            gameView.gridView.enable = false;
        }
    }
}
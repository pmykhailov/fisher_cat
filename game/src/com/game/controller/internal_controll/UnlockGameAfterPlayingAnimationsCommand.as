package com.game.controller.internal_controll {
    import com.game.view.game.GameView;

import model.grids.enum.GridTypeEnum;

import robotlegs.bender.bundles.mvcs.Command;

    /**
     * LockGameWhileAnimationsCommand class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class UnlockGameAfterPlayingAnimationsCommand extends Command {

        [Inject]
        public var gameView: GameView;


        public function UnlockGameAfterPlayingAnimationsCommand() {
            super();
        }


        override public function execute(): void {
            gameView.getGridViewByType(GridTypeEnum.TYPE_MAIN).enable = true;
        }
    }
}
package com.game.controller.internal_controll {
    import com.game.events.GameContextEvent;
    import com.game.model.GameModel;
    import com.game.model.vo.SwapItemsVO;

    import model.grids.enum.GridTypeEnum;

    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import model.level.settings.GameSettings;
    import com.game.view.game.GameView;
    import com.game.view.game_filed.grids.grid_items.item.base.GridItemView;
    import com.greensock.TimelineLite;
    import com.greensock.TweenLite;
    import com.greensock.easing.Linear;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class SwapItemsCommand extends Command {

        [Inject]
        public var event: GameContextEvent;
        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var dispatcher: IEventDispatcher;
        private var _item1: GridItemView;
        private var _item2: GridItemView;
        /** Flag that defines if it's swapping triggered by user or swapping back then no win situations found after user's swapping**/
        private var _isSwapBack: Boolean;


        public function SwapItemsCommand() {
            super();
        }


        override public function execute(): void {
            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;

            _item1 = event.data.item1 as GridItemView;
            _item2 = event.data.item2 as GridItemView;
            _isSwapBack = event.data.hasOwnProperty("isSwapBack") && event.data.isSwapBack;

            // Change model
            gameModel.logicFacade.logicGameFlow.swapItems(gridModel, _item1.id, _item2.id);

            // Change view
            swapItems();
        }


        public function swapItems(): void {
            var timeline: TimelineLite = new TimelineLite({onComplete: onSwapAnimationComplete});
            var time: Number = gameModel.gameSettings.timeSettings.swap_items_time;

            timeline.insert(TweenLite.to(_item1, time, {x: _item2.x, y: _item2.y, ease: Linear.easeNone}));
            timeline.insert(TweenLite.to(_item2, time, {x: _item1.x, y: _item1.y, ease: Linear.easeNone}));
        }


        private function onSwapAnimationComplete(): void {
            var tmp: int;

            // Swap items properties
            tmp = _item1.row;
            _item1.row = _item2.row;
            _item2.row = tmp;
            tmp = _item1.col;
            _item1.col = _item2.col;
            _item2.col = tmp;

            _item1.isSelected = false;
            _item2.isSelected = false;

            if (!_isSwapBack) {
                nextAction();
            } else {
                //dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.UNLOCK_GAME_AFTER_ANIMATIONS))
            }
        }


        private function nextAction(): void {
            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var gridItemModel1: GridItemModel = gridModel.getGridItemModelByID(_item1.id);
            var gridItemModel2: GridItemModel = gridModel.getGridItemModelByID(_item2.id);

            var winCombinations: Array;

            gameModel.logicFacade.logicGameFlow.wave = 1;
            winCombinations = gameModel.logicFacade.logicInfo.getGridItemsChanges(gameModel, new SwapItemsVO(gridItemModel1, gridItemModel2));

            if (winCombinations.length == 0) {
                gameModel.logicFacade.logicGameFlow.resetWave();
                SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEM_UNLUCKY_SWAP);
                dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.SWAP_ITEMS, {item1: _item1, item2: _item2, isSwapBack: true}));
            }
            else {
                dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.DELETE_ITEMS, winCombinations));
            }
        }

    }
}
package com.game.controller.internal_controll {
    import com.game.events.GameContextEvent;
    import com.game.view.game.GameView;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    public class NeighborItemsSelectedCommand extends Command {

        [Inject]
        public var gameView: GameView;
        [Inject]
        public var event: GameContextEvent;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function NeighborItemsSelectedCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.SWAP_ITEMS, event.data))
        }
    }
}

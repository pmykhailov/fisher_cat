﻿package com.game.controller.internal_controll.spawn_items {
    import com.game.view.game_filed.grids.grid_items.item.base.GridItemView;
    import com.greensock.TweenLite;
    import com.greensock.easing.Bounce;
    import com.greensock.easing.Linear;

    import flash.geom.Point;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class DefaultSpawnItemsCommand extends BaseSpawnItemsCommand {

        public function DefaultSpawnItemsCommand() {
            super();
        }

        override protected function changeView():void {
            defaultChangeViewStrategy();
        }

        override protected function animateExistingItems():void {
            var existingItemsArray: Array = existingItems;

            for (var i:int = 0; i < existingItemsArray.length; i++) {
                gameView.spawnTimeline.insert(getExistingItemTween(existingItemsArray[i] as GridItemView, gameModel.gameSettings.timeSettings.existing_items_fall_time));
            }
        }

        override protected function animateNewItems():void {
            var newItems: Array = createNewGridItemViews();

            newItems.sortOn(["row", "col"], Array.DESCENDING | Array.NUMERIC);

            for (var i:int = 0; i < newItems.length; i++) {
                var item: GridItemView = newItems[i] as GridItemView;

                item.y = -item.y - gridView.itemWidth;
                item.alpha = 0;

                gameView.spawnTimeline.append(getNewItemTween(newItems[i] as GridItemView), -0.05);
            }
        }


        private function getExistingItemTween(item: GridItemView, time: Number): TweenLite {
            var coordinates: Point = gridView.getItemDesiredCoordinates(item.row, item.col);
            disableItem(item);
            return new TweenLite(item, time, {x: coordinates.x, y: coordinates.y, ease: Bounce.easeOut, onComplete: onExistingItemTweenComplete, onCompleteParams: [item]});
        }


        private function getNewItemTween(item: GridItemView): TweenLite {
            var coordinates: Point = gridView.getItemDesiredCoordinates(item.row, item.col);
            disableItem(item);

            return new TweenLite(item, gameModel.gameSettings.timeSettings.new_items_fall_time, {x: coordinates.x, y: coordinates.y, alpha: 1, ease: Linear.easeNone, onComplete: onNewItemTweenComplete, onCompleteParams: [item]});
        }


        private function onNewItemTweenComplete(item: GridItemView): void {
            enableItem(item);
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEM_FALL);
        }


        private function onExistingItemTweenComplete(item: GridItemView): void {
            enableItem(item);
        }


    }
}
package com.game.controller.internal_controll.spawn_items {
    import com.game.view.game_filed.grids.grid_items.item.base.GridItemView;
    import com.greensock.TimelineLite;
    import com.greensock.TweenLite;
    import com.greensock.easing.Cubic;
    import com.greensock.easing.Linear;

    import flash.geom.Point;

    public class SpringSpawnItemsCommand extends BaseSpawnItemsCommand {
        public function SpringSpawnItemsCommand() {
            super();
        }


        override protected function changeView(): void {
            defaultChangeViewStrategy();
        }


        override protected function animateExistingItems(): void {
            addItemsTimeline(existingItems);
        }


        override protected function animateNewItems(): void {

            var newItems: Array = createNewGridItemViews();

            for (var i: int = 0; i < newItems.length; i++) {
                var item: GridItemView = newItems[i] as GridItemView;

                item.y = -item.y - gridView.itemWidth;
                item.alpha = 0;
            }

            addItemsTimeline(newItems);
        }


        private function addItemsTimeline(items:Array):void
        {
            var timeline: TimelineLite = new TimelineLite();


            items.sortOn(["row", "col"], [Array.DESCENDING | Array.NUMERIC, Array.NUMERIC]);

            for (var i: int = 0; i < items.length; i++) {
                timeline.insert(getItemTimeline(items[i] as GridItemView, i, items.length));
            }

            gameView.spawnTimeline.append(timeline);
        }


        private function getItemTimeline(item: GridItemView, index:int, maxIndex:int): TimelineLite {
            var timeline: TimelineLite = new TimelineLite();
            var coordinates: Point = gridView.getItemDesiredCoordinates(item.row, item.col);

            disableItem(item);

            // item.alpha > 0 case for existing items

            var k:Number = 1;
            var kk : int = 1;

            if (maxIndex <= 3) k = 1.5;

            timeline.delay = (index/maxIndex) * (item.alpha > 0 ? 0.4*kk/k : 0.2*kk);

            if (item.alpha > 0) {
                timeline.append(new TweenLite(item, 0.05*kk, {y: item.y - 10, ease:Linear.easeNone}));
                timeline.append(new TweenLite(item, 0.06*kk, {scaleY: 0.6, y: item.y - 20, ease:Linear.easeNone}));
                timeline.append(new TweenLite(item, 0.04*kk, {scaleY: 1, y: item.y + 10, ease:Linear.easeNone}));
            }

            timeline.append(new TweenLite(item, 0.2*kk,  {scaleY: 1, y: coordinates.y + 10, ease:Linear.easeNone, alpha:1}));
            timeline.append(new TweenLite(item, 0.05*kk, {scaleY: 1, y: coordinates.y, onComplete: onItemTweenComplete, onCompleteParams: [item], ease:Linear.easeNone}));

            return timeline;
        }


        private function onItemTweenComplete(item: GridItemView): void {
            var coordinates: Point = gridView.getItemDesiredCoordinates(item.row, item.col);

            enableItem(item);
            onItemUpdate(item);

            //SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEM_FALL);

            item.y = coordinates.y;
        }


        private function onItemUpdate(item: GridItemView): void {
            var coordinates: Point = gridView.getItemDesiredCoordinates(item.row, item.col);

            item.view.y = coordinates.y + (gameModel.gameSettings.itemHeight - gameModel.gameSettings.itemHeight * item.scaleY) / 2;
        }
    }
}

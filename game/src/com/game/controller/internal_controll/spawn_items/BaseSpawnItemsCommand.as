package com.game.controller.internal_controll.spawn_items {
import com.game.events.GameContextEvent;
import com.game.model.GameModel;
import com.game.model.grids.grid_items.GridModel;
import com.game.model.grids.grid_items.item.GridItemModel;
import com.game.model.grids.grid_items.item.GridItemPosition;
import com.game.view.game.GameView;
import com.game.view.game_filed.grids.grid_items.GridView;
import com.game.view.game_filed.grids.grid_items.item.base.GridItemView;
import com.greensock.TimelineLite;

import flash.display.DisplayObject;

import flash.events.IEventDispatcher;
import flash.geom.Point;

import model.grids.enum.GridTypeEnum;

import robotlegs.bender.bundles.mvcs.Command;

public class BaseSpawnItemsCommand extends Command {

    [Inject]
    public var event: GameContextEvent;
    [Inject]
    public var gameModel: GameModel;
    [Inject]
    public var gameView: GameView;
    [Inject]
    public var dispatcher: IEventDispatcher;

    private var _gridView: GridView;

    private var _gridModel: GridModel;

    public function BaseSpawnItemsCommand() {
        super();
    }

    protected function get gridModel(): GridModel
    {
        if (!_gridModel) {
            _gridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
        }

        return _gridModel;
    }

    protected function get gridView(): GridView {
        if (!_gridView) {
            _gridView = gameView.getGridViewByType(GridTypeEnum.TYPE_MAIN) as GridView;
        }

        return _gridView;
    }

    override public function execute(): void {
        changeModel();
        changeView();
    }

    /**
     * Generate new items in model
     */
    protected function changeModel():void {
        gameModel.logicFacade.logicGameFlow.spawnItems(gameModel);
    }

    /**
     * Present model changes in view
     */
    protected function changeView():void {

        defaultChangeViewStrategy();

        // Call animations complete here cuz all items will not be animated
        // and they will be setted with new coordinates instantly
        onSpawnItemsAnimationComplete();
    }

    protected function defaultChangeViewStrategy(): void {

        stopCurrentSpawnAnimation();

        initSpawnAnimationsTimeline();

        animateExistingItems();

        animateNewItems();

    }

    /**
     * Stop current spawn animation and
     * set to items state that should be at the end of this animation
     */
    protected function stopCurrentSpawnAnimation():void
    {
        var itemViewView: DisplayObject;
        var itemView: GridItemView;
        var i:int;

        if (gameView.spawnTimeline) {
            gameView.spawnTimeline.stop();
            gameView.spawnTimeline.clear();

            for (i = 0; i < gridView.numChildren; i++) {
                itemViewView = gridView.getChildAt(i);
                itemView = gridView.getItemViewByView(itemViewView);

                var coordinates: Point = gridView.getItemDesiredCoordinates(itemView.row, itemView.col);
                itemView.x = coordinates.x;
                itemView.y = coordinates.y;
                itemView.alpha = 1;

                enableItem(itemView);
            }
        }
    }

    protected function initSpawnAnimationsTimeline(): void {
        gameView.isSpawningItems = true;
        gameView.spawnTimeline = new TimelineLite({onComplete: onSpawnItemsAnimationComplete});
    }

    /**
     * Make some animations with existing items.
     * E.g. they can
     * - fall down
     * - go up like bubbles
     */
    protected function animateExistingItems(): void {

        var existingItemsArray: Array = existingItems;

        for (var i:int = 0; i < existingItemsArray.length; i++) {
            var item: GridItemView = existingItemsArray[i] as GridItemView;
            var coordinates: Point = gridView.getItemDesiredCoordinates(item.row, item.col);

            // Some animations item properties change can be applied
            // Disable mouse click at the beginning of animation
            // disableItem(item);
            item.x = coordinates.x;
            item.y = coordinates.y;
            // Enable mouse click at the end of animation
            // enableItem(item);
        }
    }

    /**
     * Make some animations with new items.
     */
    protected function animateNewItems(): void {

        createNewGridItemViews();
        /*
        var newItems: Array = createNewGridItemViews();
        for (var i:int = 0; i < newItems.length; i++) {
            var item: GridItemView = newItems[i] as GridItemView;
            var coordinates: Point = gridView.getItemDesiredCoordinates(item.row, item.col);

            // Some animations item properties change can be applied
            // Disable mouse click at the beginning of animation
            // disableItem(item)
            item.x = coordinates.x;
            item.y = coordinates.y;
            // Enable mouse click at the end of animation
            // enableItem(item);
        }
        */
    }


    protected function onSpawnItemsAnimationComplete(): void {
        gameView.spawnTimeline = null;
        gameView.isSpawningItems = false;

        notifySpawnItemsComplete();
    }

    /**
     * Returns array of GridItemView's that will move because other
     * matched items were deleted
     */
    protected function get existingItems(): Array /*GridItemView*/ {
        var res: Array = [];
        var itemViewView: DisplayObject;
        var itemView: GridItemView;
        // Maybe will be needed
        //var minRowDifferense: int = gridModel.rows;

        for (var i:int = 0; i < gridView.numChildren; i++) {
            itemViewView = gridView.getChildAt(i);
            itemView = gridView.getItemViewByView(itemViewView);

            if (itemView) {
                var itemPosition: GridItemPosition = gridModel.getGridItemPositionByID(itemView.id);

                // If item's view is differ from model than we gonna move it to new position
                if (itemPosition.row != itemView.row || itemPosition.col != itemView.col) {
                    /*
                     if (Math.abs(itemView.row - itemPosition.row) < minRowDifferense) {
                     minRowDifferense = Math.abs(itemView.row - itemPosition.row);
                     }
                     */

                    itemView.row = itemPosition.row;
                    itemView.col = itemPosition.col;

                    res.push(itemView);
                }
            }
        }

        return res;
    }

    /**
     * Create GridItemView's for new items that now exists in model
     */
    protected function createNewGridItemViews(): Array /*GridItemView*/{
        var res: Array = [];
        var itemModel: GridItemModel;
        for (var i:int = 0; i < gridModel.rows; i++) {
            for (var j:int = 0; j < gridModel.cols; j++) {
                itemModel = gridModel.getGridItem(i, j) as GridItemModel;

                // If we found GridItemModel that is not represented in GridView
                if (!gridView.getItemViewByID(itemModel.id)) {
                    var item: GridItemView = gridView.createNewGridItem(gridModel, i, j) as GridItemView;

                    gridView.addGridItem(item);

                    res.push(item);
                }
            }
        }

        return res;
    }


    protected function disableItem(item: GridItemView): void {
        item.view.mouseEnabled = false;
    }

    protected function enableItem(item: GridItemView): void {
        item.view.mouseEnabled = true;
    }

    /**
     * Call this function on spawn items complete.
     * Next command will be executed
     */
    protected function notifySpawnItemsComplete(): void {
        dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.SPAWN_ITEMS_COMPLETE));
    }

}
}

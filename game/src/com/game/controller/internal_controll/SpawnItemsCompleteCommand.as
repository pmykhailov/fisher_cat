package com.game.controller.internal_controll {
import com.game.events.GameContextEvent;
import com.game.model.GameModel;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

public class SpawnItemsCompleteCommand extends Command {

    [Inject]
    public var gameModel: GameModel;
    [Inject]
    public var dispatcher: IEventDispatcher;

    public function SpawnItemsCompleteCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        // Check chain reaction
        var winCombinations: Array;

        gameModel.logicFacade.logicGameFlow.wave++;

        winCombinations = gameModel.logicFacade.logicInfo.getGridItemsChanges(gameModel);

        if (winCombinations.length > 0) {
            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.DELETE_ITEMS, winCombinations));
        }else {
            gameModel.logicFacade.logicGameFlow.resetWave();
        }

    }
}
}

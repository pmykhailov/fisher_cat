﻿package com.game.controller.internal_controll {
    import com.game.events.GameContextEvent;
    import com.game.events.GameExternalEvent;
    import com.game.model.GameModel;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.grids.grid_items.item.GridItemPosition;
    import com.game.model.logic.changed_grid_items_groups.appliers.IChangedGridItemsGroupModelApplier;
    import com.game.model.logic.changed_grid_items_groups.groups.base.ChangedGridItemsGroup;
    import com.game.model.logic.changed_grid_items_groups.groups.decorated.ChangedGridItemsGroupByModelDecoration;
    import com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup;
    import com.game.model.vo.ChangedGridItemsGroupApplierVO;
    import com.game.view.game.GameView;
    import com.game.view.game_filed.grids.grid_background.GridBackgroundView;
    import com.game.view.game_filed.grids.grid_items.GridView;
    import com.game.view.game_filed.grids.grid_items.changed_grid_items_visualiser.base.BaseChangedGridItemsViewApplier;

    import flash.events.Event;
    import flash.events.IEventDispatcher;

    import model.grids.enum.GridTypeEnum;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * Actions with win combinations
     */
    public class ChangeItemsCommand extends Command {

        [Inject]
        public var event: GameContextEvent;
        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [ArrayElementType("com.game.model.vo.ChangedGridItemsGroupApplierVO")]
        private var _groupsAppliersVOs: Array;
        private var _gridItemsBackgroundsPositions: Array;
        private var _gridView: GridView;
        private var _gridBackgroundView: GridBackgroundView;

        private var _spawnItemsRigthAfterDeleteAnimationsStart: Boolean = false;
        private var _completedGroupsAnimations: int;


        public function ChangeItemsCommand() {
            super();
        }


        private function get gridView(): GridView {
            if (!_gridView) {
                _gridView = gameView.getGridViewByType(GridTypeEnum.TYPE_MAIN) as GridView;
            }
            return _gridView;
        }


        private function get gridBackgroundView(): GridBackgroundView {
            if (!_gridBackgroundView) {
                _gridBackgroundView = gameView.getGridViewByType(GridTypeEnum.TYPE_BACKGROUND) as GridBackgroundView;
            }
            return _gridBackgroundView;
        }


        override public function execute(): void {

            _groupsAppliersVOs = event.data as Array;

            changeModel();
            changeView();

            if (_spawnItemsRigthAfterDeleteAnimationsStart) {
                dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.SPAWN_ITEMS));
            }

        }


        private function changeModel(): void {

            for (var ii: int = 0; ii < _groupsAppliersVOs.length; ii++) {
                var modelApplier: IChangedGridItemsGroupModelApplier = new _groupsAppliersVOs[ii].ModelApplierClass();

                modelApplier.applyChanges(gameModel, _groupsAppliersVOs[ii].group);
            }

            _gridItemsBackgroundsPositions = gameModel.logicFacade.logicGameFlow.deleteGridBackgroundItems(gameModel, _groupsAppliersVOs);

            dispatcher.dispatchEvent(new GameExternalEvent(GameExternalEvent.ITEMS_BACKGROUNDS_DELETED, _gridItemsBackgroundsPositions.length));

            dispatcher.dispatchEvent(new GameExternalEvent(GameExternalEvent.ITEMS_DELETED, _groupsAppliersVOs));
        }


        private function changeView(): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEMS_MATCHED);

            gameView.isDeletingItems = true;

            startGroupsAnimations();

            deleteItemsBackgrounds();
        }


        private function startGroupsAnimations(): void {
            var n: int = _groupsAppliersVOs.length;

            _completedGroupsAnimations = 0;

            for (var i: int = 0; i < n; i++) {
                var groupApplierVO: ChangedGridItemsGroupApplierVO = _groupsAppliersVOs[i] as ChangedGridItemsGroupApplierVO;

                var vierApplier: BaseChangedGridItemsViewApplier = new groupApplierVO.ViewApplierClass(gameModel, gameView);

                vierApplier.addEventListener(Event.COMPLETE, onGroupAnimationsComplete);
                vierApplier.startAnimation(groupApplierVO.group);
            }

        }


        private function onAllGroupsAnimationsComplete(): void {

            gameView.isDeletingItems = false;

            if (!_spawnItemsRigthAfterDeleteAnimationsStart) {
                dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.SPAWN_ITEMS));
            }
        }


        private function deleteItemsBackgrounds(): void {
            for (var i: int = 0; i < _gridItemsBackgroundsPositions.length; i++) {
                var gridItemPosition: GridItemPosition = _gridItemsBackgroundsPositions[i] as GridItemPosition;
                gridBackgroundView.removeGridItem(gridBackgroundView.getGridBackgroundItemView(gridItemPosition.row, gridItemPosition.col));
            }
        }


        private function onGroupAnimationsComplete(event: Event): void {
            event.target.removeEventListener(Event.COMPLETE, onGroupAnimationsComplete);

            _completedGroupsAnimations++;

            if (_completedGroupsAnimations == _groupsAppliersVOs.length) {
                onAllGroupsAnimationsComplete();
            }
        }

    }
}
package com.game.controller.external_controll {
    import com.game.events.GameControlEvent;
    import com.game.model.GameModel;
    import com.game.model.utils.IDGenerator;
    import com.game.model.vo.AddGameVO;
    import model.level.settings.GameSettings;
    import com.game.view.container.GameViewContainer;
    import com.game.view.game.GameView;

    import flash.events.Event;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.framework.api.IInjector;

import model.level.settings.GameSettings;

public class AddGameCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var event: GameControlEvent;
        [Inject]
        public var injector: IInjector;


        override public function execute(): void {

            var vo: AddGameVO = event.data as AddGameVO;

            // Model
            var gameSettings:GameSettings = vo.gameSettings ? vo.gameSettings : new GameSettings();

            IDGenerator.reset();

            injector.map(GameModel).asSingleton();
            var gameModel: GameModel = injector.getInstance(GameModel);
            gameModel.init(gameSettings);

            // View
            var gameViewContainer: GameViewContainer = new GameViewContainer();
            injector.map(GameViewContainer).toValue(gameViewContainer);

            var gameView: GameView = new GameView(gameModel);
            injector.map(GameView).toValue(gameView);

            gameViewContainer.addChild(gameView.view);
            vo.container.addChild(gameViewContainer);

            dispatcher.dispatchEvent(new Event("createHint"));
        }

    }
}

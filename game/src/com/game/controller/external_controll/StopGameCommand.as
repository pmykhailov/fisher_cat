package com.game.controller.external_controll {
    import com.game.events.GameExternalEvent;
    import com.game.view.events.GameViewEvent;
    import com.game.view.game.GameView;

    import flash.events.Event;
    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * StopGameCommand class.
     *
     * Lets wait till all animations are finished. Maybe player's last action
     * can give scores to win level. Player will appreciate that.
     *
     * User: Paul Makarenko
     * Date: 03.11.13
     */
    public class StopGameCommand extends Command {

        [Inject]
        public var gameView: GameView;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function StopGameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            gameView.mouseChildren = gameView.mouseEnabled = false;
            dispatcher.dispatchEvent(new GameViewEvent(GameViewEvent.ITEM_MOUSE_UP));

            if (gameView.isDeletingItems || gameView.isSpawningItems) {
                gameView.view.addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
            } else {
                dispatcher.dispatchEvent(new GameExternalEvent(GameExternalEvent.GAME_STOPPED));
            }
        }


        private function onEnterFrameHandler(event: Event): void {
            if (!gameView.isDeletingItems && !gameView.isSpawningItems) {
                gameView.view.removeEventListener(Event.ENTER_FRAME, onEnterFrameHandler);

                dispatcher.dispatchEvent(new GameExternalEvent(GameExternalEvent.GAME_STOPPED));
            }
        }
    }
}
package com.game.controller.external_controll {
    import com.game.events.GameContextEvent;
    import com.game.model.GameModel;
    import com.game.view.game.GameView;

    import eu.alebianco.robotlegs.utils.impl.SequenceMacro;

    import flash.events.IEventDispatcher;

import model.grids.enum.GridTypeEnum;

import robotlegs.bender.bundles.mvcs.Command;

    /**
     * RestartGameCommand class.
     * User: Paul Makarenko
     * Date: 21.04.14
     */
    public class RestartGameCommand extends SequenceMacro {

        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var dispatcher: IEventDispatcher;

        public function RestartGameCommand() {
            super();
        }


        override public function prepare(): void {
            gameView.getGridViewByType(GridTypeEnum.TYPE_MAIN).removeGridItems();
            gameView.getGridViewByType(GridTypeEnum.TYPE_BACKGROUND).removeGridItems();
            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.UNLOCK_GAME_AFTER_ANIMATIONS));

            add(ResumeGameCommand);
            add(StartGameCommand);
        }
    }
}
package com.game.controller.external_controll {
    import com.game.view.game.GameView;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * ResumeGameCommand class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class ResumeGameCommand extends Command {

        [Inject]
        public var gameView: GameView;


        public function ResumeGameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            gameView.mouseChildren = true;
            gameView.mouseEnabled = true;

            gameView.view.filters = [];
        }
    }
}
package com.game.controller.external_controll {
    import com.game.view.game.GameView;

    import flash.filters.BlurFilter;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * PauseGameCommand class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class PauseGameCommand extends Command {

        [Inject]
        public var gameView: GameView;


        public function PauseGameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            gameView.mouseChildren = false;
            gameView.mouseEnabled = false;

            gameView.view.filters = [new BlurFilter()];
        }
    }
}
package com.game.controller.external_controll {
    import com.game.events.GameControlEvent;
    import com.game.model.GameModel;
    import com.game.view.container.GameViewContainer;
    import com.game.view.game.GameView;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.framework.api.IInjector;

    /**
     * RemoveGameCommand class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class RemoveGameCommand extends Command {

        [Inject]
        public var gameView: GameView;
        [Inject]
        public var gameViewContainer: GameViewContainer;
        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var event: GameControlEvent;
        [Inject]
        public var injector: IInjector;


        public function RemoveGameCommand() {
            super();
        }


        override public function execute(): void {

            injector.unmap(GameModel);
            //TODO destroyInstance can be used. Remove after
            //profiling this if it's not needed
            // injector.destroyInstance(gameModel);

            injector.unmap(GameView);
            injector.unmap(GameViewContainer);

            gameView.destroy();
        }
    }
}
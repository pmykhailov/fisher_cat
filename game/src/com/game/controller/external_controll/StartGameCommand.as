package com.game.controller.external_controll {
    import com.game.events.GameContextEvent;
    import com.game.events.GameExternalEvent;
    import com.game.model.GameModel;
    import com.game.view.game.GameView;
import com.game.view.game_filed.grids.base.BaseGridView;
import com.game.view.game_filed.grids.grid_items.GridView;
import com.game.view.game_filed.grids.grid_items.item.base.GridItemView;
    import com.greensock.TweenLite;

    import flash.events.IEventDispatcher;

    import model.grids.enum.GridTypeEnum;
    import com.game.model.grids.grid_items.GridModel;

    import com.game.model.grids.grid_items.item.GridItemModel;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.framework.api.IContext;

    /**
     * StartGameCommand class.
     * User: Paul Makarenko
     * Date: 02.10.13
     */
    public class StartGameCommand extends Command {

        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var context: IContext;


        public function StartGameCommand() {
            super();
        }


        override public function execute(): void {

            //gameModel.logicFacade.logicGeneration.randomizeGameFiledWithoutWins(gameModel);
            gameModel.logicFacade.logicGeneration.setGameField(
                    gameModel,
                    [
                        [-2,+2,+3,+2,+4,+4,+2,-2],
                        [+5,+3,+4,+1,+1,+5,+1,+3],
                        [+1,+3,+5,+3,+3,+2,+4,+5],
                        [+3,+1,+3,+5,+4,+2,+1,+4],
                        [+4,+2,+4,+3,+1,+1,+2,+3],
                        [+1,+1,+2,+3,+5,+2,+4,+5],
                        [+2,+2,+1,+2,+3,+2,+4,+2],
                        [-2,+1,+1,+4,+2,+5,+2,-2]
                    ]
            );

            var gridSettingsVOs: Array = gameModel.gameSettings.gridSettingsVOs;
            var n: int = gridSettingsVOs.length;

            for (var i:int = 0; i < n; i++) {
                var type:String = gridSettingsVOs[i].type;

                gameView.getGridViewByType(type).createGridItems(gameModel.getGridModelByType(type));
            }

            context.detain(this);
            playAnimation();
        }


        private function playAnimation(): void {
            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.LOCK_GAME_WHILE_ANIMATIONS));

            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var gridView:GridView = gameView.getGridViewByType(GridTypeEnum.TYPE_MAIN) as GridView;

            var rows: int = gridModel.rows;
            var cols: int = gridModel.cols;

            for (var i: int = 0; i < rows; i++) {
                for (var j: int = 0; j < cols; j++) {
                    var itemModel: GridItemModel = gridModel.getGridItem(i, j) as GridItemModel;

                    var itemView: GridItemView = gridView.getItemViewByID(itemModel.id);

                    itemView.alpha = 0;
                    itemView.scaleX = 1.3;
                    itemView.scaleY = 1.3;
                    TweenLite.to(itemView, 0.2, {alpha: 1, scaleX: 1, scaleY: 1, delay: i * 0.03 + j * 0.03});
                }
            }
            TweenLite.delayedCall(rows * 0.1 + cols * 0.1, onAnimationComplete);
        }


        private function onAnimationComplete(): void {
            context.release(this);
            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.UNLOCK_GAME_AFTER_ANIMATIONS));
            dispatcher.dispatchEvent(new GameExternalEvent(GameExternalEvent.GAME_STARTED));
        }

    }
}
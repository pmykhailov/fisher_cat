package com.game.controller.external_controll {
    import com.game.events.GameControlEvent;
    import com.game.view.container.GameViewContainer;

    import robotlegs.bender.bundles.mvcs.Command;

    public class EnableGameMouseClicksCommand extends Command {

        [Inject]
        public var gameViewContainer: GameViewContainer;
        [Inject]
        public var event: GameControlEvent;


        public function EnableGameMouseClicksCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var enable: Boolean = event.data as Boolean;
            gameViewContainer.mouseChildren = gameViewContainer.mouseEnabled = enable;
        }
    }
}

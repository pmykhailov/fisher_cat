package com.game.controller.external_controll {
    import com.game.events.GameContextEvent;
    import com.game.events.GameExternalEvent;
    import com.game.model.GameModel;
    import com.game.view.game.GameView;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.framework.api.IContext;

    /**
     * FinishGameCommand class.
     * User: Paul Makarenko
     * Date: 21.04.14
     */
    public class FinishGameCommand extends Command {

        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var dispatcher:IEventDispatcher;
        [Inject]
        public var context:IContext;

        public function FinishGameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            context.detain(this);
            playAnimation();
        }

        private function playAnimation(): void {
            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.LOCK_GAME_WHILE_ANIMATIONS));
            trace("Playing some animation");
            onAnimationComplete();
        }

        private function onAnimationComplete():void {
            context.release(this);
            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.UNLOCK_GAME_AFTER_ANIMATIONS));
            dispatcher.dispatchEvent(new GameExternalEvent(GameExternalEvent.GAME_FINISHED));
        }

    }
}
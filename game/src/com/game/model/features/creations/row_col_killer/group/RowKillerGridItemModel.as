package com.game.model.features.creations.row_col_killer.group {
    import com.game.model.grids.grid_items.item.GridItemModel;

    /**
     * RowKillerGridItemsModel class.
     * User: Paul Makarenko
     * Date: 16.01.2015
     */
    public class RowKillerGridItemModel extends BaseRowColKillerGridItemModel {

        public function RowKillerGridItemModel(gridItemModel: GridItemModel) {
            super(gridItemModel);
        }

    }
}
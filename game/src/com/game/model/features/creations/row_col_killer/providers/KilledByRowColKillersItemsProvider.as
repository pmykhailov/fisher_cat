package com.game.model.features.creations.row_col_killer.providers {
    import com.game.model.GameModel;
    import com.game.model.features.creations.row_col_killer.group.BaseRowColKillerGridItemModel;
    import com.game.model.features.creations.row_col_killer.group.ColKillerGridItemModel;
    import com.game.model.features.creations.row_col_killer.group.RowKillerGridItemModel;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.grids.grid_items.item.GridItemPosition;
    import com.game.model.logic.changed_grid_items_groups.appliers.ScoreGivingGridItemsGroupModelApplier;
    import com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup;
    import com.game.model.logic.changed_grid_items_groups.providers.IChangedGridItemsGroupProvider;
    import com.game.model.vo.ActionVO;
    import com.game.model.vo.ChangedGridItemsGroupApplierVO;
    import com.game.model.vo.SwapItemsVO;
    import com.game.view.game_filed.grids.grid_items.changed_grid_items_visualiser.ScoreGivingGridItemsGroupViewApplier;

    import model.grids.enum.GridTypeEnum;
    import model.grids.items.GridItemTypeEnum;

    /**
     * KilledByRowColKillersItemsProvider class.
     * User: Paul Makarenko
     * Date: 16.01.2015
     */
    public class KilledByRowColKillersItemsProvider implements IChangedGridItemsGroupProvider {

        [ArrayElementType("com.game.model.vo.ChangedGridItemsGroupApplierVO")]
        private var _groupsAppliersVOs: Array;

        private var _rowColKillers: Array;

        private var _swapModel: SwapItemsVO;


        public function KilledByRowColKillersItemsProvider(actionModel: ActionVO, groupsAppliersVOs: Array, rowColKillers: Array) {
            _swapModel = actionModel as SwapItemsVO;
            _groupsAppliersVOs = groupsAppliersVOs;
            _rowColKillers = rowColKillers;
        }


        public function getChanges(gameModel: GameModel): Array {

            checkItemsSwap(gameModel);

            if (_rowColKillers.length > 0) {
                addRowColItems(gameModel, _rowColKillers);
            }

            return [];
        }


        /**
         * Check if it was two rowColKillers swap
         */
        private function checkItemsSwap(gameModel: GameModel): void {
            if (_swapModel && _swapModel.item1 is BaseRowColKillerGridItemModel && _swapModel.item2 is BaseRowColKillerGridItemModel) {
                addRowColItems(gameModel, [_swapModel.item1, _swapModel.item2]);
            }
        }


        private function addRowColItems(gameModel: GameModel, rowColKillers: Array): void {

            var gridModel: GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var foundedRowColKillers: Array = [];
            var gridItemModel: GridItemModel;
            var col: int;
            var row: int;
            var i: int;

            for (i = 0; i < rowColKillers.length; i++) {
                var rowColKillerModel: BaseRowColKillerGridItemModel = rowColKillers[i] as BaseRowColKillerGridItemModel;
                var rowColKillerPosition: GridItemPosition = gridModel.getGridItemPositionByID(rowColKillerModel.id);
                var gridItemModels: Array = [];

                // Kill items in row
                if (rowColKillerModel is RowKillerGridItemModel) {

                    for (col = 0; col < gridModel.cols; col++) {
                        gridItemModel = gridModel.getGridItem(rowColKillerPosition.row, col) as GridItemModel;
                        if (gridItemModel.type != GridItemTypeEnum.NON_EXISTING && !hasItemInOtherCombinations(gridItemModel)) {

                            if (gridItemModel is BaseRowColKillerGridItemModel) {
                                foundedRowColKillers.push(gridItemModel);
                            }

                            gridItemModels.push(gridItemModel);
                        }
                    }
                } else
                // Kill items in column
                if (rowColKillerModel is ColKillerGridItemModel) {
                    for (row = 0; row < gridModel.rows; row++) {
                        gridItemModel = gridModel.getGridItem(row, rowColKillerPosition.col) as GridItemModel;
                        if (gridItemModel.type != GridItemTypeEnum.NON_EXISTING && !hasItemInOtherCombinations(gridItemModel)) {
                            gridItemModels.push(gridItemModel);

                            if (gridItemModel is BaseRowColKillerGridItemModel) {
                                foundedRowColKillers.push(gridItemModel);
                            }
                        }
                    }
                }

                if (gridItemModels.length > 0) {
                    var scoreGivingGridItemsGroup: ScoreGivingGridItemsGroup = new ScoreGivingGridItemsGroup(gridItemModels);
                    var vo: ChangedGridItemsGroupApplierVO = new ChangedGridItemsGroupApplierVO(scoreGivingGridItemsGroup, ScoreGivingGridItemsGroupViewApplier, ScoreGivingGridItemsGroupModelApplier);

                    scoreGivingGridItemsGroup.scorePerItem = 60;
                    scoreGivingGridItemsGroup.showScorePerEachItem = true;

                    _groupsAppliersVOs.push(vo);
                }
            }

            // Run rowColKillers chain reaction
            if (foundedRowColKillers.length > 0) {
                addRowColItems(gameModel, foundedRowColKillers);
            }
        }


        private function hasItemInOtherCombinations(gridItemModel: GridItemModel): Boolean {
            var combinationsCount: int = _groupsAppliersVOs.length;
            var i: int;
            var j: int;

            for (i = 0; i < combinationsCount; i++) {
                var combination: ScoreGivingGridItemsGroup = _groupsAppliersVOs[i].group as ScoreGivingGridItemsGroup;
                var itemsCount: int = combination.gridItems.length;

                for (j = 0; j < itemsCount; j++) {
                    if (combination.gridItems[j].id == gridItemModel.id) {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
package com.game.model.features.creations.row_col_killer.group {
    import com.game.model.features.common.BaseGridItemModelDecorator;
    import com.game.model.grids.grid_items.item.*;

    public class BaseRowColKillerGridItemModel extends BaseGridItemModelDecorator {

        public function BaseRowColKillerGridItemModel(gridItemModel: GridItemModel) {
            super(gridItemModel);
        }

    }
}

package com.game.model.features.creations.row_col_killer.providers {
    import com.game.model.GameModel;
    import com.game.model.features.common.GridItemsDecorationModelApplier;
    import com.game.model.features.creations.row_col_killer.group.BaseRowColKillerGridItemModel;
    import com.game.model.features.creations.row_col_killer.group.ColKillerGridItemModel;
    import com.game.model.features.creations.row_col_killer.group.RowKillerGridItemModel;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.grids.grid_items.item.GridItemPosition;
    import com.game.model.logic.changed_grid_items_groups.groups.decorated.ChangedGridItemsGroupByModelDecoration;
    import com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup;
    import com.game.model.logic.changed_grid_items_groups.providers.IChangedGridItemsGroupProvider;
    import com.game.model.vo.ActionVO;
    import com.game.model.vo.ChangedGridItemsGroupApplierVO;
    import com.game.model.vo.SwapItemsVO;
    import com.game.view.features.creations.RowColKillersViewApplier;

    import model.grids.enum.GridTypeEnum;

    public class RowColKillersProvider implements IChangedGridItemsGroupProvider {

        private var _swapModel: SwapItemsVO;

        [ArrayElementType("com.game.model.vo.ChangedGridItemsGroupApplierVO")]
        private var _groupsAppliersVOs: Array;

        private var _rowColKillers: Array;


        public function RowColKillersProvider(actionModel: ActionVO, groupsAppliersVOs: Array) {
            _swapModel = actionModel as SwapItemsVO;
            _groupsAppliersVOs = groupsAppliersVOs;
        }


        public function get rowColKillers(): Array {
            return _rowColKillers;
        }


        /**
         * Search in all row and col combinations
         * - if rowColKiller can be created
         * - if rowColKiller can kill other items in a row (col). Chain reaction is supported
         */
        public function getChanges(gameModel: GameModel): Array {

            var gridModel: GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var combinationsCount: int = _groupsAppliersVOs.length;
            var res:Array = [];

            var i: int;
            var j: int;

            _rowColKillers = [];

            for (i = 0; i < combinationsCount; i++) {
                var groupsAppliersVO: ChangedGridItemsGroupApplierVO = _groupsAppliersVOs[i];

                if (groupsAppliersVO.group is ScoreGivingGridItemsGroup) {
                    var gridItemsModels: Array = groupsAppliersVO.group.gridItems;
                    var itemsCount: int = gridItemsModels.length

                    // Find all rowColKillers
                    for (j = 0; j < itemsCount; j++) {
                        if (gridItemsModels[j] is BaseRowColKillerGridItemModel) {

                            _rowColKillers.push(gridItemsModels[j]);
                        }
                    }

                    // Match of 4 items will produce RowColKiller item
                    // if there is no RowColKiller among these items
                    if (itemsCount == 4 && _rowColKillers.length == 0) {

                        var itemModel: GridItemModel;
                        var ModelClass: Class;
                        var pos1: GridItemPosition;
                        var pos2: GridItemPosition;

                        // If there was an user swap action
                        if (_swapModel) {
                            for (j = 0; j < itemsCount; j++) {
                                itemModel = gridItemsModels[j] as GridItemModel;

                                if (_swapModel.item1.id == itemModel.id || _swapModel.item2.id == itemModel.id) {

                                    pos1 = gridModel.getGridItemPositionByID(_swapModel.item1.id);
                                    pos2 = gridModel.getGridItemPositionByID(_swapModel.item2.id);

                                    ModelClass = (pos1.col == pos2.col) ? ColKillerGridItemModel : RowKillerGridItemModel;

                                    break;
                                }
                            }
                        } else
                        // if 4 items were spawned
                        {
                            itemModel = gridItemsModels[0];

                            pos1 = gridModel.getGridItemPositionByID(gridItemsModels[0].id);
                            pos2 = gridModel.getGridItemPositionByID(gridItemsModels[1].id);

                            ModelClass = (pos1.col == pos2.col) ? ColKillerGridItemModel : RowKillerGridItemModel;
                        }

                        var group: ChangedGridItemsGroupByModelDecoration = new ChangedGridItemsGroupByModelDecoration([itemModel], ModelClass);
                        var vo: ChangedGridItemsGroupApplierVO = new ChangedGridItemsGroupApplierVO(group, RowColKillersViewApplier, GridItemsDecorationModelApplier);
                        (groupsAppliersVO.group as ScoreGivingGridItemsGroup).ignoreGridItems[itemModel.id] = true;

                        res.push(vo);
                    }

                }
            }

            return res;
        }
    }
}

package com.game.model.features.creations.type_killer.providers {
    import com.game.model.GameModel;
    import com.game.model.features.creations.type_killer.group.TypeKillerGridItemModel;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.logic.changed_grid_items_groups.appliers.ScoreGivingGridItemsGroupModelApplier;
    import com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup;
    import com.game.model.logic.changed_grid_items_groups.providers.IChangedGridItemsGroupProvider;
    import com.game.model.vo.ActionVO;
    import com.game.model.vo.ChangedGridItemsGroupApplierVO;
    import com.game.model.vo.SwapItemsVO;
    import com.game.view.game_filed.grids.grid_items.changed_grid_items_visualiser.ScoreGivingGridItemsGroupViewApplier;

    import model.grids.enum.GridTypeEnum;

    /**
     * KilledByRowColKillersItemsProvider class.
     * User: Paul Makarenko
     * Date: 16.01.2015
     */
    public class KilledByTypeKillersItemsProvider implements IChangedGridItemsGroupProvider {

        //[ArrayElementType("com.game.model.vo.ChangedGridItemsGroupApplierVO")]
        //private var _groupsAppliersVOs: Array;

        private var _swapModel: SwapItemsVO;


        public function KilledByTypeKillersItemsProvider(actionModel: ActionVO/*, groupsAppliersVOs: Array*/) {
            _swapModel = actionModel as SwapItemsVO;
            //_groupsAppliersVOs = groupsAppliersVOs;
        }


        public function getChanges(gameModel: GameModel): Array {
            var gridModel: GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;

            // This win will be only in case of items swapping
            if (!_swapModel) return [];

            if (
                    (_swapModel.item1 is TypeKillerGridItemModel && gridModel.usualGridItemTypes.indexOf(_swapModel.item2.type) != -1) ||
                    (_swapModel.item2 is TypeKillerGridItemModel && gridModel.usualGridItemTypes.indexOf(_swapModel.item1.type) != -1)
                    ) {

                var usualItemModel: GridItemModel;
                var typeKillerItemModel: GridItemModel;
                var items: Array = [];

                if (_swapModel.item2 is TypeKillerGridItemModel && gridModel.usualGridItemTypes.indexOf(_swapModel.item1.type) != -1) {
                    usualItemModel = _swapModel.item1;
                    typeKillerItemModel = _swapModel.item2;
                } else {
                    usualItemModel = _swapModel.item2;
                    typeKillerItemModel = _swapModel.item1;
                }

                for (var i: int = 0; i < gridModel.rows; i++) {
                    for (var j: int = 0; j < gridModel.cols; j++) {
                        var gridItem: GridItemModel = gridModel.getGridItem(i, j) as GridItemModel;
                        if (gridItem.type == usualItemModel.type) {
                            items.push(gridItem);
                        }
                    }
                }

                items.push(typeKillerItemModel);

                var scoreGivingGridItemsGroup: ScoreGivingGridItemsGroup = new ScoreGivingGridItemsGroup(items);

                scoreGivingGridItemsGroup.showScorePerEachItem = true;
                scoreGivingGridItemsGroup.scorePerItem = 60;

                var vo: ChangedGridItemsGroupApplierVO = new ChangedGridItemsGroupApplierVO(scoreGivingGridItemsGroup, ScoreGivingGridItemsGroupViewApplier, ScoreGivingGridItemsGroupModelApplier);

                return [vo];
            }

            return [];
        }

    }
}
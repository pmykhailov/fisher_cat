package com.game.model.features.creations.type_killer.group {
    import com.game.model.features.common.BaseGridItemModelDecorator;
    import com.game.model.grids.grid_items.item.*;

    import model.grids.items.GridItemTypeEnum;

    public class TypeKillerGridItemModel extends BaseGridItemModelDecorator {

        public function TypeKillerGridItemModel(gridItemModel: GridItemModel) {
            super(gridItemModel);
        }

        override public function get type(): int {
            //TODO: Do smth
            return GridItemTypeEnum.TYPE_BOMB;
        }

    }
}

package com.game.model.features.creations.type_killer.providers {
    import com.game.model.GameModel;
    import com.game.model.features.common.GridItemsDecorationModelApplier;
    import com.game.model.features.creations.row_col_killer.group.BaseRowColKillerGridItemModel;
    import com.game.model.features.creations.row_col_killer.group.ColKillerGridItemModel;
    import com.game.model.features.creations.row_col_killer.group.RowKillerGridItemModel;
    import com.game.model.features.creations.type_killer.group.TypeKillerGridItemModel;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.grids.grid_items.item.GridItemPosition;
    import com.game.model.logic.changed_grid_items_groups.groups.decorated.ChangedGridItemsGroupByModelDecoration;
    import com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup;
    import com.game.model.logic.changed_grid_items_groups.providers.IChangedGridItemsGroupProvider;
    import com.game.model.vo.ActionVO;
    import com.game.model.vo.ChangedGridItemsGroupApplierVO;
    import com.game.model.vo.SwapItemsVO;
    import com.game.view.features.creations.RowColKillersViewApplier;
    import com.game.view.features.creations.TypeKillersViewApplier;

    import model.grids.enum.GridTypeEnum;

    public class TypeKillersProvider implements IChangedGridItemsGroupProvider {

        private var _swapModel: SwapItemsVO;

        [ArrayElementType("com.game.model.vo.ChangedGridItemsGroupApplierVO")]
        private var _groupsAppliersVOs: Array;

        public function TypeKillersProvider(actionModel: ActionVO, groupsAppliersVOs: Array) {
            _swapModel = actionModel as SwapItemsVO;
            _groupsAppliersVOs = groupsAppliersVOs;
        }


        public function getChanges(gameModel: GameModel): Array {

            var gridModel: GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var combinationsCount: int = _groupsAppliersVOs.length;
            var res: Array = [];

            var i: int;
            var j: int;

            for (i = 0; i < combinationsCount; i++) {
                var groupsAppliersVO: ChangedGridItemsGroupApplierVO = _groupsAppliersVOs[i];

                if (groupsAppliersVO.group is ScoreGivingGridItemsGroup) {
                    var gridItemsModels: Array = groupsAppliersVO.group.gridItems;
                    var itemsCount: int = gridItemsModels.length

                    // Match of 5 items will produce TypeKiller item
                    if (itemsCount == 5) {

                        var itemModel: GridItemModel;

                        // If there was an user swap action
                        if (_swapModel) {
                            for (j = 0; j < itemsCount; j++) {
                                itemModel = gridItemsModels[j] as GridItemModel;

                                if (_swapModel.item1.id == itemModel.id || _swapModel.item2.id == itemModel.id) {
                                    break;
                                }
                            }
                        } else
                        // if 4 items were spawned
                        {
                            itemModel = gridItemsModels[0];
                        }

                        var group: ChangedGridItemsGroupByModelDecoration = new ChangedGridItemsGroupByModelDecoration([itemModel], TypeKillerGridItemModel);
                        var vo: ChangedGridItemsGroupApplierVO = new ChangedGridItemsGroupApplierVO(group, TypeKillersViewApplier, GridItemsDecorationModelApplier);
                        (groupsAppliersVO.group as ScoreGivingGridItemsGroup).ignoreGridItems[itemModel.id] = true;

                        res.push(vo);

                    }

                }
            }

            return res;
        }
    }
}

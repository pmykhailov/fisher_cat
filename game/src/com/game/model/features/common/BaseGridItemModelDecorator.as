package com.game.model.features.common {
    import com.game.model.grids.grid_items.item.*;

    public class BaseGridItemModelDecorator extends GridItemModel {

        private var _gridItemModel: GridItemModel;

        public function BaseGridItemModelDecorator(gridItemModel: GridItemModel) {
            super(gridItemModel.id, gridItemModel.type);

            _gridItemModel = gridItemModel;
        }


        override public function get type(): int {
            return _gridItemModel.type;
        }


        override public function set type(value: int): void {
            _gridItemModel.type = value;
        }


        override public function get id(): uint {
            return _gridItemModel.id;
        }


        override public function set id(value: uint): void {
            _gridItemModel.id = value;
        }

    }
}

package com.game.model.features.common {
    import com.game.model.GameModel;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.logic.changed_grid_items_groups.appliers.IChangedGridItemsGroupModelApplier;
    import com.game.model.logic.changed_grid_items_groups.groups.base.ChangedGridItemsGroup;
    import com.game.model.logic.changed_grid_items_groups.groups.decorated.ChangedGridItemsGroupByModelDecoration;

    import model.grids.enum.GridTypeEnum;

    public class GridItemsDecorationModelApplier implements IChangedGridItemsGroupModelApplier {

        public function GridItemsDecorationModelApplier() {
        }


        public function applyChanges(gameModel: GameModel, gridItemsGroup: ChangedGridItemsGroup) {

            var customGroup: ChangedGridItemsGroupByModelDecoration = gridItemsGroup as ChangedGridItemsGroupByModelDecoration;
            var gridModel: GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;

            for (var j: int = 0; j < customGroup.gridItems.length; j++) {
                var itemModel = customGroup.gridItems[j] as GridItemModel;

                gridModel.decorateGridItem(itemModel.id, customGroup.DecoratorClass);

                customGroup.gridItems[j] = gridModel.getGridItemModelByID(itemModel.id);
            }
        }
    }
}
package com.game.model.features.spawned {
    import com.game.model.logic.changed_grid_items_groups.providers.IChangedGridItemsGroupProvider;
    import com.game.model.logic.changed_grid_items_groups_providers.*;
    import com.game.model.GameModel;
    import com.game.model.vo.ActionVO;
    import com.game.model.vo.SwapItemsVO;

    import model.grids.enum.GridTypeEnum;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup;
    import com.game.model.logic.changed_grid_items_groups.WinCombinationTypeEnum;

import model.grids.items.GridItemTypeEnum;

/**
     * Will delete all symbols of some type in case if there was
     * swapped 2 cells one of which is a "type bomb" and another is
     * usual cell
     */
    public class TypeBombWinCombinationProvider implements IChangedGridItemsGroupProvider {

        private var _swapModel: SwapItemsVO;
        private var _someWin: int = 200;


        public function TypeBombWinCombinationProvider(actionModel: ActionVO) {
            _swapModel = actionModel as SwapItemsVO;
        }


        public function getChanges(gameModel: GameModel): Array {
            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;

            // This win will be only in case of items swapping
            if (!_swapModel) return [];

            if (
                    (_swapModel.item1.type == GridItemTypeEnum.TYPE_BOMB && gridModel.usualGridItemTypes.indexOf(_swapModel.item2.type) != -1) ||
                            (_swapModel.item2.type == GridItemTypeEnum.TYPE_BOMB && gridModel.usualGridItemTypes.indexOf(_swapModel.item1.type) != -1)
                    ) {

                var usualItem: GridItemModel;
                var typeBomb: GridItemModel;
                var items: Array = [];

                if (gridModel.usualGridItemTypes.indexOf(_swapModel.item1.type) != -1) {
                    usualItem = _swapModel.item1;
                    typeBomb = _swapModel.item2;
                } else {
                    usualItem = _swapModel.item2;
                    typeBomb = _swapModel.item1;
                }

                for (var i: int = 0; i < gridModel.rows; i++) {
                    for (var j: int = 0; j < gridModel.cols; j++) {
                        var gridItem: GridItemModel = gridModel.getGridItem(i, j) as GridItemModel;
                        if (gridItem.type == usualItem.type) {
                            items.push(gridItem)
                        }
                    }
                }

                items.push(typeBomb);

                return [new ScoreGivingGridItemsGroup(items, _someWin, WinCombinationTypeEnum.TYPE_BOMB)];
            }

            return [];
        }
    }
}

package com.game.model.features.spawned {
    import com.game.model.logic.changed_grid_items_groups.providers.IChangedGridItemsGroupProvider;
    import com.game.model.logic.changed_grid_items_groups_providers.*;
    import com.game.model.GameModel;

    import model.grids.enum.GridTypeEnum;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.grids.grid_items.item.GridItemPosition;
    import com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup;
    import com.game.model.logic.changed_grid_items_groups.WinCombinationTypeEnum;

import model.grids.items.GridItemTypeEnum;

/**
     * This checker will search for any neighbor cell sequence of the same type
     *
     * [2] [3] [3]                                          [] [] []
     * [1] [B] [1]  -> BANG! cuz bomb is in a row of [1] -> [] [] []
     * [2] [1] [3]                                          [] [] []
     */
    public class BombWinCombinationProvider implements IChangedGridItemsGroupProvider {

        private var someScore: int = 100;
        [ArrayElementType("com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup")]
        private var _usualCombinations: Array;


        /**
         * Usual combinations trigger bomb explosion
         * @param usualCombinations
         */
        public function BombWinCombinationProvider(usualCombinations: Array) {
            _usualCombinations = usualCombinations;
        }


        /**
         * Will search for any bomb on game field and check if there was
         * usual win near it
         */
        public function getChanges(gameModel: GameModel): Array {
            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var winCombinations: Array = [];

            for (var i: int = 0; i < gridModel.rows; i++) {
                for (var j: int = 0; j < gridModel.cols; j++) {

                    var bomb: GridItemModel = gridModel.getGridItem(i, j) as GridItemModel;

                    if (bomb.type == GridItemTypeEnum.BOMB) {

                        for (var k: int = 0; k < _usualCombinations.length; k++) {
                            var winCombination: ScoreGivingGridItemsGroup = _usualCombinations[k];
                            var isHorizontalLine: Boolean = this.isHorizontalLine(gridModel, winCombination);
                            var isVerticalLine: Boolean = !isHorizontalLine;

                            for (var l: int = 0; l < winCombination.gridItems.length; l++) {
                                var item: GridItemModel = winCombination.gridItems[l];
                                var itemPosition: GridItemPosition = gridModel.getGridItemPositionByID(item.id);

                                if ((isHorizontalLine && itemPosition.row == i && Math.abs(itemPosition.col - j) == 1) || (isVerticalLine && itemPosition.col == j && Math.abs(itemPosition.row - i) == 1)) {
                                    winCombinations.push(new ScoreGivingGridItemsGroup(getSymbolsAroundBomb(bomb, gridModel), someScore, WinCombinationTypeEnum.BOMB));
                                }

                            }
                        }
                    }
                }
            }

            return winCombinations;
        }


        private function isHorizontalLine(gridModel: GridModel, winCombination: ScoreGivingGridItemsGroup): Boolean {
            var gridItemModel1: GridItemModel = winCombination.gridItems[0] as GridItemModel;
            var gridItemModel2: GridItemModel = winCombination.gridItems[1] as GridItemModel;

            var gridItemPosition1: GridItemPosition = gridModel.getGridItemPositionByID(gridItemModel1.id);
            var gridItemPosition2: GridItemPosition = gridModel.getGridItemPositionByID(gridItemModel2.id);

            return gridItemPosition1.row == gridItemPosition2.row;
        }


        private function getSymbolsAroundBomb(bomb: GridItemModel, gridModel: GridModel): Array {
            var bombPosition: GridItemPosition = gridModel.getGridItemPositionByID(bomb.id);
            var res: Array = [];
            var tmp: Array = [];

            // Bomb itself

            tmp.push(gridModel.getGridItem(bombPosition.row + 0, bombPosition.col + 0));

            // And items around
            // [r-1,c-1] [r-1,c+0] [r-1,c+1]
            // [r+0,c-1] [r+0,c+0] [r+0,c+1]
            // [r+1,c-1] [r+1,c+0] [r+1,c+0]

            tmp.push(gridModel.getGridItem(bombPosition.row - 1, bombPosition.col - 1));
            tmp.push(gridModel.getGridItem(bombPosition.row - 1, bombPosition.col + 0));
            tmp.push(gridModel.getGridItem(bombPosition.row - 1, bombPosition.col + 1));

            tmp.push(gridModel.getGridItem(bombPosition.row + 0, bombPosition.col + 1));
            tmp.push(gridModel.getGridItem(bombPosition.row + 0, bombPosition.col - 1));

            tmp.push(gridModel.getGridItem(bombPosition.row + 1, bombPosition.col - 1));
            tmp.push(gridModel.getGridItem(bombPosition.row + 1, bombPosition.col + 0));
            tmp.push(gridModel.getGridItem(bombPosition.row + 1, bombPosition.col - 1));

            // We are interesting only in existing items
            for (var i: int = 0; i < tmp.length; i++) {
                if (tmp[i]) {
                    res.push(tmp[i]);
                }
            }

            return res;
        }
    }
}

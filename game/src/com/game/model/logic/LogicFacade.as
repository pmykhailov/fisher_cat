package com.game.model.logic {
    import com.game.model.logic.grid.LogicGameFlow;
    import com.game.model.logic.grid.LogicGeneration;
    import com.game.model.logic.grid.LogicInfo;

    public class LogicFacade {
        protected var _logicGameFlow: LogicGameFlow;
        protected var _logicGeneration: LogicGeneration;
        protected var _logicInfo: LogicInfo;


        public function LogicFacade() {
            createLogicGameFlow();
            createLogicGeneration();
            createLogicInfo();
        }


        public function get logicGameFlow(): LogicGameFlow {
            return _logicGameFlow;
        }


        public function get logicGeneration(): LogicGeneration {
            return _logicGeneration;
        }


        public function get logicInfo(): LogicInfo {
            return _logicInfo;
        }


        protected function createLogicInfo(): void {
            _logicInfo = new LogicInfo();
        }


        protected function createLogicGeneration(): void {
            _logicGeneration = new LogicGeneration();
        }


        protected function createLogicGameFlow(): void {
            _logicGameFlow = new LogicGameFlow();
            _logicGameFlow.resetWave();
        }

    }
}

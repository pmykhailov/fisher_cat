package com.game.model.logic.changed_grid_items_groups.providers {
    import com.game.model.GameModel;


    public interface IChangedGridItemsGroupProvider {
        function getChanges(gameModel: GameModel): Array; /*ChangedGridItemsGroupApplierVO*/
    }
}

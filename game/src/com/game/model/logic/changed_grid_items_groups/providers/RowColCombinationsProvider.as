package com.game.model.logic.changed_grid_items_groups.providers {
    import com.game.model.GameModel;
    import com.game.model.logic.changed_grid_items_groups.appliers.RowColCombinationsModelApplier;
    import com.game.model.vo.ChangedGridItemsGroupApplierVO;
    import com.game.view.game_filed.grids.grid_items.changed_grid_items_visualiser.ScoreGivingGridItemsGroupViewApplier;

    import model.grids.enum.GridTypeEnum;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup;

    import model.grids.items.GridItemTypeEnum;

/**
     * Usual win combinations checker searches for neighbor cell of the same type
     * in two directions - vertically and horizontally
     */
    public class RowColCombinationsProvider implements IChangedGridItemsGroupProvider {

        private var minWinLength: int = 3;


        public function RowColCombinationsProvider() {
        }


        public function getChanges(gameModel: GameModel): Array {
            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var processedItems: Array = new Array(); // Used for avoid checking items that were checked some steps ago
            var res: Array = new Array();
            var combination: Array;

            var sameTypeSymbolsCount: int;
            var currentItemType: int;
            var scoreGivingGridItemsGroup: ScoreGivingGridItemsGroup;
            var vo: ChangedGridItemsGroupApplierVO;
            var i: int;
            var j: int;
            var ii: int;
            var jj: int;

            for (i = 0; i < gridModel.rows; i++) {
                processedItems[i] = new Array();
                for (j = 0; j < gridModel.cols; j++) {
                    processedItems[i][j] = false;
                }
            }

            // Search horizontally
            // --->
            // [] [] ... []
            // [] [] ... []
            // [] [] ... []
            //
            for (i = 0; i < gridModel.rows; i++) {
                for (j = 0; j < gridModel.cols; j++) {
                    currentItemType = gridModel.getGridItem(i, j).type;
                    if (currentItemType != GridItemTypeEnum.EMPTY && isUsualItem(currentItemType, gridModel)) {

                        sameTypeSymbolsCount = 0;

                        for (jj = j; jj < gridModel.cols; jj++) {
                            if (!processedItems[i][jj] && currentItemType == gridModel.getGridItem(i, jj).type) {
                                processedItems[i][jj] = true;
                                sameTypeSymbolsCount++;
                            }
                            else {
                                break;
                            }
                        }

                        if (sameTypeSymbolsCount >= minWinLength) {

                            combination = [];

                            for (jj = j; jj < j + sameTypeSymbolsCount; jj++) {
                                combination.push(gridModel.getGridItem(i, jj));
                            }

                            scoreGivingGridItemsGroup = new ScoreGivingGridItemsGroup(combination);
                            scoreGivingGridItemsGroup.scorePerItem = getScorePerItem(combination, gameModel.logicFacade.logicGameFlow.wave);

                            vo = new ChangedGridItemsGroupApplierVO(scoreGivingGridItemsGroup, ScoreGivingGridItemsGroupViewApplier, RowColCombinationsModelApplier);

                            res.push(vo);
                        }
                    }
                }
            }

            for (i = 0; i < gridModel.rows; i++) {
                for (j = 0; j < gridModel.cols; j++) {
                    processedItems[i][j] = false;
                }
            }

            // Search vertically
            //
            //  |  [] [] ... []
            //  |  [] [] ... []
            // \ / [] [] ... []
            //  |

            for (i = 0; i < gridModel.rows; i++) {
                for (j = 0; j < gridModel.cols; j++) {
                    currentItemType = gridModel.getGridItem(i, j).type;
                    if (currentItemType != GridItemTypeEnum.EMPTY && isUsualItem(currentItemType, gridModel)) {

                        sameTypeSymbolsCount = 0;

                        for (ii = i; ii < gridModel.rows; ii++) {
                            if (!processedItems[ii][j] && currentItemType == gridModel.getGridItem(ii, j).type) {
                                processedItems[ii][j] = true;
                                sameTypeSymbolsCount++;
                            }
                            else {
                                break;
                            }
                        }

                        if (sameTypeSymbolsCount >= minWinLength) {
                            combination = [];

                            for (ii = i; ii < i + sameTypeSymbolsCount; ii++) {
                                combination.push(gridModel.getGridItem(ii, j));
                            }

                            scoreGivingGridItemsGroup = new ScoreGivingGridItemsGroup(combination);
                            scoreGivingGridItemsGroup.scorePerItem = getScorePerItem(combination, gameModel.logicFacade.logicGameFlow.wave);

                            vo = new ChangedGridItemsGroupApplierVO(scoreGivingGridItemsGroup, ScoreGivingGridItemsGroupViewApplier, RowColCombinationsModelApplier);

                            res.push(vo);
                        }

                    }
                }
            }

            return res;
        }


        private function isUsualItem(type: int, model: GridModel): Boolean {
            return model.usualGridItemTypes.indexOf(type) != -1;
        }

        private function getScorePerItem(combination: Array, wave: int): int {
            var scorePerItem:int = 20 + (combination.length - 3)*10;
            return scorePerItem * wave;
        }

    }
}

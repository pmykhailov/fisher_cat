package com.game.model.logic.changed_grid_items_groups.appliers {
    import com.game.model.GameModel;
    import com.game.model.logic.changed_grid_items_groups.groups.base.ChangedGridItemsGroup;
    import com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup;

    /**
     * RowColCombinationsApplier class.
     * User: Paul Makarenko
     * Date: 16.01.2015
     */
    public class RowColCombinationsModelApplier implements IChangedGridItemsGroupModelApplier {

        public function RowColCombinationsModelApplier() {
        }


        public function applyChanges(gameModel: GameModel, gridItemsGroup: ChangedGridItemsGroup) {
            gameModel.logicFacade.logicGameFlow.deleteGridItems((gridItemsGroup as ScoreGivingGridItemsGroup).notIgnoredGridItems);
        }
    }
}
package com.game.model.logic.changed_grid_items_groups.appliers {
    import com.game.model.GameModel;
    import com.game.model.logic.changed_grid_items_groups.groups.base.ChangedGridItemsGroup;

    /**
     * IChangedGridItemsGroupApplier class.
     * User: Paul Makarenko
     * Date: 16.01.2015
     */
    public interface IChangedGridItemsGroupModelApplier {
        function applyChanges(gameModel: GameModel, gridItemsGroup: ChangedGridItemsGroup)
    }
}


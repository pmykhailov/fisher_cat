package com.game.model.logic.changed_grid_items_groups.groups.decorated {
    import com.game.model.logic.changed_grid_items_groups.groups.base.ChangedGridItemsGroup;

    public class ChangedGridItemsGroupByModelDecoration extends ChangedGridItemsGroup {

    private var _DecoratorClass:Class;

    public function ChangedGridItemsGroupByModelDecoration(gridItems:Array, DecoratorClass:Class) {
        super(gridItems);

        _DecoratorClass = DecoratorClass;
    }

    public function get DecoratorClass():Class {
        return _DecoratorClass;
    }
}
}

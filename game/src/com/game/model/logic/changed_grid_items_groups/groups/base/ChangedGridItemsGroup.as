package com.game.model.logic.changed_grid_items_groups.groups.base {
public class ChangedGridItemsGroup {

    protected var _gridItems: Array;

    public function ChangedGridItemsGroup(gridItems: Array) {
        _gridItems = gridItems;
    }

    public function get gridItems(): Array /*GridItemModel*/ {
        return _gridItems;
    }

}
}

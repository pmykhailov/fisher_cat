package com.game.model.logic.changed_grid_items_groups.groups.scored {
    import com.game.model.grids.grid_items.item.GridItemModel;

    /**
     * GridItemWinCombinationVO class.
     * User: Paul Makarenko
     * Date: 09.01.2015
     */
    public class GridItemScoreVO {
        private var _gridItemModel: GridItemModel;
        private var _score: int;

        public function GridItemScoreVO(gridItemModel: GridItemModel, score: int) {
            _gridItemModel = gridItemModel;
            _score = score;

        }


        public function get gridItemModel(): GridItemModel {
            return _gridItemModel;
        }


        public function get score(): int {
            return _score;
        }
    }
}

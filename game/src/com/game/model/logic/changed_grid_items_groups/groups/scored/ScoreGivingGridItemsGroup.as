package com.game.model.logic.changed_grid_items_groups.groups.scored {
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.logic.changed_grid_items_groups.groups.base.ChangedGridItemsGroup;

    import flash.utils.Dictionary;

    public class ScoreGivingGridItemsGroup extends ChangedGridItemsGroup {

        [ArrayElementType("com.game.model.logic.changed_grid_items_groups.groups.scored.GridItemScoreVO")]
        private var _gridItemsScoresVOs: Array;
        /** Total score **/
        private var _score: int;
        /** Score that is given for 1 grid item **/
        private var _scorePerItem: int;
        /** To make sure score is calculated once **/
        private var _isTotalScoreCalculated: Boolean;
        /** Show score per each item or total score while showing grid items changed**/
        private var _showScorePerEachItem: Boolean;
        /** Some grid items animation can be ignored.
         * For instance if 4 items produce rowColKiller it is not animated
         * Also such items shouldn't be removed from grid model.
         * Items needed to set correct score text position :
         *
         *            SCORE
         *              |
         * [item] [item] [item] [rowColKiller item]
         *
         *
         * **/
        private var _ignoreGridItems: Dictionary;


        public function ScoreGivingGridItemsGroup(gridItems: Array) {
            super(gridItems);
            _gridItemsScoresVOs = [];
            _ignoreGridItems = new Dictionary();
        }


        public function get score(): int {

            if (!_isTotalScoreCalculated) {
                _isTotalScoreCalculated = true;
                _score = 0;
                for (var i: int = 0; i < _gridItemsScoresVOs.length; i++) {
                    var vo: GridItemScoreVO = _gridItemsScoresVOs[i] as GridItemScoreVO;
                    _score += vo.score;
                }
            }

            return _score;
        }

        public function get notIgnoredGridItems(): Array {
            var res: Array = [];

            for (var i: int = 0; i < _gridItems.length; i++) {
                var item: GridItemModel = _gridItems[i] as GridItemModel;

                if (!_ignoreGridItems[item.id]) res.push(item);
            }

            return res;
        }


        public function get gridItemsScoresVOs(): Array {
            return _gridItemsScoresVOs;
        }


        public function get showScorePerEachItem(): Boolean {
            return _showScorePerEachItem;
        }


        public function set showScorePerEachItem(value: Boolean): void {
            _showScorePerEachItem = value;
        }


        public function get scorePerItem(): int {
            return _scorePerItem;
        }


        public function set scorePerItem(value: int): void {
            _scorePerItem = value;

            for (var i: int = 0; i < _gridItems.length; i++) {
                var vo: GridItemScoreVO = new GridItemScoreVO(_gridItems[i], value);
                _gridItemsScoresVOs[i] = vo;
            }
        }


        public function get ignoreGridItems(): Dictionary {
            return _ignoreGridItems;
        }


        public function set ignoreGridItems(value: Dictionary): void {
            _ignoreGridItems = value;
        }
    }
}
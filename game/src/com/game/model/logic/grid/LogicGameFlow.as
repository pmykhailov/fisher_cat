package com.game.model.logic.grid {
    import com.game.model.GameModel;
    import com.game.model.logic.changed_grid_items_groups.groups.base.ChangedGridItemsGroup;
    import com.game.model.utils.IDGenerator;

    import com.game.model.grids.base.BaseGridModel;
    import com.game.model.grids.base.item.BaseGridItemModel;
    import com.game.model.vo.ChangedGridItemsGroupApplierVO;

    import model.grids.enum.GridTypeEnum;
    import model.grids.items.GridBackgroundItemEnum;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.grids.grid_items.item.GridItemPosition;
    import model.grids.items.GridItemTypeEnum;

    /**
     * Game flow logic
     */
    public class LogicGameFlow {
        /** Number of chan reactions after deleting items **/
        private var _wave: int;

        public function LogicGameFlow() {

        }


        public function get wave():int {
            return _wave;
        }


        public function set wave(value: int): void {
            _wave = value;
        }


        public function resetWave(): void {
            _wave = 0;
        }


        public function swapItems(gridModel: GridModel, item1ID: uint, item2ID: uint): void {
            gridModel.swapItems(gridModel.getGridItemModelByID(item1ID), gridModel.getGridItemModelByID(item2ID));
        }


        public function deleteGridItems(gridItems: Array): void {
            for (var j: int = 0; j < gridItems.length; j++) {
                (gridItems[j] as GridItemModel).type = GridItemTypeEnum.EMPTY;
            }
        }


        public function deleteGridBackgroundItems(gameModel: GameModel, groupsAppliersVOs: Array): Array {

            var gridModel: GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var gridBackgroundModel: BaseGridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_BACKGROUND);
            var gridItemPosition: GridItemPosition;
            var gridItemModel: BaseGridItemModel;
            var deleted: Array = [];


            for (var i: int = 0; i < groupsAppliersVOs.length; i++) {
                var group: ChangedGridItemsGroup = (groupsAppliersVOs[i] as ChangedGridItemsGroupApplierVO).group;

                for (var j: int = 0; j < group.gridItems.length; j++) {
                    gridItemPosition = gridModel.getGridItemPositionByID((group.gridItems[j] as GridItemModel).id);

                    gridItemModel = gridBackgroundModel.getGridItem(gridItemPosition.row, gridItemPosition.col);

                    if (gridItemModel.type == GridBackgroundItemEnum.WITH_BACKGROUND) {
                        gridItemModel.type = GridBackgroundItemEnum.NOT_EXISTING;
                        deleted.push(gridItemPosition);
                    }
                }
            }

            return deleted;
        }


        public function spawnItems(gameModel: GameModel): void {
            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var TMP_ITEM_TYPE: int = GridItemTypeEnum.TEMPORARY;
            var i: int;
            var j: int;

            // Bullbe deleted items (they all have EMPTY type) to the top of grid matrix
            while (gameModel.logicFacade.logicInfo.containesEmptyItems(gameModel)) {
                var isEmptyItemFound: Boolean = false;

                for (i = gridModel.rows - 1; i >= 0; i--) {
                    for (j = 0; j < gridModel.cols; j++) {
                        if (gridModel.getGridItem(i, j).type == GridItemTypeEnum.EMPTY ) {
                            var isItemToSwapWithEmptyFound: Boolean = false;

                            isEmptyItemFound = true;

                            for (var ii: int = i - 1; ii >= 0; ii--) {
                                var itemForSwap: GridItemModel = gridModel.getGridItem(ii, j) as GridItemModel;

                                if (itemForSwap.type != GridItemTypeEnum.EMPTY && itemForSwap.type != GridItemTypeEnum.NON_EXISTING) {
                                    gridModel.swapItems(itemForSwap, gridModel.getGridItem(i, j) as GridItemModel);
                                    isItemToSwapWithEmptyFound = true;
                                    break;
                                }
                            }

                            if (!isItemToSwapWithEmptyFound) {
                                gridModel.getGridItem(i, j).type = TMP_ITEM_TYPE;
                            }
                        }
                    }

                    if (isEmptyItemFound) {
                        break;
                    }
                }

            }

            // Generate type for deleted items
            for (i = 0; i < gridModel.rows; i++) {
                for (j = 0; j < gridModel.cols; j++) {
                    var gridItemModel: GridItemModel = gridModel.getGridItem(i, j) as GridItemModel;

                    if (gridItemModel.type == TMP_ITEM_TYPE) {
                        gridItemModel.id = IDGenerator.id;
                        gridItemModel.type = gameModel.logicFacade.logicGeneration.getRandomItemType(gridModel);
                    }
                }
            }
        }
    }
}
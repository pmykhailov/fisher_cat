package com.game.model.logic.grid {
    import com.game.model.GameModel;
    import com.game.model.features.creations.row_col_killer.providers.KilledByRowColKillersItemsProvider;
    import com.game.model.features.creations.row_col_killer.providers.RowColKillersProvider;
    import com.game.model.features.creations.type_killer.providers.KilledByTypeKillersItemsProvider;
    import com.game.model.features.creations.type_killer.providers.TypeKillersProvider;
    import com.game.model.vo.ActionVO;
    import com.game.model.vo.SwapItemsVO;

    import model.grids.enum.GridTypeEnum;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.grids.grid_items.item.GridItemPosition;
    import com.game.model.features.spawned.BombWinCombinationProvider;
    import com.game.model.features.spawned.TypeBombWinCombinationProvider;
    import com.game.model.logic.changed_grid_items_groups.providers.RowColCombinationsProvider;

import model.grids.items.GridItemTypeEnum;

/**
     * Methods that provide some information about game field
     */
    public class LogicInfo {

        public function LogicInfo() {
        }


        /**
         * Combinations of symbols.
         *
         * Changes to model is not applied here. That is made cuz this method is only informative
         *
         */
        public function getGridItemsChanges(gameModel: GameModel, actionModel: ActionVO = null): Array /*GridItemsChanges*/ {
            var rowColCombinationsProvider: RowColCombinationsProvider = new RowColCombinationsProvider();
            var rowColCombinations: Array = rowColCombinationsProvider.getChanges(gameModel);

            var typeKillersProvider: TypeKillersProvider = new TypeKillersProvider(actionModel, rowColCombinations);
            var typeKillers: Array = typeKillersProvider.getChanges(gameModel);

            var killedByTypeKillersItemsProvider: KilledByTypeKillersItemsProvider = new KilledByTypeKillersItemsProvider(actionModel);
            var killedByTypeKillersItems: Array = killedByTypeKillersItemsProvider.getChanges(gameModel);

            var rowColKillersProvider: RowColKillersProvider = new RowColKillersProvider(actionModel, rowColCombinations);
            var rowColKillers: Array = rowColKillersProvider.getChanges(gameModel);

            var killedByRowColKillersItemsProvider: KilledByRowColKillersItemsProvider = new KilledByRowColKillersItemsProvider(actionModel, rowColCombinations, rowColKillersProvider.rowColKillers);
            var killedByRowColKillersItems: Array = killedByRowColKillersItemsProvider.getChanges(gameModel);

            //var bombCombinations: Array = (new BombWinCombinationProvider(rowColCombinations)).getChanges(gameModel);
            //var typeBombCombinations: Array = (new TypeBombWinCombinationProvider(actionModel)).getChanges(gameModel);

            return rowColCombinations.
                    concat(typeKillers).concat(killedByTypeKillersItems).
                    concat(rowColKillers).concat(killedByRowColKillersItems);
        }


        public function hasMoves(gameModel: GameModel): Boolean {
            return getAnyItemsSwapThatMakesWinCombination(gameModel) != null;
        }


        public function getAnyItemsSwapThatMakesWinCombination(gameModel: GameModel): SwapItemsVO {
            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;

            var item1: GridItemModel;
            var item2: GridItemModel;

            var i: int;
            var j: int;

            for (i = 0; i < gridModel.rows; i++) {
                for (j = 0; j < gridModel.cols; j++) {

                    item1 = gridModel.getGridItem(i, j) as GridItemModel;

                    // Looking to the right
                    if (j + 1 < gridModel.cols) {
                        item2 = gridModel.getGridItem(i, j + 1) as GridItemModel;

                        gridModel.swapItems(item1, item2);

                        if (getGridItemsChanges(gameModel).length > 0) {
                            gridModel.swapItems(item1, item2);
                            return new SwapItemsVO(item1, item2);
                        }
                        else {
                            gridModel.swapItems(item1, item2);
                        }
                    }

                    // Looking down
                    if (i + 1 < gridModel.rows) {
                        item2 = gridModel.getGridItem(i + 1, j) as GridItemModel;

                        gridModel.swapItems(item1, item2);

                        if (getGridItemsChanges(gameModel).length > 0) {
                            gridModel.swapItems(item1, item2);
                            return new SwapItemsVO(item1, item2);
                        }
                        else {
                            gridModel.swapItems(item1, item2);
                        }
                    }
                }
            }

            return null;
        }


        /**
         * Defines if grid has empty items
         */
        public function containesEmptyItems(gameModel: GameModel): Boolean {
            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;

            for (var i: int = 0; i < gridModel.rows; i++) {
                for (var j: int = 0; j < gridModel.cols; j++) {
                    if (gridModel.getGridItem(i, j).type == GridItemTypeEnum.EMPTY) {
                        return true;
                    }
                }
            }

            return false;
        }


        /**
         * Checks if cells are neighbors
         */
        public function isNeighbors(gameModel: GameModel, item1: GridItemModel, item2: GridItemModel): Boolean {
            var gridModel:GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;
            var pos1: GridItemPosition = gridModel.getGridItemPositionByID(item1.id);
            var pos2: GridItemPosition = gridModel.getGridItemPositionByID(item2.id);

            if (pos1.row == pos2.row) {
                if (pos1.col == pos2.col - 1 || pos1.col == pos2.col + 1) {
                    return true;
                }
            }

            if (pos1.col == pos2.col) {
                if (pos1.row == pos2.row - 1 || pos1.row == pos2.row + 1) {
                    return true;
                }
            }

            return false;
        }
    }
}
package com.game.model.logic.grid {
    import com.game.model.GameModel;
    import com.game.model.grids.grid_items.GridModel;
    import com.game.model.grids.grid_items.item.GridItemModel;

    import model.grids.enum.GridTypeEnum;
    import model.grids.items.GridItemTypeEnum;

    /**
     * Here localed all logic that deals with items generation
     */
    public class LogicGeneration {
        public function LogicGeneration() {

        }


        /**
         * Only empty items receives new random type
         */
        public function randomizeEmptyGridItems(gameModel: GameModel): void {
            var gridModel: GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;

            for (var i: int = 0; i < gridModel.rows; i++) {
                for (var j: int = 0; j < gridModel.cols; j++) {
                    var gridItemModel: GridItemModel = gridModel.getGridItem(i, j) as GridItemModel;
                    if (gridItemModel.type == GridItemTypeEnum.EMPTY) {
                        gridItemModel.type = getRandomItemType(gridModel);
                    }
                }
            }
        }


        public function randomizeGameFiledWithoutWins(gameModel: GameModel): void {
            do
            {
                randomizeGameFiled(gameModel);
            } while (gameModel.logicFacade.logicInfo.getGridItemsChanges(gameModel).length > 0)
        }


        /**
         * Fill game field with random type items
         */
        public function randomizeGameFiled(gameModel: GameModel): void {
            var gridModel: GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;

            for (var i: int = 0; i < gridModel.rows; i++) {
                for (var j: int = 0; j < gridModel.cols; j++) {
                    var gridItemModel: GridItemModel = gridModel.getGridItem(i, j) as GridItemModel;
                    if (gridItemModel.type != GridItemTypeEnum.NON_EXISTING) {
                        gridItemModel.type = getRandomItemType(gridModel);
                    }
                }
            }
        }


        public function setGameField(gameModel: GameModel, gameField: Array): void {
            var gridModel: GridModel = gameModel.getGridModelByType(GridTypeEnum.TYPE_MAIN) as GridModel;

            for (var i: int = 0; i < gridModel.rows; i++) {
                for (var j: int = 0; j < gridModel.cols; j++) {
                    var gridItemModel: GridItemModel = gridModel.getGridItem(i, j) as GridItemModel;
                    gridItemModel.type = gameField[i][j];
                }
            }
        }


        /**
         * Random item type generation
         */
        public function getRandomItemType(gridModel: GridModel): int {
            var symbols: Array;

            if (Math.random() < 0.1 && gridModel.bonusGridItemTypes.length > 0) {
                symbols = gridModel.bonusGridItemTypes;
            } else {
                symbols = gridModel.usualGridItemTypes;
            }

            return symbols[int(Math.random() * symbols.length)];
        }

    }
}
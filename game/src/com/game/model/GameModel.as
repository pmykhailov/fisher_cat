package com.game.model {
    import by.lord_xaoca.robotlegs2.BaseActor;

    import com.game.model.logic.LogicFacade;

    import com.game.model.grids.base.BaseGridModel;
    import model.grids.enum.GridTypeEnum;
    import com.game.model.grids.grid_items.GridModel;
    import model.level.settings.GameSettings;
    import model.level.settings.vo.GridSettingsVO;

    public class GameModel extends BaseActor {

        protected var _gameSettings: GameSettings;
        [ArrayElementType("com.game.model.grids.base.BaseGridModel")]
        protected var _gridModels: Array;
        protected var _logicFacade: LogicFacade;


        public function GameModel() {
            super();
        }


        public function get logicFacade(): LogicFacade {
            return _logicFacade;
        }


        public function get gameSettings(): GameSettings {
            return _gameSettings;
        }


        public function getGridModelByType(type: String): BaseGridModel {
            var n: uint = _gridModels.length;

            for (var i: int = 0; i < n; i++) {
                var gridModel: BaseGridModel = _gridModels[i];
                if (gridModel.type == type) {
                    return gridModel;
                }
            }

            return null;
        }


        public function init(gameSettings: GameSettings): void {
            _gameSettings = gameSettings;
            createGrids();
            createLogicFacade();
        }


        protected function createLogicFacade(): void {
            _logicFacade = new LogicFacade();
        }


        private function createGrids(): void {
            var grids: Array = _gameSettings.gridSettingsVOs;
            var n: uint = grids.length;

            _gridModels = [];

            for (var i: int = 0; i < n; i++) {
                var gridSettingsVO: GridSettingsVO = grids[i];
                var GridModelClass: Class = getGridModelClassByType(gridSettingsVO.type);

                _gridModels.push(new GridModelClass(gridSettingsVO.type, _gameSettings));
            }
        }


        private function getGridModelClassByType(type: String): Class {
            switch (type) {
                case GridTypeEnum.TYPE_MAIN :
                    return GridModel;
                    break;

                case GridTypeEnum.TYPE_BACKGROUND :
                    return BaseGridModel;
                    break;
            }

            return null;
        }

    }
}

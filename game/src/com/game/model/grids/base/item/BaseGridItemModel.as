package com.game.model.grids.base.item {

    /**
     * BaseGridItemModel class.
     * User: Paul Makarenko
     * Date: 22.04.14
     */
    public class BaseGridItemModel {

        protected var _type: int;


        public function BaseGridItemModel(type: int) {
            _type = type;
        }


        public function get type(): int {
            return _type;
        }


        public function set type(value: int): void {
            _type = value;
        }
    }
}

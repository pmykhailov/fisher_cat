package com.game.model.grids.base {
    import com.game.model.grids.base.item.BaseGridItemModel;
    import model.grids.items.BaseGridItemEnum;
    import model.level.settings.GameSettings;
    import model.level.settings.vo.GridSettingsVO;

    /**
     * GridBaseModel class.
     *
     * Some grid variants can be
     * - grid that consists of items
     * - background of items - walls(wooden, stone, etc)
     * - foreground of items - chains
     *
     * User: Paul Makarenko
     * Date: 21.04.14
     */
    public class BaseGridModel {

        protected var _rows: int;
        protected var _cols: int;
        protected var _grid: Array;
        /*BaseGridItemModel*/
        protected var _gameSettings: GameSettings;
        /** Possible grid types are listed in GridTypesEnum**/
        private var _type: String;


        public function BaseGridModel(type: String, gameSettings: GameSettings) {
            _gameSettings = gameSettings;
            _type = type;
            init();
        }


        public function get rows(): int {
            return _rows;
        }


        public function get cols(): int {
            return _cols;
        }


        public function get type(): String {
            return _type;
        }


        public function getGridItem(row: int, col: int): BaseGridItemModel {
            if (!inRange(row, col)) return null;
            return _grid[row][col] as BaseGridItemModel;
        }


        protected function inRange(row: int, col: int): Boolean {
            return (row >= 0 && row < _rows) && (col >= 0 && col < _cols);
        }


        protected function init(): void {
            initDimensions();
            initBlankItems();
        }


        protected function initDimensions(): void {
            _rows = _gameSettings.gameFieldRows;
            _cols = _gameSettings.gameFieldCols;
        }


        protected function initBlankItems(): void {
            _grid = new Array();

            for (var i: int = 0; i < _rows; i++) {
                _grid[i] = new Array();
                for (var j: int = 0; j < _cols; j++) {
                    _grid[i][j] = createGridItemModel();

                    var gridSettingsVO: GridSettingsVO = _gameSettings.getGridSettingsVOByType(type);

                    // If we have some settings of the grid
                    if (gridSettingsVO) {
                        _grid[i][j].type = gridSettingsVO.elements[i][j];
                    }
                }
            }
        }


        protected function createGridItemModel(): BaseGridItemModel {
            return new BaseGridItemModel(BaseGridItemEnum.NOT_EXISTING);
        }
    }
}

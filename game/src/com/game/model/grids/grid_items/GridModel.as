package com.game.model.grids.grid_items {

    import com.game.model.utils.IDGenerator;

    import com.game.model.grids.base.BaseGridModel;
    import com.game.model.grids.base.item.BaseGridItemModel;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.grids.grid_items.item.GridItemPosition;
    import model.grids.items.GridItemTypeEnum;
    import model.level.settings.GameSettings;

    public class GridModel extends BaseGridModel {

        public function GridModel(type: String, settings: GameSettings) {
            super(type, settings);
        }


        /**
         * Type of usual items that are currently used in game
         */
        public function get usualGridItemTypes(): Array {
            return _gameSettings.usualGridItemTypes;
        }


        /**
         * Type of bonus items that are currently used in game
         */
        public function get bonusGridItemTypes(): Array {
            return _gameSettings.bonusGridItemTypes;
        }


        public function traceGrid(): void {
            var rowString: String = "";

            trace(" >>>> ");

            for (var i: int = 0; i < _rows; i++) {
                rowString = "";
                for (var j: int = 0; j < _cols; j++) {
                    var itemType: int = (_grid[i][j] as GridItemModel).type;
                    var itemTypeString: String;
                    itemType > 0 ? itemTypeString = "+" + itemType : itemTypeString = "" + itemType;
                    rowString += itemTypeString + " ";
                }

                trace(rowString);
            }
        }


        public function getGridItemModelByID(id: uint): GridItemModel {
            for (var i: int = 0; i < _rows; i++) {
                for (var j: int = 0; j < _cols; j++) {
                    var item: GridItemModel = _grid[i][j] as GridItemModel;
                    if (item.id == id) {
                        return item;
                    }
                }
            }

            return null;
        }


        public function getGridItemPositionByID(id: uint): GridItemPosition {
            for (var i: int = 0; i < _rows; i++) {
                for (var j: int = 0; j < _cols; j++) {
                    var item: GridItemModel = _grid[i][j] as GridItemModel;
                    if (item.id == id) {
                        return new GridItemPosition(i, j);
                    }
                }
            }

            return null;
        }


        public function decorateGridItem(id: uint, GridItemModelClass:Class): void {
            var itemPosition: GridItemPosition = getGridItemPositionByID(id);
            _grid[itemPosition.row][itemPosition.col] = new GridItemModelClass(_grid[itemPosition.row][itemPosition.col]);
        }


        public function swapItems(item1: GridItemModel, item2: GridItemModel): void {
            var tmp: GridItemModel;
            var pos1: GridItemPosition = getGridItemPositionByID(item1.id);
            var pos2: GridItemPosition = getGridItemPositionByID(item2.id);

            tmp = _grid[pos1.row][pos1.col];
            _grid[pos1.row][pos1.col] = _grid[pos2.row][pos2.col];
            _grid[pos2.row][pos2.col] = tmp;
        }


        override protected function createGridItemModel(): BaseGridItemModel {
            return new GridItemModel(IDGenerator.id, GridItemTypeEnum.EMPTY);
        }

    }
}
package com.game.model.grids.grid_items.item {
    import com.game.model.grids.base.item.BaseGridItemModel;

    public class GridItemModel extends BaseGridItemModel {

        private var _id: uint;


        public function GridItemModel(id: uint, type: int) {
            super(type)
            _id = id;
        }


        public function get id(): uint {
            return _id;
        }


        public function set id(value: uint): void {
            _id = value;
        }
    }
}
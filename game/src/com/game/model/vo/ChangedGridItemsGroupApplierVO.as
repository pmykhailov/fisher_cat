package com.game.model.vo {
    import com.game.model.logic.changed_grid_items_groups.groups.base.ChangedGridItemsGroup;

    /**
     * ChangedGridItemsAnimationVO class.
     * User: Paul Makarenko
     * Date: 10.01.2015
     */
    public class ChangedGridItemsGroupApplierVO {

        private var _group: ChangedGridItemsGroup;
        private var _ViewApplierClass: Class;
        private var _ModelApplierClass: Class;

        public function ChangedGridItemsGroupApplierVO(group:ChangedGridItemsGroup, ViewApplierClass:Class, ModelApplierClass: Class) {
            _group = group;
            _ViewApplierClass = ViewApplierClass;
            _ModelApplierClass = ModelApplierClass;
        }


        public function get group(): ChangedGridItemsGroup {
            return _group;
        }


        public function get ViewApplierClass(): Class {
            return _ViewApplierClass;
        }


        public function get ModelApplierClass(): Class {
            return _ModelApplierClass;
        }
    }
}
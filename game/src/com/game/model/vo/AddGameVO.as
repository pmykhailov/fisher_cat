package com.game.model.vo {
    import model.level.settings.GameSettings;

    import flash.display.DisplayObjectContainer;

import model.level.settings.GameSettings;

/**
     * AddGameVO class.
     * User: Paul Makarenko
     * Date: 20.04.14
     */
    public class AddGameVO {

        private var _container: DisplayObjectContainer;
        private var _gameSettings: GameSettings;


        public function AddGameVO(container: DisplayObjectContainer, gameSettings: GameSettings = null) {
            _container = container;
            _gameSettings = gameSettings;
        }


        public function get container(): DisplayObjectContainer {
            return _container;
        }


        public function get gameSettings(): GameSettings {
            return _gameSettings;
        }
    }
}
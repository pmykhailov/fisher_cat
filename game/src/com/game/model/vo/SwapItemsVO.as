package com.game.model.vo {
    import com.game.model.grids.grid_items.item.GridItemModel;

    public class SwapItemsVO extends ActionVO {

        private var _item1: GridItemModel;
        private var _item2: GridItemModel;


        public function SwapItemsVO(item1: GridItemModel, item2: GridItemModel) {
            _item1 = item1;
            _item2 = item2;
        }


        public function get item1(): GridItemModel {
            return _item1;
        }


        public function get item2(): GridItemModel {
            return _item2;
        }
    }
}

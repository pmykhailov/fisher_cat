package com.game {
    import com.game.controller.external_controll.AddGameCommand;
    import com.game.controller.external_controll.EnableGameMouseClicksCommand;
    import com.game.controller.external_controll.FinishGameCommand;
    import com.game.controller.external_controll.PauseGameCommand;
    import com.game.controller.external_controll.RemoveGameCommand;
    import com.game.controller.external_controll.RestartGameCommand;
    import com.game.controller.external_controll.ResumeGameCommand;
    import com.game.controller.external_controll.StartGameCommand;
    import com.game.controller.external_controll.StopGameCommand;
    import com.game.controller.internal_controll.CreateHintCommand;
    import com.game.controller.internal_controll.ChangeItemsCommand;
    import com.game.controller.internal_controll.ItemMouseDownCommand;
    import com.game.controller.internal_controll.ItemMouseOverCommand;
    import com.game.controller.internal_controll.ItemMouseUpCommand;
    import com.game.controller.internal_controll.LockGameWhilePlayingAnimationsCommand;
    import com.game.controller.internal_controll.NeighborItemsSelectedCommand;
    import com.game.controller.internal_controll.spawn_items.DefaultSpawnItemsCommand;
import com.game.controller.internal_controll.SpawnItemsCompleteCommand;
import com.game.controller.internal_controll.SwapItemsCommand;
    import com.game.controller.internal_controll.UnlockGameAfterPlayingAnimationsCommand;
import com.game.controller.internal_controll.spawn_items.SpringSpawnItemsCommand;
import com.game.events.GameContextEvent;
    import com.game.events.GameControlEvent;
    import com.game.view.events.GameViewEvent;
    import com.game.view.game_filed.grids.grid_items.GridView;
    import com.game.view.game_filed.grids.grid_items.GridViewMediator;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.api.IInjector;

    public class GameConfig implements IConfig {

        [Inject]
        public var context: IContext;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var mediatorMap: IMediatorMap;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: IInjector;
        [Inject]
        public var contextView: ContextView;


        public function configure(): void {

            mapCommands();
            mapMediators();
            mapInjections();

            context.afterInitializing(init);
        }


        private function mapCommands(): void {

            eventCommandMap.map("createHint").toCommand(CreateHintCommand);

            eventCommandMap.map(GameViewEvent.ITEM_MOUSE_DOWN).toCommand(ItemMouseDownCommand);
            eventCommandMap.map(GameViewEvent.ITEM_MOUSE_OVER).toCommand(ItemMouseOverCommand);
            eventCommandMap.map(GameViewEvent.ITEM_MOUSE_UP).toCommand(ItemMouseUpCommand);

            eventCommandMap.map(GameContextEvent.NEIGHBOR_ITEMS_SELECTED).toCommand(NeighborItemsSelectedCommand);
            eventCommandMap.map(GameContextEvent.SWAP_ITEMS).toCommand(SwapItemsCommand);
            eventCommandMap.map(GameContextEvent.DELETE_ITEMS).toCommand(ChangeItemsCommand);
            eventCommandMap.map(GameContextEvent.SPAWN_ITEMS).toCommand(DefaultSpawnItemsCommand);
            //eventCommandMap.map(GameContextEvent.SPAWN_ITEMS).toCommand(SpringSpawnItemsCommand);
            eventCommandMap.map(GameContextEvent.SPAWN_ITEMS_COMPLETE).toCommand(SpawnItemsCompleteCommand);
            eventCommandMap.map(GameContextEvent.LOCK_GAME_WHILE_ANIMATIONS).toCommand(LockGameWhilePlayingAnimationsCommand);
            eventCommandMap.map(GameContextEvent.UNLOCK_GAME_AFTER_ANIMATIONS).toCommand(UnlockGameAfterPlayingAnimationsCommand);

            eventCommandMap.map(GameControlEvent.ADD_GAME).toCommand(AddGameCommand);
            eventCommandMap.map(GameControlEvent.START_GAME).toCommand(StartGameCommand);
            eventCommandMap.map(GameControlEvent.PAUSE_GAME).toCommand(PauseGameCommand);
            eventCommandMap.map(GameControlEvent.RESUME_GAME).toCommand(ResumeGameCommand);
            eventCommandMap.map(GameControlEvent.RESTART_GAME).toCommand(RestartGameCommand);
            eventCommandMap.map(GameControlEvent.STOP_GAME).toCommand(StopGameCommand);
            eventCommandMap.map(GameControlEvent.FINISH_GAME).toCommand(FinishGameCommand);
            eventCommandMap.map(GameControlEvent.REMOVE_GAME).toCommand(RemoveGameCommand);
            eventCommandMap.map(GameControlEvent.ENABLE_GAME_MOUSE_CLICKS).toCommand(EnableGameMouseClicksCommand);
        }


        private function mapMediators(): void {
            mediatorMap.map(GridView).toMediator(GridViewMediator);
        }


        private function mapInjections(): void {
        }


        private function init(): void {
        }
    }
}

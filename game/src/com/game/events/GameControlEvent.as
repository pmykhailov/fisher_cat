package com.game.events {
    import flash.events.Event;

    /**
     * Events for game controlling
     */
    public class GameControlEvent extends Event {

        /** Create game model and view instances **/
        public static const ADD_GAME: String = "addGame";
        /** Starts the game **/
        public static const START_GAME: String = "startGame";
        /** Pauses game **/
        public static const PAUSE_GAME: String = "pauseGame";
        /** Game will be unpaused. All changes that were applied by pause will be reverted **/
        public static const RESUME_GAME: String = "resumeGame";
        /** Remove items and create new one **/
        public static const RESTART_GAME: String = "restartGame";
        /** Stop game **/
        public static const STOP_GAME: String = "stopGame";
        /** Finish game **/
        public static const FINISH_GAME: String = "finishGame";
        /** Destroy game model and view instances **/
        public static const REMOVE_GAME: String = "removeGame";
        /** Disable / enable mouse clicks on a game. Game can be disable while some intro animation is shown **/
        public static const ENABLE_GAME_MOUSE_CLICKS: String = "enableGameMouseClicks";

        private var _data: Object;


        public function GameControlEvent(type: String, data: Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new GameControlEvent(type, _data, bubbles, cancelable);
        }
    }
}
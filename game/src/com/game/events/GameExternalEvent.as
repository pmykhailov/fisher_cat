package com.game.events {
    import flash.events.Event;

    /**
     * Events to external system
     */
    public class GameExternalEvent extends Event {

        /** Dispatches when some animation of items is completed at the begin of the game  **/
        public static const GAME_STARTED: String = "gameStarted";
        /** Dispatches when some animation of items is completed at the end of the game  **/
        public static const GAME_FINISHED: String = "gameFinished";
        /** Game stopped when all there are no more items spawn or to delete **/
        public static const GAME_STOPPED: String = "gameStopped";
        public static const ITEMS_DELETED: String = "itemsDeleted";
        public static const ITEMS_BACKGROUNDS_DELETED: String = "itemsBacksDeleted";
        public static const GAME_NO_MORE_MOVES: String = "gameNoMoreMoves";

        private var _data: Object;


        public function GameExternalEvent(type: String, data: Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        public function set data(value: Object): void {
            _data = value;
        }


        override public function clone(): Event {
            return new GameExternalEvent(type, _data, bubbles, cancelable);
        }

    }
}
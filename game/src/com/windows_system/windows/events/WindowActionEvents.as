package com.windows_system.windows.events {
    import flash.events.Event;

    /**
     * WindowActionEvents class.
     *
     * This events can be dispatched from windows / popups
     * Any actions that will follow this events will be
     * described in commands that are mapped to events
     *
     * User: Paul Makarenko
     * Date: 18.04.14
     */
    public class WindowActionEvents extends Event {

        // Events from game settings window
        public static const GAME_SETTINGS_RESUME:String = "gameSettingsResume";
        public static const GAME_SETTINGS_RESTART:String = "gameSettingsRestart";
        public static const GAME_SETTINGS_BACK_TO_LEVEL_SELECTION:String = "gameSettingsBackToLevelSelection";

        // Event from ... window
        private var _data: Object;

        public function WindowActionEvents(type: String, data: Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new WindowActionEvents(type, _data, bubbles, cancelable);
        }
    }
}
package com.windows_system.windows.confirm_arert {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;
    import com.windows_system.core.windowManager.BaseWindow;

import flash.display.Sprite;

import flash.events.MouseEvent;
import flash.text.TextField;

import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    public class ConfirmAlertPopup extends BaseWindow {

        protected var _confirmButton: LabelButton;
        protected var _cancelButton: LabelButton;


        public function ConfirmAlertPopup() {
            super(new View_ConfirmAlert());
        }


        override public function updateData(data: Object): void {
            super.updateData(data);

            var header: Sprite = getSprite("header");
            var txtHolder: Sprite = getSprite("txtHolder", header);
            var txt: TextField = getTextField("txt", txtHolder);

            txt.text = ConfirmAlertData(_data.data).title;
        }


        override protected function initButtons(): void {
            super.initButtons();

            _confirmButton = new LabelButton(getSprite("confirmButton"));
            _confirmButton.label = "OK";
            _confirmButton.addEventListener(MouseEvent.CLICK, onConfirmButtonClickHandler);

            _cancelButton = new LabelButton(getSprite("cancelButton"));
            _cancelButton.label = "CANCEL";
            _cancelButton.addEventListener(MouseEvent.CLICK, onCancelButtonClickHandler);
        }


        protected function onConfirmButtonClickHandler(event: MouseEvent): void {
            closeMe();
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.EXECUTE_CONFIRM_ALERT_ACTIOM, ConfirmAlertData(_data.data).confirmEvent));
        }


        protected function onCancelButtonClickHandler(event: MouseEvent): void {
            closeMe();
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.EXECUTE_CONFIRM_ALERT_ACTIOM, ConfirmAlertData(_data.data).cancelEvent));
        }

    }
}

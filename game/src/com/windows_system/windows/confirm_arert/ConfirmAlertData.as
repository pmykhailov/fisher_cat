package com.windows_system.windows.confirm_arert {
    import flash.events.Event;

    /**
     * ConfirmAlertData class.
     * User: Paul Makarenko
     * Date: 17.10.13
     */
    public class ConfirmAlertData {

        private var _confirmEvent: Event;
        private var _cancelEvent: Event;
        private var _title: String;


        public function ConfirmAlertData(confirmEvent: Event, cancelEvent: Event, title:String) {
            _confirmEvent = confirmEvent;
            _cancelEvent = cancelEvent;
            _title = title;
        }


        public function get confirmEvent(): Event {
            return _confirmEvent;
        }


        public function get cancelEvent(): Event {
            return _cancelEvent;
        }


        public function get title(): String {
            return _title;
        }


        public function set title(value: String): void {
            _title = value;
        }
    }
}

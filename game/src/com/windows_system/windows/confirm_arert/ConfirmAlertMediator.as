package com.windows_system.windows.confirm_arert {
import com.screens.events.ScreensContextEvent;
import com.game.events.GameControlEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

public class ConfirmAlertMediator extends Mediator {
    public function ConfirmAlertMediator() {
        super();
    }


    override public function initialize():void {
        super.initialize();

        addViewListener(ScreensContextEvent.EXECUTE_CONFIRM_ALERT_ACTIOM, dispatch);
    }

}
}

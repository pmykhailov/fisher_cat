package com.windows_system.windows.final_level_complete {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;
    import com.windows_system.core.windowManager.BaseWindow;

import flash.display.Sprite;

import flash.events.MouseEvent;
import flash.text.TextField;

import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class FinalLevelCompletePopup extends BaseWindow {

        protected var _okButton: LabelButton;


        public function FinalLevelCompletePopup() {
            super(new View_FinalLevelComplete());
        }


        override protected function initButtons(): void {
            super.initButtons();

            _okButton = new LabelButton(getSprite("okButton"));
            _okButton.label = "OK";
            _okButton.addEventListener(MouseEvent.CLICK, onOKButtonClickHandler);
        }


        override protected function initFields(): void {
            super.initFields();

            var header: Sprite = getSprite("header");
            var txtHolder: Sprite = getSprite("txtHolder", header);
            var txt: TextField = getTextField("txt", txtHolder);

            txt.text = "Final level complete!";
        }


        protected function onOKButtonClickHandler(event: MouseEvent): void {
            closeMe();
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_CLOSE));
        }

    }
}

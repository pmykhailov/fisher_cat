package com.windows_system.windows.final_level_complete {
    import com.screens.events.ScreensContextEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    public class FinalLevelCompletePopupMediator extends Mediator {
        public function FinalLevelCompletePopupMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();
            addViewListener(ScreensContextEvent.LEVEL_CLOSE, dispatch);
        }
    }
}

package com.windows_system.windows.tutorial_feature_explain {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;
    import com.windows_system.core.windowManager.BaseWindow;

import flash.display.Sprite;

import flash.events.MouseEvent;
import flash.text.TextField;

import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class TutorialFeatureExplainPopup extends BaseWindow {

        protected var _okButton: LabelButton;


        public function TutorialFeatureExplainPopup() {
            super(new View_TutorialFeatureExplain());
        }


        override protected function initButtons(): void {
            super.initButtons();

            _okButton = new LabelButton(getSprite("okButton"));
            _okButton.label = "OK";
            _okButton.addEventListener(MouseEvent.CLICK, onOKButtonClickHandler);
        }


        override protected function initFields(): void {
            super.initFields();

            var header: Sprite = getSprite("header");
            var txtHolder: Sprite = getSprite("txtHolder", header);
            var txt: TextField = getTextField("txt", txtHolder);

            txt.text = "New feature unlock!";

            getTextField("title").htmlText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation";
        }


        protected function onOKButtonClickHandler(event: MouseEvent): void {
            closeMe();
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.FEATURE_EXPLAIN_OK));
        }

    }
}

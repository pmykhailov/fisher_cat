package com.windows_system.windows.tutorial_feature_explain {
    import com.screens.events.ScreensContextEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    public class TutorialFeatureExplainPopupMediator extends Mediator {
        public function TutorialFeatureExplainPopupMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();
            addViewListener(ScreensContextEvent.FEATURE_EXPLAIN_OK, dispatch);
        }
    }
}

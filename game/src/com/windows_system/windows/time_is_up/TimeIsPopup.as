package com.windows_system.windows.time_is_up {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;
    import com.windows_system.core.windowManager.BaseWindow;

import flash.display.Sprite;

import flash.events.MouseEvent;
import flash.text.TextField;

import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    public class TimeIsPopup extends BaseWindow {

        protected var _playAgainButton: LabelButton;
        protected var _quitButton: LabelButton;


        public function TimeIsPopup() {
            super(new View_TimeIsUp());
        }


        override protected function initButtons(): void {
            super.initButtons();

            _playAgainButton = new LabelButton(getSprite("playAgainButton"));
            _playAgainButton.label = "Play again";
            _playAgainButton.addEventListener(MouseEvent.CLICK, onPlayAgainButtonClickHandler);

            _quitButton = new LabelButton(getSprite("quitButton"));
            _quitButton.label = "Quit";
            _quitButton.addEventListener(MouseEvent.CLICK, onQuitButtonClickHandler);
        }


        override protected function initFields(): void {
            super.initFields();

            var header: Sprite = getSprite("header");
            var txtHolder: Sprite = getSprite("txtHolder", header);
            var txt: TextField = getTextField("txt", txtHolder);

            txt.text = "Time is up!";
        }


        protected function onPlayAgainButtonClickHandler(event: MouseEvent): void {
            closeMe();
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.PLAY_AGAIN_LEVEL));
        }


        protected function onQuitButtonClickHandler(event: MouseEvent): void {
            closeMe();
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_CLOSE));
        }

    }
}

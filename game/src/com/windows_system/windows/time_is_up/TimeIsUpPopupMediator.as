package com.windows_system.windows.time_is_up {
import com.screens.events.ScreensContextEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

public class TimeIsUpPopupMediator extends Mediator {
    public function TimeIsUpPopupMediator() {
        super();
    }


    override public function initialize():void {
        super.initialize();

        addViewListener(ScreensContextEvent.PLAY_AGAIN_LEVEL, dispatch);
        addViewListener(ScreensContextEvent.LEVEL_CLOSE, dispatch);
    }
}
}

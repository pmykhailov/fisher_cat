package com.windows_system.windows.settings {
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControlEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    public class SettingsPopupMediator extends Mediator {
        public function SettingsPopupMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();
            addViewListener(ScreensContextEvent.VOLUME_VALUE_CHANGED, dispatch);
            addViewListener(ScreensContextEvent.SETTINGS_CLOSE, dispatch);
        }
    }
}

package com.windows_system.windows.settings {
    import by.lord_xaoca.ui.components.buttons.LabelButton;
    import by.lord_xaoca.ui.components.scrolls.BaseScroll;
    import by.lord_xaoca.ui.components.scrolls.HorizontalScroller;

    import com.screens.events.ScreensContextEvent;
    import com.windows_system.core.windowManager.BaseWindow;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    public class SettingsPopup extends BaseWindow {

        protected var _okButton: LabelButton;
        private var _musicScroller: HorizontalScroller;
        private var _soundScroller: HorizontalScroller;


        public function SettingsPopup() {
            super(new View_Settings());
        }


        override public function updateData(data: Object): void {
            super.updateData(data);
            var popupData:Object = data.data;

            _musicScroller.value = popupData.musicVolume;
            _soundScroller.value = popupData.soundVolume;
        }


        override protected function initButtons(): void {
            super.initButtons();

            _okButton = new LabelButton(getSprite("okButton"));
            _okButton.label = "OK";
            _okButton.addEventListener(MouseEvent.CLICK, onOKButtonClickHandler);
        }


        override protected function initialize(): void {
            super.initialize();
            _initScrollers();
        }


        override protected function initFields(): void {
            super.initFields();
            getTextField("music").text = "Music";
            getTextField("sound").text = "Sound";
        }


        private function _initScrollers(): void {
            _musicScroller = new HorizontalScroller(getSprite("musicScroller"));
            _soundScroller = new HorizontalScroller(getSprite("soundScroller"));

            _musicScroller.addEventListener(Event.CHANGE, onMusicVolumeChangeHandler);
            _soundScroller.addEventListener(Event.CHANGE, onSoundVolumeChangeHandler);

            _musicScroller.addEventListener(BaseScroll.STOP_DRAG, onMusicStopDragHandler);
            _soundScroller.addEventListener(BaseScroll.STOP_DRAG, onSoundStopDragHandler);
        }


        protected function onOKButtonClickHandler(event: MouseEvent): void {
            closeMe();
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SETTINGS_CLOSE));
        }


        private function onMusicVolumeChangeHandler(event: Event): void {

        }


        private function onSoundVolumeChangeHandler(event: Event): void {
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.VOLUME_VALUE_CHANGED, {soundVolume: _soundScroller.value, musicVolume: _musicScroller.value}));
        }


        private function onSoundStopDragHandler(event: Event): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEMS_MATCHED);
        }


        private function onMusicStopDragHandler(event: Event): void {
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.VOLUME_VALUE_CHANGED, {soundVolume: _soundScroller.value, musicVolume: _musicScroller.value}));
        }
    }
}

package com.windows_system.windows.game_settings.controller {
    import com.game.events.GameControlEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * GameSettingPopupResumeCommand class.
     * User: Paul Makarenko
     * Date: 18.04.14
     */
    public class GameSettingPopupResumeCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;


        public function GameSettingPopupResumeCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.RESUME_GAME));
        }
    }
}
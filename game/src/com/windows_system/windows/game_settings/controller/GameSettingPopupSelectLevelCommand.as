package com.windows_system.windows.game_settings.controller {
    import com.screens.events.ScreensContextEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * GameSettingPopupSelectLevelCommand class.
     * User: Paul Makarenko
     * Date: 18.04.14
     */
    public class GameSettingPopupSelectLevelCommand extends Command {

        [Inject]
        public var dispatcher:IEventDispatcher;

        public function GameSettingPopupSelectLevelCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_CLOSE));
        }
    }
}
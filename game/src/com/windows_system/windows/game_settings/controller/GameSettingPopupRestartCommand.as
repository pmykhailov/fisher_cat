package com.windows_system.windows.game_settings.controller {
    import com.game_wrapper.event.GameWrapperContextEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * GameSettingPopupRestartCommand class.
     * User: Paul Makarenko
     * Date: 18.04.14
     */
    public class GameSettingPopupRestartCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;


        public function GameSettingPopupRestartCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            dispatcher.dispatchEvent(new GameWrapperContextEvent(GameWrapperContextEvent.RESTART_GAME));
        }
    }
}
package com.windows_system.windows.game_settings {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.windows_system.core.windowManager.BaseWindow;
    import com.windows_system.windows.events.WindowActionEvents;

    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.text.TextField;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class GameSettingsPopup extends BaseWindow {

        private static const RESUME_BUTTON: String = "resumeButton";
        private static const RESTART_BUTTON: String = "restartButton";
        private static const BACK_TO_LEVEL_SELECTION_BUTTON: String = "backToLevelSelectionButton";
        private var _resumeButton: LabelButton;
        private var _restartButton: LabelButton;
        private var _backToLevelSelectionButton: LabelButton;


        public function GameSettingsPopup() {
            super(new View_GameSettings());
        }


        override protected function initButtons(): void {
            super.initButtons();
            _resumeButton = new LabelButton(getMovieClip(RESUME_BUTTON));
            _restartButton = new LabelButton(getMovieClip(RESTART_BUTTON));
            _backToLevelSelectionButton = new LabelButton(getMovieClip(BACK_TO_LEVEL_SELECTION_BUTTON));

            _resumeButton.label = "RESUME";
            _restartButton.label = "RESTART";
            _backToLevelSelectionButton.label = "SELECT LEVEL";

            _resumeButton.addEventListener(MouseEvent.CLICK, onButtonClickHandler);
            _restartButton.addEventListener(MouseEvent.CLICK, onButtonClickHandler);
            _backToLevelSelectionButton.addEventListener(MouseEvent.CLICK, onButtonClickHandler);
        }


        override protected function initFields(): void {
            super.initFields();

            var header: Sprite = getSprite("header");
            var txtHolder: Sprite = getSprite("txtHolder", header);
            var txt: TextField = getTextField("txt", txtHolder);

            txt.text = "GAME SETTINGS";
        }


        override protected function onCloseButtonClickHandler(event: MouseEvent): void {
            super.onCloseButtonClickHandler(event);
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new WindowActionEvents(WindowActionEvents.GAME_SETTINGS_RESUME))
        }


        private function onButtonClickHandler(event: MouseEvent): void {
            var targetName: String = (event.target.name as String).split("_")[0];

            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            closeMe();

            switch (targetName) {
                case RESTART_BUTTON:
                    dispatchEvent(new WindowActionEvents(WindowActionEvents.GAME_SETTINGS_RESTART));
                    break;

                case RESUME_BUTTON:
                    dispatchEvent(new WindowActionEvents(WindowActionEvents.GAME_SETTINGS_RESUME));
                    break;

                case BACK_TO_LEVEL_SELECTION_BUTTON:
                    dispatchEvent(new WindowActionEvents(WindowActionEvents.GAME_SETTINGS_BACK_TO_LEVEL_SELECTION));
                    break;
            }
        }
    }
}

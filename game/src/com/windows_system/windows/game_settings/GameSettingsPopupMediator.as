package com.windows_system.windows.game_settings {
    import com.windows_system.windows.events.WindowActionEvents;

    import robotlegs.bender.bundles.mvcs.Mediator;

    public class GameSettingsPopupMediator extends Mediator {
        public function GameSettingsPopupMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();
            addViewListener(WindowActionEvents.GAME_SETTINGS_RESUME, dispatch);
            addViewListener(WindowActionEvents.GAME_SETTINGS_RESTART, dispatch);
            addViewListener(WindowActionEvents.GAME_SETTINGS_BACK_TO_LEVEL_SELECTION, dispatch);
        }
    }
}

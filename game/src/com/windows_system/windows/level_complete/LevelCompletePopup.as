package com.windows_system.windows.level_complete {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;
    import com.windows_system.core.windowManager.BaseWindow;

import flash.display.Sprite;

import flash.events.MouseEvent;
import flash.text.TextField;

import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    public class LevelCompletePopup extends BaseWindow {

        protected var _okButton: LabelButton;


        public function LevelCompletePopup() {
            super(new View_LevelComplete());
        }


        override public function updateData(data: Object): void {
            super.updateData(data);
            var popupData: Object = data.data;
            getTextField("score").text = popupData.score;
        }


        override protected function initButtons(): void {
            super.initButtons();

            _okButton = new LabelButton(getSprite("nextLevelButton"));
            _okButton.label = "Next level";
            _okButton.addEventListener(MouseEvent.CLICK, onOKButtonClickHandler);
        }


        override protected function initFields(): void {
            super.initFields();

            var header: Sprite = getSprite("header");
            var txtHolder: Sprite = getSprite("txtHolder", header);
            var txt: TextField = getTextField("txt", txtHolder);

            txt.text = "Level complete!";
        }


        protected function onOKButtonClickHandler(event: MouseEvent): void {
            closeMe();
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.PLAY_NEXT_LEVEL));
        }
    }
}

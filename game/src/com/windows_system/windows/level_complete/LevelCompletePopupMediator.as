package com.windows_system.windows.level_complete {
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControlEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    public class LevelCompletePopupMediator extends Mediator {
        public function LevelCompletePopupMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();
            addViewListener(ScreensContextEvent.PLAY_NEXT_LEVEL, dispatch);
        }
    }
}

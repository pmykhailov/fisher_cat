package com.windows_system.core {
    import com.windows_system.*;

    import com.screens.events.ScreensContextEvent;
    import com.windows_system.core.windowManager.IWindow;
    import com.windows_system.core.windowManager.WindowEvent;
    import com.windows_system.core.windowManager.WindowLayer;
    import com.windows_system.core.windowManager.WindowManager;

    import flash.display.Sprite;

    import robotlegs.bender.bundles.mvcs.Mediator;

    /**
     * WindowService class.
     * Ideology not the best, but looks like best solution, cause we need to handle resize and create command for this
     * looks pretty expensive, but only mediators can listen context events, so we define it like singleton-service, but
     * extends Mediator class.
     * @author: Ivan Shaban
     * @date: 28.05.13 11:45
     */

    public class WindowService extends Mediator {

        // probably better inject window factory
        private var _factory: WindowFactory;
        private var _manager: WindowManager;
        private var _lastWindow: IWindow;
        private var _winData: Array = [];
        private var _lightPopups: Array = [
                /*
            WindowTypes.BUY_CURRENCY_POPUP,
            WindowTypes.MY_PROFILE,
            WindowTypes.FRIEND_PROFILE,
            WindowTypes.HIGH_WIN_POPUP,
            WindowTypes.TIME_BONUS_POPUP,
            WindowTypes.TOWER_BONUS_POPUP,
            WindowTypes.NAVIGATOR_FLOOR_HOVER,
            WindowTypes.NAVIGATOR_LEVEL_HOVER,
            WindowTypes.FROM_TO_HOVER,
            WindowTypes.USER_PROFILE_HOVER,
            WindowTypes.FRIEND_PROFILE_HOVER,
            WindowTypes.BUY_BTN_HOVER,
            WindowTypes.EARN_BTN_HOVER,
            WindowTypes.BALANCE_BAR_HOVER
            */
        ];


        public function WindowService() {
            _factory = new WindowFactory();

            _manager = new WindowManager(new Sprite());
            _manager.addEventListener(WindowEvent.ADDED, onWindowAddedHandler);
            _manager.addEventListener(WindowEvent.REMOVED, onWindowRemovedHandler);
        }


        public function get view(): Sprite {
            return _manager.view;
        }


        [PostConstruct]
        override public function initialize(): void {
            addContextListener(ScreensContextEvent.ADD_WINDOW, onWindowShowHandler);
            addContextListener(ScreensContextEvent.SHOW_NEXT_WINDOW, onWindowShowNextHandler);
            addContextListener(ScreensContextEvent.REMOVE_WINDOW, onWindowRemoveHandler);
            //addContextListener(CoreEvent.RESIZE, onResizeHandler);
        }


        private function _displayNextWindow(): void {

            if (_winData.length) {

                for (var i: int = 0; i < _winData.length; i++) {

                    var data: Object = _winData[i];
                    var priority: int = WindowManager.PRIORITY_LIGHT;
                    var doEnable: Boolean = true;

                    if (data.hasOwnProperty("priority")) {
                        priority = data.priority;
                    }

                    if (data.hasOwnProperty("enable")) {
                        doEnable = data.enable;
                    }

                    var isModal: Boolean = data.hasOwnProperty("isModal") ? data.isModal : true;
                    var windowLayer: WindowLayer = _manager.getPriorityLayer(priority);
                    if (windowLayer.hasModalWindow() && isModal) {
                        continue;
                    }
                    else {

                        _winData.splice(i, 1);
                        i--;
                    }

                    var window: IWindow = _factory.getWindow(data.type);
                    if (window) {
                        window.updateData(data);

                        _manager.addWindow(window, priority, data.isModal == undefined ? true : isModal, false, doEnable);
                    }
                }

            }
        }


        private function onWindowShowHandler(event: ScreensContextEvent): void {

            var data: Object = event.data;
            if (data == null) {
                return;
            }

            var isModal: Boolean = data.hasOwnProperty("isModal") ? data.isModal : true;

            if (isModal) {
                for each (var winType: String in _lightPopups) {
                    _manager.removeIfOpen(winType);
                }
            }

            _winData.push(data);
            if (!data.hasOwnProperty("show") || data.show) {
                _displayNextWindow();
            }
        }


        private function onWindowRemoveHandler(event: ScreensContextEvent): void {
            _manager.removeIfOpen(event.data.type);
        }


        private function onResizeHandler(event: ScreensContextEvent): void {
            _manager.resize(event.data.width, event.data.height);
        }


        private function onWindowAddedHandler(event: WindowEvent): void {
            _lastWindow = event.data as IWindow;
            dispatch(event);
        }


        private function onWindowShowNextHandler(event: ScreensContextEvent): void {
            _displayNextWindow();
        }


        private function onWindowRemovedHandler(event: WindowEvent): void {
            _lastWindow = null;
            dispatch(event);
            _displayNextWindow();
        }

    }
}

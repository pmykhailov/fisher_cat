package com.windows_system.core {
    import flash.display.Sprite;

    /**
     * WindowLayerView class.
     *
     * @author: Ivan Shaban
     * @date: 31.05.13 0:06
     */

    public class WindowLayerView extends Sprite {

        public function WindowLayerView() {
            mouseEnabled = false;
        }

    }
}

package com.windows_system.core.windowManager {
    import flash.events.Event;

    /**
     * WindowEvent__ class.
     *
     * @author Ivan Shaban
     * @date 04.10.2011 17:47
     */
    public class WindowEvent extends Event {

        // ------------------ STATIC VARIABLES -------------------------

        public static const OPEN_NEW_WINDOW: String = "windowOpenNewWindow";
        public static const ACCEPT: String = "windowAccept";
        public static const CANCEL: String = "windowCancel";
        public static const CLOSE: String = "windowClose";
        public static const REMOVED: String = "windowRemoved";
        public static const ADDED: String = "windowAdded";

        public static const SELECT: String = "windowSelect";
        public static const SUCCESS: String = "windowSuccess";
        public static const INVITE: String = "windowInvite";

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        private var __data: Object;

        // ------------------ CONSTRUCTOR ------------------------------

        public function WindowEvent(type: String, data: Object = null) {
            super(type, false, false);
            this.__data = data;
        }


        // ------------------ PROPERTIES -------------------------------

        public function get data(): Object {
            return __data;
        }


        // ------------------ PUBLIC METHODS ---------------------------

        public override function clone(): Event {
            return new WindowEvent(type, __data);
        }


        public override function toString(): String {
            return formatToString("WindowEvent", "type", "bubbles", "cancelable", "eventPhase");
        }


        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------
    }
}
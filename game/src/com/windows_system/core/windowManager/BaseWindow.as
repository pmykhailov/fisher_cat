package com.windows_system.core.windowManager {

    import by.lord_xaoca.robotlegs2.BaseView;
    import by.lord_xaoca.ui.components.buttons.MovieClipButton;
    import by.lord_xaoca.utils.Painter;

    import com.greensock.TweenLite;
    import com.greensock.easing.Expo;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Matrix;
    import flash.geom.Rectangle;
    import flash.text.TextField;

    /**
     * BaseWindow class.
     *
     * @author: Ivan Shaban
     * @date: 02.04.13 16:30
     */

    [Event(name="windowClose", type="com.yarkistudio.tower.library.windowManager.WindowEvent")]
    [Event(name="windowAccept", type="com.yarkistudio.tower.library.windowManager.WindowEvent")]
    [Event(name="windowCancel", type="com.yarkistudio.tower.library.windowManager.WindowEvent")]
    [Event(name="windowSelect", type="com.yarkistudio.tower.library.windowManager.WindowEvent")]
    [Event(name="windowRemoved", type="com.yarkistudio.tower.library.windowManager.WindowEvent")]
    public class BaseWindow extends BaseView implements IWindow {

        protected var _isModal: Boolean;
        protected var _isOpen: Boolean;
        protected var _isPermanent: Boolean;
        protected var _isAutoClosable: Boolean = false;
        protected var _isEscapeClosable: Boolean = false;
        protected var _windowID: int;
        protected var _bg: DisplayObject;
        protected var _showEffectDuration: Number = 0.3;
        protected var _hideEffectDuration: Number = 0.3;
        protected var _closeBtn: MovieClipButton;
        protected var _sheet: Bitmap;
        protected var _sheetMatrix: Matrix;
        protected var _sheetRect: Rectangle;
        protected var _titleTf: TextField;
        protected var _data: Object;


        public function BaseWindow(view: DisplayObject) {
            super(view);
        }


        override public function get width(): Number {
            return _bg.width;
        }


        override public function get height(): Number {
            return _bg.height;
        }


        public function get type(): Object {
            return _data && _data.hasOwnProperty("type") ? _data.type : null;
        }


        public function get windowID(): int {
            return _windowID;
        }


        /**
         * Type of window for placement it in WindowManager.
         * For windows default value is "windowCenter".
         */
        public function get windowType(): String {
            return WindowManager.CENTER_WINDOW;
        }


        /**
         * Can be closed by mouseclick out of window or no
         */
        public function get isAutoClosable(): Boolean {
            return _isAutoClosable;
        }


        /**
         * Can be closed by Esc pressing
         */
        public function get isEscapeClosable(): Boolean {
            return _isEscapeClosable;
        }


        public function get isPermanent(): Boolean {
            return _isPermanent;
        }


        public function get isModal(): Boolean {
            return _isModal;
        }


        public function get isOpen(): Boolean {
            return _isOpen;
        }


        public function get enable(): Boolean {
            return _view.mouseChildren;
        }


        public function set enable(value: Boolean): void {
            _view.mouseChildren = value;
        }


        public function set title(value: String): void {
            if (_titleTf) {
                _titleTf.text = value;
            }
        }


        public function updateData(data: Object): void {
            _data = data;
        }


        public function close(): void {
            if (!_view.parent) {
                _sheet.parent.removeChild(_sheet);
            } else {
                _view.parent.removeChild(_view);
            }

            !_isOpen && dispatchEvent(new WindowEvent(WindowEvent.CLOSE));
        }


        public function show(): void {
            mouseChildren = true;
            _isOpen = true;

            prepareSheet();
            addSheet();
            renderSheet();

            _sheet.alpha = 0;

            tweenShow();
        }


        public function hide(): void {
            mouseChildren = false;
            _isOpen = false;

            prepareSheet();
            addSheet();
            renderSheet();

            _sheet.alpha = 1;

            TweenLite.killTweensOf(_sheet);
            TweenLite.to(_sheet, _hideEffectDuration, {alpha: 0, onComplete: close});
        }


        public function setup(windowID: uint, isModal: Boolean): void {
            _windowID = windowID;
            _isModal = isModal;
        }


        public function closeMe(): void {
            dispatchEvent(new WindowEvent(WindowEvent.CLOSE));
        }


        override protected function initialize(): void {
            super.initialize();

            initBG();
            initCloser();
            initFields();
            initButtons();
            initSheet();
        }


        protected function tweenShow(): void {
            TweenLite.killTweensOf(_sheet);
            TweenLite.to(_sheet, _showEffectDuration, {alpha: 1, onComplete: removeSheet, ease: Expo.easeIn});
        }


        protected function initFields(): void {
            _titleTf = getTextField("title");
        }


        protected function initButtons(): void {
            // ABSTRACT
        }


        protected function initBG(): void {
            _bg = Painter.rasterize(getMovieClip("bg"), true);
        }


        protected function initSheet(): void {
            _sheetMatrix = new Matrix();
            _sheetRect = new Rectangle();
            _sheet = new Bitmap();
            _sheet.smoothing = true;
        }


        protected function prepareSheet(): void {
            var bounds: Rectangle = _view.getBounds(_view);
            bounds.width *= _view.scaleX;
            bounds.height *= _view.scaleY;

            _sheetMatrix.createBox(_view.scaleX, _view.scaleY, 0, -bounds.x, -bounds.y);
            _sheetRect.width = bounds.width;
            _sheetRect.height = bounds.height;

            _sheet.bitmapData && _sheet.bitmapData.dispose();
            _sheet.bitmapData = new BitmapData(bounds.width, bounds.height, true, 0xFFFFFFF);
            _sheet.x = _view.x + bounds.x;
            _sheet.y = _view.y + bounds.y;
        }


        protected function addSheet(): void {
            var parent: DisplayObjectContainer = _view.parent;
            if (parent) {
                parent.addChildAt(_sheet, parent.getChildIndex(_view));
                parent.removeChild(_view);
            }
        }


        protected function removeSheet(): void {
            var parent: DisplayObjectContainer = _sheet.parent;
            if (parent) {
                parent.addChildAt(_view, parent.getChildIndex(_sheet));
                parent.removeChild(_sheet);
            }
        }


        private function initCloser(): void {
            var asset: MovieClip = getChildByName("close_btn") as MovieClip;
            if (!asset) {
                return;
            }
            _closeBtn = new MovieClipButton(asset);
            _closeBtn.addEventListener(MouseEvent.CLICK, onCloseButtonClickHandler);
        }


        protected function renderSheet(event: Event = null): void {
            _sheet.bitmapData.fillRect(_sheetRect, 0xFFFFFF);
            _sheet.bitmapData.draw(_view, _sheetMatrix);
        }


        protected function onCloseButtonClickHandler(event: MouseEvent): void {
            closeMe();
        }
    }
}

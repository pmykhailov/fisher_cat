package com.windows_system.core.windowManager {
    import flash.display.DisplayObject;
    import flash.display.Sprite;

    public class WindowLayer extends Sprite {
        private var __rate: int;
        private var __backGround: Sprite;
        private var _windows: Array = [];


        public function WindowLayer(id: int, backGround: Sprite) {
            __backGround = backGround;
            __rate = id * 10000;
            name = "windowLayer" + id;

        }


        override public function addChild(child: DisplayObject): DisplayObject {
            if (child != __backGround) {
                __rate++;
            }
            return super.addChild(child);
        }


        public function get rate(): int {
            return __rate;
        }


        public function hasModalWindow(): Boolean {
            for (var i: int = 0; i < _windows.length; i++) {
                var window: IWindow = _windows[i];

                if (window.isModal) {
                    return true;
                }
            }

            return false;

        }


        public function addWindow(value: IWindow): void {

            _windows.push(value);
            value.addEventListener(WindowEvent.CLOSE, onWindowClose, false, 1000, true);
            addChild(value.view);
        }


        private function onWindowClose(event: WindowEvent): void {
            for (var i: int = 0; i < _windows.length; i++) {

                var window: IWindow = _windows[i];
                if (window == event.target) {
                    _windows.splice(i, 1);
                    return;
                }
            }

        }
    }
}
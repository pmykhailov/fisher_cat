package com.windows_system.core.windowManager {
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import com.greensock.TweenLite;
    import com.share.ApplicationDimensions;

    import flash.display.DisplayObjectContainer;
    import flash.display.Graphics;
    import flash.display.Sprite;
    import flash.errors.IllegalOperationError;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.ui.Keyboard;

    /**
     * WindowManager class.
     *
     * @author Ivan Shaban
     * @date 04.10.2011 17:44
     */
    [Event(name="windowAdded", type="com.yarkistudio.tower.library.windowManager.WindowEvent")]
    [Event(name="windowRemoved", type="com.yarkistudio.tower.library.windowManager.WindowEvent")]
    public class WindowManager extends BaseDisplayObjectContainer {

        /**
         * Window align types.
         */
        public static const GENERAL_WINDOW: String = "windowGeneral";
        public static const BOTTOM_WINDOW: String = "windowBottom";
        public static const BOTTOM_RIGHT_WINDOW: String = "windowBottomRight";
        public static const CENTER_WINDOW: String = "windowCenter";
        public static const INDEPENDENT_WINDOW: String = "windowIndependent";
        /**
         * Priority levels.
         */
        public static var PRIORITY_DEFAULT: int = -1;
        public static var PRIORITY_NONE: int = 0;
        public static var PRIORITY_LIGHT: int = 1;
        public static var PRIORITY_MEDIUM: int = 2;
        public static var PRIORITY_HIGH: int = 3;
        public static var PRIORITY_VERY_HIGH: int = 4;
        public static var PRIORITY_MAXIMUM: int = 5;

        //        public static const STAGE_OFFSET: int = 5;
        //        public static const TOP_OFFSET: int = 100;
        [ArrayElementType("com.yarkistudio.tower.library.windowManager.IWindow")]
        private var __windows: Array;
        [ArrayElementType("Object")]
        private var __queueWindows: Array;
        private var __bg: Sprite;
        private var __isWindowOpened: Boolean;


        public function WindowManager(view: Sprite) {
            super(view);
        }


        public function get enable(): Boolean {
            return mouseChildren;
        }


        public function set enable(value: Boolean): void {
            mouseChildren = value;
            mouseEnabled = value;
        }


        public function resize(width: int, height: int): void {
            var g: Graphics = __bg.graphics;
            g.clear();
            g.beginFill(0x000000, 0.8);
            g.drawRect(0, 0, width, height);
            g.endFill();

            var len: int = __windows.length;
            for (var i: int = 0; i < len; i++) {
                placeWindow(__windows[i]);
            }
        }


        //        public function isOpenByType(type: String): Boolean {
        //                var window: IWindow;
        //                var i: int;
        //                var len: int = __windows.length;
        //                for (i = 0; i < len; i++) {
        //                    window = __windows[i];
        //                    if (window.windowType == type) {
        //                        return true;
        //                    }
        //                }
        //
        //            return false;
        //        }

        public function getFirstWindowOfType(type: String): IWindow {
            var window: IWindow;
            var i: int;
            var len: int = __windows.length;
            for (i = 0; i < len; i++) {
                window = __windows[i];
                if (window.type == type) {
                    return window;
                }
            }

            return null;
        }


        public function addWindow(value: IWindow, priority: int = -1, isModal: Boolean = true, isInQueue: Boolean = false, enable: Boolean = true): IWindow {
            if (priority == WindowManager.PRIORITY_DEFAULT) {
                priority = WindowManager.PRIORITY_LIGHT;
            }
            resize(ApplicationDimensions.WIDTH, ApplicationDimensions.HEIGHT);
            return add(value, priority, isModal, isInQueue, enable);
        }


        public function addPopup(value: IWindow, priority: int = -1, isModal: Boolean = false, isInQueue: Boolean = false): IWindow {
            if (priority == WindowManager.PRIORITY_DEFAULT) {
                priority = WindowManager.PRIORITY_MEDIUM;
            }

            return add(value, priority, isModal, isInQueue);
        }


        public function killWindows(killPermanent: Boolean = false, killFromQueue: Boolean = true, killOpened: Boolean = true): void {
            if (killFromQueue) {
                var newQueue: Array = [];
                for (var j: int = 0; j < __queueWindows.length; j++) {
                    if (!killPermanent && __queueWindows[j].value.isPermanent) {
                        newQueue.push(__queueWindows[j]);
                    }
                }
                __queueWindows = newQueue;
            }
            if (killOpened) {
                for (var i: int = 0; i < __windows.length; i++) {
                    if (killPermanent || !__windows[i].isPermanent) {
                        remove(__windows[i]);
                    }
                }
            }
        }


        public function add(value: IWindow, priority: int = 0, isModal: Boolean = true, isInQueue: Boolean = false, doEnable: Boolean = true): IWindow {
            if (!value) {
                return value;
            }
            if (isInQueue && __windows.length) {
                __queueWindows.push({value: value, priority: priority, isModal: isModal, doEnable: doEnable});
                return value;
            }
            if (priority == WindowManager.PRIORITY_DEFAULT) {
                priority = WindowManager.PRIORITY_NONE;
            }

            var layer: WindowLayer = getPriorityLayer(priority);

            /**
             * If window already exist, just reopen most important window.
             */
            if (__windows.indexOf(value) == -1) {
                __windows.push(value);
            }
            __isWindowOpened = true;
            enable = doEnable;
            placeWindow(value);

            layer.addWindow(value);

            value.setup(layer.rate, isModal);
            value.addEventListener(WindowEvent.CLOSE, onWindowHandler, false, 100);
            dispatchEvent(new WindowEvent(WindowEvent.ADDED, value));

            if (isModal) {
                __addBG();
            }

            value.show();

            return value;
        }


        public function removeIfOpen(type: String): void {
            var window: IWindow = getFirstWindowOfType(type);
            window && remove(window);
        }


        public function remove(value: IWindow): IWindow {
            for (var i: int = 0; i < __queueWindows.length; i++) {
                if (__queueWindows[i].value == value) {
                    __queueWindows.splice(i, 1);
                    return null;
                }
            }
            var indexW: int = __windows.indexOf(value);
            if (indexW == -1) {
                return null;
            }

            __windows.splice(indexW, 1);

            /**
             * If have anything else...
             */
            if (__windows.length) {
                // if there are only popups in stack — topWindow == null, cause popup is not modal
                // and there are some non-modal windows.
                var topWindow: IWindow = getMaxRatedModalWindow();
                enable = true;
                if (topWindow) {
                    var windowParent: WindowLayer = WindowLayer(topWindow.view.parent);
                    if (windowParent) {
                        if (__bg.parent) {
                            __bg.parent.removeChild(__bg);
                        }
                        windowParent.addChildAt(__bg, windowParent.getChildIndex(topWindow.view));
                        TweenLite.to(__bg, 0.3, { alpha: 1 });
                    }
                } else {
                    removeBG();
                }
            } else {
                enable = false;
                __isWindowOpened = false;

                removeBG();
            }

            value.hide();
            value.removeEventListener(WindowEvent.CLOSE, onWindowHandler);
            dispatchEvent(new WindowEvent(WindowEvent.REMOVED, value));

            if (!__windows.length && __queueWindows.length) {
                var obj: Object = __queueWindows.shift();
                add(obj.value, obj.priority, obj.isModal, true, obj.doEnable);
            }

            return value;
        }


        public function placeWindow(value: IWindow): void {
            switch (value.windowType) {
                case WindowManager.GENERAL_WINDOW:
                {
                    //                    value.x = __bg.width - value.width >> 1;
                    //                    var bottomOff: int = __bg.height - value.height - WindowManager.TOP_OFFSET;
                    //                    if (bottomOff < WindowManager.TOP_OFFSET) {
                    //                        value.y = (__bg.height - value.height) / 2;
                    //                    } else {
                    //                        value.y = WindowManager.TOP_OFFSET;
                    //                    }
                    break;
                }

                case WindowManager.BOTTOM_WINDOW:
                {
                    //                    if (Globals.useMirroring) {
                    //                        value.x = int(WindowManager.__bg.width - value.width - WindowManager.STAGE_OFFSET);
                    //                    } else {
                    //                        value.x = WindowManager.STAGE_OFFSET;
                    //                    }
                    //                    value.y = int(__bg.height - value.height - value.bgYOffset - WindowManager.STAGE_OFFSET);
                    break;
                }
                case WindowManager.BOTTOM_RIGHT_WINDOW:
                {
					
					value.x = __bg.width - value.width;
					value.y = __bg.height - value.height;
					
                    //                    if (Globals.useMirroring) {
                    //                        value.x = int(WindowManager.__bg.width - value.width - WindowManager.STAGE_OFFSET);
                    //                    } else {
                    //                        value.x = WindowManager.STAGE_OFFSET;
                    //                    }
                    //                    value.y = int(__bg.height - value.height - value.bgYOffset - WindowManager.STAGE_OFFSET);
                    break;
                }

                case WindowManager.CENTER_WINDOW:
                {
                    value.x = __bg.width - value.width >> 1;
                    value.y = __bg.height - value.height >> 1;
                    //                    value.y = int((Sizes.TABLE_HEIGHT - value.height) / 2) + Sizes.CAREER_SECTION_HEIGHT;
                    //                    value.x = int((__bg.width - value.width) / 2);
                    break;
                }

                case WindowManager.INDEPENDENT_WINDOW:
                {
                    // do nothing...
                    break;
                }
            }
        }


        override protected function initialize(): void {
            super.initialize();

            __initBG();
            __initLayers();
            __initWindows();
            _addEventListeners()
        }


        private function removeBG(): void {
            var parent: Sprite = __bg.parent as Sprite;
            if (parent) {
                TweenLite.to(__bg, 0.3, { alpha: 0, onComplete: parent.removeChild, onCompleteParams: [__bg] });
            }
        }


        private function _addEventListeners(): void {
            addStageListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
        }


        private function __initBG(): void {
            __bg = new Sprite();
            __bg.alpha = 0;
            __bg.addEventListener(MouseEvent.CLICK, bgClickHandler);
        }


        private function __initLayers(): void {
            addChild(new WindowLayer(WindowManager.PRIORITY_NONE, __bg));
            addChild(new WindowLayer(WindowManager.PRIORITY_LIGHT, __bg));
            addChild(new WindowLayer(WindowManager.PRIORITY_MEDIUM, __bg));
            addChild(new WindowLayer(WindowManager.PRIORITY_HIGH, __bg));
            addChild(new WindowLayer(WindowManager.PRIORITY_VERY_HIGH, __bg));
            addChild(new WindowLayer(WindowManager.PRIORITY_MAXIMUM, __bg));
        }


        private function __initWindows(): void {
            /**
             * Vector hasn't sortOn method.
             */
            __windows = [];
            __queueWindows = [];

        }


        /**
         * Get first high-rated MODAL window.
         *
         * FIX ME: Bug, when i have 1 window and i change type of modality, from modal to non-modal, and change priority to lower, then BG appear over window.
         * @return
         */
        private function getMaxRatedModalWindow(): IWindow {
            __windows.sortOn("windowID");
            var len: int = __windows.length;
            for (var i: int = len - 1; i > -1; i--) {
                if (__windows[i].isModal) {
                    return __windows[i] as IWindow;
                }
            }
            return null;
        }


        /**
         * Take layer based on priority level.
         * @param    id priority level.
         * @return
         */
        public function getPriorityLayer(id: uint): WindowLayer {
            switch (id) {
                case WindowManager.PRIORITY_NONE:
                case WindowManager.PRIORITY_LIGHT:
                case WindowManager.PRIORITY_MEDIUM:
                case WindowManager.PRIORITY_HIGH:
                case WindowManager.PRIORITY_VERY_HIGH:
                case WindowManager.PRIORITY_MAXIMUM:
                    return _view.getChildAt(id) as WindowLayer;
                default:
                    throw new IllegalOperationError("WindowManager#getPriorityLayer, Switch block, invalid value statement : " + id);
                    return null;
            }
        }


        private function __addBG(): void {
            var topWindow: IWindow = getMaxRatedModalWindow();
            var windowParent: DisplayObjectContainer = topWindow.view.parent;
            if (windowParent.contains(__bg)) {
                windowParent.addChildAt(__bg, windowParent.getChildIndex(topWindow.view) - 1);
            } else {
                windowParent.addChildAt(__bg, windowParent.getChildIndex(topWindow.view));
            }

            TweenLite.killTweensOf(__bg);
            TweenLite.to(__bg, 0.3, {alpha: 1});
        }


        private function onWindowHandler(event: WindowEvent): void {
            switch (event.type) {
                case WindowEvent.CLOSE:
                {
                    remove(event.currentTarget as IWindow);
                    break;
                }

                default:
                {
                }
            }
        }


        private function onKeyDownHandler(event: KeyboardEvent): void {
            if (!__windows.length || (event.keyCode != Keyboard.ESCAPE)) {
                return;
            }
            var window: IWindow = __windows[__windows.length - 1];
            if (window.isEscapeClosable) {
                remove(window);
            }
        }


        private function bgClickHandler(event: MouseEvent): void {
            if (!__windows.length) {
                return;
            }
            var len: int = __windows.length;
            var window: IWindow;
            for (var i: int = len - 1; i > -1; i--) {
                window = __windows[i];
                if (window.isModal && window.isAutoClosable) {
                    remove(window);
                    return;
                }
            }
        }

    }
}


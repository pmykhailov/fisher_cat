package com.windows_system.core.windowManager {
    import flash.display.Sprite;
    import flash.events.IEventDispatcher;

    /**
     * IWindow__ interface.
     *
     * @author Ivan Shaban
     * @date 03.10.20_11 20:43
     */
    public interface IWindow extends IEventDispatcher {

        function close(): void


        function hide(): void


        function show(): void


        function setup(windowID: uint, isModal: Boolean): void


        function updateData(data: Object): void;


        function get view(): Sprite


        function get windowType(): String


        function get windowID(): int


        function get isModal(): Boolean


        function get type(): Object


        /**
         * Can be closed by mouseclick out of window or no
         */
        function get isAutoClosable(): Boolean


        function get isOpen(): Boolean


        function get isPermanent(): Boolean


        function get width(): Number


        function get height(): Number


        function get x(): Number


        function set x(value: Number): void


        function get y(): Number


        function set y(value: Number): void


        //        function get type(): String

        function set enable(value: Boolean): void;


        /**
         * Can be closed by Esc pressing
         */
        function get isEscapeClosable(): Boolean;

    }

}
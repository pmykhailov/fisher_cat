package com.windows_system.core {
    import com.windows_system.core.windowManager.IWindow;
    import com.windows_system.windows.confirm_arert.ConfirmAlertPopup;
    import com.windows_system.windows.final_level_complete.FinalLevelCompletePopup;
    import com.windows_system.windows.game_settings.GameSettingsPopup;
    import com.windows_system.windows.level_complete.LevelCompletePopup;
    import com.windows_system.windows.settings.SettingsPopup;
    import com.windows_system.windows.time_is_up.TimeIsPopup;
    import com.windows_system.windows.tutorial_feature_explain.TutorialFeatureExplainPopup;

    /**
     * WindowFactory class.
     *
     * @author: Ivan Shaban
     * @date: 30.05.13 22:53
     */

    public class WindowFactory {

        private var _windows: Object = {};
        private var _windowClassList: Object = {};


        public function WindowFactory() {
            initialize();
        }


        public function getWindow(type: String): IWindow {
            return _windows[type] as IWindow || createWindow(type);
        }


        public function checkWindowType(type: String, instance: IWindow): Boolean {
            return instance && _windowClassList[type] && instance is _windowClassList[type];
        }


        protected function initialize(): void {
            _windowClassList[WindowTypes.LEVEL_COMPLETE] = LevelCompletePopup;
            _windowClassList[WindowTypes.FINAL_LEVEL_COMPLETE] = FinalLevelCompletePopup;
            _windowClassList[WindowTypes.TIME_IS_UP] = TimeIsPopup;
            _windowClassList[WindowTypes.CONFIRM_ALERT] = ConfirmAlertPopup;
            _windowClassList[WindowTypes.TUTORIAL_FEATURE_EXPLAIN] = TutorialFeatureExplainPopup;
            _windowClassList[WindowTypes.SETTINGS] = SettingsPopup;
            _windowClassList[WindowTypes.GAME_SETTINGS] = GameSettingsPopup;
        }


        protected function createWindow(type: String): IWindow {
            var windowClass: Class = _windowClassList[type] as Class;
            if (windowClass) {
                return _windows[type] = new windowClass() as IWindow;
            } else {
                return null;
            }
        }
    }
}

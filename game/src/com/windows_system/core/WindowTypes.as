package com.windows_system.core {
    public class WindowTypes {
        public static const LEVEL_COMPLETE: String = "levelComplete";
        public static const FINAL_LEVEL_COMPLETE: String = "finalLevelComplete";
        public static const TIME_IS_UP: String = "timeIsUp";
        public static const CONFIRM_ALERT: String = "confirmAlert";
        public static const TUTORIAL_FEATURE_EXPLAIN: String = "tutorialFeatureExplain";
        public static const SETTINGS: String = "model.level.settings";
        public static const GAME_SETTINGS: String = "gameSettings";
    }
}
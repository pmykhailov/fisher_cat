package com.tutorial.controller {
import com.game.events.GameControlEvent;
import com.share.events.GameWrapperControllEvent;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

public class TutorialFeatureExplainConfirmedCommand extends Command {

    [Inject]
    public var dispatcher: IEventDispatcher;


    public function TutorialFeatureExplainConfirmedCommand() {
        super();
    }


    override public function execute():void {
        super.execute();
        dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.PLAY_READY_GO_ANIMATION));
   }
}
}

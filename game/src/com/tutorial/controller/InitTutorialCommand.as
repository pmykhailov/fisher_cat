package com.tutorial.controller {
    import com.screens.events.ScreensContextEvent;
    import com.screens.view.layer.ScreensLayerView;
    import com.windows_system.core.WindowService;
    import com.windows_system.core.WindowTypes;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    /**
     * InitScreensCommand class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class InitTutorialCommand extends Command {

        [Inject]
        public var contextView:ContextView;


        public function InitTutorialCommand() {
            super();
        }


        override public function execute(): void {
            //contextView.view.addChild(screensLayerView.view);
        }
    }
}
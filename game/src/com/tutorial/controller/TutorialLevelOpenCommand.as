package com.tutorial.controller {
import com.screens.events.ScreensContextEvent;
import com.screens.model.LevelsModel;
import com.game.events.GameControlEvent;
import com.windows_system.core.WindowTypes;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

public class TutorialLevelOpenCommand extends Command {
    [Inject]
    public var event: ScreensContextEvent;
    [Inject]
    public var dispatcher: IEventDispatcher;
    [Inject]
    public var levelsModel:LevelsModel;

    public function TutorialLevelOpenCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        if (levelsModel.levelsHasNewFeature(event.data as int))
        {
            trace("Applying tutorial for level " + event.data);
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.ADD_WINDOW, {type:WindowTypes.TUTORIAL_FEATURE_EXPLAIN}));
        }
    }
}
}

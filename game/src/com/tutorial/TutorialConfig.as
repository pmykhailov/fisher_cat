package com.tutorial {
    import com.screens.events.ScreensContextEvent;
    import com.tutorial.controller.InitTutorialCommand;
    import com.tutorial.controller.TutorialFeatureExplainConfirmedCommand;
    import com.tutorial.controller.TutorialLevelOpenCommand;
    import com.tutorial.events.TutorialContextEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.api.IInjector;

    public class TutorialConfig implements IConfig {

        [Inject]
        public var context: IContext;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var mediatorMap: IMediatorMap;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: IInjector;
        [Inject]
        public var contextView: ContextView;


        public function configure(): void {

            mapCommands();
            mapMediators();
            mapInjections();

            context.afterInitializing(init);
        }


        private function mapCommands(): void {
            eventCommandMap.map(TutorialContextEvent.INIT).toCommand(InitTutorialCommand);

            eventCommandMap.map(ScreensContextEvent.LEVEL_OPEN).toCommand(TutorialLevelOpenCommand);
            eventCommandMap.map(ScreensContextEvent.FEATURE_EXPLAIN_OK).toCommand(TutorialFeatureExplainConfirmedCommand);
        }


        private function mapMediators(): void {
        }


        private function mapInjections(): void {
        }


        private function init(): void {
            dispatcher.dispatchEvent(new TutorialContextEvent(TutorialContextEvent.INIT));
        }
    }
}

package com.tutorial.events {
    import flash.events.Event;

    /**
     * ScreensContextEvent class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class TutorialContextEvent extends Event {

        public static const INIT: String = "initTutorial";

        private var _data: Object;


        public function TutorialContextEvent(type: String, data: Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new TutorialContextEvent(type, _data, bubbles, cancelable);
        }
    }
}
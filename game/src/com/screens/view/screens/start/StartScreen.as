package com.screens.view.screens.start {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;
    import com.screens.view.screens.base.BaseScreen;

    import flash.events.MouseEvent;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * StartScreen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class StartScreen extends BaseScreen {

        private var _playButton: LabelButton;
        private var _settingsButton: LabelButton;
        private var _moreGamesButton: LabelButton;


        public function StartScreen(type: String) {
            super(type);
        }


        override protected function initView(): void {
            super.initView();
            _initButtons();
        }


        private function _initButtons(): void {
            _playButton = new LabelButton(getMovieClip("playButton"));
            _playButton.label = "Play";
            _playButton.addViewListener(MouseEvent.CLICK, onPlayClickHandler);

            _settingsButton = new LabelButton(getMovieClip("settingsButton"));
            _settingsButton.label = "Settings";
            _settingsButton.addViewListener(MouseEvent.CLICK, onSettingsClickHandler);

            _moreGamesButton = new LabelButton(getMovieClip("moreGamesButton"));
            _moreGamesButton.label = "More games";

        }


        private function onPlayClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_SELECTION_SCREEN_OPEN));
        }


        private function onSettingsClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SETTINGS_OPEN));
        }
    }
}
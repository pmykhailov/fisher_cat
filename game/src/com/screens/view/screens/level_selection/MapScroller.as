package com.screens.view.screens.level_selection {
import by.lord_xaoca.helpers.StageReference;

import com.share.ApplicationDimensions;

import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Point;

public class MapScroller {

    private var _map:Sprite;
    private var _startY:Number;
    private var _startMapY:Number;
    private var _maxY:Number;
    private var _minY:Number;

    public function MapScroller(map:Sprite) {
        _map = map;
        init();
    }

    public function destroy():void {
        StageReference.stage.removeEventListener(MouseEvent.MOUSE_DOWN, onStageMouseDownHandler);
        StageReference.stage.removeEventListener(MouseEvent.MOUSE_UP, onStageMouseUpHandler);
        StageReference.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onStageMouseMoveHandler);
    }

    private function init():void {
        _maxY = 0;
        _minY = -(_map.height - ApplicationDimensions.HEIGHT);

        StageReference.stage.addEventListener(MouseEvent.MOUSE_DOWN, onStageMouseDownHandler);
        StageReference.stage.addEventListener(MouseEvent.MOUSE_UP, onStageMouseUpHandler);
    }

    private function onStageMouseDownHandler(event:MouseEvent):void {
        _startY = event.stageY;
        _startMapY = _map.y;

        StageReference.stage.addEventListener(MouseEvent.MOUSE_MOVE, onStageMouseMoveHandler);
    }

    private function onStageMouseUpHandler(event:MouseEvent):void {
        StageReference.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onStageMouseMoveHandler);
    }

    private function onStageMouseMoveHandler(event:MouseEvent):void {
        var scrolledDistance:Number = event.stageY - _startY;

        _map.y = _startMapY + scrolledDistance;

        if (_map.y < _minY) _map.y = _minY;
        if (_map.y > _maxY) _map.y = _maxY;
    }
}
}

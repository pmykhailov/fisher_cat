package com.screens.view.screens.level_selection {
import by.lord_xaoca.ui.BaseUIElement;
import by.lord_xaoca.utils.ClassUtils;

import com.game_wrapper.model.level.LevelModel;

import flash.display.MovieClip;
import flash.display.Sprite;

import flash.events.MouseEvent;
import flash.text.TextField;

/**
 * LevelIR class.
 * User: Paul Makarenko
 * Date: 08.10.13
 */
public class LevelIR extends BaseUIElement {

    public function LevelIR(view:Sprite) {
        super(view);
    }

    override protected function _fillData():void {
        super._fillData();

        var data:LevelModel = _data as LevelModel;

        buttonMode = true;

        mouseEnabled = !data.isLocked;
        data.isLocked ? alpha = 0.5 : alpha = 1;
        data.isLocked ? (_view as MovieClip).gotoAndStop("_disabled") : (_view as MovieClip).gotoAndStop("_enabled");
        addViewListener(MouseEvent.CLICK, dispatchEvent);
    }

    override protected function initView():void {
        super.initView();
        mouseChildren = false;
    }
}
}
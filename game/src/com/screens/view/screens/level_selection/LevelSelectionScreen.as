package com.screens.view.screens.level_selection {
import by.lord_xaoca.helpers.StageReference;
import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.game_wrapper.model.level.LevelModel;
    import com.screens.events.ScreensContextEvent;
    import com.screens.view.screens.base.BaseScreen;
    import com.screens.view.screens.enum.ScreensTypeEnum;
    import com.share.ApplicationDimensions;

import flash.display.MovieClip;

import flash.display.Sprite;
    import flash.events.MouseEvent;

    import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    /**
     * LevelSelectionScreen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class LevelSelectionScreen extends BaseScreen {

        private var _backToStartScreen: LabelButton;
        private var _mapScroller:MapScroller;

        public function LevelSelectionScreen(type: String) {
            super(type);
        }


        override protected function initView(): void {
            super.initView();
            _initButtons();
        }


        override protected function _fillData(): void {
            super._fillData();
            _initLevels();
            _initMapScroller();
        }


        private function _initMapScroller():void {
            _mapScroller = new MapScroller(getMovieClip("map") as Sprite);
        }

        private function _initLevels(): void {
            var map:MovieClip = getMovieClip("map");
            var levels:MovieClip = getMovieClip("levels", map);

            for (var i: int = 0; i < data.length; i++) {
                var level: LevelIR = new LevelIR(getSprite("level_" + i, levels));
                level.data = data[i];
                level.addEventListener(MouseEvent.CLICK, onLevelClickHandler);
            }
        }


        private function _initButtons(): void {
            _backToStartScreen = new LabelButton(getMovieClip("backToStartScreen"));
            _backToStartScreen.label = "Back";
            _backToStartScreen.addViewListener(MouseEvent.CLICK, onBackToStartScreenClickHandler);
        }


        private function onBackToStartScreenClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.START}));
        }


        private function onLevelClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_OPEN, ((event.target as LevelIR).data as LevelModel).number));
        }

        override public function destroy():void {
            super.destroy();

            _mapScroller.destroy();
        }
    }
}
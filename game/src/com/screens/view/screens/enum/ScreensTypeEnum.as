package com.screens.view.screens.enum  {

    /**
     * ScreensTypeEnum class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensTypeEnum {

        public static const START:String = "StartScreen";
        public static const LEVEL_SELECTION:String = "LevelSelectionScreen";
        public static const GAME:String = "GameScreen";

        public function ScreensTypeEnum() {
        }

    }
}
package com.screens.view.screens.game {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;
    import com.screens.view.screens.base.BaseScreen;
    import com.screens.view.screens.enum.ScreensTypeEnum;
    import com.game.events.GameControlEvent;
    import com.share.events.GameWrapperControllEvent;

    import flash.display.Sprite;
    import flash.events.MouseEvent;

    /**
     * LevelSelectionScreen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class GameScreen extends BaseScreen {
        private var _backToLevelSelectionScreen: LabelButton;
        private var _gameContainer: Sprite;


        public function GameScreen(type: String) {
            super(type);
        }


        public function get gameContainer(): Sprite {
            return _gameContainer;
        }


        override protected function initView(): void {
            super.initView();
            _initButtons();
            _initGameContainer();
        }


        private function _initGameContainer(): void {
            _gameContainer = getSprite("gameWrapperContainer");
        }


        private function _initButtons(): void {
            _backToLevelSelectionScreen = new LabelButton(getMovieClip("backToLevelSelectionScreen"));
            _backToLevelSelectionScreen.label = "Back";
            _backToLevelSelectionScreen.addViewListener(MouseEvent.CLICK, onBackToLevelSelectionScreenClickHandler);

            // Remove this button temporary
            removeChild(_backToLevelSelectionScreen.view);
        }


        private function onBackToLevelSelectionScreenClickHandler(event: MouseEvent): void {
                //dispatchEvent(new ScreensContextEvent(ScreensContextEvent.BACK_TO_LEVEL_SELECTION_FROM_GAME));
        }
    }
}
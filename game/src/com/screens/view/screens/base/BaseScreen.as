package com.screens.view.screens.base {
import by.lord_xaoca.ui.BaseUIElement;
import by.lord_xaoca.utils.ClassUtils;
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    /**
     * Screen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class BaseScreen extends BaseUIElement {

        public function BaseScreen(type: String) {
            super(ClassUtils.getInstanceByClassName("View_" + type));
        }


        public function show(): void {
            visible = true;
        }


        public function hide(): void {
            visible = false;
        }

    }
}
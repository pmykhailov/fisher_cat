package com.screens.view.factory  {
    import com.screens.view.screens.game.GameScreen;
    import com.screens.view.screens.level_selection.LevelSelectionScreen;
    import com.screens.view.screens.start.StartScreen;
    import com.screens.view.screens.base.BaseScreen;
    import com.screens.view.screens.enum.ScreensTypeEnum;

    /**
     * ScreensFactory class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensFactory {

        public function ScreensFactory() {
        }


        public function createScreen(type: String): BaseScreen {
            switch (type) {
                case ScreensTypeEnum.START:
                    return new StartScreen(type);
                    break;


                case ScreensTypeEnum.LEVEL_SELECTION:
                    return new LevelSelectionScreen(type);
                    break;


                case ScreensTypeEnum.GAME:
                    return new GameScreen(type);
                    break;
            }

            return null;
        }

    }
}
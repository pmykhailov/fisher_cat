package com.screens.view.layer {
    import by.lord_xaoca.robotlegs2.BaseView;

    import com.screens.events.ScreensContextEvent;
    import com.screens.view.factory.ScreensFactory;
    import com.screens.view.screens.base.BaseScreen;

    import flash.display.Sprite;

    /**
     * ScreensManager class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensLayerView extends BaseView {

        private var _activeScreen: BaseScreen;
        private var _factory: ScreensFactory;


        public function ScreensLayerView() {
            super(new Sprite());
        }


        public function get activeScreen(): BaseScreen {
            return _activeScreen;
        }


        public function showScreen(screenData: Object): void {
            if (_activeScreen) {
                //_activeScreen.hide();
                _activeScreen.destroy();
            }

            _activeScreen = _factory.createScreen(screenData.type);
            if (screenData.hasOwnProperty("data")) {
                _activeScreen.data = screenData.data;
            }

            addActiveScreenListeners();
            //_activeScreen.show();

            addChild(_activeScreen.view);
        }


        override protected function initialize(): void {
            super.initialize();

            _factory = new ScreensFactory();
        }


        private function addActiveScreenListeners(): void {
            _activeScreen.addEventListener(ScreensContextEvent.SHOW_SCREEN, dispatchEvent);
            _activeScreen.addEventListener(ScreensContextEvent.LEVEL_OPEN, dispatchEvent);
            _activeScreen.addEventListener(ScreensContextEvent.LEVEL_CLOSE, dispatchEvent);
            _activeScreen.addEventListener(ScreensContextEvent.LEVEL_SELECTION_SCREEN_OPEN, dispatchEvent);
            _activeScreen.addEventListener(ScreensContextEvent.SETTINGS_OPEN, dispatchEvent);
        }
    }
}
package com.screens.view.layer {
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControlEvent;
    import com.share.events.GameWrapperControllEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    /**
     * ScreenLayerMediator class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreenLayerMediator extends Mediator {

        [Inject]
        public var _screensLayer:ScreensLayerView;

        public function ScreenLayerMediator() {
            super();
        }

        override public function initialize(): void {
            super.initialize();

            addContextListener(ScreensContextEvent.SHOW_SCREEN, onShowScreenHandler);

            addViewListener(ScreensContextEvent.SHOW_SCREEN, dispatch);
            addViewListener(ScreensContextEvent.LEVEL_OPEN, dispatch);
            addViewListener(ScreensContextEvent.LEVEL_CLOSE, dispatch);
            addViewListener(ScreensContextEvent.LEVEL_SELECTION_SCREEN_OPEN, dispatch);
            addViewListener(ScreensContextEvent.SETTINGS_OPEN, dispatch);
        }

        private function onShowScreenHandler(event: ScreensContextEvent): void {
            _screensLayer.showScreen(event.data);
        }
    }
}
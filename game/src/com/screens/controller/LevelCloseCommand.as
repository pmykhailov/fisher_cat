package com.screens.controller {
    import com.screens.events.ScreensContextEvent;
    import com.screens.view.layer.ScreensLayerView;
    import com.screens.view.screens.game.GameScreen;
    import com.game.events.GameControlEvent;
    import com.share.events.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    public class LevelCloseCommand extends Command {

        [Inject]
        public var event: ScreensContextEvent;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var screensLayer: ScreensLayerView;


        public function LevelCloseCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_STOP));
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_SELECTION_SCREEN_OPEN));
        }
    }
}

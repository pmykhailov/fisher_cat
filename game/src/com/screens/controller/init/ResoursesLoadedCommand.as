package com.screens.controller.init {
    import com.screens.events.ScreensContextEvent;
    import com.screens.view.screens.enum.ScreensTypeEnum;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * ResoursesLoadedCommand class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ResoursesLoadedCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;


        public function ResoursesLoadedCommand() {
            super();
        }


        override public function execute(): void {
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.START}));
        }

    }
}
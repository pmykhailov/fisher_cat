package com.screens.controller {
import com.game_wrapper.model.level.LevelModel;
import com.screens.events.ScreensContextEvent;
import com.screens.model.LevelsModel;
import com.screens.view.layer.ScreensLayerView;
import com.screens.view.screens.game.GameScreen;
import com.game.events.GameControlEvent;
import com.share.events.GameWrapperControllEvent;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

public class LevelStartCommand extends Command {

    [Inject]
    public var event: ScreensContextEvent;
    [Inject]
    public var levelsModel:LevelsModel;
    [Inject]
    public var dispatcher: IEventDispatcher;
    [Inject]
    public var screensLayer: ScreensLayerView;

    public function LevelStartCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        var levels:Array = levelsModel.levels;
        var levelModel:LevelModel;

        for (var i:int = 0; i < levels.length; i++) {
            levelModel = levels[i];
            if (levelModel.number == event.data as int){
                break;
            }
        }

        levelsModel.openedLevelNumber = levelModel.number;

        var data: Object = {levelModel: levelModel, container: (screensLayer.activeScreen as GameScreen).gameContainer};
        dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.ADD_GAME_WRAPPER, data))

        dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.START_GAME));
        dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.ENABLE_GAME_MOUSE_CLICKS, false));

        // TODO: that condition is not very good
        // it says to play readyGo animation if level have no new feature
        // and do nothing in other case. That actually mean that
        // this here we know in this place about tutorial
        // 1. This event can be tracked as global
        // 2. This is hardcode, cuz tutorial will dispatch PLAY_READY_GO_ANIMATION event

        if (!levelsModel.levelsHasNewFeature(levelModel.number)){
            dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.PLAY_READY_GO_ANIMATION));
        }
    }
}
}

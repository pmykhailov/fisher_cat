package com.screens.controller {
import com.screens.model.LevelsModel;

import flash.events.IEventDispatcher;

import org.swiftsuspenders.Injector;

import robotlegs.bender.bundles.mvcs.Command;

public class LevelGoalsCompletedCommand extends Command {

    [Inject]
    public var levelsModel:LevelsModel;
    [Inject]
    public var dispatcher:IEventDispatcher;
    [Inject]
    public var injector:Injector;

    public function LevelGoalsCompletedCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        var nextLevelModel = levelsModel.getLevelModelByNumber(levelsModel.openedLevelNumber + 1);

        if (nextLevelModel) {
            injector.injectInto(nextLevelModel);
            nextLevelModel.isLocked = false;

        }
    }
}
}

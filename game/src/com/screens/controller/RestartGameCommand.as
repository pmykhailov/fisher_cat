package com.screens.controller {
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControlEvent;
    import com.share.events.GameWrapperControllEvent;
    import com.windows_system.core.WindowTypes;
    import com.windows_system.windows.confirm_arert.ConfirmAlertData;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * RestartGameCommand class.
     * User: Paul Makarenko
     * Date: 17.10.13
     */
    public class RestartGameCommand extends Command {

        [Inject]
        public var dispatcher:IEventDispatcher;

        public function RestartGameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var confirmEvent:ScreensContextEvent = new ScreensContextEvent(ScreensContextEvent.PLAY_AGAIN_LEVEL);
            var cancelEvent:GameControlEvent = new GameControlEvent(GameControlEvent.RESUME_GAME);
            var confirmAlertData: ConfirmAlertData = new ConfirmAlertData(confirmEvent, cancelEvent, "Restart game?");

            dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.STOP_TIME_CALCULATING_COMMAND));
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.ADD_WINDOW, {type: WindowTypes.CONFIRM_ALERT, data: confirmAlertData}));

        }
    }
}
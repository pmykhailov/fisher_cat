package com.screens.controller.screens {
    import com.screens.events.ScreensContextEvent;
    import com.screens.model.LevelsModel;
    import com.screens.view.screens.enum.ScreensTypeEnum;
import com.windows_system.core.WindowTypes;

import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * LevelSelectionScreenOpenCommand class.
     * User: Paul Makarenko
     * Date: 12.10.13
     */
    public class SettingsOpenCommand extends Command {

        [Inject]
        public var dispatcher:IEventDispatcher;
        [Inject]
        public var levelsModel:LevelsModel;

        public function SettingsOpenCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.ADD_WINDOW, {type:WindowTypes.SETTINGS, data:{soundVolume: levelsModel.soundVolume, musicVolume: levelsModel.musicVolume}}));
        }
    }
}
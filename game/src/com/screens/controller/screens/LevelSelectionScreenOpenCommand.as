package com.screens.controller.screens {
    import com.screens.events.ScreensContextEvent;
    import com.screens.model.LevelsModel;
    import com.screens.view.screens.enum.ScreensTypeEnum;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * LevelSelectionScreenOpenCommand class.
     * User: Paul Makarenko
     * Date: 12.10.13
     */
    public class LevelSelectionScreenOpenCommand extends Command {

        [Inject]
        public var dispatcher:IEventDispatcher;
        [Inject]
        public var levelsModel:LevelsModel;

        public function LevelSelectionScreenOpenCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type:ScreensTypeEnum.LEVEL_SELECTION, data:levelsModel.levels}));
        }
    }
}
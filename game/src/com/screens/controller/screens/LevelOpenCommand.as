package com.screens.controller.screens {
    import com.screens.events.ScreensContextEvent;
    import com.screens.view.layer.ScreensLayerView;
    import com.screens.view.screens.enum.ScreensTypeEnum;
    import com.screens.view.screens.game.GameScreen;
    import com.game.events.GameControlEvent;
    import com.share.events.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    public class LevelOpenCommand extends Command {

        [Inject]
        public var event: ScreensContextEvent;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function LevelOpenCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type:ScreensTypeEnum.GAME}));
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_START, event.data));
        }
    }
}

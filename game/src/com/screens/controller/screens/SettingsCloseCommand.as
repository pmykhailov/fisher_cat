package com.screens.controller.screens {
    import com.screens.model.LevelsModel;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * LevelSelectionScreenOpenCommand class.
     * User: Paul Makarenko
     * Date: 12.10.13
     */
    public class SettingsCloseCommand extends Command {

        [Inject]
        public var levelsModel: LevelsModel;


        public function SettingsCloseCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            levelsModel.save();
        }
    }
}
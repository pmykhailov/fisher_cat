/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 21.10.13
 * Time: 19:45
 * To change this template use File | Settings | File Templates.
 */
package com.screens.controller.screens {
    import com.screens.events.ScreensContextEvent;
    import com.screens.model.LevelsModel;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class VolumeValueChangedCommand extends Command {

        [Inject]
        public var event: ScreensContextEvent;
        [Inject]
        public var levelsModel: LevelsModel;


        public function VolumeValueChangedCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            levelsModel.musicVolume = event.data.musicVolume as Number;
            levelsModel.soundVolume = event.data.soundVolume as Number;

            SoundAS.group(SoundModel.GROUP_MUSIC).masterVolume = levelsModel.musicVolume;
            SoundAS.group(SoundModel.GROUP_SFX).masterVolume = levelsModel.soundVolume;
        }
    }
}

package com.screens.controller {
import com.screens.events.ScreensContextEvent;
import com.screens.view.layer.ScreensLayerView;
import com.screens.view.screens.game.GameScreen;
import com.game.events.GameControlEvent;
import com.share.events.GameWrapperControllEvent;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

public class LevelStopCommand extends Command {

    [Inject]
    public var event: ScreensContextEvent;
    [Inject]
    public var dispatcher: IEventDispatcher;
    [Inject]
    public var screensLayer: ScreensLayerView;

    public function LevelStopCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.STOP_GAME));
        dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.REMOVE_GAME_WRAPPER, (screensLayer.activeScreen as GameScreen).gameContainer));
    }
}
}

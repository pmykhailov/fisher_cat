package com.screens.controller {
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControlEvent;
    import com.windows_system.core.WindowTypes;
    import com.windows_system.windows.confirm_arert.ConfirmAlertData;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    [Deprecated]
    /**
     *  Can be used to show warning alert that will give you
     *  a choice to leave the game or continue playing
     **/
    public class BackToLevelSelectionFromGameCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;


        public function BackToLevelSelectionFromGameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.PAUSE_GAME));

            var confirmEvent: ScreensContextEvent = new ScreensContextEvent(ScreensContextEvent.LEVEL_CLOSE);
            var cancelEvent: GameControlEvent = new GameControlEvent(GameControlEvent.RESUME_GAME);
            var confirmAlertData: ConfirmAlertData = new ConfirmAlertData(confirmEvent, cancelEvent, "Do you really want to quit?");

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.ADD_WINDOW, {type: WindowTypes.CONFIRM_ALERT, data: confirmAlertData}));
        }
    }
}

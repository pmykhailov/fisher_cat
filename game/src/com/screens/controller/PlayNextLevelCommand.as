package com.screens.controller {
import com.screens.events.ScreensContextEvent;
import com.screens.model.LevelsModel;

import flash.events.IEventDispatcher;

import org.swiftsuspenders.Injector;

import robotlegs.bender.bundles.mvcs.Command;

public class PlayNextLevelCommand extends Command {

    [Inject]
    public var levelsModel:LevelsModel;
    [Inject]
    public var dispatcher:IEventDispatcher;
    [Inject]
    public var injector:Injector;

    public function PlayNextLevelCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_STOP));
        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_START,levelsModel.openedLevelNumber + 1));
    }
}
}

package com.screens.controller {
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControlEvent;
    import com.windows_system.core.WindowTypes;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    public class OpenGameSettingsCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;


        public function OpenGameSettingsCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.PAUSE_GAME));
            trace("OpenGameSettingsCommand");
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.ADD_WINDOW, {type: WindowTypes.GAME_SETTINGS}));
        }
    }
}

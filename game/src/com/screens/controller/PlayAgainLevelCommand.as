package com.screens.controller {
import com.screens.events.ScreensContextEvent;
import com.screens.model.LevelsModel;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

public class PlayAgainLevelCommand extends Command {

    [Inject]
    public var levelsModel:LevelsModel;
    [Inject]
    public var dispatcher:IEventDispatcher;


    public function PlayAgainLevelCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_STOP));
        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEVEL_START,levelsModel.openedLevelNumber));
    }
}
}

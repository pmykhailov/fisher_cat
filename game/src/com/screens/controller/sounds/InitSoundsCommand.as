package com.screens.controller.sounds {
    import by.lord_xaoca.utils.ClassUtils;

    import com.screens.events.ScreensContextEvent;
    import com.screens.model.LevelsModel;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    public class InitSoundsCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;

        [Inject]
        public var levelsModel: LevelsModel;


        override public function execute(): void {

            var sounds: Array = SoundModel.ALL_FX;

            var n: int = sounds.length;
            for (var i: int = 0; i < n; i++) {
                _addSound(sounds[i], SoundModel.GROUP_SFX);
            }


            _addSound(SoundModel.AMBIENCE, SoundModel.GROUP_MUSIC);

            SoundAS.group(SoundModel.GROUP_MUSIC).playLoop(SoundModel.AMBIENCE);

            SoundAS.group(SoundModel.GROUP_MUSIC).masterVolume = levelsModel.musicVolume;
            SoundAS.group(SoundModel.GROUP_SFX).masterVolume = levelsModel.soundVolume;
        }


        private function _addSound(soundClassName: String, group:String): void {
            SoundAS.group(group).addSound(soundClassName, ClassUtils.getInstanceByClassName(soundClassName));
        }
    }
}

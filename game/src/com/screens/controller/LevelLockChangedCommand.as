package com.screens.controller {
import com.screens.model.LevelsModel;

import robotlegs.bender.bundles.mvcs.Command;

public class LevelLockChangedCommand extends Command {

    [Inject]
    public var levelsModel:LevelsModel;

    public function LevelLockChangedCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        levelsModel.save();
    }
}
}

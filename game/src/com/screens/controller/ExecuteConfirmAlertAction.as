package com.screens.controller {
    import com.screens.events.ScreensContextEvent;

    import flash.events.Event;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * ExecuteConfirmAlertAction class.
     * User: Paul Makarenko
     * Date: 17.10.13
     */
    public class ExecuteConfirmAlertAction extends Command {

        [Inject]
        public var event:ScreensContextEvent;

        [Inject]
        public var dispatcher:IEventDispatcher;

        public function ExecuteConfirmAlertAction() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(event.data as Event);
        }
    }
}
package com.screens {
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.screens.controller.BackToLevelSelectionFromGameCommand;
    import com.screens.controller.ExecuteConfirmAlertAction;
    import com.screens.controller.LevelCloseCommand;
    import com.screens.controller.LevelGoalsCompletedCommand;
    import com.screens.controller.LevelLockChangedCommand;
    import com.screens.controller.LevelStartCommand;
    import com.screens.controller.LevelStopCommand;
    import com.screens.controller.OpenGameSettingsCommand;
    import com.screens.controller.PlayAgainLevelCommand;
    import com.screens.controller.PlayNextLevelCommand;
    import com.screens.controller.RestartGameCommand;
    import com.screens.controller.init.ResoursesLoadedCommand;
    import com.screens.controller.screens.InitScreensCommand;
    import com.screens.controller.screens.LevelOpenCommand;
    import com.screens.controller.screens.LevelSelectionScreenOpenCommand;
    import com.screens.controller.screens.SettingsCloseCommand;
    import com.screens.controller.screens.SettingsOpenCommand;
    import com.screens.controller.screens.VolumeValueChangedCommand;
    import com.screens.events.ScreensContextEvent;
    import com.screens.model.LevelsModel;
    import com.screens.view.layer.ScreenLayerMediator;
    import com.screens.view.layer.ScreensLayerView;
    import com.share.events.ApplicationEvent;
    import com.windows_system.core.WindowService;
    import com.windows_system.windows.confirm_arert.ConfirmAlertMediator;
    import com.windows_system.windows.confirm_arert.ConfirmAlertPopup;
    import com.windows_system.windows.events.WindowActionEvents;
    import com.windows_system.windows.final_level_complete.FinalLevelCompletePopup;
    import com.windows_system.windows.final_level_complete.FinalLevelCompletePopupMediator;
    import com.windows_system.windows.game_settings.GameSettingsPopup;
    import com.windows_system.windows.game_settings.GameSettingsPopupMediator;
    import com.windows_system.windows.game_settings.controller.GameSettingPopupRestartCommand;
    import com.windows_system.windows.game_settings.controller.GameSettingPopupResumeCommand;
    import com.windows_system.windows.game_settings.controller.GameSettingPopupSelectLevelCommand;
    import com.windows_system.windows.level_complete.LevelCompletePopup;
    import com.windows_system.windows.level_complete.LevelCompletePopupMediator;
    import com.windows_system.windows.settings.SettingsPopup;
    import com.windows_system.windows.settings.SettingsPopupMediator;
    import com.windows_system.windows.time_is_up.TimeIsPopup;
    import com.windows_system.windows.time_is_up.TimeIsUpPopupMediator;
    import com.windows_system.windows.tutorial_feature_explain.TutorialFeatureExplainPopup;
    import com.windows_system.windows.tutorial_feature_explain.TutorialFeatureExplainPopupMediator;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.api.IInjector;

    public class ScreensConfig implements IConfig {

        [Inject]
        public var context: IContext;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var mediatorMap: IMediatorMap;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: IInjector;
        [Inject]
        public var contextView: ContextView;


        public function configure(): void {

            mapCommands();
            mapMediators();
            mapInjections();

            context.afterInitializing(init);
        }


        private function mapCommands(): void {
            eventCommandMap.map(ApplicationEvent.RESOURSES_LOADED).toCommand(ResoursesLoadedCommand);
            eventCommandMap.map(ScreensContextEvent.INIT_SCREENS_LAYER).toCommand(InitScreensCommand);

            eventCommandMap.map(ScreensContextEvent.LEVEL_OPEN).toCommand(LevelOpenCommand);
            eventCommandMap.map(ScreensContextEvent.LEVEL_CLOSE).toCommand(LevelCloseCommand);
            eventCommandMap.map(ScreensContextEvent.LEVEL_START).toCommand(LevelStartCommand);
            eventCommandMap.map(ScreensContextEvent.LEVEL_STOP).toCommand(LevelStopCommand);
            eventCommandMap.map(ScreensContextEvent.LEVEL_SELECTION_SCREEN_OPEN).toCommand(LevelSelectionScreenOpenCommand);
            eventCommandMap.map(ScreensContextEvent.SETTINGS_OPEN).toCommand(SettingsOpenCommand);
            eventCommandMap.map(ScreensContextEvent.SETTINGS_CLOSE).toCommand(SettingsCloseCommand);
            eventCommandMap.map(ScreensContextEvent.EXECUTE_CONFIRM_ALERT_ACTIOM).toCommand(ExecuteConfirmAlertAction);
            eventCommandMap.map(ScreensContextEvent.VOLUME_VALUE_CHANGED).toCommand(VolumeValueChangedCommand);
            eventCommandMap.map(GameWrapperContextEvent.CLOSE_GAME).toCommand(BackToLevelSelectionFromGameCommand);
            eventCommandMap.map(GameWrapperContextEvent.RESTART_GAME).toCommand(RestartGameCommand);
            eventCommandMap.map(GameWrapperContextEvent.GAME_SETTINGS_BUTTON_CLICKED).toCommand(OpenGameSettingsCommand);

            eventCommandMap.map(ScreensContextEvent.PLAY_NEXT_LEVEL).toCommand(PlayNextLevelCommand);
            eventCommandMap.map(ScreensContextEvent.PLAY_AGAIN_LEVEL).toCommand(PlayAgainLevelCommand);
            eventCommandMap.map(GameWrapperContextEvent.LEVEL_GOALS_COMPLETED).toCommand(LevelGoalsCompletedCommand);

            eventCommandMap.map(GameWrapperContextEvent.LEVEL_LOCK_CHANGED).toCommand(LevelLockChangedCommand);

            eventCommandMap.map(WindowActionEvents.GAME_SETTINGS_RESUME).toCommand(GameSettingPopupResumeCommand);
            eventCommandMap.map(WindowActionEvents.GAME_SETTINGS_RESTART).toCommand(GameSettingPopupRestartCommand);
            eventCommandMap.map(WindowActionEvents.GAME_SETTINGS_BACK_TO_LEVEL_SELECTION).toCommand(GameSettingPopupSelectLevelCommand);
        }


        private function mapMediators(): void {
            mediatorMap.map(ScreensLayerView).toMediator(ScreenLayerMediator);

            mediatorMap.map(LevelCompletePopup).toMediator(LevelCompletePopupMediator);
            mediatorMap.map(FinalLevelCompletePopup).toMediator(FinalLevelCompletePopupMediator);
            mediatorMap.map(TimeIsPopup).toMediator(TimeIsUpPopupMediator);
            mediatorMap.map(ConfirmAlertPopup).toMediator(ConfirmAlertMediator);
            mediatorMap.map(TutorialFeatureExplainPopup).toMediator(TutorialFeatureExplainPopupMediator);
            mediatorMap.map(SettingsPopup).toMediator(SettingsPopupMediator);
            mediatorMap.map(GameSettingsPopup).toMediator(GameSettingsPopupMediator);
        }


        private function mapInjections(): void {
            injector.map(LevelsModel).asSingleton();
            injector.getInstance(LevelsModel);

            injector.map(ScreensLayerView).toValue(new ScreensLayerView());

            injector.map(WindowService).asSingleton();
        }


        private function init(): void {
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.INIT_SCREENS_LAYER));
        }
    }
}

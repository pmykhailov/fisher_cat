package com.screens.events {
    import flash.events.Event;

    /**
     * ScreensContextEvent class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensContextEvent extends Event {

        public static const INIT_SCREENS_LAYER: String = "initScreensLayer";

        // Screens general
        public static const SHOW_SCREEN: String = "showScreen";

        // Events from different screens
        public static const LEVEL_SELECTION_SCREEN_OPEN: String = "LevelSelectionScreenOpen";
        public static const SETTINGS_OPEN: String = "SettingsOpen";
        public static const SETTINGS_CLOSE: String = "SettingsClose";
        public static const LEVEL_OPEN: String = "levelOpen";
        public static const LEVEL_CLOSE: String = "levelClose";
        public static const LEVEL_START: String = "levelStart";
        public static const LEVEL_STOP: String = "levelStop";
        public static const FEATURE_EXPLAIN_OK: String = "featureExplainOk";
        public static const VOLUME_VALUE_CHANGED: String = "volumeValueChanged";

        public static const ADD_WINDOW: String =  "windowAdd";
        public static const SHOW_NEXT_WINDOW: String = "windowShowNext";
        public static const REMOVE_WINDOW: String = "windowRemove";

        public static const INIT_SOUNDS: String = "initSounds";
        public static const ADD_SOUND: String = "addSound";

        public static const PLAY_NEXT_LEVEL: String = "playNextLevel";
        public static const PLAY_AGAIN_LEVEL: String = "playAgainLevel";

        public static const EXECUTE_CONFIRM_ALERT_ACTIOM: String = "execureCOnfirmAlertAction";

        //public static const GAME_SETTINGS_POPUP_RE: String = "playAgainLevel";

        private var _data: Object;


        public function ScreensContextEvent(type: String, data: Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new ScreensContextEvent(type, _data, bubbles, cancelable);
        }
    }
}
package com.screens.model {
import com.game_wrapper.model.level.LevelModel;

import flash.net.SharedObject;

public class SharedObjectService {

    private var so:SharedObject;

    public function SharedObjectService() {
        _init();
    }

    private function _init():void {
        so = SharedObject.getLocal("com.supergames.fishercat");
    }

    public function initLevelsData(levelsModel:LevelsModel):void
    {
        var levelsLockArray:Array;

        if (so.data.hasOwnProperty("levelsLockArray")){
            levelsLockArray =  so.data.levelsLockArray;
        }

        if (so.data.hasOwnProperty("soundVolume")){
            levelsModel.soundVolume = so.data.soundVolume;
        }

        if (so.data.hasOwnProperty("musicVolume")){
            levelsModel.musicVolume = so.data.musicVolume;
        }

        if (levelsLockArray){
            for (var i:int = 0; i < levelsLockArray.length; i++) {
                var levelModel:LevelModel = levelsModel.getLevelModelByNumber(i + 1);
                levelModel.isLocked = false; //levelsLockArray[i];
            }
        }else{
            levelsModel.getLevelModelByNumber(1).isLocked = false;
            saveLevelsData(levelsModel);
        }

    }

    public function saveLevelsData(levelsModel:LevelsModel):void{
        var levelsLockArray:Array = [];

        for (var i:int = 0; i < levelsModel.levelsCount; i++) {
            var levelModel:LevelModel = levelsModel.getLevelModelByNumber(i + 1);
            levelsLockArray[i] = levelModel.isLocked;
        }

        so.data.soundVolume = levelsModel.soundVolume;
        so.data.musicVolume = levelsModel.musicVolume;

        so.data.levelsLockArray = levelsLockArray;
        so.flush();
    }


}
}

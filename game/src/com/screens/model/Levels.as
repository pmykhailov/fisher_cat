package com.screens.model {
    import com.game_wrapper.model.goals.NumberOfDeletedItemsBackgroundsGoalModel;
    import com.game_wrapper.model.goals.ReachScoreGoalModel;
    import com.game_wrapper.model.level.LevelModel;

    import model.grids.enum.GridTypeEnum;
    import model.level.parsers.LevelsDataParser;
    import model.level.settings.GameSettings;

    /**
     * Levels class.
     * User: Paul M
     * Date: 23.10.13
     */
    public class Levels {

        public static var ALL: Array;


        public static function init(levelsData: XML): void {
            ALL = [];

            var parser: LevelsDataParser = new LevelsDataParser();
            var levelsCount: int = levelsData.level.length();

            // Some default values
            // TODO: Remove this values and make them all tunable in level editor
            var goals: Array = [
                new ReachScoreGoalModel(400),
                //new NumberOfDeletedItemsBackgroundsGoalModel(6)
            ];

            var timeToCompleteLevel:int = 600;

            for (var i: int = 0; i < levelsCount; i++) {
                var levelModel: LevelModel = new LevelModel();

                levelModel.goals = goals;
                levelModel.timeToCompleteLevel = timeToCompleteLevel;

                ALL.push(levelModel);
            }

            parser.parse(levelsData, ALL);
        }

    }
}

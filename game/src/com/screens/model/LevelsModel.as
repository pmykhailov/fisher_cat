package com.screens.model {
import by.lord_xaoca.robotlegs2.BaseActor;

import com.game_wrapper.event.GameWrapperContextEvent;
import com.game_wrapper.model.goals.ReachScoreGoalModel;
import com.game_wrapper.model.level.LevelModel;

/**
 * LevelsModel class.
 * User: Paul Makarenko
 * Date: 12.10.13
 */
public class LevelsModel extends BaseActor {

    [ArrayElementType("com.game_wrapper.model.level.LevelModel")]
    private var _levels:Array;
    private var _levelsCount:int;
    private var _openedLevelNumber:int;
    private var _soService:SharedObjectService;
    private var _levelsWithNewFeature:Array;
    private var _soundVolume:Number;
    private var _musicVolume:Number;

    public function LevelsModel() {
        super();
        _init();
    }

    public function get levels():Array {
        return _levels;
    }

    public function get openedLevelNumber():int {
        return _openedLevelNumber;
    }

    public function set openedLevelNumber(value:int):void {
        _openedLevelNumber = value;
    }

    public function get levelsCount():int {
        return _levelsCount;
    }

    public function get soundVolume():Number {
        return _soundVolume;
    }

    public function set soundVolume(value:Number):void {
        _soundVolume = value;
    }

    public function get musicVolume():Number {
        return _musicVolume;
    }

    public function set musicVolume(value:Number):void {
        _musicVolume = value;
    }

    public function levelsHasNewFeature(levelNumber: int):Boolean {
        return _levelsWithNewFeature[levelNumber - 1];
    }


    public function getLevelModelByNumber(number:int):LevelModel {
        var n:int = _levels.length;
        for (var i:int = 0; i < n; i++) {
            var levelModel:LevelModel = _levels[i];
            if (levelModel.number == number) {
                return levelModel;
            }
        }
        return null;
    }

    private function _init():void {
        _initService();
        _initLevels();
        _initLevelsFromService();
    }


    private function _initLevelsFromService():void {
        _soService.initLevelsData(this);
    }

    private function _initService():void {
        _soService = new SharedObjectService();
    }

    private function _initLevels():void {
        var j:int;

        _levels = Levels.ALL;
        _levelsCount = _levels.length;

        for (j = 0; j < _levelsCount; j++) {
            _levels[j].number = j + 1;
        }

        _levels[_levels.length - 1].isFinalLevel = true;

        _levelsWithNewFeature = [];
        for (j = 0; j < _levelsCount; j++) {
            _levelsWithNewFeature[j] = false;
        }
        _levelsWithNewFeature[0] = true;
    }

    public function save():void{
        _soService.saveLevelsData(this);
    }

}
}
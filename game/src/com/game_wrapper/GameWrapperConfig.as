package com.game_wrapper {
    import com.game.events.GameControlEvent;
    import com.game.events.GameExternalEvent;
    import com.game_wrapper.controller.external_controll.AddGameWrapperCommand;
    import com.game_wrapper.controller.external_controll.RemoveGameWrapperCommand;
    import com.game_wrapper.controller.external_controll.time.StartTimeCalculatingCommand;
    import com.game_wrapper.controller.external_controll.time.StopTimeCalculatingCommand;
    import com.game_wrapper.controller.game.PauseGameCommand;
    import com.game_wrapper.controller.game.ReadyGoAnimationCompleteCommand;
    import com.game_wrapper.controller.game.ResumeGameCommand;
    import com.game_wrapper.controller.game.StartGameCommand;
    import com.game_wrapper.controller.game.StopGameCommand;
    import com.game_wrapper.controller.game.TimeIsUpCommand;
    import com.game_wrapper.controller.goals.GameItemsBackgroundsDeletedCommand;
    import com.game_wrapper.controller.goals.GameItemsDeletedCommand;
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.view.game_warapper.GameWrapperMediator;
    import com.game_wrapper.view.game_warapper.GameWrapperView;
    import com.game_wrapper.view.goals_panel.GoalsPanelMediator;
    import com.game_wrapper.view.goals_panel.GoalsPanelView;
    import com.game_wrapper.view.ready_go.ReadyGo;
    import com.game_wrapper.view.ready_go.ReadyGoMediator;
    import com.game_wrapper.view.top_bar.TopBarMediator;
    import com.game_wrapper.view.top_bar.TopBarView;
    import com.share.events.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.api.IInjector;

    public class GameWrapperConfig implements IConfig {

        [Inject]
        public var context: IContext;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var mediatorMap: IMediatorMap;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: IInjector;
        [Inject]
        public var contextView: ContextView;


        public function configure(): void {

            mapCommands();
            mapMediators();
            mapInjections();

            context.afterInitializing(init);
        }


        private function mapCommands(): void {
            eventCommandMap.map(GameExternalEvent.ITEMS_DELETED).toCommand(GameItemsDeletedCommand);
            eventCommandMap.map(GameExternalEvent.ITEMS_BACKGROUNDS_DELETED).toCommand(GameItemsBackgroundsDeletedCommand);
            eventCommandMap.map(GameControlEvent.START_GAME).toCommand(StartGameCommand);
            eventCommandMap.map(GameControlEvent.STOP_GAME).toCommand(StopGameCommand);
            eventCommandMap.map(GameControlEvent.PAUSE_GAME).toCommand(PauseGameCommand);
            eventCommandMap.map(GameControlEvent.RESUME_GAME).toCommand(ResumeGameCommand);
            eventCommandMap.map(GameWrapperContextEvent.TIME_IS_UP).toCommand(TimeIsUpCommand);
            eventCommandMap.map(GameWrapperContextEvent.READY_GO_ANIMATION_COMPLETE).toCommand(ReadyGoAnimationCompleteCommand);

            eventCommandMap.map(GameWrapperControllEvent.ADD_GAME_WRAPPER).toCommand(AddGameWrapperCommand);
            eventCommandMap.map(GameWrapperControllEvent.REMOVE_GAME_WRAPPER).toCommand(RemoveGameWrapperCommand);

            eventCommandMap.map(GameWrapperControllEvent.START_TIME_CALCULATING_COMMAND).toCommand(StartTimeCalculatingCommand);
            eventCommandMap.map(GameWrapperControllEvent.STOP_TIME_CALCULATING_COMMAND).toCommand(StopTimeCalculatingCommand);
        }


        private function mapMediators(): void {
            mediatorMap.map(GameWrapperView).toMediator(GameWrapperMediator);
            mediatorMap.map(TopBarView).toMediator(TopBarMediator);
            mediatorMap.map(GoalsPanelView).toMediator(GoalsPanelMediator);
            mediatorMap.map(ReadyGo).toMediator(ReadyGoMediator);
        }


        private function mapInjections(): void {
        }


        private function init(): void {

        }
    }
}

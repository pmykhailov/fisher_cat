package com.game_wrapper.controller.game {
import com.game.events.GameContextEvent;
import com.game.events.GameControlEvent;
import com.share.events.GameWrapperControllEvent;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

public class ReadyGoAnimationCompleteCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    public function ReadyGoAnimationCompleteCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.ENABLE_GAME_MOUSE_CLICKS, true));
        dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.START_TIME_CALCULATING_COMMAND));
    }
}
}

package com.game_wrapper.controller.game {
    import com.game_wrapper.model.level.LevelModel;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * ResumeGameCommand class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class ResumeGameCommand extends Command {

        [Inject]
        public var levelModel:LevelModel;


        public function ResumeGameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            levelModel.startTimeCalculating();
        }
    }
}
package com.game_wrapper.controller.game {
    import com.game_wrapper.model.level.LevelModel;
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControlEvent;
    import com.windows_system.core.WindowTypes;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    /**
     * TimeIsUpCommand class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class TimeIsUpCommand extends Command {

        [Inject]
        public var levelModel: LevelModel;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function TimeIsUpCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            levelModel.stopTimeCalculating();

            dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.STOP_GAME));
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.TIME_IS_UP);
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.ADD_WINDOW, {type: WindowTypes.TIME_IS_UP}));
        }
    }
}
package com.game_wrapper.controller.game {
    import com.game_wrapper.controller.goals.LevelGoalsCompletedCommand;
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.model.level.LevelModel;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;

    /**
     * RestartGameCommand class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class StartGameCommand extends Command {

        [Inject]
        public var levelModel: LevelModel;
        [Inject]
        public var eventCommandMap: IEventCommandMap;


        public function StartGameCommand() {
            super();
        }


        override public function execute(): void {
            levelModel.reset();
            eventCommandMap.map(GameWrapperContextEvent.LEVEL_GOALS_COMPLETED).toCommand(LevelGoalsCompletedCommand);
        }
    }
}
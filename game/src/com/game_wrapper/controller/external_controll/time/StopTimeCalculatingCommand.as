package com.game_wrapper.controller.external_controll.time {
import com.game_wrapper.model.level.LevelModel;

import robotlegs.bender.bundles.mvcs.Command;

public class StopTimeCalculatingCommand extends Command {
    [Inject]
    public var levelModel:LevelModel;

    public function StopTimeCalculatingCommand() {
        super();
    }


    override public function execute():void {
        super.execute();
        levelModel.stopTimeCalculating();
    }
}
}

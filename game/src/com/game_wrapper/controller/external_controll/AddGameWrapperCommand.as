package com.game_wrapper.controller.external_controll {
    import com.game.events.GameControlEvent;
    import com.game.model.vo.AddGameVO;
    import com.game_wrapper.controller.goals.CheckGoalsCompleteCommand;
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.model.goals.base.IGoalModel;
    import com.game_wrapper.model.level.LevelModel;
    import com.game_wrapper.view.game_warapper.GameWrapperView;
    import com.share.events.GameWrapperControllEvent;

    import flash.display.DisplayObjectContainer;
    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.framework.api.IInjector;

    /**
     * InitTopBarCommand class.
     * User: Paul Makarenko
     * Date: 03.10.13
     */
    public class AddGameWrapperCommand extends Command {

        [Inject]
        public var event: GameWrapperControllEvent;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: IInjector;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        private var levelModel: LevelModel;


        public function AddGameWrapperCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            setupModel();
            setupView();
        }


        public function setupModel(): void {
            levelModel = event.data.levelModel as LevelModel;
            var goals: Array = levelModel.goals;
            var allTrigeringEvents: Array = [];

            for (var i: int = 0; i < goals.length; i++) {
                var goalModel: IGoalModel = goals[i];

                var events: Array = goalModel.checkTriggeringEventsList;
                for (var j: int = 0; j < events.length; j++) {
                    var isAddedAlready: Boolean = false;

                    for (var jj: int = 0; jj < allTrigeringEvents.length; jj++) {
                        if (allTrigeringEvents[jj] == events[j]) {
                            isAddedAlready = true;
                            break;
                        }
                    }

                    if (!isAddedAlready) {
                        allTrigeringEvents.push(events[j]);
                    }
                }
            }

            for (var k: int = 0; k < allTrigeringEvents.length; k++) {
                eventCommandMap.map(allTrigeringEvents[k]).toCommand(CheckGoalsCompleteCommand);
            }

            injector.injectInto(levelModel);
            injector.map(LevelModel).toValue(levelModel);
        }


        private function setupView(): void {
            var container: DisplayObjectContainer = (event.data.container as DisplayObjectContainer);

            // Add game wrapper to it's container
            var gameWrapperView: GameWrapperView = new GameWrapperView();
            injector.map(GameWrapperView).toValue(gameWrapperView);
            container.addChild(gameWrapperView.view);

            // Add game into game wrapper
            var vo: AddGameVO = new AddGameVO(gameWrapperView.gameContainer, levelModel.gameSettings);
            dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.ADD_GAME, vo));

            // Setup view with model's data
            gameWrapperView.topBar.level = levelModel.number;
            gameWrapperView.topBar.score = 0;
            dispatcher.dispatchEvent(new GameWrapperContextEvent(GameWrapperContextEvent.LEVEL_GOALS_ADD, levelModel.goals));
        }
    }
}
package com.game_wrapper.controller.external_controll {
    import com.game.events.GameControlEvent;
    import com.game_wrapper.controller.goals.CheckGoalsCompleteCommand;
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.model.goals.base.IGoalModel;
    import com.game_wrapper.model.level.LevelModel;
    import com.game_wrapper.view.game_warapper.GameWrapperView;
    import com.share.events.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.framework.api.IInjector;

    /**
     * InitTopBarCommand class.
     * User: Paul Makarenko
     * Date: 03.10.13
     */
    public class RemoveGameWrapperCommand extends Command {

        [Inject]
        public var event: GameWrapperControllEvent;
        [Inject]
        public var gameWrapperView: GameWrapperView;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: IInjector;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var levelModel: LevelModel;


        public function RemoveGameWrapperCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            releaseModel();
            releaseView();
        }


        public function releaseView(): void {
            dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.REMOVE_GAME, event.data));

            injector.unmap(GameWrapperView);
            gameWrapperView.destroy();
        }


        private function releaseModel(): void {

            var goals: Array = levelModel.goals;

            for (var i: int = 0; i < goals.length; i++) {
                var goalModel: IGoalModel = goals[i];

                var events: Array = goalModel.checkTriggeringEventsList;
                for (var j: int = 0; j < events.length; j++) {
                    eventCommandMap.unmap(events[j]).fromCommand(CheckGoalsCompleteCommand);
                }
            }

            dispatcher.dispatchEvent(new GameWrapperContextEvent(GameWrapperContextEvent.LEVEL_GOALS_REMOVE));

            injector.unmap(LevelModel);
        }
    }
}
package com.game_wrapper.controller.goals {
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.model.level.LevelModel;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * CheckGoalsCompleteCommand class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class CheckGoalsCompleteCommand extends Command {

        [Inject]
        public var levelModel:LevelModel;

        [Inject]
        public var dispatcher:IEventDispatcher;

        public function CheckGoalsCompleteCommand() {
            super();
        }


        override public function execute(): void {
            //  trace("Checking goals completion " + levelModel.isGoalsCompleted);
            if (levelModel.isGoalsCompleted){
                dispatcher.dispatchEvent(new GameWrapperContextEvent(GameWrapperContextEvent.LEVEL_GOALS_COMPLETED));
            }
        }
    }
}
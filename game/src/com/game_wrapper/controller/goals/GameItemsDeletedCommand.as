package com.game_wrapper.controller.goals {
    import com.game.events.GameExternalEvent;
    import com.game.model.grids.grid_items.item.GridItemModel;
    import com.game.model.logic.changed_grid_items_groups.groups.scored.ScoreGivingGridItemsGroup;
    import com.game.model.vo.ChangedGridItemsGroupApplierVO;
    import com.game_wrapper.model.level.LevelModel;

    import model.grids.items.GridItemTypeEnum;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * GameItemsDeletedCommand class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class GameItemsDeletedCommand extends Command {

        [Inject]
        public var levelModel: LevelModel;

        [Inject]
        public var event: GameExternalEvent;


        public function GameItemsDeletedCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            // TODO: in the nearest future we need game model also here
            var groupsAppliersVOs: Array = event.data as Array;
            var group: ScoreGivingGridItemsGroup;
            var i: int

            // Update score
            var totalScore: int;
            for (i = 0; i < groupsAppliersVOs.length; i++) {
                group = (groupsAppliersVOs[i] as ChangedGridItemsGroupApplierVO).group as ScoreGivingGridItemsGroup;

                if (group) {
                    totalScore += group.score;
                }

            }
            levelModel.score += totalScore;

            // Update deleted number of items of some type
            var itemTypesCount: Array = [];
            for (i = 0; i < GridItemTypeEnum.TYPE_MAX_ID; i++) {
                itemTypesCount[i] = 0;
            }

            for (i = 0; i < groupsAppliersVOs.length; i++) {
                group = (groupsAppliersVOs[i] as ChangedGridItemsGroupApplierVO).group as ScoreGivingGridItemsGroup;

                if (group) {
                    var items: Array = group.gridItems;
                    for (var j: int = 0; j < items.length; j++) {
                        var gridItemModel: GridItemModel = items[j];
                        itemTypesCount[gridItemModel.type] += 1;
                    }
                }
            }

            for (i = 0; i < GridItemTypeEnum.TYPE_MAX_ID; i++) {
                levelModel.addDeleteItems(itemTypesCount[i], i);
            }

        }
    }
}
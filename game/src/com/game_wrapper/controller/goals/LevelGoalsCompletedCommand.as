package com.game_wrapper.controller.goals {
    import com.game_wrapper.model.level.LevelModel;
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControlEvent;
    import com.windows_system.core.WindowTypes;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * LevelGoalsCompletedCommand class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class LevelGoalsCompletedCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var levelModel: LevelModel;


        public function LevelGoalsCompletedCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.STOP_GAME));

            if (!levelModel.isFinalLevel) {
                SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.LEVEL_COMPLETE);
                dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.ADD_WINDOW, {type: WindowTypes.LEVEL_COMPLETE, data: {score: levelModel.score}}));
            } else {
                SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.FINAL_LEVEL_COMPLETE);
                dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.ADD_WINDOW, {type: WindowTypes.FINAL_LEVEL_COMPLETE}));
            }

        }
    }
}
package com.game_wrapper.controller.goals {
    import com.game_wrapper.model.level.LevelModel;
    import com.game.events.GameExternalEvent;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * GameItemsBackgroundsDeletedCommand class.
     * User: Paul M
     * Date: 23.10.13
     */
    public class GameItemsBackgroundsDeletedCommand extends Command {

        [Inject]
        public var levelModel:LevelModel;

        [Inject]
        public var event:GameExternalEvent;


        public function GameItemsBackgroundsDeletedCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            levelModel.deletedItemsBackgroundsCount += event.data as int;
        }
    }
}
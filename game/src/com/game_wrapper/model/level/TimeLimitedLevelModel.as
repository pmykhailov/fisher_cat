package com.game_wrapper.model.level {
import by.lord_xaoca.robotlegs2.BaseActor;

import com.game_wrapper.event.GameWrapperContextEvent;

import flash.events.TimerEvent;
import flash.utils.Timer;

/**
 * Model that provides time logic
 */
public class TimeLimitedLevelModel extends BaseActor {

    /** How much time player plays the game. Time is in seconds**/
    protected var _timeSpend:int;
    /** Time given to player to complete level **/
    private var _timeToCompleteLevel:int;
    /** Timer for time flow process **/
    private var _timeTickTimer:Timer;

    public function TimeLimitedLevelModel() {
        super();

        _init();
    }

    public function set timeToCompleteLevel(value:int):void {
        _timeToCompleteLevel = value;
    }

    protected function set timeSpend(value:int):void {
        _timeSpend = value;

        var timeLeft:int = _timeToCompleteLevel - value;

        dispatch(new GameWrapperContextEvent(GameWrapperContextEvent.TIME_CHANGED, timeLeft));

        if (timeLeft == 0) {
            dispatch(new GameWrapperContextEvent(GameWrapperContextEvent.TIME_IS_UP));
        }
    }

    public function startTimeCalculating():void {
        _timeTickTimer.addEventListener(TimerEvent.TIMER, onTimerHandler);
        _timeTickTimer.start();
    }

    public function stopTimeCalculating():void {
        _timeTickTimer.removeEventListener(TimerEvent.TIMER, onTimerHandler);
        _timeTickTimer.stop();
    }

    protected function _init():void {
        _timeTickTimer = new Timer(1000);
    }

    private function onTimerHandler(event:TimerEvent):void {
        timeSpend = _timeSpend + 1;
    }

}
}

package com.game_wrapper.model.level {
    import model.grids.items.GridItemTypeEnum;

    import com.game_wrapper.event.GameWrapperContextEvent;

import model.level.ICoreLevelModel;

import model.level.settings.GameSettings;
import model.level.settings.LevelSettings;

/**
     * LevelModel class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class LevelModel extends LevelModelWithGoals implements ICoreLevelModel{

        /** Defines if user can play level or not **/
        private var _isLocked: Boolean = true;
        /** Defines if level is final **/
        private var _isFinalLevel: Boolean;
        /** Number that level has **/
        private var _number: int;
        /** Player score **/
        private var _score: int;
        /** Index of this array is type of item and  element with this index is deleted items count of this type**/
        private var _deletedItemsCount: Array;
        /** Player score **/
        private var _deletedItemsBackgroundsCount: int;
        /** Some game customize **/
        private var _gameSettings: GameSettings;
        /** Level settings **/
        private var _levelSettings: LevelSettings;

        public function LevelModel() {
            super();

            _init();
        }


        public function get score(): int {
            return _score;
        }


        public function set score(value: int): void {
            _score = value;
            dispatch(new GameWrapperContextEvent(GameWrapperContextEvent.SCORE_CHANGED, value));
        }


        public function get deletedItemsCount(): Array {
            return _deletedItemsCount;
        }


        public function get number(): int {
            return _number;
        }


        public function set number(value: int): void {
            _number = value;
        }


        public function get isLocked(): Boolean {
            return _isLocked;
        }


        public function set isLocked(value: Boolean): void {
            _isLocked = value;
            dispatch(new GameWrapperContextEvent(GameWrapperContextEvent.LEVEL_LOCK_CHANGED, _number));
        }


        public function get gameSettings(): GameSettings {
            return _gameSettings;
        }


        public function set gameSettings(value: GameSettings): void {
            _gameSettings = value;
        }


        public function get isFinalLevel(): Boolean {
            return _isFinalLevel;
        }


        public function set isFinalLevel(value: Boolean): void {
            _isFinalLevel = value;
        }


        public function get deletedItemsBackgroundsCount(): int {
            return _deletedItemsBackgroundsCount;
        }


        public function set deletedItemsBackgroundsCount(value: int): void {
            _deletedItemsBackgroundsCount = value;
            dispatch(new GameWrapperContextEvent(GameWrapperContextEvent.ITEMS_BACKGROUNDS_COUNT_CHANGED, _deletedItemsBackgroundsCount));
        }


        public function addDeleteItems(itemsCount: int, type: int): void {
            _deletedItemsCount[type] += itemsCount;
            dispatch(new GameWrapperContextEvent(GameWrapperContextEvent.DELETE_ITEMS_COUNT_CHANGED, _deletedItemsCount));
        }


        public function reset(): void {
            // Clear level goals first, because e.g. score changing can run some goal checking
            resetLevelGoals();

            _score = 0;
            _timeSpend = 0;
            _deletedItemsBackgroundsCount = 0;
            resetDeletedItemsCount();
        }


        protected function resetDeletedItemsCount(): void {
            _deletedItemsCount = [];
            for (var i: int = 0; i < GridItemTypeEnum.TYPE_MAX_ID; i++) {
                _deletedItemsCount[i] = 0;
            }
        }

        public function get levelSettings():LevelSettings {
            return _levelSettings;
        }

        public function set levelSettings(value:LevelSettings):void {
            _levelSettings = value;
        }
}
}
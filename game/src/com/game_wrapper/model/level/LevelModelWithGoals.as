package com.game_wrapper.model.level {
    import com.game_wrapper.model.*;
    import com.game_wrapper.event.GameWrapperContextEvent;
import com.game_wrapper.model.goals.base.IGoalModel;

/**
 * Goals API focused in this class
 */
public class LevelModelWithGoals extends TimeLimitedLevelModel {

    [ArrayElementType("com.game_wrapper.model.goals.base.IGoalModel")]
    private var _goals:Array;

    public function LevelModelWithGoals() {
        super();
    }

    override protected function _init():void {
        super._init();
        _goals = [];
    }


    public function get goals():Array {
        return _goals;
    }


    public function set goals(value:Array):void {
        _goals = value;
    }


    public function get isGoalsCompleted():Boolean {
        var res:Boolean = true;
        for (var i:int = 0; i < _goals.length; i++) {
            var goalModel:IGoalModel = _goals[i];
            if (!goalModel.checkGoalCondition(this)) {
                res = false;
            }
        }
        return res;
    }


    protected function resetLevelGoals():void {
        if (_goals && _goals.length > 0) {
            for (var i:int = 0; i < _goals.length; i++) {
                var goalModel:IGoalModel = _goals[i];
                goalModel.reset();
            }
        }
    }


}
}

package com.game_wrapper.model.goals {
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.model.level.LevelModel;
    import com.game_wrapper.model.goals.base.BaseGoalModel;
    import com.game_wrapper.model.goals.enum.GoalTypesEnum;

    /**
     * ReachScoreGoal class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class ReachScoreGoalModel extends BaseGoalModel {

        private var _targetScore: int;
        private var _currentScore: int;


        public function ReachScoreGoalModel(targetScore: int) {
            _targetScore = targetScore;
        }


        override public function get checkTriggeringEventsList(): Array {
            return [GameWrapperContextEvent.SCORE_CHANGED];
        }


        override public function get id(): String {
            return GoalTypesEnum.REACH_SCORE_GOAL;
        }


        public function get targetScore(): int {
            return _targetScore;
        }


        public function get currentScore(): int {
            return _currentScore;
        }


        override public function checkGoalCondition(levelModel: Object): Boolean {
            _currentScore = levelModel.score;
            _notifyGoalDataUpdated();
            return _currentScore >= _targetScore;
        }


        override protected function _initGoalVariables():void {
            _currentScore = 0;
        }
    }
}

package com.game_wrapper.model.goals {
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.model.level.LevelModel;
    import com.game_wrapper.model.goals.base.BaseGoalModel;
    import com.game_wrapper.model.goals.enum.GoalTypesEnum;

    /**
     * DeleteNumberOfItemsGoalModel class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class NumberOfDeletedItemsGoalModel extends BaseGoalModel {
        private var _targetNumber: int;
        private var _currentNumber:int;
        private var _itemType: int;


        public function NumberOfDeletedItemsGoalModel(targetNumber: int, itemType: int = -1) {
            super();
            _targetNumber = targetNumber;
            _itemType = itemType;
        }


        override public function get checkTriggeringEventsList(): Array {
            return [GameWrapperContextEvent.DELETE_ITEMS_COUNT_CHANGED];
        }


        override public function get id(): String {
            return GoalTypesEnum.NUMBER_OF_DELETED_ITEMS_WITH_SOME_TYPE_GOAL;
        }


        public function get targetNumber(): int {
            return _targetNumber;
        }


        public function get itemType(): int {
            return _itemType;
        }


        override public function checkGoalCondition(levelModel: Object): Boolean {
            _currentNumber = levelModel.deletedItemsCount[_itemType];
            _notifyGoalDataUpdated();
            return  _currentNumber >= _targetNumber;
        }


        public function get currentNumber(): int {
            return _currentNumber;
        }


        override protected function _initGoalVariables():void {
            _currentNumber = 0;
        }
    }
}
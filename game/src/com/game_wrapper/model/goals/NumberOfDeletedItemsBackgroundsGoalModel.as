package com.game_wrapper.model.goals {
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.model.level.LevelModel;
    import com.game_wrapper.model.goals.base.BaseGoalModel;
    import com.game_wrapper.model.goals.enum.GoalTypesEnum;

    /**
     * ReachScoreGoal class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class NumberOfDeletedItemsBackgroundsGoalModel extends BaseGoalModel {

        private var _targetCount: int;
        private var _currentCount: int;


        public function NumberOfDeletedItemsBackgroundsGoalModel(targetCount: int) {
            _targetCount = targetCount;
        }


        override public function get checkTriggeringEventsList(): Array {
            return [GameWrapperContextEvent.ITEMS_BACKGROUNDS_COUNT_CHANGED];
        }


        override public function get id(): String {
            return GoalTypesEnum.NUMBER_OF_DELETED_ITEMS_BACKGROUNDS;
        }


        public function get targetCount(): int {
            return _targetCount;
        }


        public function get currentCount(): int {
            return _currentCount;
        }


        override public function checkGoalCondition(levelModel: Object): Boolean {
            _currentCount = levelModel.deletedItemsBackgroundsCount;
            _notifyGoalDataUpdated();
            return _currentCount >= _targetCount;
        }


        override protected function _initGoalVariables():void {
            _currentCount = 0;
        }
    }
}

package com.game_wrapper.model.goals.enum {

    /**
     * GoalsEnum class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class GoalTypesEnum {
        /** Goal is complete when player reaches target score**/
        public static const REACH_SCORE_GOAL:String = "ReachScoreGoal";
        /** Goal is to delete some number of items of some type **/
        public static const NUMBER_OF_DELETED_ITEMS_WITH_SOME_TYPE_GOAL:String = "NumberOfDeletedItemsWithSomeType";
        /** Goal is to delete some number item's backgrounds **/
        public static const NUMBER_OF_DELETED_ITEMS_BACKGROUNDS:String = "NumberOfDeletedItemsBackgrounds";

        public function GoalTypesEnum() {
        }

    }
}

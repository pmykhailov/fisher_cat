package com.game_wrapper.model.goals.base {
    import com.game_wrapper.controller.goals.CheckGoalsCompleteCommand;
    import com.game_wrapper.model.*;

    import flash.events.EventDispatcher;

    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;

    /**
     * BaseGoalModel class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class BaseGoalModel extends EventDispatcher implements IGoalModel {

        private var _eventCommandMap: IEventCommandMap


        public function BaseGoalModel() {
            _initGoalVariables();
        }


        /**
         * ABSTRACT
         */
        public function get id(): String {
            return "";
        }


        /**
         * ABSTRACT
         *
         * List of events that will trigger running check of goal complete condition
         *
         */
        public function get checkTriggeringEventsList(): Array {
            return [];
        }


        /**
         * ABSTRACT
         */
        public function checkGoalCondition(levelModel: Object): Boolean {
            return false;
        }


        protected function _notifyGoalDataUpdated(): void {
            dispatchEvent(new GoalModelEvent(GoalModelEvent.GOAL_DATA_UPDATED))
        }

        public function reset():void {
            _initGoalVariables();
            _notifyGoalDataUpdated();
        }

        /**
         * ABSTRACT
         * For most levels will be setting some variables to 0. For instance start score.
         * But some time can be not 0 - for instance when we have goal like
         * "Reach XXX scores in YYY moves". YYY will be inited as positive number
         */
        protected function _initGoalVariables():void {

        }
    }
}
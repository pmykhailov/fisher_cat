package com.game_wrapper.model.goals.base {
    import com.game_wrapper.model.level.LevelModel;

    import flash.events.IEventDispatcher;

    /**
     * IGoalModel class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public interface IGoalModel extends IEventDispatcher {

        function get id(): String;


        function get checkTriggeringEventsList(): Array


        function checkGoalCondition(levelModel: Object): Boolean


        function reset(): void
    }
}


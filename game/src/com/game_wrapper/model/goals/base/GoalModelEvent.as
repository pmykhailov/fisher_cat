package com.game_wrapper.model.goals.base {
    import flash.events.Event;

    /**
     * GoalModelEvent class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class GoalModelEvent extends Event {

        public static const GOAL_DATA_UPDATED:String = "goalDataUpdated";

        public function GoalModelEvent(type: String, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
        }


        override public function clone(): Event {
            return new GoalModelEvent(type, bubbles, cancelable);
        }
    }
}
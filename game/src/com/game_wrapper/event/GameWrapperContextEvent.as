package com.game_wrapper.event {
    import com.game.events.*;

    import flash.events.Event;

    public class GameWrapperContextEvent extends Event {

        public static const SCORE_CHANGED: String = "scoreChanged";
        public static const LEVEL_LOCK_CHANGED: String = "levelLockChanged";
        public static const TIME_CHANGED: String = "timeChanged";
        public static const TIME_IS_UP: String = "timeIsUp";
        public static const LEVEL_GOALS_ADD: String = "levelGoalsAdde";
        public static const LEVEL_GOALS_REMOVE: String = "levelGoalsRemove";
        public static const LEVEL_GOALS_COMPLETED: String = "levelGoalsCompleted";
        public static const CLOSE_GAME: String = "closeGame";
        public static const RESTART_GAME: String = "restartGame";
        public static const DELETE_ITEMS_COUNT_CHANGED: String = "deleteItemsCountChanged";
        public static const ITEMS_BACKGROUNDS_COUNT_CHANGED: String = "itemsBackgroundsCountChanged";
        public static const READY_GO_ANIMATION_COMPLETE: String = "readyGoAnimationComplete";
        public static const GAME_SETTINGS_BUTTON_CLICKED: String = "gameSettingsButtonClicked";
        private var _data: Object;


        public function GameWrapperContextEvent(type: String, data: Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new GameContextEvent(type, _data, bubbles, cancelable);
        }
    }
}

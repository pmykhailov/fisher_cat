package com.game_wrapper.misc {
    public class Format {
        /**
         * Format to MM:SS time
         * @param seconds
         * @return
         *
         */
        public static function time(seconds: int): String {
            var sec: int = seconds % 60;
            var min: int = int(seconds / 60);

            return toXXFormat(min) + ":" + toXXFormat(sec);
        }


        /**
         * Converts any 2 digit number to XX format
         * @param number
         * @return
         *
         */
        private static function toXXFormat(number: int): String {
            var res: String;

            if (number > 9) {
                res = number + "";
            } else {
                res = "0" + number;
            }

            return res;
        }


        public function Format() {

        }

    }
}
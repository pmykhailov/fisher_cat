package com.game_wrapper.view.goals_panel {
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.model.goals.base.IGoalModel;
    import com.game.events.GameControlEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    /**
     * GoalsPanelMediator class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class GoalsPanelMediator extends Mediator {

        [Inject]
        public var goalsPanelView:GoalsPanelView;

        public function GoalsPanelMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();

            addContextListener(GameWrapperContextEvent.LEVEL_GOALS_ADD, onLevelGoalAddedHandler);
            addContextListener(GameWrapperContextEvent.LEVEL_GOALS_REMOVE, onLevelGoalsClearedHandler);
        }


        private function onLevelGoalAddedHandler(event: GameWrapperContextEvent): void {
            goalsPanelView.addGoals(event.data as Array)
        }


        private function onLevelGoalsClearedHandler(event: GameWrapperContextEvent): void {
            goalsPanelView.clearAllGoals();
        }
    }
}
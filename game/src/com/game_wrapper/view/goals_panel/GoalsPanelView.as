package com.game_wrapper.view.goals_panel {
    import by.lord_xaoca.robotlegs2.BaseView;
    import by.lord_xaoca.utils.ClassUtils;
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import com.game_wrapper.model.goals.enum.GoalTypesEnum;
    import com.game_wrapper.model.goals.base.IGoalModel;
    import com.game_wrapper.view.goals_panel.goals.NumberOfDeletedItemsBackgroundsGoalView;
    import com.game_wrapper.view.goals_panel.goals.NumberOfDeletedItemsGoalView;
    import com.game_wrapper.view.goals_panel.goals.ReachScoreGoalView;

    import flash.display.DisplayObject;
    import flash.text.TextField;

    /**
     * GoalsView class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class GoalsPanelView extends BaseView {

        private var _goalViews: Array;
        private var _header: TextField;


        public function GoalsPanelView(view: DisplayObject) {
            super(view);
        }


        public function addGoals(goals: Array): void {

            for (var i:int = 0; i < goals.length; i++) {

                var goalView: BaseDisplayObjectContainer;
                var goalModel:IGoalModel = goals[i] as IGoalModel;

                switch (goalModel.id) {
                    case GoalTypesEnum.REACH_SCORE_GOAL:
                        goalView = new ReachScoreGoalView(goalModel);
                        break;

                    case GoalTypesEnum.NUMBER_OF_DELETED_ITEMS_WITH_SOME_TYPE_GOAL:
                        goalView = new NumberOfDeletedItemsGoalView(goalModel);
                        break;


                    case GoalTypesEnum.NUMBER_OF_DELETED_ITEMS_BACKGROUNDS:
                        goalView = new NumberOfDeletedItemsBackgroundsGoalView(goalModel);
                        break;
                }

                _goalViews.push(goalView);

                addChild(goalView.view);
            }

            _alignGoals();
        }


        public function clearAllGoals(): void {
            for (var i: int = 0; i < _goalViews.length; i++) {
                _goalViews[i].destroy();
            }
            _goalViews = [];
        }


        override protected function initView(): void {
            super.initView();

            //_header = getTextField("header");
            //_header.text = "Goals";
        }


        override protected function initialize(): void {
            super.initialize();

            _goalViews = [];
        }


        private function _alignGoals(): void {
            var yy: Number = 0; //_header.height;
            for (var i: int = 0; i < _goalViews.length; i++) {
                var goalView: BaseDisplayObjectContainer = _goalViews[i];
                goalView.y = yy;
                yy += goalView.height;
            }
        }
    }
}
package com.game_wrapper.view.goals_panel.goals {
    import by.lord_xaoca.utils.ClassUtils;
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import com.game_wrapper.model.goals.base.GoalModelEvent;
    import com.game_wrapper.model.goals.base.IGoalModel;

    /**
     * BaseGoalView class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class BaseGoalView extends BaseDisplayObjectContainer {

        protected var _goalModel: IGoalModel;


        public function BaseGoalView(goalModel: IGoalModel) {
            _goalModel = goalModel;
            _goalModel.addEventListener(GoalModelEvent.GOAL_DATA_UPDATED, onGoalDataUpdatedHandler);
            super(ClassUtils.getInstanceByClassName("View_" + _goalModel.id));
        }


        override public function destroy(): void {
            super.destroy();

            _goalModel.removeEventListener(GoalModelEvent.GOAL_DATA_UPDATED, onGoalDataUpdatedHandler);
        }


        override protected function initView(): void {
            super.initView();
            updateViewFromModel();
        }


        protected function updateViewFromModel(): void {

        }


        protected function onGoalDataUpdatedHandler(event: GoalModelEvent): void {
            updateViewFromModel();
        }
    }
}
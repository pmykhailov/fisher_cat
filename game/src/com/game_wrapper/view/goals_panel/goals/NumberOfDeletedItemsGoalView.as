package com.game_wrapper.view.goals_panel.goals {
    import com.game_wrapper.model.goals.NumberOfDeletedItemsGoalModel;
    import com.game_wrapper.model.goals.base.IGoalModel;

    /**
     * NumberOfDeletedItemsGoalModel class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class NumberOfDeletedItemsGoalView extends BaseGoalView {

        public function NumberOfDeletedItemsGoalView(goalModel: IGoalModel) {
            super(goalModel);
        }


        private function get currentGoalModel(): NumberOfDeletedItemsGoalModel {
            return _goalModel as NumberOfDeletedItemsGoalModel;
        }


        override protected function updateViewFromModel(): void {
            getMovieClip("types").gotoAndStop(currentGoalModel.itemType);
            getTextField("goal").text = currentGoalModel.currentNumber + "/" + currentGoalModel.targetNumber;
        }
    }
}
package com.game_wrapper.view.goals_panel.goals {
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import com.game_wrapper.model.goals.base.IGoalModel;
    import com.game_wrapper.model.goals.ReachScoreGoalModel;

    /**
     * ReachScoreGoalView class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class ReachScoreGoalView extends BaseGoalView {

        public function ReachScoreGoalView(goalModel: IGoalModel) {
            super(goalModel);
        }


        override protected function updateViewFromModel(): void {
            super.updateViewFromModel();
            getTextField("goal").text = "GOAL: ";
            getTextField("goal_score").text = (_goalModel as ReachScoreGoalModel).targetScore + "";
        }
    }
}
package com.game_wrapper.view.goals_panel.goals {
    import com.game_wrapper.model.goals.NumberOfDeletedItemsBackgroundsGoalModel;
    import com.game_wrapper.model.goals.NumberOfDeletedItemsGoalModel;
    import com.game_wrapper.model.goals.base.IGoalModel;

    /**
     * NumberOfDeletedItemsGoalModel class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class NumberOfDeletedItemsBackgroundsGoalView extends BaseGoalView {

        public function NumberOfDeletedItemsBackgroundsGoalView(goalModel: IGoalModel) {
            super(goalModel);
        }


        private function get currentGoalModel(): NumberOfDeletedItemsBackgroundsGoalModel {
            return _goalModel as NumberOfDeletedItemsBackgroundsGoalModel;
        }


        override protected function updateViewFromModel(): void {
            getTextField("goal").text = currentGoalModel.currentCount + "/" + currentGoalModel.targetCount;
        }
    }
}
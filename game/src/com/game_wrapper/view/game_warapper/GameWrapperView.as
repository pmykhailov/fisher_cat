package com.game_wrapper.view.game_warapper {
    import by.lord_xaoca.robotlegs2.BaseView;
    import by.lord_xaoca.ui.components.buttons.MovieClipButton;
    import by.lord_xaoca.utils.ClassUtils;

    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.view.goals_panel.GoalsPanelView;
    import com.game_wrapper.view.ready_go.ReadyGo;
    import com.game_wrapper.view.top_bar.TopBarView;

    import flash.display.Sprite;
    import flash.events.MouseEvent;

    /**
     * GameWrapper class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class GameWrapperView extends BaseView {

        private var _topBar: TopBarView;
        private var _goalsPanelView: GoalsPanelView;
        private var _readyGo: ReadyGo;
        private var _gameContainer: Sprite;
        private var _gameSettingsButton: MovieClipButton;


        public function GameWrapperView() {
            super(ClassUtils.getInstanceByClassName("View_GameWrapper"));
        }


        public function get topBar(): TopBarView {
            return _topBar;
        }


        public function get goalsPanelView(): GoalsPanelView {
            return _goalsPanelView;
        }


        public function get gameContainer(): Sprite {
            return _gameContainer;
        }


        override public function destroy(): void {
            super.destroy();

            _topBar.destroy();
            _goalsPanelView.destroy();
            _readyGo.destroy();
            //TODO set to null or not?
        }


        override protected function initView(): void {
            super.initView();

            mouseEnabled = false;

            _topBar = new TopBarView(getSprite("topBar"));
            _goalsPanelView = new GoalsPanelView(getSprite("goalsPanel"));
            _gameContainer = getSprite("gameContainer");
            _readyGo = new ReadyGo(getMovieClip("readyGo"));
            _initGameSettingsButton();
        }


        private function _initGameSettingsButton(): void {
            _gameSettingsButton = new MovieClipButton(getMovieClip("gameSettingsButton"));
            _gameSettingsButton.addEventListener(MouseEvent.CLICK, onGameSettingsButtonClickHandler);
        }


        private function _destroyGameSettingsButton(): void {
            _gameSettingsButton.destroy();
            // TODO? check if listener was removed
            trace("!Checking " + _gameSettingsButton.hasEventListener(GameWrapperContextEvent.GAME_SETTINGS_BUTTON_CLICKED));
        }


        private function onGameSettingsButtonClickHandler(event: MouseEvent): void {
            dispatchEvent(new GameWrapperContextEvent(GameWrapperContextEvent.GAME_SETTINGS_BUTTON_CLICKED));
        }
    }
}
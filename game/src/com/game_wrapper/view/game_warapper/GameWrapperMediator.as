package com.game_wrapper.view.game_warapper {
    import com.game_wrapper.event.GameWrapperContextEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    public class GameWrapperMediator extends Mediator {

        [Inject]
        public var gameWrapperView:GameWrapperView;

        public function GameWrapperMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();

            addViewListener(GameWrapperContextEvent.GAME_SETTINGS_BUTTON_CLICKED, dispatch);
        }
    }
}

package com.game_wrapper.view.ready_go {
import by.lord_xaoca.robotlegs2.BaseView;
import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

import com.game_wrapper.event.GameWrapperContextEvent;

import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.events.Event;

public class ReadyGo extends BaseView {
    public function ReadyGo(view:DisplayObject) {
        super(view);
    }

    override protected function initView():void {
        super.initView();

        mouseChildren = false;
        mouseEnabled = false;

        currentView.gotoAndStop(1);
    }

    public function play():void{
        addViewListener(Event.COMPLETE, onAnimationCompleteHandler);
        currentView.gotoAndPlay(1);
    }

    private function onAnimationCompleteHandler(event: Event):void {
        currentView.gotoAndStop(1);
        dispatchEvent(new GameWrapperContextEvent(GameWrapperContextEvent.READY_GO_ANIMATION_COMPLETE));
    }

    private function get currentView():MovieClip{
        return _view as MovieClip;
    }


}
}

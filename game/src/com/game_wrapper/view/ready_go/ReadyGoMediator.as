package com.game_wrapper.view.ready_go {
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.share.events.GameWrapperControllEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    /**
     * TopBarMediator class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class ReadyGoMediator extends Mediator {

        [Inject]
        public var readyGo: ReadyGo;


        public function ReadyGoMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();

            addContextListener(GameWrapperControllEvent.PLAY_READY_GO_ANIMATION, onPlayReadyGoAnimationHandler);

            addViewListener(GameWrapperContextEvent.READY_GO_ANIMATION_COMPLETE, dispatch);
        }


        private function onPlayReadyGoAnimationHandler(event: GameWrapperControllEvent): void {
            //SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEMS_MATCHED);
            readyGo.play();
        }

    }
}
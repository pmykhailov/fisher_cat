package com.game_wrapper.view.top_bar {
    import by.lord_xaoca.robotlegs2.BaseView;
    import by.lord_xaoca.ui.components.buttons.DoubleStateButton;

    import com.game_wrapper.misc.Format;

    import flash.display.DisplayObject;
    import flash.text.TextField;

    /**
     * TopBarView class.
     * User: Paul Makarenko
     * Date: 03.10.13
     */
    public class TopBarView extends BaseView {
        private var _soundButton: DoubleStateButton;
        private var _score: TextField;
        private var _time: TextField;
        private var _level: TextField;


        public function TopBarView(view: DisplayObject) {
            super(view);
        }


        public function set score(value: int): void {
            _score.text = value + "";
        }


        public function set time(value: int): void {
            _time.text = Format.time(value);
        }


        public function set level(value: int): void {
            _level.text = "Level " + value;
        }


        override protected function initView(): void {
            super.initView();

            _initButtons();
            _initTextFields();
        }


        private function _initTextFields(): void {
            _score = getTextField("score");
            _time = getTextField("time");
            _level = getTextField("level");
        }


        private function _initButtons(): void {
            _soundButton = new DoubleStateButton(getMovieClip("soundButton"));
            _soundButton.visible = false;
        }
    }
}
package com.game_wrapper.view.top_bar {
    import com.game_wrapper.view.*;
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game.events.GameControlEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    /**
     * TopBarMediator class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class TopBarMediator extends Mediator {

        [Inject]
        public var topBarView:TopBarView;

        public function TopBarMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();

            addContextListener(GameWrapperContextEvent.SCORE_CHANGED, onScoreChangedHandler);
            addContextListener(GameWrapperContextEvent.TIME_CHANGED, onTimeChangedHandler);
        }


        private function onScoreChangedHandler(event: GameWrapperContextEvent): void {
            topBarView.score = event.data as int;
        }


        private function onTimeChangedHandler(event: GameWrapperContextEvent): void {
            topBarView.time = event.data as int;
        }

    }
}
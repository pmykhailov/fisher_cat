package com.application {
    import com.application.controller.TestGameControlCommand;
    import com.application.controller.init.InitSoundsCommand;
    import com.application.controller.init.LoadResoursesCommand;
    import com.share.events.ApplicationEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.api.IInjector;

    /**
     * Application will
     * - make basic initialization
     * - control communication between all modules
     */
    public class ApplicationConfig implements IConfig {

        [Inject]
        public var context: IContext;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var mediatorMap: IMediatorMap;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: IInjector;
        [Inject]
        public var contextView: ContextView;


        public function configure(): void {

            mapCommands();
            mapMediators();
            mapInjections();

            context.afterInitializing(init);
        }


        private function mapCommands(): void {
            eventCommandMap.map(ApplicationEvent.LOAD_RESOURSES).toCommand(LoadResoursesCommand);
            eventCommandMap.map(ApplicationEvent.RESOURSES_LOADED).toCommand(InitSoundsCommand);
            //eventCommandMap.map(ApplicationEvent.RESOURSES_LOADED).toCommand(TestGameControlCommand);
        }


        private function mapMediators(): void {
        }


        private function mapInjections(): void {
        }


        private function init(): void {
            dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.LOAD_RESOURSES));
        }
    }
}

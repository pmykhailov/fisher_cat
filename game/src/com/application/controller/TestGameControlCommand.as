package com.application.controller {
    import com.game.events.GameControlEvent;
    import com.game.model.vo.AddGameVO;

    import flash.display.Sprite;
    import flash.events.IEventDispatcher;
    import flash.events.MouseEvent;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;

import model.grids.items.GridItemTypeEnum;

import model.level.settings.GameSettings;
import model.level.settings.vo.GridSettingsVO;

import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    /**
     * TestGameControllCommand class.
     * User: Paul Makarenko
     * Date: 20.04.14
     */
    public class TestGameControlCommand extends Command {

        [Inject]
        public var contextView: ContextView;
        [Inject]
        public var dispatcher: IEventDispatcher;
        private var _enableMouseClicks: Boolean = true;


        public function TestGameControlCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            addControlButtons();

            //dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.ADD_GAME, new AddGameVO(contextView.view)));
            //dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.START_GAME));
        }


        private function addControlButtons(): void {
            var buttonsHolder: Sprite = new Sprite();
            var add: Sprite = new Sprite();
            var start: Sprite = new Sprite();
            var pause: Sprite = new Sprite();
            var resume: Sprite = new Sprite();
            var restart: Sprite = new Sprite();
            var finish: Sprite = new Sprite();
            var remove: Sprite = new Sprite();
            var mouseEnable: Sprite = new Sprite();

            var initButton: Function = function (btn: Sprite, label: String): void {
                btn.graphics.beginFill(0xFFCCCC);
                btn.graphics.drawRect(0, 0, 100, 40);
                btn.graphics.endFill();

                var tf: TextField = new TextField();
                tf.autoSize = TextFieldAutoSize.CENTER;
                tf.text = label;
                tf.x = (btn.width - tf.width) / 2;
                tf.y = (btn.height - tf.height) / 2;
                btn.addChild(tf);

                btn.mouseChildren = false;
            };

            var buttons: Array = [
                {button: add, label: "ADD"},
                {button: start, label: "START"},
                {button: pause, label: "PAUSE"},
                {button: resume, label: "RESUME"},
                {button: restart, label: "RESTART"},
                {button: finish, label: "FINISH"},
                {button: remove, label: "REMOVE"},
                {button: mouseEnable, label: "MOUSE_ENABLE"},
            ]

            for (var i: int = 0; i < buttons.length; i++) {
                var object: Object = buttons[i];
                initButton(object.button, object.label);
                object.button.buttonMode = true;
                object.button.y = 100 + i * 50;
                object.button.name = object.label;
                object.button.addEventListener(MouseEvent.CLICK, onButtonClickHandler);
                buttonsHolder.addChild(object.button);
            }

            buttonsHolder.x = 500;
            contextView.view.addChild(buttonsHolder);
        }


        private function onButtonClickHandler(event: MouseEvent): void {
            var targetName: String = event.target.name;

            switch (targetName) {
                case "ADD":
                    var gameSettings:GameSettings = new GameSettings();
                    var gridSettingsVOs:Array = gameSettings.gridSettingsVOs;
                    var gridSettingsVO:GridSettingsVO = gridSettingsVOs[1] as GridSettingsVO;
                    var rows:int = gameSettings.gameFieldRows;
                    var cols:int = gameSettings.gameFieldCols;

                    for (var i:int = 0; i < rows; i++) {
                        for (var j:int = 0; j < cols; j++) {
                            gridSettingsVO.elements[i][j] = GridItemTypeEnum.EMPTY;
                        }
                    }

                    dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.ADD_GAME, new AddGameVO(contextView.view, gameSettings)));
                    break;

                case "START":
                    dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.START_GAME));
                    break;

                case "PAUSE":
                    dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.PAUSE_GAME));
                    break;

                case "RESUME":
                    dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.RESUME_GAME));
                    break;

                case "RESTART":
                    dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.RESTART_GAME));
                    break;

                case "FINISH":
                    dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.FINISH_GAME));
                    break;

                case "REMOVE":
                    dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.REMOVE_GAME));
                    break;

                case "MOUSE_ENABLE":
                    _enableMouseClicks = !_enableMouseClicks;
                    dispatcher.dispatchEvent(new GameControlEvent(GameControlEvent.ENABLE_GAME_MOUSE_CLICKS, _enableMouseClicks));
                    break;
            }
        }
    }
}
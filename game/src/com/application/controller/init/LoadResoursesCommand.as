package com.application.controller.init {
    import by.lord_xaoca.helpers.LoaderHelper;

    import com.share.ApplicationDimensions;
    import com.share.events.ApplicationEvent;

    import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.events.IEventDispatcher;
    import flash.system.ApplicationDomain;
    import flash.system.LoaderContext;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    /**
     * LoadResoursesCommand class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class LoadResoursesCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var contextView: ContextView;
        private var _preloader: MovieClip;
        private var _resourses: Array = ["game.swf", "game_wrapper.swf", "screens.swf", "sounds.swf"];
        private var _currentResourseIndex: int;


        public function LoadResoursesCommand() {
            super();
        }


        override public function execute(): void {
            _currentResourseIndex = 0;
            loadResourse();
            showLoadingText();
        }


        private function showLoadingText(): void {
            _preloader = new View_Preloader();
            var tf: TextFormat = new TextFormat();
            tf.size = 40;
            tf.color = 0xFFFFFF;
            contextView.view.addChild(_preloader);
        }


        private function loadResourse(): void {
            var loaderHelper: LoaderHelper = new LoaderHelper(_resourses[_currentResourseIndex], onLoadCompleteHandler, onErrorHandler);
            var context: LoaderContext = new LoaderContext();
            context.applicationDomain = ApplicationDomain.currentDomain;

            loaderHelper.addLoaderContext(context);
            loaderHelper.load();
        }


        private function onLoadCompleteHandler(content: DisplayObject): void {
            if (_currentResourseIndex == _resourses.length - 1) {
                hideLoadingText();
                dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.RESOURSES_LOADED));
            } else {
                _currentResourseIndex++;
                loadResourse();
            }
        }


        private function hideLoadingText(): void {
            contextView.view.removeChild(_preloader);
        }


        private function onErrorHandler(error: String): void {
            trace("Error " + error);
        }

    }
}
package com.share {

    /**
     * ApplicationDimensions class.
     * User: Paul Makarenko
     * Date: 03.10.13
     */
    public class ApplicationDimensions {

        public static const WIDTH:int = 720;
        public static const HEIGHT:int = 1280;

        public function ApplicationDimensions() {
        }

    }
}
package com.share.events {
    import flash.events.Event;

    /**
     * ApplicationEvent class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class ApplicationEvent extends Event {

        public static const LOAD_RESOURSES:String = "loadResourses";
        public static const RESOURSES_LOADED:String = "resoursesLoaded";

        public function ApplicationEvent(type: String, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
        }


        override public function clone(): Event {
            return new ApplicationEvent(type, bubbles, cancelable);
        }
    }
}
package by.lord_xaoca.ui {

    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import flash.display.Sprite;

    /**
     * BaseItemRenderer class.
     *
     * @author: Ivan Shaban
     * @date: 18.01.12 18:33
     */

    public class BaseUIElement extends BaseDisplayObjectContainer {

        protected var _data: Object;
        /**
         * If data == null, so _fillData doesn't calls
         */
        protected var _preventNullData: Boolean;
        /**
         * If new data value equals to old data value, so _fillData doesn't calls
         */
        protected var _preventRefillData: Boolean;


        public function BaseUIElement(view: Sprite) {
            super(view);
        }


        public function get data(): Object {
            return _data;
        }


        public function set data(value: Object): void {
            if (_preventRefillData && _data == value || _preventNullData && value == null) {
                return;
            }

            _prepareData(value);
            _fillData();
        }


        protected function _prepareData(value: Object): void {
            _data = value;
        }


        protected function _fillData(): void {
            // ABSTRACT
        }

    }
}

package by.lord_xaoca.ui.components.sliders {

    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    /**
     * HorizontalSlider class.
     *
     * @author Ivan Shaban
     * @date 01.10.2011 18:39
     */

    [Event(name="change", type="flash.events.Event")]

    public class HorizontalSlider extends MovableContentContainer implements IObserver {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _slider:MovieClipButton;
        protected var _downButton:MovieClipButton;
        protected var _upButton:MovieClipButton;

        protected var _bar:Sprite;
        protected var _minValue:Number;
        protected var _maxValue:Number;
        protected var _step:Number = 0;

        protected var _isIncrease:Boolean; // should we increase value per iteration or decrease
        protected var _needToUpdate:Boolean; // should we dispatch change events if value is changed or not

        // ------------------ PRIVATE VARIABLES ------------------------

        protected var _actualValue:Number; // value without applying corrections.
        private var _predefinedValues:Array;
        private var _sliderPos:int;
        private var _corrector:Boolean;

        // ------------------ CONSTRUCTOR ------------------------------

        public function HorizontalSlider (view:Sprite, minValue:Number = 0, maxValue:Number = 1) {
            super();

            _minValue = minValue;
            _maxValue = maxValue;

            initView(view);
            initButtons();
            initSlider();

            _setupPlacement();
            _checkPlacement();
        }

        // ------------------ PROPERTIES -------------------------------

        public function get minValue ():Number {
            return _minValue;
        }

        public function get maxValue ():Number {
            return _maxValue;
        }

        public function get enable ():Boolean {
            return _view.mouseChildren;
        }

        public function set enable ($value:Boolean):void {
            _view.mouseChildren = $value;
        }

        public function get value ():Number {
            return _actualValue;
        }

        public function set value ($value:Number):void {
            if ($value > _maxValue) {
                $value = _maxValue;
            } else if ($value < _minValue) {
                $value = _minValue;
            }

            _corrector = Boolean($value % _step);
            _sliderPos = int(($value - _minValue) / _step);
            _needToUpdate = false;// why was true?;
            actualValue = $value;
        }

        public function set actualValue ($value:Number):void {
            if ($value > _maxValue) {
                $value = _maxValue;
            } else if ($value < _minValue) {
                $value = _minValue;
            }

            if ($value == _actualValue) {
                return;
            }

            _actualValue = $value;

            if (Globals.useMirroring) {
                _slider.x = _bar.x + (1 - (_actualValue - _minValue) / (_maxValue - _minValue)) * (_bar.width - _slider.width);
            } else {
                _slider.x = _bar.x + (_actualValue - _minValue) / (_maxValue - _minValue) * (_bar.width - _slider.width);
            }

            if (_needToUpdate) {
                _needToUpdate = false;
                dispatchEvent(new Event(Event.CHANGE));
            }
        }

        // ------------------ PUBLIC METHODS ---------------------------

        /**
         * Setup new max and min values;
         * @param    minValue
         * @param    maxValue
         * @param    step
         */
        public function refresh (minValue:Number, maxValue:Number, step:int):void {
            _minValue = minValue;
            _maxValue = maxValue;
            _actualValue = 0;
            _step = step;

            _createPredefinedValues();

            //hide slider when we have no option to choose value
            _slider.visible = !(minValue == maxValue);
        }

        /* INTERFACE com.popover.library.utils.observers.IObserver */

        public function updateByEnterframe ():void {
            if ((_isIncrease && (_actualValue == _maxValue)) || (!_isIncrease && (_actualValue == _minValue))) {
                return GlobalDispatcher.remove(this);
            }

            _needToUpdate = true;

            if (_isIncrease) {
                _sliderPos++;
            } else if (!_corrector) {
                _sliderPos--;
            }

            _corrector = false;

            actualValue = _predefinedValues[_sliderPos];
        }

        // ------------------ PROTECTED METHODS ------------------------

        protected function initView (view:Sprite):void {
            _view = view;
            _view.mouseEnabled = false;
        }

        protected function initButtons ():void {
            _downButton = new MovieClipButton(getMovieClip("downButton"));
            _downButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressHandler);

            _upButton = new MovieClipButton(getMovieClip("upButton"));
            _upButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressHandler);
        }

        protected function initSlider ():void {
            _bar = getMovieClip("bar");
            _bar.addEventListener(MouseEvent.CLICK, onBarClickHandler);

            _slider = new MovieClipButton(getMovieClip("slider"));
            _slider.x = _bar.x;
            _slider.addEventListener(MouseEvent.MOUSE_DOWN, onSliderPressHandler);
        }

        /**
         * Calculate value based of slider position.
         */
        protected function calculateValue ():void {
            var newValue:Number;
            if (Globals.useMirroring) {
                newValue = _minValue + (_maxValue - _minValue) * (1 - (_slider.x - _bar.x) / (_bar.width - _slider.width));
            } else {
                newValue = _minValue + (_maxValue - _minValue) * ((_slider.x - _bar.x) / (_bar.width - _slider.width));
            }

            if (_actualValue == newValue) {
                return;
            }

            if (newValue > _maxValue) {
                newValue = _maxValue;
            } else if (newValue < _minValue) {
                newValue = _minValue;
            }

            _corrector = false;
            _sliderPos = Math.round((newValue - minValue) / _step);

            _actualValue = _predefinedValues[_sliderPos];

            if (_needToUpdate) {
                _needToUpdate = false;
                dispatchEvent(new Event(Event.CHANGE));
            }
        }

        override protected function _initMovableItems ():void {
            _movableItems.push(_downButton, _upButton);
        }

        // ------------------ PRIVATE METHODS --------------------------

        private function _createPredefinedValues ():void {
            _predefinedValues = [];

            var a:int = Math.ceil(_minValue / _step);
            var b:int = Math.floor(_maxValue / _step);

            if (_minValue != (a * _step)) {
                _predefinedValues.push(_minValue);
            }

            var len:int = b + 1;
            for (var j:int = a; j < len; j++) {
                _predefinedValues.push(j * _step);
            }

            if (_maxValue != (b * _step)) {
                _predefinedValues.push(_maxValue);
            }

            _sliderPos = _predefinedValues.length - 1;
        }

        // ------------------ EVENT HANDLERS ---------------------------

        protected function onSliderPressHandler (event:MouseEvent):void {
            _slider.view.startDrag(false, new Rectangle(_bar.x, _slider.y, _bar.width - _slider.width, 0));

            GlobalDispatcher.addCustomEventListener(MouseEvent.MOUSE_UP, onSliderReleaseHandler);
            GlobalDispatcher.addCustomEventListener(MouseEvent.MOUSE_MOVE, onSliderMoveHandler);
        }

        protected function onSliderReleaseHandler (event:MouseEvent):void {
            _slider.view.stopDrag();

            GlobalDispatcher.removeCustomEventListener(MouseEvent.MOUSE_UP, onSliderReleaseHandler);
            GlobalDispatcher.removeCustomEventListener(MouseEvent.MOUSE_MOVE, onSliderMoveHandler);
        }

        protected function onSliderMoveHandler (event:MouseEvent):void {
            _needToUpdate = true;
            calculateValue();
        }

        protected function onButtonPressHandler (event:MouseEvent):void {
            switch (event.currentTarget) {
                case _upButton:
                {
                    _isIncrease = true;
                    break;
                }

                case _downButton:
                {
                    _isIncrease = false;
                    break;
                }

                default:
                {
                    return;
                }
            }
            updateByEnterframe();

            TweenLite.killDelayedCallsTo(GlobalDispatcher.add);
            TweenLite.delayedCall(0.5, GlobalDispatcher.add, [this]);

            GlobalDispatcher.addCustomEventListener(MouseEvent.MOUSE_UP, onButtonReleaseHandler);
        }

        protected function onButtonReleaseHandler (event:MouseEvent):void {
            TweenLite.killDelayedCallsTo(GlobalDispatcher.add);

            GlobalDispatcher.remove(this);
            GlobalDispatcher.removeCustomEventListener(MouseEvent.MOUSE_UP, onButtonReleaseHandler);
        }

        protected function onBarClickHandler (event:MouseEvent):void {
            _needToUpdate = true;
            var newValue:int;
            if (!Globals.useMirroring) {
                newValue = _minValue + (_maxValue - _minValue) * event.localX / _bar.width * _bar.scaleX;
            } else {
                newValue = _maxValue - (_maxValue - _minValue) * event.localX / _bar.width * _bar.scaleX;
            }

            _corrector = false;
            _sliderPos = Math.round((newValue - minValue) / _step);

            actualValue = _predefinedValues[_sliderPos];
        }

        // ------------------ END CLASS --------------------------------

    }
}

package by.lord_xaoca.ui.components.checkbox {

    /**
     * SimpleCheckBoxButtonBlack class.
     *
     * @author Ivan Shaban
     * @date 03.01.2012 11:32
     */
    public class SimpleCheckBoxButtonBlack extends CheckBoxButton {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        override protected function _initLabel ():void {
            super._initLabel();
            _labelTF.textColor = 0x3D2628;
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
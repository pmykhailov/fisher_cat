package by.lord_xaoca.ui.components.checkbox {

    import flash.text.TextFieldAutoSize;

    /**
     * ...
     * @author Vladimir Ivanchenko
     */
    public class CheckBoxButton extends BaseCheckBoxButton {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function CheckBoxButton () {
            _autoSize = true;
            super(new View_CheckBoxButton());
        }

        // ------------------ PROPERTIES -------------------------------

        override public function set label (value:String):void {
            _labelTF.text = value;

            _reInitItemPosition(_checkPositions);
        }

        override public function set autoSize (value:Boolean):void {
            if (value == _autoSize) {
                return;
            }
            _autoSize = value;

            _reInitItemPosition(_checkPositions);
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        override protected function setSelectedState ():void {
            super.setSelectedState();
            _bg.visible = true;
        }

        override protected function _checkPositions ():void {
            if (_autoSize) {
                _labelTF.autoSize = TextFieldAutoSize.LEFT;
                _bg.width = _labelTF.x + _labelTF.width + 4;
            } else if (_center) {
                _labelTF.x = int((_bg.width - _labelTF.width) / 2);
            } else {
                _labelTF.x = 4;
                _bg.width = LabelButton.MIN_BUTTON_WIDTH;//DEF_WIDTH;
            }
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
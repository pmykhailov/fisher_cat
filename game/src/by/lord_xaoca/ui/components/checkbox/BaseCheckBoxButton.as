package by.lord_xaoca.ui.components.checkbox {

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.MouseEvent;

    /**
     * CheckBoxButton class.
     *
     * @author Ivan Shaban
     * @date 09.11.2011 15:00
     */
    public class BaseCheckBoxButton extends LabelButton implements IItemRenderer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _checkBox:MovieClip;
        protected var _checkedMc:Sprite;

        protected var _id:int;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function BaseCheckBoxButton (view:DisplayObject = null) {
            super(view ? view : new View_TH_CheckboxButton(), true);
            _view.addEventListener(MouseEvent.ROLL_OVER, onOverHandler);
            _view.addEventListener(MouseEvent.ROLL_OUT, onOutHandler);
        }

        // ------------------ PROPERTIES -------------------------------

        override public function get data ():Object {
            return _data;
        }

        override public function set data (value:Object):void {
            _data = value;
            label = _data.title || _data.label;
        }

        public function get id ():int {
            return _id;
        }

        public function set id (value:int):void {
            _id = value
        }

        override public function get label ():String {
            return super.label;
        }

        override public function set label (value:String):void {
            _labelTF.text = value;
            _reInitItemPosition(_checkPositions);
        }

        override public function get autoSize ():Boolean {
            return super.autoSize;
        }

        override public function set autoSize (value:Boolean):void {
            if (value == _autoSize) {
                return;
            }
            _autoSize = value;
            _reInitItemPosition(_checkPositions);
        }

        override public function get center ():Boolean {
            return super.center;
        }

        override public function set center (value:Boolean):void {
            if (value == _center) {
                return;
            }
            _center = value;
            _reInitItemPosition(_centerLabel);
        }

        override public function get width ():Number {
            return super.width;
        }

        override public function set width (value:Number):void {
            _reInitItemPosition(function ():void {
                _bg.width = value;

                if (_checkedMc) {
                    _checkedMc.width = _bg.width;
                }

                _centerLabel();
            });
        }

        override public function get enabled ():Boolean {
            return state != LabelButton.STATE_DISABLED;
        }

        // ------------------ PUBLIC METHODS ---------------------------
        public function get textField ():TLFTextField {
            return _labelTF;
        }

        override public function destroy ():void {
            view.removeEventListener(MouseEvent.ROLL_OVER, onOverHandler);
            view.removeEventListener(MouseEvent.ROLL_OUT, onOutHandler);

            _checkBox = null;
            _data = null;

            super.destroy();
        }

        // ------------------ PROTECTED METHODS ------------------------

        override protected function _initOwnMethods ():void {
            _initCheckBox();
            _initCheckedMc();
        }

        protected function _initCheckedMc ():void {
            if (!getMovieClip("checked")) {
                return;
            }
            _checkedMc = getMovieClip("checked");
            _checkedMc.visible = false;
        }

        protected function _initCheckBox ():void {
            _checkBox = getMovieClip("checkbox");
            _checkBox.mouseChildren = false;
            _checkBox.mouseEnabled = false;
        }

        override protected function setNormalState ():void {
            super.setNormalState();
            _checkBox.gotoAndStop(1);
            if (_checkedMc) {
                _checkedMc.visible = false;
            }
            _bg.visible = true;

            mouseChildren = true;
        }

        override protected function _checkPositions ():void {
            if (_autoSize) {
                _labelTF.x = _checkBox.x + _checkBox.width + 4;
                _bg.width = _labelTF.x + _labelTF.width + 4;
                if (_checkedMc) {
                    _checkedMc.width = _bg.width;
                }
            } else if (_center) {
                _labelTF.x = _checkBox.x + _checkBox.width + int((_bg.width - _checkBox.x - _checkBox.width - _labelTF.width) / 2);
            } else {
                _labelTF.x = _checkBox.x + _checkBox.width + 4;
            }
        }

        override protected function setSelectedState ():void {
            _checkBox.gotoAndStop(3);
            if (_checkedMc) {
                _checkedMc.visible = true;
            }
            _bg.visible = false;
        }

        override protected function setDisableState ():void {
            super.setDisableState();

            _view.mouseChildren = false;
            _bg.state = MovieClipButton.STATE_DISABLE;
        }

        override protected function _initMovableItems ():void {
            super._initMovableItems();
            _movableItems.push(_checkBox);
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        private function onOverHandler (event:MouseEvent):void {
            if (selected) {
                return;
            }
            _checkBox.gotoAndStop(2);
        }

        private function onOutHandler (event:MouseEvent):void {
            if (selected) {
                return;
            }
            _checkBox.gotoAndStop(1);
        }

        // ------------------ END CLASS --------------------------------

    }
}
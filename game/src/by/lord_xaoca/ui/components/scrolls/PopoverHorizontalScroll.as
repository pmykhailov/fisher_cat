package by.lord_xaoca.ui.components.scrolls {

    import flash.display.Sprite;
    import flash.events.MouseEvent;

    /**
     * PopoverHorizontalScroll class.
     *
     * @author: Ivan Shaban
     * @date: 21.11.11 14:49
     */

    public class PopoverHorizontalScroll extends HorizontalScroller implements IDisplayedComponent {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _isMouseOver:Boolean;
        protected var _isShows:Boolean = true;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function PopoverHorizontalScroll (view:Sprite, useMouseWheel:Boolean = false) {
            TweenPlugin.activate([AutoAlphaPlugin]);

            super(view, useMouseWheel);
        }

        // ------------------ PROPERTIES -------------------------------

        override protected function initView (view:Sprite):void {
            super.initView(view);

            _view.addEventListener(MouseEvent.ROLL_OVER, onOverHandler);
            _view.addEventListener(MouseEvent.ROLL_OUT, onOutHandler);

            _bar.alpha = 0.7;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        public function show ():void {
            if (_isShows || scrollWidthByRatio == 1) {
                return;
            }

            _isShows = true;
            MouseWheelDispatcher.add(this);
            TweenLite.to(this, 0.5, {autoAlpha: 1});
            TweenLite.to(_bar, 0.3, {alpha: 0.7});
        }

        public function hide ():void {
            if (!_isShows) {
                return;
            }

            _isShows = false;
            MouseWheelDispatcher.remove(this);
            TweenLite.to(this, 0.5, {autoAlpha: 0});
            TweenLite.to(_bar, 0.3, {alpha: 0.7});
        }

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        private function onOverHandler (event:MouseEvent):void {
            _isMouseOver = true;
            if (BaseScroll._isDragged) {
                return;
            }
            TweenLite.to(_bar, 0.3, {alpha: 1});
        }

        private function onOutHandler (event:MouseEvent):void {
            _isMouseOver = false;
            if (BaseScroll._isDragged) {
                return;
            }
            TweenLite.to(_bar, 0.3, {alpha: 0.7});
        }

        // ------------------ END CLASS --------------------------------

    }
}

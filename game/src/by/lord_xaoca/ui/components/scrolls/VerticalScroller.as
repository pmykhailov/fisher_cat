package by.lord_xaoca.ui.components.scrolls {

    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    /**
     * VerticalScroller class.
     *
     * @author: Ivan Shaban
     * @date: 14.11.11 15:31
     */

    public class VerticalScroller extends BaseScroll {

        public function VerticalScroller(view: Sprite, useMouseWheel: Boolean = false) {
            super(view, useMouseWheel);
        }


        override public function updateByMouseWheel(deltaY: int): void {
            _scroll.y -= (_bar.height - _scroll.height - _topIndent - _bottomIndent) * _percentsPerStep * deltaY;

            super.updateByMouseWheel(deltaY);
        }


        override protected function updateValueByScrollPosition(): void {
            var newValue: Number = ((_scroll.y - _topIndent) / (_bar.height - _scroll.height - _topIndent - _bottomIndent));
            if (_value == newValue) {
                return;
            }

            _value = isNaN(newValue) ? 0 : newValue;

            super.updateValueByScrollPosition();
        }


        override protected function updateScrollPositionByValue(): void {
            _scroll.y = _topIndent + int(_value * (_bar.height - _scroll.height - _topIndent - _bottomIndent));
        }


        override protected function checkScrollPosition(): void {
            if (_scroll.y < _topIndent) {
                _scroll.y = _topIndent;
            }
            if (_scroll.y > _bar.height - _scroll.height - _bottomIndent) {
                _scroll.y = _bar.height - _scroll.height - _bottomIndent;
            }
        }


        override protected function onStartDragHandler(event: MouseEvent): void {
            _scroll.startDrag(false, new Rectangle(_scroll.x, _topIndent, 0, _bar.height - _scroll.height - _topIndent - _bottomIndent));

            super.onStartDragHandler(event);
        }


        override protected function onStopDragHandler(event: MouseEvent): void {
            _scroll.stopDrag();

            super.onStopDragHandler(event);
        }


        override protected function onBarClickHandler(event: MouseEvent): void {
            var point: Point = _view.globalToLocal(new Point(event.stageX, event.stageY));
            _scroll.y = point.y - _scroll.height / 2;

            super.onBarClickHandler(event);
        }
    }
}

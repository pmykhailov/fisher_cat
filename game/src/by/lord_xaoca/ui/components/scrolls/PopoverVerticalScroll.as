package by.lord_xaoca.ui.components.scrolls {

    import by.lord_xaoca.interfaces.IDisplayedComponent;

    import com.greensock.TweenLite;
    import com.greensock.plugins.AutoAlphaPlugin;
    import com.greensock.plugins.TweenPlugin;

    import flash.display.Sprite;
    import flash.events.MouseEvent;

    /**
     * PopoverVerticalScroll class.
     * In this class we always use mouseWheel.
     *
     * @author: Ivan Shaban
     * @date: 21.11.11 12:21
     */

    public class PopoverVerticalScroll extends VerticalScroller implements IDisplayedComponent {

        protected var _isMouseOver: Boolean;
        protected var _isShows: Boolean = true;


        public function PopoverVerticalScroll(view: Sprite) {
            TweenPlugin.activate([AutoAlphaPlugin]);

            super(view, false);
        }


        public function show(): void {
            if (_isShows || scrollHeightByRatio == 1) {
                return;
            }

            _isShows = true;
            //            MouseWheelDispatcher.add(this);
            TweenLite.to(this, 0.5, {autoAlpha: 1});
            TweenLite.to(_bar, 0.3, {alpha: 0.7});
        }


        public function hide(): void {
            if (!_isShows) {
                return;
            }

            _isShows = false;
            //            MouseWheelDispatcher.remove(this);
            TweenLite.to(this, 0.5, {autoAlpha: 0});
            TweenLite.to(_bar, 0.3, {alpha: 0.7});
        }


        override protected function initView(): void {
            super.initView();

            _view.addEventListener(MouseEvent.ROLL_OVER, onOverHandler);
            _view.addEventListener(MouseEvent.ROLL_OUT, onOutHandler);

            _bar.alpha = 0.7;
        }


        private function onOverHandler(event: MouseEvent): void {
            _isMouseOver = true;
            if (BaseScroll._isDragged) {
                return;
            }
            TweenLite.to(_bar, 0.3, {alpha: 1});
        }


        private function onOutHandler(event: MouseEvent): void {
            _isMouseOver = false;
            if (BaseScroll._isDragged) {
                return;
            }
            TweenLite.to(_bar, 0.3, {alpha: 0.7});
        }

    }
}

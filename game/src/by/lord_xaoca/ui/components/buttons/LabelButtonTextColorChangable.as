package by.lord_xaoca.ui.components.buttons {

    import flash.display.DisplayObject;
    import flash.events.MouseEvent;

    /**
     * LabelButtonTextColorChangable class.
     * That class we need for action controll buttons.
     * E.g. while mouse down on CALL button text color
     * should be changed to black.
     * @author: Pavel Mykhailov
     * @date: 30.04.12 12:47
     */

    public class LabelButtonTextColorChangable extends LabelButton {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        private var __normalStateLabelTextColor:Number;
        private var __selectedStateLabelTextColor:Number;

        // ------------------ CONSTRUCTOR ------------------------------

        public function LabelButtonTextColorChangable (view:DisplayObject, useShadow:Boolean = true, usePlacement:Boolean = true, selectedStateLabelTextColor:Number = 0x000000) {
            super(view, useShadow, usePlacement);

            __normalStateLabelTextColor = _labelTF.textColor;
            __selectedStateLabelTextColor = selectedStateLabelTextColor;

            initListeners();
        }

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        override public function destroy ():void {
            _bg.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
            super.destroy();
        }

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        private function initListeners ():void {
            _bg.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
        }

        private function onMouseDownHandler (event:MouseEvent):void {
            _labelTF.textColor = __selectedStateLabelTextColor;

            GlobalDispatcher.addCustomEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
        }

        private function onMouseUpHandler (event:MouseEvent):void {
            _labelTF.textColor = __normalStateLabelTextColor;

            GlobalDispatcher.removeCustomEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
        }

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}

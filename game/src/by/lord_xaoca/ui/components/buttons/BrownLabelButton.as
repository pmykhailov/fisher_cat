package by.lord_xaoca.ui.components.buttons {

    /**
     * BrownLabelButton class.
     *
     * @author Ivan Shaban
     * @date 10.11.2011 16:07
     */
    public class BrownLabelButton extends LabelButton implements IItemRenderer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        private var _id:int;

        // ------------------ CONSTRUCTOR ------------------------------

        public function BrownLabelButton () {
            super(new View_BrownLabelButton());
            autoSize = true;
        }

        // ------------------ PROPERTIES -------------------------------

        override public function get data ():Object {
            return _data;
        }

        override public function set data (value:Object):void {
            _data = value;
            label = _data.title;
        }

        public function get id ():int {
            return _id;
        }

        public function set id (value:int):void {
            _id = value
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
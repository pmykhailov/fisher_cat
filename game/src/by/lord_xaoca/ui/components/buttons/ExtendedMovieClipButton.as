package by.lord_xaoca.ui.components.buttons {

    import flash.display.DisplayObject;

    /**
     * ExtendedMovieClipButton class.
     *
     *  Used to set not actual properties of DisplayObject.
     *
     *  E.g. when we have button and it's animation. Animation uses mask that is bigger than
     *  button size. The dimensions of the button in such case, will include mask dimensions.
     *
     * @author: Pavel Mykhailov
     * @date: 30.05.12 17:39
     */

    public class ExtendedMovieClipButton extends MovieClipButton {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------
        protected var _notActualWidth:Number;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function ExtendedMovieClipButton (view:DisplayObject, useShadow:Boolean = false) {
            super(view, useShadow);
        }

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        override public function get width ():Number {
            return _notActualWidth;
        }

        // ------------------ PROTECTED METHODS ------------------------
        public function setWidth (value:Number):void {
            _notActualWidth = value;
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}

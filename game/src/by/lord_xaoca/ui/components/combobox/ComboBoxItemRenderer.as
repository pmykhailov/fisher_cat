package by.lord_xaoca.ui.components.combobox {

    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.text.TextFieldAutoSize;

    /**
     * ...
     * @author Ivan Shaban
     * @date 23.05.2011 10:34
     */
    public class ComboBoxItemRenderer extends MovableContentContainer implements IItemRenderer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _normalColor:uint = 0xFFFFFF;
        protected var _selectedColor:uint;
        protected var _overColor:uint;

        // ------------------ PRIVATE VARIABLES ------------------------

        private var _selected:Boolean = false;
        private var _data:Object;
        private var _id:int;

        private var _bg:Sprite;
        private var _labelTF:TLFTextField;

        // ------------------ CONSTRUCTOR ------------------------------

        public function ComboBoxItemRenderer () {
            super();

            TweenPlugin.activate([TintPlugin]);

            __initView(new View_ComboboxItem());
            __initMisc();

            _setupPlacement();
            _checkPlacement();
        }

        // ------------------ PROPERTIES -------------------------------

        public function get selected ():Boolean {
            return _selected;
        }

        public function set selected (value:Boolean):void {
            _selected = value;
            _selected ? __stateSelected() : __stateCustom();
        }

        override public function get width ():Number {
            return _bg.width;
        }

        override public function set width (value:Number):void {
            _reInitItemPosition(function ():void {
                _bg.width = value;
            });
        }

        public function get data ():Object {
            return _data;
        }

        public function set data (value:Object):void {
            _data = value;
            _labelTF.text = _data.title;
        }

        public function get id ():int {
            return _id;
        }

        public function set id (value:int):void {
            _id = value;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        public function destroy ():void {
            _view.removeEventListener(MouseEvent.ROLL_OVER, onOverHandler);
            _view.removeEventListener(MouseEvent.ROLL_OUT, onOutHandler);
            _view.removeEventListener(MouseEvent.CLICK, onClickHandler);
            _view = null;
            _data = null;
        }

        // ------------------ PROTECTED METHODS ------------------------

        override protected function _initMovableItems ():void {
            _movableItems.push(_labelTF);
        }

        // ------------------ PRIVATE METHODS --------------------------

        private function __initView (view:Sprite):void {
            _view = view;
            _view.mouseChildren = false;
            _view.buttonMode = true;
            _view.addEventListener(MouseEvent.ROLL_OVER, onOverHandler);
            _view.addEventListener(MouseEvent.ROLL_OUT, onOutHandler);
            _view.addEventListener(MouseEvent.CLICK, onClickHandler);
        }

        private function __initMisc ():void {
            _bg = getMovieClip("bg")

            _labelTF = getTLFTextField("label");
            TextUtils.embedFont(_labelTF, FontsManager.instance.getDefaultFontName());

            var style:Object = {};
            style.autoSize = TextFieldAutoSize.LEFT;
            style.fontSize = 10;
            TextUtils.applyStyleOnTLF(_labelTF, style);
        }

        private function __stateCustom ():void {
            TweenLite.to(_labelTF, 0.2, { tint: 0x391713, x: Globals.useMirroring ? width - 2 - _labelTF.width : 2 });
            TweenLite.to(_bg, 0.2, { tint: _normalColor });
        }

        private function __stateOver ():void {
            TweenLite.to(_labelTF, 0.2, { tint: 0x391713, x: Globals.useMirroring ? width - 10 - _labelTF.width : 10});
            TweenLite.to(_bg, 0.2, { tint: _overColor });
        }

        private function __stateSelected ():void {
            TweenLite.to(_labelTF, 0.2, { tint: 0xFFFFFF, x: Globals.useMirroring ? width - 5 - _labelTF.width : 5 });
            TweenLite.to(_bg, 0.2, { tint: _selectedColor });
        }

        // ------------------ EVENT HANDLERS ---------------------------

        private function onClickHandler (event:MouseEvent):void {
            if (_selected) {
                return;
            }
            dispatchEvent(event);
        }

        private function onOverHandler (event:MouseEvent):void {
            if (_selected) {
                return;
            }
            __stateOver();
        }

        private function onOutHandler (event:MouseEvent):void {
            if (_selected) {
                return;
            }
            __stateCustom();
        }

        // ------------------ END CLASS --------------------------------

    }
}

package by.lord_xaoca.ui.components.combobox {

    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.filters.ColorMatrixFilter;
    import flash.filters.DropShadowFilter;

    /**
     * ...
     * @author Ivan Shaban
     * @date 23.05.2011 10:20
     */

    [Event(name="change", type="flash.events.Event")]

    public class ComboBox extends MovableContentContainer {

        // ------------------ STATIC VARIABLES -------------------------

        public static const DIRECTION_UP:String = "directionTop";
        public static const DIRECTION_DOWN:String = "directionsDown";

        protected static const DEFAULT_MESSAGE:String = "-------";

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _shiftButton:Sprite;
        protected var _list:VBox;
        protected var _cont:Sprite;
        protected var _data:Array;
        protected var _selectedItemLabelTF:TLFTextField;
        protected var _direction:String = ComboBox.DIRECTION_DOWN;
        protected var _enable:Boolean = true;
        protected var _bg:Sprite;
        protected var _contMask:Sprite;
        protected var _isOpen:Boolean;

        protected var _sideOffset:int;
        protected var _animationTime:Number = 0.5;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function ComboBox (view:DisplayObject, itemRendererClass:Class) {
            super();
            _view = view as Sprite;

            _initParameters();
            _initButtons();
            _initLabels();
            _initCont();
            _initBG();
            _initList(itemRendererClass);
            _initContMask();
            _updateList();

            _setupPlacement();
            _checkPlacement();

            enable = false;
        }

        // ------------------ PROPERTIES -------------------------------

        public function get data ():Array {
            return _list.data;
        }

        public function set data (value:Array):void {
            clear();

            _list.data = value;

            var requiredWidth:int = _list.width + _sideOffset * 2;
            if (_bg) {
                _bg.width = requiredWidth;
                _bg.height = _list.y * 2 + _list.height;
            }

            _cont.x = Globals.useMirroring ? int((_shiftButton.width - requiredWidth) / 2) : int(_shiftButton.x + (_shiftButton.width - requiredWidth) / 2);
            _cont.visible = true;

            _contMask.x = _cont.x - _sideOffset;
            _contMask.width = _cont.width + _sideOffset * 2;
            _contMask.height = _cont.height + _sideOffset;

            if (value.length) {
                if (_list.selectedItem) {
                    title = _list.selectedItemData.title;
                } else {
                    setSelectionByIndex(0);
                }
            }

            _updateList();
            enable = true;
        }

        public function get selectedItemID ():int {
            return _list.selectedItemID;
        }

        public function get selectedData ():Object {
            return _list.selectedItemData;
        }

        public function get direction ():String {
            return _direction;
        }

        public function set direction (value:String):void {
            if (value == _direction) {
                return;
            }
            _direction = value;
            _updateList();
        }

        // TODO rename to "enabled"
        public function get enable ():Boolean {
            return _enable;
        }

        public function set enable (value:Boolean):void {
            if (value == _enable) {
                return;
            }
            _enable = value;
            mouseChildren = value;
            mouseEnabled = value;
            if (value) {
                _view.filters = null;
            } else {
                _view.filters = [new ColorMatrixFilter([0.3, 0.6, 0.1, 0, 0, 0.3, 0.6, 0.1, 0, 0, 0.3, 0.6, 0.1, 0, 0, 0, 0, 0, 1, 0])];
            }
        }

        public function get isOpen ():Boolean {
            return _isOpen;
        }

        override public function get width ():Number {
            return _shiftButton.width;
        }

        override public function set width (value:Number):void {
            _apply_LTR_Mirroring();
            _apply_LTR_Placement();

            _shiftButton.width = value;
            _list.width = value;
            if (_bg) {
                _bg.width = _list.width + _sideOffset * 2;
            }
            _contMask.width = value + _sideOffset * 2;

            _reInitItemPosition();
        }

        override public function get height ():Number {
            return _shiftButton.height;
        }

        public function get title ():String {
            return _selectedItemLabelTF.text;
        }

        public function set title (value:String):void {
            _selectedItemLabelTF.text = value;
            _selectedItemLabelTF.x = Globals.useMirroring ? width - _selectedItemLabelTF.width - 5 : 5;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        public function clear ():void {
            _list.clear();
            if (_isOpen) {
                hide();
            }
        }

        public function setSelectionByIndex (value:int):void {
            if (value == -1) {
                return;
            }
            _list.setSelectionByIndex(value);

            _fillSelectedData();
        }

        public function show ():void {
            _isOpen = true;
            if (_direction == ComboBox.DIRECTION_DOWN) {
                TweenLite.to(_cont, _animationTime, {y: _shiftButton.y + _shiftButton.height + _sideOffset});
            } else {
                TweenLite.to(_cont, _animationTime, {y: -_cont.height - _sideOffset});
            }
            GlobalDispatcher.dispatchEvent(new ComponentEvent(ComponentEvent.COMBOBOX_DROPDOWN_HIDE));
            GlobalDispatcher.addCustomEventListener(ComponentEvent.COMBOBOX_DROPDOWN_HIDE, onComboboxDropdownHide);
            GlobalDispatcher.addCustomEventListener(MouseEvent.MOUSE_DOWN, onStageClickHandler);
        }

        public function hide ():void {
            _isOpen = false;
            if (_direction == ComboBox.DIRECTION_DOWN) {
                TweenLite.to(_cont, _animationTime, {y: _shiftButton.y + _shiftButton.height + 2 - _cont.height - 2 * 2});
            } else {
                TweenLite.to(_cont, _animationTime, {y: _sideOffset});
            }
            GlobalDispatcher.removeCustomEventListener(MouseEvent.MOUSE_DOWN, onStageClickHandler);
            GlobalDispatcher.removeCustomEventListener(ComponentEvent.COMBOBOX_DROPDOWN_HIDE, onComboboxDropdownHide);
        }

        override public function updatePlacement ():void {
            super.updatePlacement();

            _cont.x = Globals.useMirroring ? int((_shiftButton.width - _cont.width) / 2) : int(_shiftButton.x + (_shiftButton.width - _cont.width) / 2);
            _contMask.x = _cont.x - _sideOffset;
        }

        // ------------------ PROTECTED METHODS ------------------------

        protected function _fillSelectedData ():void {
            title = _list.selectedItemData ? _list.selectedItemData.title : DEFAULT_MESSAGE;
        }

        protected function _initParameters ():void {
            _sideOffset = 2;
        }

        protected function _initLabels ():void {
            _selectedItemLabelTF = TextUtils.initTLFProperties(getTLFTextField("selectedItemLabel"));
            _selectedItemLabelTF.text = DEFAULT_MESSAGE;
        }

        protected function _initButtons ():void {
            _shiftButton = new Scale9GridSprite(getSprite("shift_btn"));
            _shiftButton.buttonMode = true;
            _shiftButton.filters = Globals.isPoker() ? null : [new DropShadowFilter(1, 90, 0x000000, 0.65, 5, 5)];
            _shiftButton.addEventListener(MouseEvent.CLICK, onButtonClickHandler);
            _shiftButton.addEventListener(MouseEvent.ROLL_OVER, onButtonOverHandler);
            _shiftButton.addEventListener(MouseEvent.ROLL_OUT, onButtonOutHandler);
        }

        protected function _initCont ():void {
            _cont = new Sprite();
            _cont.mouseEnabled = false;
            _cont.visible = false;
            _cont.addEventListener(MouseEvent.ROLL_OUT, onContOutHandler);
            _view.addChild(_cont);
        }

        protected function _initList (itemClass:Class):void {
            _list = new VBox(itemClass);
            _list.x = _sideOffset;
            _list.y = _sideOffset;
            _list.width = width;
            _list.addEventListener(ComponentEvent.SELECTED, onListChangeHandler);
            _cont.addChild(_list.view);
        }

        protected function _initBG ():void {
            _bg = getMovieClip("bg");
            if (!_bg) {
                return;
            }
            _bg.y = 0;
            _cont.addChild(_bg);
        }

        protected function _initContMask ():void {
            var tempWidth:int = _cont.width || _sideOffset ? _cont.width + _sideOffset * 2 : 2;
            var tempHeight:int = _cont.height || _sideOffset ? _cont.width + _sideOffset : 2;
            _contMask = Painter.drawRoundedRect(tempWidth, tempHeight, 0, 0x00ccff, 0.2);
            _cont.mask = _contMask;
            _view.addChild(_contMask);
        }

        protected function _updateList ():void {
            if (_direction == ComboBox.DIRECTION_DOWN) {
                _contMask.y = _shiftButton.y + _shiftButton.height + _sideOffset;
                _cont.y = _contMask.y - _cont.height - _sideOffset * 2;
            } else {
                _contMask.y = -_contMask.height;
                _cont.y = _sideOffset;
            }
        }

        override protected function _initMirrorableItems ():void {
            _mirrorableItems.push(_shiftButton);
        }

        override protected function _initMovableItems ():void {
            _movableItems.push(_selectedItemLabelTF, _cont, _contMask);
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        protected function onComboboxDropdownHide (event:ComponentEvent):void {
            hide();
        }

        protected function onButtonClickHandler (event:MouseEvent):void {
            switch (event.currentTarget) {
                case _shiftButton:
                {
                    if (_isOpen) {
                        hide();
                    } else {
                        show();
                    }
                    break;
                }
            }
        }

        protected function onButtonOverHandler (event:MouseEvent):void {
            switch (event.currentTarget) {
                case _shiftButton:
                {
                    _shiftButton.filters = [new DropShadowFilter(1, 90, 0x000000, 0.65, 5, 5)];
                    break;
                }

                default:
                {
                }
            }
        }

        protected function onButtonOutHandler (event:MouseEvent):void {
            switch (event.currentTarget) {
                case _shiftButton:
                {
                    _shiftButton.filters = Globals.isPoker() ? null : [new DropShadowFilter(1, 90, 0x000000, 0.65, 5, 5)];
                    break;
                }

                default:
                {
                }
            }
        }

        protected function onStageClickHandler (event:MouseEvent):void {
            if (_shiftButton.hitTestPoint(event.stageX, event.stageY) || _view.hitTestPoint(event.stageX, event.stageY)) {
                event.preventDefault();
            } else {
                hide();
            }
        }

        protected function onListChangeHandler (event:ComponentEvent):void {
            hide();
            _fillSelectedData()

            dispatchEvent(new Event(Event.CHANGE));
        }

        protected function onContOutHandler (event:MouseEvent):void {
            hide();
        }

        // ------------------ END CLASS --------------------------------

    }
}

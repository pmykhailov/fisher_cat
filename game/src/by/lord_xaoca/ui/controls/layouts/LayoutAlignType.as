package by.lord_xaoca.ui.controls.layouts {

    /**
     * LayoutAlignType class.
     *
     * @author Ivan Shaban
     * @date 31.10.2011 19:35
     */
    public class LayoutAlignType {

        // ------------------ STATIC VARIABLES -------------------------

        /**
         * Horizontal align types.
         */
        public static var LEFT:int = 0;
        public static var CENTER:int = 1;
        public static var RIGHT:int = 2;

        /**
         * Vertical align types.
         */
        public static var TOP:int = 0;
        public static var MIDDLE:int = 1;
        public static var BOTTOM:int = 2;

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
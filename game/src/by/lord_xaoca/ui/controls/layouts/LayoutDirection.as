package by.lord_xaoca.ui.controls.layouts {

    /**
     * LayoutDirection class.
     *
     * @author Ivan Shaban
     * @date 09.11.2011 22:28
     */
    public class LayoutDirection {

        // ------------------ STATIC VARIABLES -------------------------

        public static const VERTICAL:String = "vertical";
        public static const HORIZONTAL:String = "horizontal";

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
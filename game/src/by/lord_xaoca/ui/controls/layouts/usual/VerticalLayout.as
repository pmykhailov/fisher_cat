package by.lord_xaoca.ui.controls.layouts.usual {

    import by.lord_xaoca.interfaces.ILayoutElement;
    import by.lord_xaoca.ui.controls.layouts.LayoutDirection;
    import by.lord_xaoca.ui.controls.layouts.base.BaseLayout;

    /**
     * HorizontalLayout class.
     *
     * @author Ivan Shaban
     * @date 31.10.2011 19:33
     */
    public class VerticalLayout extends BaseLayout {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function VerticalLayout () {
            super(LayoutDirection.VERTICAL);
        }

        // ------------------ PROPERTIES -------------------------------

        override public function set hSpacing (value:int):void {
            trace("Your cannot use \"hSpacing\" in this class.")
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        override protected function placeItem (value:ILayoutElement, index:int):void {
            if (_useFixedSpacing) {
                value.y = _topIndent + index * _vSpacing;
            } else {
                if (index == -1) {
                    value.y = (value.height + _vSpacing) * -1;
                } else {
                    value.y = _vOffset;
                    _vOffset += Math.round(value.height + _vSpacing);
                }
            }
            value.x = _leftIndent;
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
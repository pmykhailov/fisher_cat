package by.lord_xaoca.ui.controls.layouts.usual {

    import by.lord_xaoca.interfaces.ILayoutElement;
    import by.lord_xaoca.ui.controls.layouts.LayoutDirection;
    import by.lord_xaoca.ui.controls.layouts.base.BaseLayout;

    /**
     * TileLayout class.
     *
     * @author Ivan Shaban
     * @date 31.10.2011 22:06
     */
    public class TileLayout extends BaseLayout {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _rowsCount:int;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function TileLayout (rowsCount:int, direction:String) {
            super(direction);
            _rowsCount = rowsCount;
        }

        // ------------------ PROPERTIES -------------------------------

        public function get rowsCount ():int {
            return _rowsCount;
        }

        public function set rowsCount (value:int):void {
            _rowsCount = value;
            refresh();
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        override protected function placeItem (value:ILayoutElement, index:int):void {
            if (_direction == LayoutDirection.HORIZONTAL) {
                setHorizontalPlacement(value, index);
            } else {
                setVerticalPlacement(value, index);
            }
        }

        protected function setHorizontalPlacement (value:ILayoutElement, index:int):void {
            value.x = _hOffset;
            value.y = _vOffset;

            if (_useFixedSpacing) {
                if ((index % _rowsCount) == _rowsCount - 1) {
                    _hOffset = _topIndent;
                    _vOffset += _vSpacing;
                } else {
                    _hOffset += _hSpacing;
                }
            } else {
                if ((index % _rowsCount) == _rowsCount - 1) {
                    _hOffset = _topIndent;
                    _vOffset += Math.round(value.height + _vSpacing);
                } else {
                    _hOffset += Math.round(value.width + _hSpacing);
                }
            }
        }

        protected function setVerticalPlacement (value:ILayoutElement, index:int):void {
            value.x = _hOffset;
            value.y = _vOffset;

            if (_useFixedSpacing) {
                if ((index % _rowsCount) == _rowsCount - 1) {
                    _vOffset = _leftIndent;
                    _hOffset += _hSpacing;
                } else {
                    _vOffset += _vSpacing;
                }
            } else {
                if ((index % _rowsCount) == _rowsCount - 1) {
                    _vOffset = _leftIndent;
                    _hOffset += Math.round(value.width + _hSpacing);
                } else {
                    _vOffset += Math.round(value.height + _vSpacing);
                }
            }
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
package by.lord_xaoca.ui.controls.layouts.movable {

    /**
     * MTileLayout class.
     *
     * @author Ivan Shaban
     * @date 14.11.2011 19:13
     */
    public class MTileLayout extends TileLayout {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function MTileLayout (rowsCount:int, direction:String) {
            super(rowsCount, direction);
        }

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        override protected function setHorizontalPlacement (value:ILayoutElement, index:int):void {
            var xIndex:int = Globals.useMirroring ? _rowsCount - (index % _rowsCount) - 1 : (index % _rowsCount);
            var indent:int = Globals.useMirroring ? _rightIndent : _leftIndent;
            if (_useFixedSpacing) {
                value.x = indent + _hSpacing * xIndex;
                value.y = _topIndent + _vSpacing * int(index / _rowsCount);
            } else {
                value.x = Math.round(indent + (value.width + _hSpacing) * xIndex);
                value.y = Math.round(_topIndent + (value.height + _vSpacing) * int(index / _rowsCount));
            }
        }

        override protected function setVerticalPlacement (value:ILayoutElement, index:int):void {
            var xIndex:int = Globals.useMirroring ? int((_items.length - 1) / _rowsCount) - int(index / _rowsCount) : index / _rowsCount;
            var indent:int = Globals.useMirroring ? _rightIndent : _leftIndent;
            if (_useFixedSpacing) {
                value.x = indent + _hSpacing * xIndex;
                value.y = _topIndent + _vSpacing * int(index % _rowsCount);
            } else {
                value.x = Math.round(indent + (value.width + _hSpacing) * xIndex);
                value.y = Math.round(_topIndent + (value.height + _vSpacing) * int(index % _rowsCount));
            }
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
package by.lord_xaoca.ui.controls.layouts {

    import by.lord_xaoca.interfaces.ILayoutElement;

    import flash.display.DisplayObjectContainer;
    import flash.events.IEventDispatcher;

    /**
     * ILayout interface.
     *
     * @author Ivan Shaban
     * @date 09.11.2011 21:51
     */
    public interface ILayout extends IEventDispatcher {

        function addElement (value:ILayoutElement, doRefresh:Boolean = true):ILayoutElement

        function removeElement (value:ILayoutElement, doRefresh:Boolean = true):ILayoutElement

        function refresh ():void

        function reset ():void

        /**
         * Freeze layout from refreshing.
         */
        function lock ():void;

        /**
         * Allow layout refreshing.
         */
        function unlock ():void;

        function get container ():DisplayObjectContainer

        function set container (value:DisplayObjectContainer):void

        function get vSpacing ():int

        function set vSpacing ($value:int):void

        function get hSpacing ():int

        function set hSpacing (value:int):void

        function get direction ():String

        function set direction (value:String):void

        function get isBlocked ():Boolean;

        function get useFixedSpacing ():Boolean

        function set useFixedSpacing (value:Boolean):void

        function get leftIndent ():int;

        function set leftIndent (value:int):void;

        function get topIndent ():int;

        function set topIndent (value:int):void;

        function get rightIndent ():int;

        function set rightIndent (value:int):void;

        function get bottomIndent ():int;

        function set bottomIndent (value:int):void;

        function get maxHeightInRow ():int;

        function get maxWidthInColumn ():int;

        function get startIndex ():int

        function set startIndex (value:int):void
    }
}

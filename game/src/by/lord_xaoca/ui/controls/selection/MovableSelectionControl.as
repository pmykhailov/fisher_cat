package by.lord_xaoca.ui.controls.selection {

    /**
     * MovableSelectionControl class.
     *
     * @author: Ivan Shaban
     * @date: 27.04.12 16:16
     */

    public class MovableSelectionControl extends BaseSelectionControl {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function MovableSelectionControl () {
        }

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        override protected function getItemIndex (currentID:int):int {
            return Globals.useMirroring ? _container.data.length - 1 - currentID : currentID;
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}

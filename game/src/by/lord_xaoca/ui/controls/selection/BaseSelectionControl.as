package by.lord_xaoca.ui.controls.selection {

    import by.lord_xaoca.events.ComponentEvent;
    import by.lord_xaoca.interfaces.IItemRenderer;
    import by.lord_xaoca.ui.controls.containers.base.BaseInteractiveContainer;
    import by.lord_xaoca.ui.ir.ItemRendererHandlerVO;

    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.MouseEvent;

    /**
     * BaseSelectionControl class.
     *
     * @author: Ivan Shaban
     * @date: 27.04.12 14:49
     */

    public class BaseSelectionControl extends EventDispatcher implements ISelectionControl {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _currentItem:IItemRenderer;

        protected var _isDeselectable:Boolean;
        protected var _isSelectable:Boolean = true;

        protected var _container:BaseInteractiveContainer;
        protected var _items:Vector.<IItemRenderer>;
        protected var _itemListeners:Vector.<ItemRendererHandlerVO>;

        /**
         * Dispatch events only for inner interaction.
         */
        protected var _innerUpdate:Boolean = false;

        /**
         * Now we can dynamicaly change trigger event.
         */
        protected var _selectionEventType:String = MouseEvent.CLICK;

        /**
         * Deselection event type can be null
         */
        protected var _deselectionEventType:String;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function BaseSelectionControl () {
            super();

            _itemListeners = new <ItemRendererHandlerVO>[];
        }

        // ------------------ PROPERTIES -------------------------------

        public function get isDeselectable ():Boolean {
            return _isDeselectable;
        }

        public function set isDeselectable (value:Boolean):void {
            _isDeselectable = value;
        }

        public function get isSelectable ():Boolean {
            return _isSelectable;
        }

        public function set isSelectable (value:Boolean):void {
            _isSelectable = value;
            if (_currentItem && value) {
                _currentItem.selected = value;
            }
            var len:int = _items.length;
            var i:int;
            if (value) {
                for (i = 0; i < len; i++) {
                    _items[i].addEventListener(_selectionEventType, onItemSelectHandler);
                }
            } else {
                for (i = 0; i < len; i++) {
                    _items[i].removeEventListener(_selectionEventType, onItemSelectHandler);
                }
            }
        }

        public function get selectedItem ():IItemRenderer {
            return _currentItem;
        }

        public function get selectedItemID ():int {
            return _currentItem ? _currentItem.id : -1;
        }

        public function get selectedItemData ():Object {
            return _currentItem ? _currentItem.data : null;
        }

        public function get selectionEventType ():String {
            return _selectionEventType;
        }

        public function set selectionEventType (value:String):void {
            if (_selectionEventType == value || !value) {
                return;
            }

            var len:int = _items.length;
            var i:int;
            // remove all trigger events
            for (i = 0; i < len; i++) {
                _items[i].removeEventListener(_selectionEventType, onItemSelectHandler);
            }
            _selectionEventType = value;
            // add new trigger events
            for (i = 0; i < len; i++) {
                _items[i].addEventListener(_selectionEventType, onItemSelectHandler);
            }
        }

        public function get deselectionEventType ():String {
            return _deselectionEventType;
        }

        public function set deselectionEventType (value:String):void {
            if (_deselectionEventType == value || !value) {
                return;
            }

            var len:int = _items.length;
            var i:int;
            // remove all trigger events
            if (_deselectionEventType) {
                for (i = 0; i < len; i++) {
                    _items[i].removeEventListener(_deselectionEventType, onItemDeselectHandler);
                }
            }
            _deselectionEventType = value;
            // add new trigger events
            if (_deselectionEventType) {
                for (i = 0; i < len; i++) {
                    _items[i].addEventListener(_deselectionEventType, onItemDeselectHandler);
                }
            }
        }

        // ------------------ PUBLIC METHODS ---------------------------

        public function clear ():void {
            if (_currentItem) {
                if (_isSelectable) {
                    _currentItem.selected = false;
                }
                _currentItem = null;
            }
        }

        public function setContainer (container:BaseInteractiveContainer):void {
            _container = container;
            _items = container.items;
        }

        public function setSelectionByIndex (value:int):void {
            // WARNING: WE OVERRIDE THIS METHOD IN LIST CONTROL, AND DO NOT CALL SUPER METHOD.
            // so if you work with lists, please check properly class @see: by.lord_xaoca.ui.controls.selection.BaseListSelectionControl
            if (value < 0 || value > _items.length) {
                clear();
                return;
            }

            if (_currentItem) {
                /**
                 * Deselect selected item if it is possible.
                 */
                if (_currentItem.id == value) {
                    if (_innerUpdate && _isDeselectable) {
                        _currentItem.selected = false;
                        dispatchEvent(new ComponentEvent(ComponentEvent.DESELECTED));
                        _currentItem = null;
                        return;
                    } else if (_isSelectable) {
                        return;
                    }
                } else {
                    _currentItem.selected = false;
                }
            }

            _currentItem = _items[getItemIndex(value)];


            // if it is external selection call, so we force selection, even if it disabled
            if (_isSelectable || !_innerUpdate) {
                // setup selected item at the top
                _container.addChild(_currentItem.view);
                _currentItem.selected = true;
            }
            if (_innerUpdate) {
                dispatchEvent(new ComponentEvent(ComponentEvent.SELECTED));
            }
        }

        public function addIRListener (type:String, listener:Function):void {
            _itemListeners.push(new ItemRendererHandlerVO(type, listener));

            var len:int = _items.length;
            for (var i:int = 0; i < len; i++) {
                _items[i].addEventListener(type, listener);
            }
        }

        public function init ():void {
            var listenersLen:int = _itemListeners.length;
            var len:int = _items.length;
            var ir:IItemRenderer;
            for (var i:int = 0; i < len; i++) {
                ir = _items[i];
                ir.addEventListener(_selectionEventType, onItemSelectHandler);
                if (_deselectionEventType) {
                    ir.addEventListener(_deselectionEventType, onItemDeselectHandler);
                }
                for (var j:int = 0; j < listenersLen; j++) {
                    ir.addEventListener(_itemListeners[j].type, _itemListeners[j].listener);
                }
            }
        }

        public function destroy ():void {
            clear();
            var listenersLen:int = _itemListeners.length;
            var len:int = _items.length;
            var ir:IItemRenderer;
            for (var i:int = 0; i < len; i++) {
                ir = _items[i];
                ir.removeEventListener(_selectionEventType, onItemSelectHandler);
                if (_deselectionEventType) {
                    ir.removeEventListener(_deselectionEventType, onItemDeselectHandler);
                }
                for (var j:int = 0; j < listenersLen; j++) {
                    ir.removeEventListener(_itemListeners[j].type, _itemListeners[j].listener);
                }
            }
            _items = null;
            _container = null;
        }

        // ------------------ PROTECTED METHODS ------------------------

        protected function getItemIndex (currentID:int):int {
            return currentID;
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        protected function onItemSelectHandler (event:Event):void {
            _innerUpdate = true;
            setSelectionByIndex((event.currentTarget as IItemRenderer).id);
            _innerUpdate = false;
        }

        protected function onItemDeselectHandler (event:Event):void {
            if (_currentItem == event.currentTarget as IItemRenderer) {
                _currentItem.selected = false;
                dispatchEvent(new ComponentEvent(ComponentEvent.DESELECTED));
                _currentItem = null;
            }
        }

        // ------------------ END CLASS --------------------------------

    }
}

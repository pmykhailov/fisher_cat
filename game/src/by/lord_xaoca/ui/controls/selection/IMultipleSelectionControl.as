package by.lord_xaoca.ui.controls.selection {

    /**
     * IMultipleSelectionControl interface.
     *
     * @author: Ivan Shaban
     * @date: 27.04.12 17:19
     */
    public interface IMultipleSelectionControl {
        function get selectedItemsID ():Array;

        function get selectedItemsData ():Array;
    }
}

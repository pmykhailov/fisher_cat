package by.lord_xaoca.ui.controls.selection {

    import by.lord_xaoca.events.ComponentEvent;
    import by.lord_xaoca.interfaces.IItemRenderer;

    /**
     * MultiSelectionControl class.
     *
     * @author: Ivan Shaban
     * @date: 27.04.12 15:03
     */

    public class MultiSelectionControl extends BaseSelectionControl implements IMultipleSelectionControl {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _selectedItemsID:Array;
        protected var _selectedItemsData:Array;
        protected var _minSelectedItems:int;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function MultiSelectionControl (minSelectedItems:int = 0) {
            _selectedItemsData = [];
            _selectedItemsID = [];
            _minSelectedItems = minSelectedItems;
            _isDeselectable = true;
        }

        // ------------------ PROPERTIES -------------------------------

        public function get selectedItemsID ():Array {
            return _selectedItemsID;
        }

        public function get selectedItemsData ():Array {
            return _selectedItemsData;
        }

        public function get minSelectedItems ():int {
            return _minSelectedItems;
        }

        public function set minSelectedItems (value:int):void {
            _minSelectedItems = value;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        override public function clear ():void {
            super.clear();

            _selectedItemsID.length = 0;
            _selectedItemsData.length = 0;

            var len:int = _items.length;
            for (var i:int = 0; i < len; i++) {
                var item:IItemRenderer = _items[i];
                if (item.selected) {
                    item.selected = false;
                }
            }
        }

        override public function setSelectionByIndex (value:int):void {
            if (value < 0) {
                clear();
            } else {
                _currentItem = _items[getItemIndex(value)];

                if (_currentItem.selected) {
                    if (_selectedItemsID.length - 1 < _minSelectedItems) {
                        return;
                    }
                    _currentItem.selected = false;

                    var index:int = _selectedItemsID.indexOf(_currentItem.id);
                    if (index != -1) {
                        _selectedItemsID.splice(index, 1);
                        _selectedItemsData.splice(index, 1);
                    }
                    dispatchEvent(new ComponentEvent(ComponentEvent.DESELECTED));
                } else {
                    _currentItem.selected = true;
                    _selectedItemsData.push(_currentItem.data);
                    _selectedItemsID.push(_currentItem.id);
                    if (_innerUpdate) {
                        dispatchEvent(new ComponentEvent(ComponentEvent.SELECTED));
                    }
                }

            }
        }

        override public function destroy ():void {
            _selectedItemsID.length = 0;
            _selectedItemsID = null;

            _selectedItemsData.length = 0;
            _selectedItemsData = null;

            super.destroy();
        }

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}

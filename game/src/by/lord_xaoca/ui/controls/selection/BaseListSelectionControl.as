package by.lord_xaoca.ui.controls.selection {

    import by.lord_xaoca.events.ComponentEvent;
    import by.lord_xaoca.interfaces.IItemRenderer;

    /**
     * BaseListSelectionControl class.
     *
     * @author: Ivan Shaban
     * @date: 28.04.12 19:25
     */

    public class BaseListSelectionControl extends BaseSelectionControl implements IListSelectionControl {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _firstItemIndex: int;
        protected var _isFirstIndexChanged: Boolean;
        protected var _currentItemID: int;
        protected var _size: uint;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function BaseListSelectionControl(size: uint) {
            _size = size;
        }


        // ------------------ PROPERTIES -------------------------------

        override public function get selectedItemData(): Object {
            return _currentItemID == -1 ? null : _container.data[_currentItemID];
        }


        override public function get selectedItemID(): int {
            return _currentItemID;
        }


        public function get firstItemIndex(): int {
            return _firstItemIndex;
        }


        public function get isFirstIndexChanged(): Boolean {
            return _isFirstIndexChanged;
        }


        public function get size(): uint {
            return _size;
        }


        public function set size(value: uint): void {
            _size = value;
            if (_container.data) {
                _firstItemIndex = Math.min(Math.max(_container.data.length - _size, 0), _firstItemIndex);
            }
        }


        // ------------------ PUBLIC METHODS ---------------------------

        override public function setSelectionByIndex(value: int): void {
            if (!_isFirstIndexChanged && _currentItemID == value) {
                return;
            }

            if (_currentItem) {
                /**
                 * Deselect selected item if it is possible.
                 */
                if (_currentItem.id == value) {
                    if (_innerUpdate && _isDeselectable) {
                        _currentItem.selected = false;
                        dispatchEvent(new ComponentEvent(ComponentEvent.DESELECTED));
                        _currentItem = null;
                        return;
                    } else if (_isSelectable) {
                        return;
                    }
                } else {
                    _currentItem.selected = false;
                }
            }

            // we should check not only _currentItemID == value, but notice changing of _firstItemIndex
            if (value == -1) {
                return;
            }

            _currentItemID = value;
            _isFirstIndexChanged = false;

            var len: int = _items.length;
            for (var i: int = 0; i < len; i++) {
                var itemRenderer: IItemRenderer = _items[i];
                if (itemRenderer.id == value) {
                    _currentItem = itemRenderer;
                    // if it is external selection call, so we force selection, even if it disabled
                    if (_isSelectable || !_innerUpdate) {
                        _currentItem.selected = true;
                    }
                    if (_innerUpdate) {
                        dispatchEvent(new ComponentEvent(ComponentEvent.SELECTED));
                    }
                    break;
                }
            }
        }


        public function setPosition(value: uint): void {
            // define index of  top-visible element (in real it is second element)
            var newPosition: int = value > _container.data.length - _size ? _container.data.length - _size : value;
            if (_firstItemIndex == newPosition) {
                return;
            }
            _firstItemIndex = newPosition;
            _isFirstIndexChanged = true;

            if (_firstItemIndex < 0) {
                _firstItemIndex = 0;
            }
        }


        override public function clear(): void {
            super.clear();

            _currentItemID = -1;
            _firstItemIndex = 0;
            _isFirstIndexChanged = true;
        }


        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}

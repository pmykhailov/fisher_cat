package by.lord_xaoca.ui.controls.movable {

    import flash.display.DisplayObject;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFieldType;

    import flashx.textLayout.formats.Direction;

    /**
     * MovableContentContainer class.
     *
     * Update method : _reInitItemPosition
     * Add new method: _addItemToPlacement
     * Add new method: _removeItemfromPlacement
     * Add new method: _addItemToMirroring
     * Add new method: _removeItemfromMirroring
     *
     * @author Ivan Shaban
     * @date 14.10.2011 16:27
     */
    public class MovableContentContainer extends GUIAdapter implements IMovableContent {

        // ------------------ STATIC VARIABLES -------------------------

        protected static const PLACEMENT:String = "itemsPlacement";
        protected static const MIRRORING:String = "itemsMirroring";

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _movableItems:Array;
        protected var _mirrorableItems:Array;
        protected var _placement:Object;
        protected var _locale:LocaleManager = LocaleManager.instance;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function MovableContentContainer () {
            _placement = {};
            _placement[PLACEMENT] = {};
            _placement[MIRRORING] = {};

            _movableItems = [];
            _mirrorableItems = [];
        }

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        /* INTERFACE com.popover.library.ui.controls.movable.IMovableContent */
        public function updatePlacement ():void {
            if (Globals.useMirroring) {
                _apply_RLT_Placement();
                _apply_RTL_Mirroring();
            } else {
                _apply_LTR_Placement();
                _apply_LTR_Mirroring();
            }
        }

        // ------------------ PROTECTED METHODS ------------------------

        protected function _initOwnMethods ():void {
            // ABSTRACT
        }

        protected function _initMovableItems ():void {
            // ABSTRACT
        }

        protected function _initMirrorableItems ():void {
            // ABSTRACT
        }

        /**
         * Add argument with link to reinited item.
         */
        protected function _reInitItemPosition (changeSomethingCallBack:Function = null):void {
            _apply_LTR_Mirroring();
            _apply_LTR_Placement();

            changeSomethingCallBack && changeSomethingCallBack();

            _mirrorableItems.length = 0;
            _movableItems.length = 0;

            _setupPlacement();
            _checkPlacement();
        }

        /**
         * Setup possible only in eng state.
         */
        protected function _setupPlacement ():void {
            _initMovableItems();
            _initMirrorableItems();

            _setup_LTR_Placement();
            _setup_RTL_Placement();

            _setup_LTR_Mirroring();
            _setup_RTL_Mirroring();
        }

        protected function _setup_LTR_Placement ():void {
            _placement[PLACEMENT][Direction.LTR] = {};
            var len:int = _movableItems.length;
            for (var i:int = 0; i < len; i++) {
                switch (true) {
                    case _movableItems[i] is TLFTextField:
                    case _movableItems[i] is TextField:
                        if (_movableItems[i].autoSize == TextFieldAutoSize.NONE && _movableItems[i].type != TextFieldType.INPUT) {
                            _movableItems[i].autoSize = TextFieldAutoSize.LEFT;
                        }
                    default:
                        _placement[PLACEMENT][Direction.LTR][_movableItems[i].name] = {x: _movableItems[i].scaleX < 0 ? _movableItems[i].x + _movableItems[i].width : _movableItems[i].x, y: _movableItems[i].y};
                        break;
                }
            }
        }

        protected function _setup_RTL_Placement ():void {
            _placement[PLACEMENT][Direction.RTL] = {};
            var len:int = _movableItems.length;
            for (var i:int = 0; i < len; i++) {
                switch (true) {
                    case _movableItems[i] is TLFTextField:
                    case _movableItems[i] is TextField:
                        if (_movableItems[i].autoSize == TextFieldAutoSize.NONE && _movableItems[i].type != TextFieldType.INPUT) {
                            _movableItems[i].autoSize = TextFieldAutoSize.LEFT;
                        }
                    case _movableItems[i] is IMovableContent:
                    default:
                        _placement[PLACEMENT][Direction.RTL][_movableItems[i].name] = {x: _movableItems[i].scaleX < 0 ? width - _movableItems[i].x - _movableItems[i].width : width - _movableItems[i].x, y: _movableItems[i].y};
                        break;
                }
            }
        }

        protected function _setup_LTR_Mirroring ():void {
            _placement[MIRRORING][Direction.LTR] = {};
            var len:int = _mirrorableItems.length;
            for (var i:int = 0; i < len; i++) {
                _placement[MIRRORING][Direction.LTR][_mirrorableItems[i].name] = {x: _mirrorableItems[i].scaleX < 0 ? _mirrorableItems[i].x + _mirrorableItems[i].width : _mirrorableItems[i].x, y: _mirrorableItems[i].y, scaleX: _mirrorableItems[i].scaleX};
            }
        }

        protected function _setup_RTL_Mirroring ():void {
            _placement[MIRRORING][Direction.RTL] = {};
            var len:int = _mirrorableItems.length;
            for (var i:int = 0; i < len; i++) {
                _placement[MIRRORING][Direction.RTL][_mirrorableItems[i].name] = { x: _mirrorableItems[i].scaleX < 0 ? width - _mirrorableItems[i].x - _mirrorableItems[i].width : width - _mirrorableItems[i].x, y: _mirrorableItems[i].y, scaleX: -_mirrorableItems[i].scaleX };
            }
        }

        protected function _checkPlacement ():void {
            if (Globals.useMirroring) {
                updatePlacement();
            }
        }

        protected function _addItemToPlacement (value:DisplayObject, direction:String):void {
            _removeItemfromPlacement(value);
            _removeItemfromMirroring(value);

            _movableItems.push(value);

            if (Globals.useMirroring) {
                _placement[PLACEMENT][Direction.LTR][value.name] = {x: value.scaleX < 0 ? width - value.x : width - value.x - value.width, y: value.y};
                _placement[PLACEMENT][Direction.RTL][value.name] = {x: value.scaleX < 0 ? value.x + value.width : value.x, y: value.y};
                value.x = _placement[PLACEMENT][Direction.RTL][value.name].x;
            } else {
                _placement[PLACEMENT][Direction.LTR][value.name] = {x: value.scaleX < 0 ? value.x + value.width : value.x, y: value.y};
                _placement[PLACEMENT][Direction.RTL][value.name] = {x: value.scaleX < 0 ? width - value.x - value.width : width - value.x, y: value.y};
                value.x = _placement[PLACEMENT][Direction.LTR][value.name].x;
            }
        }

        protected function _removeItemfromPlacement (value:DisplayObject):void {
            var index:int = _movableItems.indexOf(value);
            if (index == -1) {
                return;
            }

            _movableItems.splice(index, 1);

            delete _placement[PLACEMENT][Direction.LTR][value.name];
            delete _placement[PLACEMENT][Direction.RTL][value.name];
        }

        protected function _addItemToMirroring (value:DisplayObject, language:String):void {
            _removeItemfromPlacement(value);
            _removeItemfromMirroring(value);
        }

        protected function _removeItemfromMirroring (value:DisplayObject):void {
            var index:int = _mirrorableItems.indexOf(value);
            if (index == -1) {
                return;
            }

            _mirrorableItems.splice(index, 1);

            delete _placement[MIRRORING][Direction.LTR][value.name];
            delete _placement[MIRRORING][Direction.RTL][value.name];
        }

        /*        protected function _applyNonDirectEnglishPlacement(): void {
         var len: int = _movableItems.length;
         for (var i: int = 0; i < len; i++) {
         switch (true) {
         case _movableItems[i] is TLFTextField:
         {
         _movableItems[i].direction = Direction.LTR;
         }

         case _movableItems[i] is TextField:
         {
         if (_movableItems[i].autoSize == TextFieldAutoSize.CENTER) {
         continue;
         } else {
         _movableItems[i].autoSize = TextFieldAutoSize.LEFT;
         }
         _movableItems[i].x = width - (_movableItems[i].x + _movableItems[i].width);
         break;
         }

         case _movableItems[i] is IMovableContent:
         {
         _movableItems[i].x = width - (_movableItems[i].x + _movableItems[i].width);
         //_movableItems[i].updatePlacement();
         break;
         }

         default :
         {
         _movableItems[i].x = width - (_movableItems[i].x + _movableItems[i].width);
         }
         }
         }
         }
         */

        /*        protected function _applyNonDirectEnglishMirroring(): void {
         var len: int = _mirrorableItems.length;
         for (var i: int = 0; i < len; i++) {
         switch (true) {
         case _mirrorableItems[i] is IMovableContent:
         {
         _mirrorableItems[i].x = width - (_mirrorableItems[i].x + _mirrorableItems[i].width);
         //_mirrorableItems[i].updatePlacement();
         break;
         }

         default :
         {
         _mirrorableItems[i].scaleX *= -1;
         _mirrorableItems[i].x = width - (_mirrorableItems[i].x + _mirrorableItems[i].width);
         }
         }
         }
         }
         */

        protected function _apply_LTR_Placement ():void {
            var len:int = _movableItems.length;
            var point:Object;
            for (var i:int = 0; i < len; i++) {
                point = _placement[PLACEMENT][Direction.LTR][_movableItems[i].name];
                switch (true) {
                    case _movableItems[i] is TLFTextField:
                        _movableItems[i].direction = Direction.LTR;
                    case _movableItems[i] is TextField:
                        if (_movableItems[i].autoSize == TextFieldAutoSize.CENTER) {
                            continue;
                        } else if (_movableItems[i].type != TextFieldType.INPUT) {
                            _movableItems[i].autoSize = TextFieldAutoSize.LEFT;
                        }
                        _movableItems[i].x = point.x;
                        break;
                    case _movableItems[i] is IMovableContent:
                        _movableItems[i].x = point.x;
                        _movableItems[i].updatePlacement();
                        break;
                    default:
                        _movableItems[i].x = point.x;
                }
            }
        }

        protected function _apply_RLT_Placement ():void {
            var len:int = _movableItems.length;
            var point:Object;
            for (var i:int = 0; i < len; i++) {
                point = _placement[PLACEMENT][Direction.RTL][_movableItems[i].name];
                switch (true) {
                    case _movableItems[i] is TLFTextField:
                        _movableItems[i].direction = Direction.RTL;
                    case _movableItems[i] is TextField:
                        if (_movableItems[i].autoSize == TextFieldAutoSize.CENTER) {
                            continue;
                        } else if (_movableItems[i].autoSize == TextFieldAutoSize.RIGHT) {
                            _movableItems[i].autoSize = TextFieldAutoSize.LEFT;
                        } else if (_movableItems[i].type != TextFieldType.INPUT) {
                            _movableItems[i].autoSize = TextFieldAutoSize.RIGHT;
                        }
                        _movableItems[i].x = point.x - _movableItems[i].width;
                        break;
                    case _movableItems[i] is IMovableContent:
                        _movableItems[i].x = point.x - _movableItems[i].width;
                        _movableItems[i].updatePlacement();
                        break;
                    default:
                        _movableItems[i].x = point.x - _movableItems[i].width;
                        break;
                }
            }
        }

        protected function _apply_LTR_Mirroring ():void {
            var len:int = _mirrorableItems.length;
            var point:Object;
            for (var i:int = 0; i < len; i++) {
                point = _placement[MIRRORING][Direction.LTR][_mirrorableItems[i].name];
                switch (true) {
                    case _mirrorableItems[i] is IMovableContent:
                        _mirrorableItems[i].updatePlacement();
                    default:
                        _mirrorableItems[i].scaleX = point.scaleX;
                        _mirrorableItems[i].x = point.x;
                        break;
                }
            }
        }

        protected function _apply_RTL_Mirroring ():void {
            var len:int = _mirrorableItems.length;
            var point:Object;
            for (var i:int = 0; i < len; i++) {
                point = _placement[MIRRORING][Direction.RTL][_mirrorableItems[i].name];
                switch (true) {
                    case _mirrorableItems[i] is IMovableContent:
                        _mirrorableItems[i].updatePlacement();
                    default:
                        _mirrorableItems[i].scaleX = point.scaleX;
                        _mirrorableItems[i].x = point.x;
                        break;
                }
            }
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}

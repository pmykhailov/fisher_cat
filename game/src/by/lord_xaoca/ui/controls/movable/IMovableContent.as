package by.lord_xaoca.ui.controls.movable {

    /**
     * IMovableContent interface.
     *
     * @author Ivan Shaban
     * @date 12.10.2011 14:04
     */
    public interface IMovableContent {
        function updatePlacement ():void
    }
}
package by.lord_xaoca.ui.controls.containers.lists {

    import by.lord_xaoca.interfaces.IItemRenderer;
    import by.lord_xaoca.ui.controls.containers.base.BaseInteractiveContainer;
    import by.lord_xaoca.ui.controls.layouts.ILayout;
    import by.lord_xaoca.ui.controls.selection.IListSelectionControl;
    import by.lord_xaoca.ui.controls.selection.ISelectionControl;
    import by.lord_xaoca.utils.ClassUtils;
    import by.lord_xaoca.utils.ObjectUtils;

    import flash.display.Sprite;
    import flash.events.Event;
    import flash.geom.Rectangle;

    /**
     * BaseList class.
     *
     * @author: ishaban
     * @date: 05.02.12 20:12
     */

    public class BaseList extends BaseInteractiveContainer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _cont: Sprite;
        protected var _contentWidth: Number = 0;
        protected var _contentHeight: Number = 0;
        protected var _width: Number = 0;
        protected var _height: Number = 0;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function BaseList(layout: ILayout, selectionControl: ISelectionControl, itemRendererClass: Class, itemRendererArguments: Array = null) {
            super(new Sprite(), layout, selectionControl, itemRendererClass, itemRendererArguments);
        }


        // ------------------ PROPERTIES -------------------------------

        override public function get width(): Number {
            return _width;
        }


        override public function get height(): Number {
            return _height;
        }


        /**
         *
         * @param value Array or Vector
         */
        override public function set data(value: Object): void {
            clear();

            if (!value || !ObjectUtils.isList(value)) {
                return;
            }
            _data = value;
            _layout.removeEventListener(Event.RESIZE, onLayoutResizeHandler);
            initItems();
            initSizeProperties();
            fillData();
            _layout.addEventListener(Event.RESIZE, onLayoutResizeHandler);
            _layout.refresh();
        }


        public function get contentHeight(): Number {
            return _contentHeight;
        }


        public function get contentWidth(): Number {
            return _contentWidth;
        }


        public function get size(): int {
            return _typedSelectionControl.size;
        }


        public function set size(value: int): void {
            if (value == _typedSelectionControl.size) {
                return;
            }
            _typedSelectionControl.size = value;

            if (!_data || !_data.length) {
                return;
            }
            //            initSizeProperties();
            initItems();
            initSizeProperties();
            fillData();
        }


        protected function get _typedSelectionControl(): IListSelectionControl {
            return _selection as IListSelectionControl;
        }


        // ------------------ PUBLIC METHODS ---------------------------

        override public function clear(): void {
            _selection.clear();

            var len: int = _items.length;
            for (var i: int = 0; i < len; i++) {
                _items[i].view.visible = false;
            }

            _cont.y = 0;
            _contentWidth = 0;
            _contentHeight = 0;
            // not that! : _data.length = 0;
            _data = null;
        }


        override public function setSelectionByIndex(value: int): void {
            _selection.setSelectionByIndex(value);
        }


        /**
         * Refreshing itemrenderer's data, without content scrolling.
         * @param value
         */
        public function setPosition(value: uint): void {
            var oldIndex: int = _typedSelectionControl.firstItemIndex;
            _typedSelectionControl.setPosition(value);

            if (oldIndex != _typedSelectionControl.firstItemIndex) {
                fillData();
            }
        }


        /**
         * Scrolling content to mark.
         * @param xValueByRatio from 0 to 1
         * @param yValueByRatio from 0 to 1
         */
        public function setContentPosition(xValueByRatio: Number, yValueByRatio: Number): void {
            // ABSTRACT
        }


        // ------------------ PROTECTED METHODS ------------------------

        override protected function initialize(): void {
            initContainer();

            super.initialize();
        }


        override protected function initLayout(): void {
            _layout.container = _cont;
            _layout.startIndex = -1;
        }


        override protected function initItems(): void {
            var len: int;
            var i: int;
            var requiredSize: uint = _typedSelectionControl.size + 2;
            var ir: IItemRenderer;

            if (_items.length == requiredSize) {
                return;
            }
            if (_items.length < requiredSize) {
                for (i = 0; i < requiredSize; i++) {
                    if (i >= _items.length) {
                        ir = _items[i] = ClassUtils.getInstanceByClass.apply(null, _itemRendererArguments) as IItemRenderer;
                        if (_itemWidth > 0) {
                            ir.width = _itemWidth;
                        }

                        if (_itemHeight > 0) {
                            ir.height = _itemHeight;
                        }
                        add(ir);
                    }
                }

                if (!_itemWidth) {
                    _itemWidth = _items[i - 1].width;
                }

                if (!_itemHeight) {
                    _itemHeight = _items[i - 1].height;
                }
            } else {
                // WARNING: bugs happens when code below executes, so uncomment if only after debug
                //                len = _items.length;
                //                for (i = requiredSize; i < len; i++) {
                //                    // potential mistake, cause we dont remove listeners from item renderers.
                //                    removeChild(_items[i].view);
                //                }
                //                _items.length = requiredSize;
            }

            _selection.init();
        }


        /**
         * I'm strongly don't recommend to use "selected" property in IR data.
         */
        override protected function fillData(): void {
            var len: int = _typedSelectionControl.size + 2;
            var index: int;
            var dataIndex: int;

            for (var i: int = 0; i < len; i++) {
                index = _getItemIndex(i);
                dataIndex = _typedSelectionControl.firstItemIndex + index - 1;
                if (dataIndex < _data.length && dataIndex > -1) {
                    _items[i].id = dataIndex;
                    _items[i].data = _data[dataIndex];
                    _items[i].view.visible = true;
                } else {
                    _items[i].id = -1;
                    _items[i].view.visible = false;
                }
            }

            var rect: Rectangle = _cont.scrollRect || new Rectangle();
            rect.width = width;
            rect.height = height;
            _cont.scrollRect = rect;

            _selection.setSelectionByIndex(_selection.selectedItemID);
        }


        protected function initContainer(): void {
            _cont = new Sprite();
            _cont.mouseEnabled = false;
            _view.addChild(_cont);
        }


        protected function initSizeProperties(): void {
            // ABSTRACT
        }


        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        override protected function onLayoutResizeHandler(event: Event): void {
            if (_data && _data.length) {
                super.onLayoutResizeHandler(event);
            }
        }


        // ------------------ END CLASS --------------------------------

    }
}

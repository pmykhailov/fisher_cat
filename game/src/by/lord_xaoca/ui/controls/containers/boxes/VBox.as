package by.lord_xaoca.ui.controls.containers.boxes {

    import by.lord_xaoca.ui.controls.containers.base.BaseInteractiveContainer;
    import by.lord_xaoca.ui.controls.layouts.usual.VerticalLayout;
    import by.lord_xaoca.ui.controls.selection.BaseSelectionControl;

    import flash.display.Sprite;

    /**
     * VBox class.
     *
     * @author: Ivan Shaban
     * @date: 10.11.11 11:36
     */

    public class VBox extends BaseInteractiveContainer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function VBox (itemRendererClass:Class, ...itemRendererArguments) {
            super(new Sprite(),
                  new VerticalLayout(),
                  new BaseSelectionControl(),
                  itemRendererClass,
                  itemRendererArguments);
        }

        // ------------------ PROPERTIES -------------------------------

        override public function get width ():Number {
            return _itemWidth ? _itemWidth : _layout.maxWidthInColumn;
        }

        override public function set width (value:Number):void {
            itemWidth = value;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}

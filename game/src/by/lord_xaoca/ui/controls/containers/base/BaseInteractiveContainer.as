package by.lord_xaoca.ui.controls.containers.base {

    import by.lord_xaoca.events.ComponentEvent;
    import by.lord_xaoca.interfaces.IItemRenderer;
    import by.lord_xaoca.interfaces.ILayoutElement;
    import by.lord_xaoca.ui.controls.layouts.ILayout;
    import by.lord_xaoca.ui.controls.selection.ISelectionControl;
    import by.lord_xaoca.utils.ClassUtils;
    import by.lord_xaoca.utils.ObjectUtils;

    import flash.display.Sprite;

    /**
     * BaseInteractiveContainer class.
     *
     * @author: Ivan Shaban
     * @date: 10.11.11 17:59
     */

    [Event(name="deselected", type="by.lord_xaoca.events.ComponentEvent")]
    [Event(name="selected", type="by.lord_xaoca.events.ComponentEvent")]

    public class BaseInteractiveContainer extends BaseContainer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        /**
         * Array or Vector
         */
        protected var _data: Object;
        protected var _items: Vector.<IItemRenderer>;
        protected var _itemRendererArguments: Array;

        protected var _itemWidth: int;
        protected var _itemHeight: int;

        protected var _selection: ISelectionControl;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function BaseInteractiveContainer(view: Sprite, layout: ILayout, selectionControl: ISelectionControl, itemRendererClass: Class, itemRendererArguments: Array = null) {
            super(view, layout);

            _itemRendererArguments = itemRendererArguments;
            if (_itemRendererArguments) {
                _itemRendererArguments.unshift(itemRendererClass);
            } else {
                _itemRendererArguments = [itemRendererClass];
            }
            _items = new <IItemRenderer>[];
            _initSelectionControl(selectionControl);
        }


        // ------------------ PROPERTIES -------------------------------

        public function get data(): Object {
            return _data;
        }


        /**
         *
         * @param value Array or Vector
         */
        public function set data(value: Object): void {
            clear();
            if (!value || !ObjectUtils.isList(value)) {
                return;
            }

            _data = value;
            initItems();
            fillData();
        }


        public function get selectedItem(): IItemRenderer {
            return _selection.selectedItem;
        }


        public function get selectedItemID(): int {
            return _selection.selectedItemID;
        }


        public function get selectedItemData(): Object {
            return _selection.selectedItemData;
        }


        public function get itemWidth(): int {
            return _itemWidth;
        }


        public function set itemWidth(value: int): void {
            _itemWidth = value;
            if (!_itemWidth) {
                return;
            }

            var len: int = _items.length;
            for (var i: int = 0; i < len; i++) {
                _items[i].width = _itemWidth;
            }
            _layout.refresh();
        }


        public function get itemHeight(): int {
            return _itemHeight;
        }


        public function set itemHeight(value: int): void {
            _itemHeight = value;
            if (!_itemHeight) {
                return;
            }

            var len: int = _items.length;
            for (var i: int = 0; i < len; i++) {
                _items[i].height = _itemHeight;
            }
            _layout.refresh();
        }


        public function get items(): Vector.<IItemRenderer> {
            return _items;
        }


        public function get selection(): ISelectionControl {
            return _selection;
        }


        public function set selection(value: ISelectionControl): void {
            if (!value || value == _selection) {
                return;
            }
            if (_selection) {
                _selection.removeEventListener(ComponentEvent.SELECTED, dispatchEvent);
                _selection.removeEventListener(ComponentEvent.DESELECTED, dispatchEvent);
                _selection.destroy();
            }

            _initSelectionControl(value);
        }


        // ------------------ PUBLIC METHODS ---------------------------

        public function clear(): void {
            _selection.clear();

            _layout.lock();
            var len: int = _items.length;
            for (var i: int = 0; i < len; i++) {
                _layout.removeElement(_items[i]);
            }
            _layout.unlock();
            _layout.reset();

            _view.graphics.clear();
            _data = null;
            // WARNING _item.length != 0, all items are still in Vector and have listeners for CLICK event.
            // WARNING not that! : _data.length = 0;
        }


        public function setSelectionByIndex(value: int): void {
            _selection.setSelectionByIndex(value);
        }


        public function setSelectionByValue(value: *): void {
            if (!value || !_data) {
                return;
            }
            var len: int = _data.length;
            for (var i: int = 0; i < len; i++) {
                var itemData: Object = _data[i];
                if (itemData && itemData.value && (itemData.value == value)) {
                    setSelectionByIndex(i);
                    return;
                }
            }
        }


        /**
         * That method works only for LTR direction placement.
         * @param value
         * @return
         */
        override public function add(value: ILayoutElement): ILayoutElement {
            var child: IItemRenderer = IItemRenderer(value);
            var index: Number = _items.indexOf(child);
            if (index == -1) {
                child.id = _getItemIndex(_items.length);
                _items.push(child);
            }
            return super.add(value);
        }


        /**
         * That method works only for LTR direction placement.
         * @param value
         * @return
         */
        override public function remove(value: ILayoutElement): ILayoutElement {
            var child: IItemRenderer = IItemRenderer(value);
            var index: Number = _items.indexOf(child);
            if (index != -1) {
                _items.splice(index, 1);
            }
            return super.remove(value);
        }


        public function addIRListener(type: String, listener: Function): void {
            _selection.addIRListener(type, listener);
        }


        override public function destroy(): void {
            clear();
            _items.length = 0;
            _items = null;

            _itemRendererArguments.length = 0;
            _itemRendererArguments = null;

            _selection.destroy();
            _selection = null;

            _layout = null;

            super.destroy();
        }


        // ------------------ PROTECTED METHODS ------------------------

        protected function _initSelectionControl(selectionControl: ISelectionControl): void {
            _selection = selectionControl;
            _selection.setContainer(this);
            _selection.addEventListener(ComponentEvent.SELECTED, dispatchEvent);
            _selection.addEventListener(ComponentEvent.DESELECTED, dispatchEvent);
        }


        protected function initItems(): void {
            var len: int = _data.length;
            var ir: IItemRenderer;
            for (var i: int = _items.length; i < len; i++) {
                ir = _items[i] = ClassUtils.getInstanceByClass.apply(null, _itemRendererArguments) as IItemRenderer;
                if (_itemWidth) {
                    ir.width = _itemWidth;
                }
                if (_itemHeight) {
                    ir.height = _itemHeight;
                }
            }

            _selection.init();
        }


        protected function fillData(): void {
            _layout.reset();
            _layout.lock();

            var len: int = _data.length;
            var index: int;
            var itemsToSelect: Array = [];
            for (var i: int = 0; i < len; i++) {
                index = _getItemIndex(i);
                _items[i].id = index;
                _items[i].data = _data[index];

                _layout.addElement(_items[i]);

                if (_data[index] && _data[index].hasOwnProperty("selected") && _data[index].selected) {
                    itemsToSelect.push(index);
                }
            }

            if (itemsToSelect.length) {
                len = itemsToSelect.length;
                for (i = 0; i < len; i++) {
                    _selection.setSelectionByIndex(itemsToSelect[i]);
                }
            }
            _layout.unlock();
            updateBGSize();
        }


        protected function _getItemIndex(currentID: int): int {
            return currentID;
        }


        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
package by.lord_xaoca.ui.controls.containers.base {

    import by.lord_xaoca.helpers.StageReference;
    import by.lord_xaoca.interfaces.IDisplayedComponent;
    import by.lord_xaoca.interfaces.IScroll;
    import by.lord_xaoca.ui.components.scrolls.BaseScroll;
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    /**
     * BaseScrollableContainer class.
     * Подумать — не дохера ли условий для такого простого компонента?.
     * Подумать — стоит ли добаивть автоизменения размеров скроллов на основе отступов, на данный момент этого нет и значение надо ставить ручками.
     *
     * @author: Ivan Shaban
     * @date: 18.11.11 15:53
     */

    public class BaseScrollableContainer extends BaseDisplayObjectContainer {

        public static const SCROLL_ADDED: String = "scrollAdded";
        public static const SCROLL_REMOVED: String = "scrollRemoved";
        protected var _content: DisplayObject;
        protected var _horizontalScroll: IScroll;
        protected var _vertivalScroll: IScroll;
        protected var _width: int;
        protected var _height: int;
        protected var _scrollRect: Rectangle;
        protected var _scrollableContainer: Sprite;
        protected var _scrollsAutosize: Boolean = true;
        protected var _scrollsAutoPosition: Boolean = false;
        protected var _scrollsAutoHide: Boolean = false;
        protected var _scrollLeftIndent: int;
        protected var _scrollTopIndent: int;
        protected var _scrollRightIndent: int;
        protected var _scrollBottomIndent: int;


        public function BaseScrollableContainer(content: DisplayObject, vertivalScroll: IScroll = null, horizontalScroll: IScroll = null) {
            super(new Sprite());

            if (horizontalScroll == null && vertivalScroll == null) {
                throw Error("One of the scrolls should exist.");
            }

            initVScroll(vertivalScroll);
            initHScroll(horizontalScroll);
            initContent(content);

            _width = _content.width;
            _height = _content.height;

            updateBGSize();
            checkScrollsSize();
            checkScrollsPosition();
        }


        override public function get width(): Number {
            return _width;
        }


        override public function set width(value: Number): void {
            trace("*executing* >> [BaseScrollableContainer.width], use setSize() method.");
        }


        override public function get height(): Number {
            return _height;
        }


        override public function set height(value: Number): void {
            trace("*executing* >> [BaseScrollableContainer.height], use setSize() method.");
        }


        public function get horizontalScroll(): IScroll {
            return _horizontalScroll;
        }


        public function get vertivalScroll(): IScroll {
            return _vertivalScroll;
        }


        public function get content(): DisplayObject {
            return _content;
        }


        public function get scrollsAutosize(): Boolean {
            return _scrollsAutosize;
        }


        public function set scrollsAutosize(value: Boolean): void {
            _scrollsAutosize = value;
        }


        public function get scrollsAutoPosition(): Boolean {
            return _scrollsAutoPosition;
        }


        public function set scrollsAutoPosition(value: Boolean): void {
            _scrollsAutoPosition = value;
        }


        public function get scrollBottomIndent(): int {
            return _scrollBottomIndent;
        }


        public function set scrollBottomIndent(value: int): void {
            _scrollBottomIndent = value;
            checkScrollsPosition();
        }


        public function get scrollRightIndent(): int {
            return _scrollRightIndent;
        }


        public function set scrollRightIndent(value: int): void {
            _scrollRightIndent = value;
            checkScrollsPosition();
        }


        public function get scrollTopIndent(): int {
            return _scrollTopIndent;
        }


        public function set scrollTopIndent(value: int): void {
            _scrollTopIndent = value;
            checkScrollsPosition();
        }


        public function get scrollLeftIndent(): int {
            return _scrollLeftIndent;
        }


        public function set scrollLeftIndent(value: int): void {
            _scrollLeftIndent = value;
            checkScrollsPosition();
        }


        /**
         * Should we hide scrolls when cursor go out from container borders
         */
        public function get scrollsAutoHide(): Boolean {
            return _scrollsAutoHide;
        }


        public function set scrollsAutoHide(value: Boolean): void {
            _scrollsAutoHide = value;

            if (_scrollsAutoHide) {
                addViewListener(MouseEvent.ROLL_OVER, onOverHandler);
                addViewListener(MouseEvent.ROLL_OUT, onOutHandler);
                // methods "show" \ "hide" has objects who implements IDisplayedComponent interface.
                // in other case there is nothing happens.
                if (_vertivalScroll && _vertivalScroll is IDisplayedComponent) {
                    _vertivalScroll.addEventListener(BaseScroll.START_DRAG, onScrollStartDragHandler);
                }
                if (_horizontalScroll && _horizontalScroll is IDisplayedComponent) {
                    _horizontalScroll.addEventListener(BaseScroll.START_DRAG, onScrollStartDragHandler);
                }
            } else {
                removeViewListener(MouseEvent.ROLL_OVER, onOverHandler);
                removeViewListener(MouseEvent.ROLL_OUT, onOutHandler);
                if (_vertivalScroll && _vertivalScroll is IDisplayedComponent) {
                    _vertivalScroll.removeEventListener(BaseScroll.START_DRAG, onScrollStartDragHandler);
                }
                if (_horizontalScroll && _horizontalScroll is IDisplayedComponent) {
                    _horizontalScroll.removeEventListener(BaseScroll.START_DRAG, onScrollStartDragHandler);
                }
            }

            _checkScrollVisible(_vertivalScroll, StageReference.mouseX, StageReference.mouseY);
            _checkScrollVisible(_horizontalScroll, StageReference.mouseX, StageReference.mouseY);
        }


        /**
         * Setup size of visible area;
         *
         * @param    width
         * @param    height
         */
        public function setSize(width: int, height: int): void {
            _width = width;
            _height = height;

            updateBGSize();
            refresh();
        }


        public function refresh(): void {
            checkScrollsSize();
            checkContentPosition();
            checkScrollsPosition();

            if (_vertivalScroll) {
                _vertivalScroll.view.visible = _height < _content.height;
            }
            if (_horizontalScroll) {
                _horizontalScroll.view.visible = _width < _content.width;
            }

            _checkScrollVisible(_vertivalScroll, StageReference.mouseX, StageReference.mouseY);
            _checkScrollVisible(_horizontalScroll, StageReference.mouseX, StageReference.mouseY);
        }


        override protected function initView(): void {
            _scrollableContainer = new Sprite();
            //_scrollableContainer.mouseEnabled = false;

            addChild(_scrollableContainer);

            _scrollRect = new Rectangle();
        }


        protected function initVScroll(scroll: IScroll): void {
            if (!scroll) {
                return;
            }

            _vertivalScroll = scroll;
            _vertivalScroll.view.visible = false;
            _vertivalScroll.addEventListener(Event.CHANGE, onVScrollHandler);
            addChild(_vertivalScroll.view);
        }


        protected function initHScroll(scroll: IScroll): void {
            if (!scroll) {
                return;
            }

            _horizontalScroll = scroll;
            _horizontalScroll.view.visible = false;
            _horizontalScroll.addEventListener(Event.CHANGE, onHScrollHandler);
            addChild(_horizontalScroll.view);
        }


        protected function initContent(content: DisplayObject): void {
            _content = content;
            x = _content.x;
            y = _content.y;
            _content.x = 0;
            _content.y = 0;
            if (_content.parent) {
                _content.parent.addChild(_view);
            }
            _scrollableContainer.addChild(_content);
        }


        /**
         * Check scroller size, if it wrong we setup right value.
         */
        protected function checkScrollsSize(): void {
            if (!_scrollsAutosize) {
                return;
            }
            if (_vertivalScroll) {
                _vertivalScroll.scrollHeightByRatio = _height / _content.height;
            }

            if (_horizontalScroll) {
                _horizontalScroll.scrollWidthByRatio = _width / _content.width;
            }
        }


        /**
         * Check scrolls positions factor into indents.
         */
        protected function checkScrollsPosition(): void {
            if (!_scrollsAutoPosition) {
                return;
            }
            if (_vertivalScroll) {
                _vertivalScroll.x = _width - _scrollRightIndent - _vertivalScroll.width;
                _vertivalScroll.y = _scrollTopIndent;
            }
            if (_horizontalScroll) {
                _horizontalScroll.x = _scrollLeftIndent;
                _horizontalScroll.y = _height - _scrollBottomIndent - _horizontalScroll.height;
            }
        }


        /**
         * Draw "mask".
         */
        protected function checkContentPosition(): void {
            if ((_horizontalScroll && _width < _content.width) || (_vertivalScroll && _height < _content.height)) {
                _scrollRect.x = _horizontalScroll ? (_content.width - _width) * _horizontalScroll.value : 0;
                _scrollRect.y = _vertivalScroll ? (_content.height - _height) * _vertivalScroll.value : 0;
                _scrollRect.width = _width;
                _scrollRect.height = _height;

                // for debug
                //_scrollableContainer.graphics.clear();
                //_scrollableContainer.graphics.beginFill(0x99cc00);
                //_scrollableContainer.graphics.drawRect(_scrollRect.x, _scrollRect.y, _scrollRect.width, _scrollRect.height);
                //_scrollableContainer.graphics.endFill();

                _scrollableContainer.scrollRect = _scrollRect;
            } else {
                _scrollableContainer.scrollRect = null;
            }
        }


        /**
         * Ckecking, should we show scrolls.
         *
         * @param    value
         * @param    mouseX
         * @param    mouseY
         */
        protected function _checkScrollVisible(value: IScroll, mouseX: int, mouseY: int): void {
            if (!_scrollsAutoHide) {
                return;
            }
            if (!value || !value is IDisplayedComponent) {
                return;
            }

            var viewPoint: Point = view.localToGlobal(new Point());
            if (mouseX < viewPoint.x ||
                    mouseX > viewPoint.x + view.width ||
                    mouseY < viewPoint.y ||
                    mouseY > viewPoint.y + view.height) {
                (value as IDisplayedComponent).hide();
            }
        }


        protected function updateBGSize(): void {
            _view.graphics.clear();
            _view.graphics.beginFill(0x99CC00, _isDebugMode ? 0.6 : 0);
            _view.graphics.drawRect(0, 0, _width, _height);
            _view.graphics.endFill();
        }


        protected function onScrollStartDragHandler(event: Event): void {
            addStageListener(MouseEvent.MOUSE_UP, onScrollStopDragHandler);
        }


        protected function onScrollStopDragHandler(event: MouseEvent): void {
            removeStageListener(MouseEvent.MOUSE_UP, onScrollStopDragHandler);

            if (_height < _content.height && _vertivalScroll) {
                _vertivalScroll.view.visible = true;
                _checkScrollVisible(_vertivalScroll, event.stageX, event.stageY);
            }

            if (_width < _content.width && _horizontalScroll) {
                _horizontalScroll.view.visible = true;
                _checkScrollVisible(_horizontalScroll, event.stageX, event.stageY);
            }
        }


        private function onVScrollHandler(event: Event): void {
            checkContentPosition();
        }


        private function onHScrollHandler(event: Event): void {
            checkContentPosition();
        }


        private function onOverHandler(event: MouseEvent): void {
            if (_height < _content.height && _vertivalScroll && !BaseScroll.isDragged && _vertivalScroll is IDisplayedComponent) {
                (_vertivalScroll as IDisplayedComponent).show();
            }

            if (_width < _content.width && _horizontalScroll && !BaseScroll.isDragged && _horizontalScroll is IDisplayedComponent) {
                (_horizontalScroll as IDisplayedComponent).show();
            }
        }


        private function onOutHandler(event: MouseEvent): void {
            if (_height < _content.height && _vertivalScroll && !BaseScroll.isDragged && _vertivalScroll is IDisplayedComponent) {
                (_vertivalScroll as IDisplayedComponent).hide();
            }

            if (_width < _content.width && _horizontalScroll && !BaseScroll.isDragged && _horizontalScroll is IDisplayedComponent) {
                (_horizontalScroll as IDisplayedComponent).hide();
            }
        }

    }
}

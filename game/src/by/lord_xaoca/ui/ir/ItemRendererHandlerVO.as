package by.lord_xaoca.ui.ir {

    import by.lord_xaoca.interfaces.IDestructableComponent;

    /**
     * ItemRendererHandlerVO class.
     *
     * @author: Ivan Shaban
     * @date: 04.04.13 20:56
     */

    public class ItemRendererHandlerVO implements IDestructableComponent {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        private var _type:String;
        private var _listener:Function;

        // ------------------ CONSTRUCTOR ------------------------------

        public function ItemRendererHandlerVO (type:String, listsner:Function) {
            _type = type;
            _listener = listsner;
        }

        // ------------------ PROPERTIES -------------------------------

        public function get type ():String {
            return _type;
        }

        public function get listener ():Function {
            return _listener;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        public function destroy ():void {
            _type = null;
            _listener = null;
        }

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}

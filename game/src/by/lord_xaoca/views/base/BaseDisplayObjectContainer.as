package by.lord_xaoca.views.base {

    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;
    import flash.display.Shape;
    import flash.display.Sprite;
    import flash.text.TextField;

    /**
     * Decorator. Similar to "BaseDisplayObject", but have methods to easy handle children.
     * I'm recommend to use this class instead of "BaseDisplayObject".
     *
     * @author: Ivan Shaban
     */

    public class BaseDisplayObjectContainer extends BaseDisplayObject {

        public function BaseDisplayObjectContainer(view: DisplayObject) {
            super(view);
        }


        public function contains(value: DisplayObject): Boolean {
            return value && _view.contains(value);
        }


        /**
         * Checks if value not nested child.
         * @param value
         * @return
         */
        public function containsDirectly(value: DisplayObject): Boolean {
            return value && value.parent == view && view;
        }


        /**
         * Add child like original method. Generates error if we trying to add null-value.
         * @param value Any not-null DisplayObject
         * @return
         */
        public function addChild(value: DisplayObject): DisplayObject {
            return _view.addChild(value);
        }


        public function addChildAt(view: DisplayObject, index: int): DisplayObject {
            return _view.addChildAt(view, index);
        }


        /**
         * Safely removeChild method. Checks if value is child of current container and then remove it if its possible.
         * @param    value
         * @return
         */
        public function removeChild(value: DisplayObject): DisplayObject {
            return containsDirectly(value) ? _view.removeChild(value) : value;
        }


        public function getMovieClip(name: String, parentContainer: DisplayObjectContainer = null): MovieClip {
            return getChildByName(name + "_mc", parentContainer) as MovieClip;
        }


        public function getSprite(name: String, parentContainer: DisplayObjectContainer = null): Sprite {
            return getChildByName(name, parentContainer) as Sprite;
        }


        public function getTextField(name: String, parentContainer: DisplayObjectContainer = null): TextField {
            return getChildByName(name + "_tf", parentContainer) as TextField;
        }


        public function getShape(name: String, parentContainer: DisplayObjectContainer = null): Shape {
            return getChildByName(name, parentContainer) as Shape;
        }


        public function getChildByName(name: String, parentContainer: DisplayObjectContainer = null): DisplayObject {
            parentContainer ||= _view;
            if (!parentContainer) {
                throw Error("Parent container is null, please set non-null object.");
            }
            return parentContainer.getChildByName(name);
        }


        public function getChildIndex(child: DisplayObject, parentContainer: DisplayObjectContainer = null): int {
            parentContainer ||= _view;
            if (!parentContainer) {
                throw Error("Parent container is null, please set non-null object.");
            }
            return parentContainer.getChildIndex(child);
        }


        public function removeAllChildren(): void {
            while (_view && _view.numChildren) {
                _view.removeChildAt(0);
            }
        }


        override public function destroy(): void {
            removeAllChildren();

            super.destroy();
        }

    }
}

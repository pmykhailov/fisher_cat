package by.lord_xaoca.views.base {

    import by.lord_xaoca.interfaces.IVirtualObject;

    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.Graphics;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;

    /**
     * Decorator. Allow to add custom behaviour to simple viewController object (usually sprite or movie clip).
     *
     * TODO: implement pivotX and pivotY functional.
     *
     * @author Ivan Shaban
     */
    public class BaseDisplayObject extends BaseEventDispatcher implements IVirtualObject {

        protected var _view: Sprite;
        /**
         * Do we need to round coordinates to floor.
         */
        protected var _useRoundCoordinates: Boolean = true;
        /**
         * Does we need to dispatch custom button events, like: rollOver, rollOut, click
         */
        protected var _isButtonBehaviour: Boolean;
        protected var _isDebugMode: Boolean;


        public function BaseDisplayObject(view: DisplayObject) {
            _view = view as Sprite;

            super();
        }


        public function get name(): String {
            return _view.name;
        }


        public function get view(): Sprite {
            return _view;
        }


        public function set view(value: Sprite): void {
            _view = value;
        }


        public function get useRoundCoordinates(): Boolean {
            return _useRoundCoordinates;
        }


        public function set useRoundCoordinates(value: Boolean): void {
            _useRoundCoordinates = value;
            if (value) {
                x = x;
                y = y;
            }
        }


        public function get x(): Number {
            return _view.x;
        }


        public function set x(value: Number): void {
            _view.x = _useRoundCoordinates ? int(value) : value;
        }


        public function get y(): Number {
            return _view.y;
        }


        public function set y(value: Number): void {
            _view.y = _useRoundCoordinates ? int(value) : value;
        }


        public function get scaleX(): Number {
            return _view.scaleX;
        }


        public function set scaleX(value: Number): void {
            _view.scaleX = value;
        }


        public function get scaleY(): Number {
            return _view.scaleY;
        }


        public function set scaleY(value: Number): void {
            _view.scaleY = value;
        }


        public function get width(): Number {
            return _view.width;
        }


        public function set width(value: Number): void {
            _view.width = value;
        }


        public function get height(): Number {
            return _view.height;
        }


        public function set height(value: Number): void {
            _view.height = value;
        }


        public function get alpha(): Number {
            return _view.alpha;
        }


        public function set alpha(value: Number): void {
            _view.alpha = value;
        }


        public function get visible(): Boolean {
            return _view.visible;
        }


        public function set visible(value: Boolean): void {
            _view.visible = value;
        }


        public function get mouseEnabled(): Boolean {
            return _view.mouseEnabled;
        }


        public function set mouseEnabled(value: Boolean): void {
            _view.mouseEnabled = value;
        }


        public function get mouseChildren(): Boolean {
            return _view.mouseChildren;
        }


        public function set mouseChildren(value: Boolean): void {
            _view.mouseChildren = value;
        }


        public function get buttonMode(): Boolean {
            return _view.buttonMode;
        }


        public function set buttonMode(value: Boolean): void {
            _view.buttonMode = value;
        }


        public function get isButtonBehaviour(): Boolean {
            return _isButtonBehaviour;
        }


        public function set isButtonBehaviour(value: Boolean): void {
            _isButtonBehaviour = value;
            buttonMode = value;
            if (_isButtonBehaviour) {
                addViewListener(MouseEvent.ROLL_OVER, dispatchEvent);
                addViewListener(MouseEvent.ROLL_OUT, dispatchEvent);
                addViewListener(MouseEvent.CLICK, dispatchEvent);
                addViewListener(MouseEvent.MOUSE_DOWN, dispatchEvent);
            }
            else {
                removeViewListener(MouseEvent.ROLL_OVER, dispatchEvent);
                removeViewListener(MouseEvent.ROLL_OUT, dispatchEvent);
                removeViewListener(MouseEvent.CLICK, dispatchEvent);
                removeViewListener(MouseEvent.MOUSE_DOWN, dispatchEvent);
            }
        }


        public function get rotation(): Number {
            return _view.rotation;
        }


        public function set rotation(value: Number): void {
            _view.rotation = value;
        }


        public function get isDebugMode(): Boolean {
            return _isDebugMode;
        }


        public function set isDebugMode(value: Boolean): void {
            if (_isDebugMode == value) {
                return;
            }

            _isDebugMode = value;

            if (value) {
                updateDebugInfo();
                addViewListener(Event.ADDED, childUpdateHandler);
                addViewListener(Event.REMOVED, childUpdateHandler);
            } else {
                _view.graphics.clear();
                removeViewListener(Event.ADDED, childUpdateHandler);
                removeViewListener(Event.REMOVED, childUpdateHandler);
            }
        }


        public function get parent(): DisplayObjectContainer {
            return _view && _view.parent;
        }


        public function addViewListener(type: String, listener: Function, useCapture: Boolean = false, priority: int = 0): void {
            // if it will be necessary, add support for "useCapture" and "priority"
            _eventHandler.addListenerTo(_view, type, listener, true, useCapture, priority);
        }


        public function removeViewListener(type: String, listener: Function/*, useCapture: Boolean = false*/): void {
            _eventHandler.removeListenerFrom(_view, type, listener);
        }


        override public function destroy(): void {
            if (_view.parent) {
                _view.parent.removeChild(_view);
            }
            _view = null;

            super.destroy();
        }


        override protected function initialize(): void {
            super.initialize();

            initView();
        }


        protected function initView(): void {
            // ABSTRACT
        }


        protected function updateDebugInfo(): void {
            // TODO: add bounds
            var g: Graphics = _view.graphics;
            g.clear();
            g.lineStyle(1, 0xFF0000);
            g.beginFill(0x00CCFF, 0.4);
            g.drawRect(0, 0, _view.width, _view.height);
            g.endFill();
        }


        protected function childUpdateHandler(event: Event): void {
            if (event.target == _view) {
                return;
            }

            updateDebugInfo();
        }

    }
}
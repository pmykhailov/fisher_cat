package by.lord_xaoca.helpers {
    import by.lord_xaoca.interfaces.IMouseWheelObserver;
    import by.lord_xaoca.ui.components.scrolls.BaseScroll;

    import flash.errors.IllegalOperationError;
    import flash.events.EventDispatcher;
    import flash.external.ExternalInterface;

    /**
     * MouseWheelDispatcher class.
     * User: Vladimir Ivanchenko
     * Date: 29.08.13
     */

    public class MouseWheelDispatcher extends EventDispatcher {

        public static var debugMode: Boolean = false;
        private static var __instance: MouseWheelDispatcher;
        private static var __items: Vector.<IMouseWheelObserver>;


        public static function initialize(): void {
            if (__instance) {
                throw IllegalOperationError("You cannot init MouseWheelDispatcher twice!")
            }

            __instance = new MouseWheelDispatcher();
            __items = new Vector.<IMouseWheelObserver>();

            if (ExternalInterface.available) {
                ExternalInterface.addCallback("onScrollHandler", onWheelHandler);
            } else {
                //throw IllegalOperationError("ExternalInterface is unavailable!");
            }
        }


        public static function add(value: IMouseWheelObserver): void {
            if (!__instance) {
                throw IllegalOperationError("First you need to call MouseWheelDispatcher.initialize() method.");
            }

            remove(value);
            __items.push(value);

            if (debugMode) {
                trace(value, "added to listen MouseWheel event.");
            }
        }


        public static function remove(value: IMouseWheelObserver): void {
            if (!__instance) {
                throw IllegalOperationError("First you need to call MouseWheelDispatcher.initialize() method.");
            }

            var index: int = __items.indexOf(value);
            if (index == -1) {
                return;
            }
            if (debugMode) {
                trace(value, "removed from listen MouseWheel event.");
            }
            __items.splice(index, 1);
        }


        private static function onWheelHandler(delta: Number, deltaX: int, deltaY: int): void {
            if (BaseScroll.isDragged) {
                return;
            }

            var len_i: int = __items.length;
            for (var i: int = len_i - 1; i > -1; i--) {
                __items[i].updateByMouseWheel(deltaY);
            }
        }

    }
}

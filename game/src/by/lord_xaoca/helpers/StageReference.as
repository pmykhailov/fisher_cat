package by.lord_xaoca.helpers {

    import flash.display.Stage;
    import flash.display.StageDisplayState;

    public class StageReference {
        private static var _stage: Stage;

        public static function get stage(): Stage {
            return _stage;
        }


        public static function get width(): int {
            return _stage ? _stage.stageWidth : 0;
        }


        public static function get height(): int {
            return _stage ? _stage.stageHeight : 0;
        }


        public static function get mouseX(): int {
            return _stage ? _stage.mouseX : 0;
        }


        public static function get mouseY(): int {
            return _stage ? _stage.mouseY : 0;
        }


        public static function get isFullscreenMode(): Boolean {
            return StageReference.stage.displayState != StageDisplayState.NORMAL;
        }


        public static function init(stage: Stage): void {
            _stage = stage;
        }


        public static function toggleFullscreenMode(): void {
            StageReference.stage.displayState = StageReference.isFullscreenMode ? StageDisplayState.NORMAL : StageDisplayState.FULL_SCREEN;
        }
    }
}
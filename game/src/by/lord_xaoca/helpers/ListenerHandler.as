package by.lord_xaoca.helpers {

    import flash.events.IEventDispatcher;
    import flash.utils.Dictionary;

    /**
     * @author: Ivan Shaban
     */

    public class ListenerHandler {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        private var _target:IEventDispatcher;
        private var _handlers:Dictionary = new Dictionary(false);

        /**
         * Flag that indicates that we in process of removing listeners, and we shouldnt clear "listners list" in "remove" method.
         */
        private var _isRemoving:Boolean;

        // ------------------ CONSTRUCTOR ------------------------------

        public function ListenerHandler (target:IEventDispatcher) {
            _target = target;
        }

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        public function addListener (eventType:String, listener:Function, addListener:Boolean = true):void {
            add(_target, eventType, listener, addListener);
        }

        public function addListenerTo (target:IEventDispatcher, eventType:String, listener:Function, addListener:Boolean = true, useCapture:Boolean = false, priority:int = 0):void {
            add(target, eventType, listener, addListener, useCapture, priority);
        }

        public function removeListener (eventType:String, listener:Function, removeListener:Boolean = true, forceGC:Boolean = true):void {
            remove(_target, eventType, listener, removeListener, forceGC);
        }

        public function removeListenerFrom (target:IEventDispatcher, eventType:String, listener:Function, removeListener:Boolean = true, forceGC:Boolean = true):void {
            remove(target, eventType, listener, removeListener, forceGC);
        }

        public function removeAllListeners ():void {
            for (var keyObject1:Object in _handlers) {
                removeAllListenersFor(keyObject1 as IEventDispatcher);
            }
        }

        public function removeAllListenersFor (target:IEventDispatcher):void {
            if (!target) {
                throw Error("You cannot remove listeners from null-object.");
            }

            _isRemoving = true;
            if (_handlers[target]) {
                for (var eventType:String in _handlers[target]) {
                    var listeners:Array = _handlers[target][eventType];
                    var len:uint = listeners.length;
                    for (var i:int = 0; i < len; i++) {
                        target.removeEventListener(eventType, listeners[i]);
                    }
                    listeners.length = 0;
                    delete (_handlers[target][eventType]);
                }
                delete (_handlers[target]);
            }
            _isRemoving = false;
        }

        public function destroy ():void {
            removeAllListeners();

            _handlers = null;
            _target = null;
        }

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        private function add (target:IEventDispatcher, eventType:String, listener:Function, addListener:Boolean = true, useCapture:Boolean = false, priority:int = 0):void {
            if (!target && addListener) {
                throw Error("You cannot add listener to null-object.");
            }
            else if (!eventType) {
                throw Error("You cannot add listener to object with null-type event.");
            }
            else if (listener == null) {
                throw Error("You cannot add null-listener to " + target);
            }

            _handlers[target] ||= {};
            _handlers[target][eventType] ||= [];
            if (_handlers[target][eventType].indexOf(listener) == -1) {
                _handlers[target][eventType].push(listener);
            }

            if (addListener) {
                target.addEventListener(eventType, listener, useCapture, priority);
            }
        }

        private function remove (target:IEventDispatcher, eventType:String, listener:Function, removeListener:Boolean = true, forceGC:Boolean = true):void {
            if (!target && removeListener) {
                throw Error("You cannot remove listener from null-object.");
            }
            else if (!eventType) {
                throw Error("You cannot remove listener from object with null-type event.");
            }
            else if (listener == null) {
                throw Error("You cannot remove null-listener from " + target + " " + eventType);
            }

            if (!_isRemoving && _handlers[target] && _handlers[target][eventType] && _handlers[target][eventType].indexOf(listener) != -1) {
                var listenersList:Array = _handlers[target][eventType];
                var index:int = listenersList.indexOf(listener);
                listenersList.splice(index, 1);
                if (forceGC) {
                    handlerGarbage(target, eventType);
                }
            }
            if (removeListener) {
                target.removeEventListener(eventType, listener);
            }
        }

        private function handlerGarbage (target:IEventDispatcher, eventType:String):void {
            var listenersList:Array = _handlers[target][eventType];
            var hasListeners:Boolean;
            if (!listenersList.length) {
                delete(_handlers[target][eventType]);
            }
            for (var string:String in _handlers[target]) {
                hasListeners = true;
                break;
            }
            if (!hasListeners) {
                delete (_handlers[target]);
            }
        }

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}

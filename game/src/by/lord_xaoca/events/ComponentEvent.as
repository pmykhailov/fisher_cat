package by.lord_xaoca.events {

    import flash.events.Event;

    /**
     * ...
     * @author Svitovyda
     */

    public class ComponentEvent extends Event {

        // ------------------ STATIC VARIABLES -------------------------

        static public const DESELECTED:String = "ce:deselected";
        static public const SELECTED:String = "ce:selected";
        static public const DESTROY:String = "ce:destroy";
        static public const INITIALIZE:String = "ce:initialize";
        public static const RESIZE:String = "ce:resize";

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        private var _data:*;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function ComponentEvent (type:String, data:* = null, bubbles:Boolean = false, cancelable:Boolean = false) {
            super(type, bubbles, cancelable);

            _data = data;
        }

        // ------------------ PROPERTIES -------------------------------

        public function get data ():* {
            return _data;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        public override function clone ():Event {
            return new ComponentEvent(type, data, bubbles, cancelable);
        }

        public override function toString ():String {
            return formatToString("ComponentEvent", "type", "data", "bubbles", "cancelable", "eventPhase");
        }

        // ------------------ END CLASS --------------------------------
    }
}

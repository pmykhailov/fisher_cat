package by.lord_xaoca.interfaces {

    import flash.events.IEventDispatcher;

    /**
     * IScroll interface.
     *
     * @author: Ivan Shaban
     * @date: 14.11.11 14:42
     */
    public interface IScroll extends IEventDispatcher, IMovableComponent, IResizableComponent, IVirtualObject {

        function get topIndent ():int;

        function set topIndent ($value:int):void;

        function get bottomIndent ():int;

        function set bottomIndent ($value:int):void;

        function get leftIndent ():int;

        function set leftIndent ($value:int):void;

        function get rightIndent ():int;

        function set rightIndent ($value:int):void;

        function get useMouseWheel ():Boolean;

        function set useMouseWheel ($value:Boolean):void;

        function get value ():Number;

        function set value ($value:Number):void;

        function get enable ():Boolean;

        function set enable ($value:Boolean):void;

        function get scrollWidth ():int;

        function set scrollWidth ($value:int):void;

        function get scrollHeight ():int;

        function set scrollHeight ($value:int):void;

        function get scrollWidthByRatio ():Number;

        function set scrollWidthByRatio ($value:Number):void;

        function get scrollHeightByRatio ():Number;

        function set scrollHeightByRatio ($value:Number):void;

        function get percentsPerStep ():int;

        function set percentsPerStep ($value:int):void;

    }
}

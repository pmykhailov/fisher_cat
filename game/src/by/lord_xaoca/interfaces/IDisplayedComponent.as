package by.lord_xaoca.interfaces {

    /**
     * IComponentSscroll interface.
     *
     * @author: Ivan Shaban
     * @date: 21.11.11 12:38
     */
    public interface IDisplayedComponent {
        function show ():void

        function hide ():void
    }
}

package by.lord_xaoca.interfaces {

    /**
     * IMovableComponent interface.
     *
     * @author Ivan Shaban
     * @date 15.10.2011 14:04
     */
    public interface IMovableComponent {
        function get x ():Number

        function set x (value:Number):void

        function get y ():Number

        function set y (value:Number):void
    }
}
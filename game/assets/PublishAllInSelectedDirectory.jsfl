// Folder can be chosen
//var folder = fl.browseForFolderURL("Choose a folder to publish:");


var scriptURI=fl.scriptURI;
scriptURI=scriptURI.substr(0,scriptURI.lastIndexOf("/")+1);

var folder = scriptURI;
var files = FLfile.listFolder(folder + "/*.fla", "files");

for (file in files) 
{
	var curFile = files[file];

	// open document, publish, and close
	fl.openDocument(folder + "/" + curFile);
	fl.getDocumentDOM().publish();
	fl.closeDocument(fl.getDocumentDOM());
}
package model.level.parsers {
import model.level.ICoreLevelModel;

import level_editor.model.LevelEditorLevelModel;

import model.level.settings.GameSettings;
import model.level.settings.LevelSettings;
import model.level.settings.vo.GridSettingsVO;

    /**
     * LevelDataParser class.
     *
     * Parses XML data to array of GameSettings
     * Each GameSettings instance contain set of setting to a game
     *
     * User: Paul Makarenko
     * Date: 06.10.2014
     */
    public class LevelsDataParser {

        public function LevelsDataParser() {
        }


        public function parse(levelsData: XML, levels:Array /*ICoreLevelModel*/): void {
            var levelsCount: int = levelsData.level.length();

            for (var i: int = 0; i < levelsCount; i++) {
                var levelData: XML = levelsData.level[i] as XML;
                var levelModel: ICoreLevelModel = levels[i] as ICoreLevelModel;

                var gameSettings: GameSettings = new GameSettings();
                var levelSettings: LevelSettings = new LevelSettings();

                fillLevelDimensions(gameSettings, levelData.game_settings[0]);
                fillLevelGridItemsTypes(gameSettings, levelData.game_settings[0]);
                fillLevelGrids(gameSettings, levelData.game_settings[0]);

                fillLevelSettings(levelSettings, levelData.level_settings[0]);

                levelModel.gameSettings = gameSettings;
                levelModel.levelSettings = levelSettings;
            }
        }

        private function fillLevelSettings(levelSettings:LevelSettings, levelSettingsData:XML):void {
            levelSettings.name = levelSettingsData.level_name;
        }


        private function fillLevelDimensions(gameSettings: GameSettings, gameSettingsData: XML): void {
            var rows: int = int(gameSettingsData.dimensions.@rows);
            var cols: int = int(gameSettingsData.dimensions.@cols);

            gameSettings.gameFieldRows = rows;
            gameSettings.gameFieldCols = cols;
        }

        private function fillLevelGridItemsTypes(gameSettings: GameSettings, gameSettingsData: XML): void {
            var usualItemsData:String = String(gameSettingsData.grid_items_types.@usual);
            var bonusItemsData:String = String(gameSettingsData.grid_items_types.@bonus);
            var usualItems:Array = usualItemsData == "" ? [] : usualItemsData.split(",") ;
            var bonusItems:Array = bonusItemsData == "" ? [] : bonusItemsData.split(",");
            var i:int;

            for (i = 0; i < usualItems.length; i++) {
                usualItems[i] = int(usualItems[i]);
            }

            for (i = 0; i < bonusItems.length; i++) {
                bonusItems[i] = int(bonusItems[i]);
            }

            gameSettings.usualGridItemTypes = usualItems;
            gameSettings.bonusGridItemTypes = bonusItems;
        }

        private function fillLevelGrids(gameSettings: GameSettings, gameSettingsData: XML): void {
            var gridsCount: int = gameSettingsData.grid.length();

            for (var i: int = 0; i < gridsCount; i++) {
                var gridData: XML = gameSettingsData.grid[i] as XML;
                var type: String = gridData.@type;

                fillGridSettings(gameSettings.getGridSettingsVOByType(type), gridData);
            }
        }

        private function fillGridSettings(gridSettingsVO: GridSettingsVO, gridData: XML): void {
            var rowsCount: int = gridData.row.length();

            for (var i: int = 0; i < rowsCount; i++) {
                var rowData: XML = gridData.row[i] as XML;
                var typesData: String = rowData.@data;
                var typesArray = typesData.split(",");
                var cols: int = typesArray.length;

                gridSettingsVO.elements[i] = [];

                for (var j: int = 0; j < cols; j++) {
                    gridSettingsVO.elements[i][j] = int(typesArray[j]);
                }
            }
        }

    }
}

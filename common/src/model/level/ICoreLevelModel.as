package model.level {
import model.level.settings.GameSettings;
import model.level.settings.LevelSettings;

/**
 * Level model data that will not be changed.
 * This data is a core (minimal) data to identify level
 */
public interface ICoreLevelModel {
    function get levelSettings():LevelSettings;
    function set levelSettings(value: LevelSettings):void;

    function get gameSettings():GameSettings;
    function set gameSettings(value: GameSettings):void;
}
}

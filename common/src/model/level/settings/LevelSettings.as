package model.level.settings {
public class LevelSettings {

    private var _name:String;

    public function LevelSettings() {
        _name = "";
    }

    public function get name():String {
        return _name;
    }

    public function set name(value:String):void {
        _name = value;
    }

}
}

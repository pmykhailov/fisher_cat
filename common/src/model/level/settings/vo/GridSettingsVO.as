package model.level.settings.vo {

    /**
     * GridVO class.
     * User: Paul Makarenko
     * Date: 05.10.2014
     */
    public class GridSettingsVO {

        private var _type: String;
        private var _elements: Array;

        public function GridSettingsVO(type:String, elements:Array) {
            _type = type;
            _elements = elements;
        }


        public function get type(): String {
            return _type;
        }


        public function get elements(): Array {
            return _elements;
        }
    }
}

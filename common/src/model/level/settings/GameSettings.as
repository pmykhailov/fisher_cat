package model.level.settings {
import flash.geom.Point;

import model.grids.enum.GridTypeEnum;
import model.grids.items.BaseGridItemEnum;
import model.grids.items.GridItemTypeEnum;
    import model.level.settings.vo.GridSettingsVO;

    /**
     * Contains tunable parameters for the game.
     * It's like a game configuration.
     *
     * All this parameters can be edited in
     * external level editor
     */
    public class GameSettings {

        private var _itemWidth: int;
        private var _itemHeight: int;
        private var _itemSpacing: Point;
        private var _gameFieldRows: int;
        private var _gameFieldCols: int;

        /** This could be shape of game filed, background grid etc**/
        [ArrayElementType("model.level.settings.vo.GridSettingsVO")]
        private var _gridSettingsVOs: Array;

        private var _usualGridItemTypes: Array;
        private var _bonusGridItemTypes: Array;
        private var _timeSettings: TimeSettings;


        public function GameSettings() {
            initDefaultSettings();
        }


        public function get timeSettings(): TimeSettings {
            return _timeSettings;
        }


        public function get itemWidth(): int {
            return _itemWidth;
        }


        public function set itemWidth(value: int): void {
            _itemWidth = value;
        }


        public function get itemHeight(): int {
            return _itemHeight;
        }


        public function set itemHeight(value: int): void {
            _itemHeight = value;
        }


        public function get itemSpacing():Point {
            return _itemSpacing;
        }


        public function set itemSpacing(value:Point):void {
            _itemSpacing = value;
        }


        public function get gameFieldRows(): int {
            return _gameFieldRows;
        }


        public function set gameFieldRows(value: int): void {
            _gameFieldRows = value;
        }


        public function get gameFieldCols(): int {
            return _gameFieldCols;
        }


        public function set gameFieldCols(value: int): void {
            _gameFieldCols = value;
        }


        public function get usualGridItemTypes(): Array {
            return _usualGridItemTypes;
        }


        public function set usualGridItemTypes(value: Array): void {
            _usualGridItemTypes = value;
        }


        public function get bonusGridItemTypes(): Array {
            return _bonusGridItemTypes;
        }


        public function set bonusGridItemTypes(value: Array): void {
            _bonusGridItemTypes = value;
        }


        public function get gridSettingsVOs(): Array {
            return _gridSettingsVOs;
        }

        /**
         * TODO Maybe addGridSettingsVO is not needed cuz ALL grid types are added to _gridSettingsVOs
         * array by default
         *
         */
        public function addGridSettingsVO(type: String, elements: Array): GridSettingsVO {
            var gridSettingsVO:GridSettingsVO = new GridSettingsVO(type, elements);

            _gridSettingsVOs.push(gridSettingsVO);

            return gridSettingsVO;
        }


        public function getGridSettingsVOByType(type: String): GridSettingsVO {
            var n: uint = _gridSettingsVOs.length;

            for (var i: int = 0; i < n; i++) {
                if (_gridSettingsVOs[i].type == type) {
                    return _gridSettingsVOs[i]
                }
            }

            return null;
        }


        /**
         * Some default parameters set is initialized here
         */
        private function initDefaultSettings(): void {

            _timeSettings = new TimeSettings();

            // [ --- TODO Extract this settings to some view config class ---]

            _itemWidth = 82;
            _itemHeight = 82;

            _itemSpacing = new Point(4,4);

            // [ --- END --- ]

            _gameFieldCols = 8;
            _gameFieldRows = 8;

            _gridSettingsVOs = [];

            // Let here be all possible grids with BaseGridItemEnum.NOT_EXISTING cells by default
            var allPosibleGridsCount: int = GridTypeEnum.TYPES_ALL.length;

            for (var k:int = 0; k < allPosibleGridsCount; k++) {
                var gridSettingsVO:GridSettingsVO = new GridSettingsVO(GridTypeEnum.TYPES_ALL[k],[]);

                for (var i:int = 0; i < _gameFieldRows; i++) {

                    gridSettingsVO.elements[i] = [];

                    for (var j:int = 0; j < _gameFieldCols; j++) {
                        gridSettingsVO.elements[i][j] = BaseGridItemEnum.NOT_EXISTING;
                    }
                }

                _gridSettingsVOs[k] = gridSettingsVO;
            }

            _usualGridItemTypes = GridItemTypeEnum.USUAL_ITEMS_IDS.concat();
            _bonusGridItemTypes = GridItemTypeEnum.BONUS_ITEMS_IDS.concat();
        }

    }
}
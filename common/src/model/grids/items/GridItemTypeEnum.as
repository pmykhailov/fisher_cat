package model.grids.items {
    import model.grids.items.BaseGridItemEnum;

    public class GridItemTypeEnum {

        /** Used by model for calculations **/
        public static const TEMPORARY: int = -3;
        /** Defines cell where items can't be spawned **/
        public static const NON_EXISTING: int = BaseGridItemEnum.NOT_EXISTING;
        /** Defines empty cell **/
        public static const EMPTY: int = BaseGridItemEnum.EXISTING;

        // Usual items ids
        public static const USUAL_ITEMS_IDS: Array = [1, 2, 3, 4, 5];

        // Bonus items ids
        public static const BOMB: int = 6;
        public static const TYPE_BOMB: int = 7;
        public static const BONUS_ITEMS_IDS: Array = [BOMB, TYPE_BOMB];

        // All items
        public static const ALL_ITEMS_IDS: Array = USUAL_ITEMS_IDS.concat(BONUS_ITEMS_IDS);

        // Maximum possible id
        public static const TYPE_MAX_ID: int = TYPE_BOMB;


        public function GridItemTypeEnum() {

        }
    }
}

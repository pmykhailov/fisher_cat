package model.grids.items {
    import model.grids.*;
    import model.grids.items.BaseGridItemEnum;

    public class GridBackgroundItemEnum {

        /** There is nothing in this cell **/
        public static const NOT_EXISTING: int = BaseGridItemEnum.NOT_EXISTING;

        /** There is some background, that player should remove, in the cell**/
        public static const WITH_BACKGROUND: int = BaseGridItemEnum.EXISTING;


        public function GridBackgroundItemEnum() {

        }
    }
}

package model.grids.items {

    /**
     * BaseGridItemEnum class.
     * User: Paul Makarenko
     * Date: 22.09.2014
     */
    public class BaseGridItemEnum {
        public static const NOT_EXISTING: int = -2;
        public static const EXISTING: int = -1;
    }
}
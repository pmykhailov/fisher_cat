package model.grids.enum {

    /**
     * GridTypeEnum class.
     * User: Paul Makarenko
     * Date: 22.09.2014
     */
    public class GridTypeEnum {

        /** Grid that defines shape of game field **/
        public static const TYPE_MAIN:String = "main";

        /** Grid that defines something that is under the cell of game field cells **/
        public static const TYPE_BACKGROUND:String = "background";

        public static const TYPES_ALL: Array = [TYPE_BACKGROUND, TYPE_MAIN];
    }
}